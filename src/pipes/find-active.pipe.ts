import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'findActive'})
export class FindActivePipe implements PipeTransform {
  transform(value, args): any {
   
        if(value==args){
            return true;
        }
        else{
            return false;
        }
  }
}