import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'programPosterPipe', pure: false})
export class ProgramPosterPipe implements PipeTransform {

  transform(program): any {
   
    return program.posterw154 || program.posterw92 || program.poster || program.thumbnailw92 || program.thumbnailw154 || program.thumbnail || program.thumbnailw780;

  }
}