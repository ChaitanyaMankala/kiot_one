import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'channel_num' })
export class ChannelNumPipe implements PipeTransform {
    transform(value, providerMap, channelMeta, override?): any {
        let value1;
        value1 = value;
        if (override) {
            value1 = value.channelid || value.id;
        }
        let temp = channelMeta[value1].base_channels;

        if (temp.length == 1) {
            return providerMap[temp[0]];
        }
        else {
            for (let i of temp) {
                if (providerMap[temp]) {
                    return providerMap[temp];
                }
                else {
                    return 'unknown';
                }
            }

        }

    }
}