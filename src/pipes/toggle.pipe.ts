import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'toggleValue'})
export class TogglePipe implements PipeTransform {
  transform(value, args): any {
   
            
    if(value==0 || !value){
            return 'OFF';
        }
     else if(value == 1 || value){
        return 'ON';
    }

  }
}