import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'timeFormatter'})
export class TimePipe implements PipeTransform {
  transform(value, args): any {
    if ((!value && value!=0)|| value=='00') return value;

    if(parseInt(value) < 10){
        return '0'+value.toString();
    }
    else {
        return value;
    }
    
  }
}