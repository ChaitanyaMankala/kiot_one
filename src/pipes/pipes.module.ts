//pipes
import { ChannelNumPipe } from './channel_num.pipe';
import { FindActivePipe } from './find-active.pipe';
import { OrderByPipe } from './order-by.pipe'
import { ProgramPosterPipe } from './program-poster.pipe';
import { TimePipe } from './time.pipe';
import { TogglePipe } from './toggle.pipe';


import { NgModule } from '@angular/core';

@NgModule({
    declarations: [
        ChannelNumPipe,
        FindActivePipe,
        OrderByPipe,
        ProgramPosterPipe,
        TimePipe,
        TogglePipe
    ],
    imports: [

    ],
    exports: [
        ChannelNumPipe,
        FindActivePipe,
        OrderByPipe,
        ProgramPosterPipe,
        TimePipe,
        TogglePipe
    ]
})
export class PipesModule { }