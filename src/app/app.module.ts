import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { NativeAudio } from '@ionic-native/native-audio';
import { GooglePlus } from '@ionic-native/google-plus';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppVersion } from '@ionic-native/app-version';

import { BarcodeScanner } from '@ionic-native/barcode-scanner';

//import { ChartsModule } from 'ng2-charts/ng2-charts';

import { IonicApp, IonicModule } from 'ionic-angular';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { Ionic2RatingModule } from 'ionic2-rating';
import { authToken } from '../providers/app/authToken/authToken';
import { MyApp } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//mqtt
import {
    MqttMessage,
    MqttModule,
    MqttService,
    MqttServiceOptions
} from 'ngx-mqtt';


//pipes
import { TimePipe } from '../pipes/time.pipe';
import { TogglePipe } from '../pipes/toggle.pipe';
import { OrderByPipe } from '../pipes/order-by.pipe';
import { ProgramPosterPipe } from '../pipes/program-poster.pipe';
import { FindActivePipe } from '../pipes/find-active.pipe';
import { ChannelNumPipe } from '../pipes/channel_num.pipe';

//providers
import { HttpServ } from '../providers/HttpServ';
import { DataService } from '../providers/DataService';
import { LocalStorageService } from '../providers/LocalStorageService';
import { ToastService } from '../providers/ToastService';
import { AlertService } from '../providers/AlertService';
import { PouchService } from '../providers/PouchService';
import { LoadingService } from '../providers/LoadingService';
import { Helper } from '../providers/Helper';
import { HttpLocalServ } from '../providers/HttpLocalService';
import { LocalCommService } from '../providers/LocalCommunicationServ';
import { SqliteService } from '../providers/SqliteService';
import { MetaService } from '../providers/MetaService';
import { NotificationService } from '../providers/NotificationService';
import { IntroService } from '../providers/IntroService';


//Start Folder
import { SliderPage } from '../pages/Start/slider/slider.page';
import { LoginPage } from '../pages/Start/login/login';
import { SignupPage } from '../pages/Start/signup/signup';
import { ForgotPasswordPage } from '../pages/Start/forgot-password/forgot-password';
import { ForgotPasswordDetailsPage } from '../pages/Start/forgot-password-details/forgot-password-details';
import { SetupPage } from '../pages/Start/setup/setup';
import { OfflinePage } from '../pages/Start/offline/offline';
import { UpdatePage } from '../pages/Start/update/update';

//Tabs launcher
import { TabsPage } from '../pages/tabs/tabs.page';


//Tab1
import { HomePage } from '../pages/Tab1/home/home.page';
import { NotificationDispPage } from '../pages/Tab1/notification-disp/notification-disp';
import { SwitchesListPage } from '../pages/Tab1/switches-list/switches-list';

//Tab2
import { RoomsPage } from '../pages/Tab2/rooms/rooms.page';
import { SwitchesInRoomPage } from '../pages/Tab2/switches-in-room/switches-in-room';
import { SwitchStatsPage } from '../pages/Tab2/switch-stats/switch-stats';
import { SwitchHistoryPage } from '../pages/Tab2/switch-history/switch-history';
import { DisplaySchedulePage } from '../pages/Tab2/display-schedule/display-schedule';
import { CreateSchedulePage } from '../pages/Tab2/create-schedule/create-schedule';
import { ScheduleDetailsPage } from '../pages/Tab2/schedule-details/schedule-details';
import { EditSchedulePage } from '../pages/Tab2/edit-schedule/edit-schedule';
import { PowerStatsPage } from '../pages/Tab2/power-stats/power-stats';

//Tab3
import { ScenesPage } from '../pages/Tab3/scenes/scenes.page';
import { RoomListPage } from '../pages/Tab3/room-list/room-list';
import { AppsForScenePage } from '../pages/Tab3/apps-for-scene/apps-for-scene';
import { CreateScenePage } from '../pages/Tab3/create-scene/create-scene';
import { ScheduleSwitchPage } from '../pages/Tab3/schedule-switch/schedule-switch';
import { SceneDetailsPage } from '../pages/Tab3/scene-details/scene-details';
import { EditScenePage } from '../pages/Tab3/edit-scene/edit-scene';
import { CreateWorkflowPage } from '../pages/Tab3/create-workflow/create-workflow';
import { ConfigWorkflowPage } from '../pages/Tab3/config-workflow/config-workflow';
import { NewTriggerPage } from '../pages/Tab3/new-trigger/new-trigger';
import { EditWorkflowPage } from '../pages/Tab3/edit-workflow/edit-workflow';


//Tab4
import { RemotePage } from '../pages/Tab4/remote/remote.page';
import { RealRemotePage } from '../pages/Tab4/real-remote/real-remote';
import { EpgMainPage } from '../pages/Tab4/epg-main/epg-main';
import { ProgramDetailsPage } from '../pages/Tab4/program-details/program-details';
import { PopoverPage } from '../pages/Tab4/epg-main/epg-main';
import { NumPopPage } from '../pages/Tab4/real-remote/real-remote';
import { LaterThisWeekPage } from '../pages/Tab4/later-this-week/later-this-week';
import { ViewAllNowPage } from '../pages/Tab4/view-all-now/view-all-now';
import { ChannelProgramsPage } from '../pages/Tab4/channel-programs/channel-programs';
import { RemoteModalPage } from '../pages/Tab4/remote-modal/remote-modal';
import { SearchModalPage } from '../pages/Tab4/search-modal/search-modal';
import { NumPadPage } from '../pages/Tab4/num-pad/num-pad';
import { ExtraButtonsPage } from '../pages/Tab4/extra-buttons/extra-buttons';
import { CreateTimerPage } from '../pages/Tab4/create-timer/create-timer';
import { ChannelListPage } from '../pages/Tab4/channel-list/channel-list';
import { NumPad2Page } from '../pages/Tab4/num-pad2/num-pad2';
import { RemotesListPage } from '../pages/Tab4/remotes-list/remotes-list';
import { SelectKeysPage } from '../pages/Tab4/select-keys/select-keys';
import { CreateCustomRemotePage } from '../pages/Tab4/create-custom-remote/create-custom-remote';
import { CustomRemotePage } from '../pages/Tab4/custom-remote/custom-remote';

//Tab5
import { IonSimpleWizard } from '../pages/Tab5/ion-simple-wizard/ion-simple-wizard.component';
import { IonSimpleWizardStep } from '../pages/Tab5/ion-simple-wizard/ion-simple-wizard.step.component';
//profile
import { ProfilePage } from '../pages/Tab5/1profile/profile/profile.page';
import { NotificationSettingsPage } from '../pages/Tab5/1profile/notification-settings/notification-settings';
import { ChangePasswordPage } from '../pages/Tab5/1profile/change-password/change-password';
import { DisplayProfilePage } from '../pages/Tab5/1profile/display-profile/display-profile';
import { EditProfilePage } from '../pages/Tab5/1profile/edit-profile/edit-profile';

//homes
import { RegisterHomePage } from '../pages/Tab5/2homes/registerhome/registerhome';
import { CreateHomePage } from '../pages/Tab5/2homes/create-home/create-home';
import { HomeUsersPage } from '../pages/Tab5/2homes/home-users/home-users';
import { HomeDetailsPage } from '../pages/Tab5/2homes/home-details/home-details';
import { EditHomePage } from '../pages/Tab5/2homes/edit-home/edit-home';
import { PrimeUserDetailsPage } from '../pages/Tab5/2homes/prime-user-details/prime-user-details';
import { JoinHomeSearchPage } from '../pages/Tab5/2homes/join-home-search/join-home-search';
import { PendingUserDetailsPage } from '../pages/Tab5/2homes/pending-user-details/pending-user-details';
import { OtherUserDetailsPage } from '../pages/Tab5/2homes/other-user-details/other-user-details';
import { DisplayWifiRoutersPage } from '../pages/Tab5/2homes/display-wifi-routers/display-wifi-routers';
import { CreateWifiRouterPage } from '../pages/Tab5/2homes/create-wifi-router/create-wifi-router';
import { WifiDetailsPage } from '../pages/Tab5/2homes/wifi-details/wifi-details';
import { EditWifiRouterPage } from '../pages/Tab5/2homes/edit-wifi-router/edit-wifi-router';

//rooms
import { CreateRoomPage } from '../pages/Tab5/3rooms/createroom/createroom';
import { DisplayRoomsPage } from '../pages/Tab5/3rooms/display-rooms/display-rooms';
import { RoomDetailsPage } from '../pages/Tab5/3rooms/room-details/room-details';
import { EditRoomPage } from '../pages/Tab5/3rooms/edit-room/edit-room';


//sboards
import { SwitchBoardDisplayPage } from '../pages/Tab5/4sboards/switch-board-display/switch-board-display';
import { CreatesboardPage } from '../pages/Tab5/4sboards/createsboard/createsboard';
import { HomeListPage } from '../pages/Tab5/4sboards/home-list/home-list';
import { SboardListPage } from '../pages/Tab5/4sboards/sboard-list/sboard-list';
import { SwitchBoardEditPage } from '../pages/Tab5/4sboards/edit-switch-board/edit-switch-board';
import { SwitchBoardConfigPage } from '../pages/Tab5/4sboards/configure-switch-board/config-switch-board';
import { Configure2SwitchBoardPage } from '../pages/Tab5/4sboards/configure2-switch-board/configure2-switch-board';

//switches
import { DisplaySwitchesPage } from '../pages/Tab5/5switches/display-switches/display-switches';
import { SwitchDetailsPage } from '../pages/Tab5/5switches/switch-details/switchDetails';
import { EditSwitchPage } from '../pages/Tab5/5switches/edit-switch/edit-switch';

//irAppliances
import { DisplayIrAppsPage } from '../pages/Tab5/6irAppliances/display-ir-apps/display-ir-apps';
import { CreateIrAppPage } from '../pages/Tab5/6irAppliances/create-ir-app/create-ir-app';
import { ConfigureRemotePage } from '../pages/Tab5/6irAppliances/configure-remote/configure-remote';
import { BrandsPage } from '../pages/Tab5/6irAppliances/brands/brands';
import { SetUpRemotePage } from '../pages/Tab5/6irAppliances/set-up-remote/set-up-remote';
import { IrAppDetailsPage } from '../pages/Tab5/6irAppliances/ir-app-details/ir-app-details';
import { EditIrAppPage } from '../pages/Tab5/6irAppliances/edit-ir-app/edit-ir-app';


//accounts
import { AllAccountsPage } from '../pages/Tab5/7accounts/all-accounts/all-accounts';
import { SettingsAlexaPage } from '../pages/Tab5/7accounts/settings-alexa/settings-alexa';
import { SettingsGooglePage } from '../pages/Tab5/7accounts/settings-google/settings-google';
import { ConfigureAlexaPage } from '../pages/Tab5/7accounts/configure-alexa/configure-alexa';
import { InstructionsAlexaPage } from '../pages/Tab5/7accounts/instructions-alexa/instructions-alexa';

//support
import { SupportMainPage } from '../pages/Tab5/8support/support-main/support-main';
import { ContactUsPage } from '../pages/Tab5/8support/contact-us/contact-us';
import { CreateTicketPage } from '../pages/Tab5/8support/create-ticket/create-ticket';


//for MQTT

export const MQTT_SERVICE_OPTIONS: MqttServiceOptions = {
    connectOnCreate: false,
    hostname: '139.59.30.136',
    port: 8083,
    protocol: 'ws',
    path: '/mqtt'
};

export function mqttServiceFactory() {
    return new MqttService(MQTT_SERVICE_OPTIONS);
}



@NgModule({
    declarations: [
        MyApp,
        PopoverPage,
        NumPopPage,
        TabsPage
        
    ],
    imports: [
        IonicModule.forRoot(MyApp),
        Ionic2RatingModule,
        RoundProgressModule,
        BrowserModule,
        HttpModule,
        BrowserAnimationsModule,
        MqttModule.forRoot({
            provide: MqttService,
            useFactory: mqttServiceFactory
        })
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        PopoverPage,
        NumPopPage,
        TabsPage
    ],
    providers: [
        HttpLocalServ,
        ToastService,
        AlertService,
        LoadingService,
        Helper,
        LocalCommService,
        PouchService,
        SqliteService,
        MetaService,
        DataService,
        HttpServ,
        LocalStorageService,
        SQLite,
        NativeAudio,
        GooglePlus,
        BarcodeScanner,
        NotificationService,
        IntroService,
        AppVersion,
        AndroidPermissions
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {

}

/* BACKUP ---

SetupPage,
        OfflinePage,
        UpdatePage,
        RoomsPage,
        SwitchesInRoomPage,
        PowerStatsPage,
        SwitchStatsPage,
        SwitchHistoryPage,
        ScenesPage,
        CreateScenePage,
        CreateWorkflowPage,
        ConfigWorkflowPage,
        EditWorkflowPage,
        NewTriggerPage,
        AppsForScenePage,
        ScheduleSwitchPage,
        SceneDetailsPage,
        EditScenePage,
        HomePage,
        SwitchesListPage,
        NotificationDispPage,
        SliderPage,
        LoginPage,
        SignupPage,
        BrandsPage,
        RemotePage,
        RealRemotePage,
        CreateTimerPage,
        ExtraButtonsPage,
        EpgMainPage,
        ProgramDetailsPage,
        LaterThisWeekPage,
        ViewAllNowPage,
        ChannelProgramsPage,
        ChannelListPage,
        ProfilePage,
        CreateRoomPage,
        RegisterHomePage,
        HomeDetailsPage,
        HomeUsersPage,
        DisplayRoomsPage,
        SwitchBoardDisplayPage,
        CreatesboardPage,
        DisplaySwitchesPage,
        DisplayProfilePage,
        ChangePasswordPage,
        ForgotPasswordPage,
        ForgotPasswordDetailsPage,
        CreateHomePage,
        EditHomePage,
        PrimeUserDetailsPage,
        JoinHomeSearchPage,
        PendingUserDetailsPage,
        OtherUserDetailsPage,
        DisplayWifiRoutersPage,
        CreateWifiRouterPage,
        EditWifiRouterPage,
        WifiDetailsPage,
        RoomDetailsPage,
        EditRoomPage,
        HomeListPage,
        SboardListPage,
        EditProfilePage,
        RoomListPage,
        SwitchDetailsPage,
        SwitchBoardEditPage,
        SwitchBoardConfigPage,
        Configure2SwitchBoardPage,
        EditSwitchPage,
        DisplayIrAppsPage,
        CreateIrAppPage,
        ConfigureRemotePage,
        SetUpRemotePage,
        IrAppDetailsPage,
        EditIrAppPage,
        TimePipe,
        TogglePipe,
        OrderByPipe,
        ProgramPosterPipe,
        FindActivePipe,
        ChannelNumPipe,
        RemoteModalPage,
        SearchModalPage,
        NumPadPage,
        NumPad2Page,
        AcRemotePage,
        DisplaySchedulePage,
        CreateSchedulePage,
        EditSchedulePage,
        ScheduleDetailsPage,
        NotificationSettingsPage,
        SettingsAlexaPage,
        SettingsGooglePage,
        ConfigureAlexaPage,
        AllAccountsPage,
        InstructionsAlexaPage,
        RemotesListPage,
        SelectKeysPage,
        CreateCustomRemotePage,
        CustomRemotePage,
        SupportMainPage,
        ContactUsPage,
        CreateTicketPage






        SetupPage,
        OfflinePage,
        UpdatePage,
        RoomsPage,
        SwitchesInRoomPage,
        PowerStatsPage,
        SwitchStatsPage,
        SwitchHistoryPage,
        ScenesPage,
        CreateScenePage,
        CreateWorkflowPage,
        ConfigWorkflowPage,
        EditWorkflowPage,
        NewTriggerPage,
        AppsForScenePage,
        ScheduleSwitchPage,
        SceneDetailsPage,
        EditScenePage,
        HomePage,
        SwitchesListPage,
        NotificationDispPage,
        SliderPage,
        LoginPage,
        SignupPage,
        BrandsPage,
        RemotePage,
        RealRemotePage,
        CreateTimerPage,
        ExtraButtonsPage,
        EpgMainPage,
        ProgramDetailsPage,
        LaterThisWeekPage,
        ViewAllNowPage,
        ChannelProgramsPage,
        ChannelListPage,
        ProfilePage,
        CreateRoomPage,
        RegisterHomePage,
        HomeDetailsPage,
        HomeUsersPage,
        DisplayRoomsPage,
        SwitchBoardDisplayPage,
        CreatesboardPage,
        DisplaySwitchesPage,
        DisplayProfilePage,
        ChangePasswordPage,
        ForgotPasswordPage,
        ForgotPasswordDetailsPage,
        CreateHomePage,
        EditHomePage,
        PrimeUserDetailsPage,
        JoinHomeSearchPage,
        PendingUserDetailsPage,
        OtherUserDetailsPage,
        DisplayWifiRoutersPage,
        CreateWifiRouterPage,
        WifiDetailsPage,
        EditWifiRouterPage,
        RoomDetailsPage,
        EditRoomPage,
        HomeListPage,
        SboardListPage,
        EditProfilePage,
        RoomListPage,
        SwitchDetailsPage,
        SwitchBoardEditPage,
        SwitchBoardConfigPage,
        Configure2SwitchBoardPage,
        EditSwitchPage,
        DisplayIrAppsPage,
        CreateIrAppPage,
        ConfigureRemotePage,
        SetUpRemotePage,
        IrAppDetailsPage,
        EditIrAppPage,
        RemoteModalPage,
        SearchModalPage,
        NumPadPage,
        NumPad2Page,
        AcRemotePage,
        DisplaySchedulePage,
        CreateSchedulePage,
        EditSchedulePage,
        ScheduleDetailsPage,
        NotificationSettingsPage,
        SettingsAlexaPage,
        SettingsGooglePage,
        ConfigureAlexaPage,
        AllAccountsPage,
        InstructionsAlexaPage,
        RemotesListPage,
        SelectKeysPage,
        CreateCustomRemotePage,
        CustomRemotePage,
        SupportMainPage,
        ContactUsPage,
        CreateTicketPage

*/
