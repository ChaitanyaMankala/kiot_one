import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, ViewController, Nav, App, IonicApp } from 'ionic-angular';
import { ModalController, Events } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import {
    MqttMessage,
    MqttModule,
    MqttService
} from 'ngx-mqtt';
//native
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Network } from '@ionic-native/network';
import { OneSignal } from '@ionic-native/onesignal';

//import { Deeplinks } from '@ionic-native/deeplinks';
//declare let ace: any;
//providers
import { HttpServ } from '../providers/HttpServ';
import { SqliteService } from '../providers/SqliteService';
import { MetaService } from '../providers/MetaService';
import { DataService } from '../providers/DataService';
import { SliderPage } from '../pages/Start/slider/slider.page';
import { LocalStorageService } from '../providers/LocalStorageService';
import { AlertService } from '../providers/AlertService';
import { ToastService } from '../providers/ToastService';
import { NotificationService } from '../providers/NotificationService';

//pages
import { TabsPage } from '../pages/tabs/tabs.page';
import { LoginPage } from '../pages/Start/login/login';
import { NotificationDispPage } from '../pages/Tab1/notification-disp/notification-disp';
import { OfflinePage } from '../pages/Start/offline/offline';
import { UpdatePage } from '../pages/Start/update/update';
import { SetupPage } from '../pages/Start/setup/setup';

@Component({
    template: `<ion-nav [root]="rootPage"></ion-nav>`,
    providers: [OneSignal, Network, SplashScreen, StatusBar]
})

export class MyApp {

    @ViewChild(Nav) nav: Nav;
    rootPage: any;
    _db;
    constructor(private platform: Platform, private _mqttService: MqttService, private oneSignal: OneSignal, private noti: NotificationService, private network: Network, private toast: ToastService, private splashScreen: SplashScreen, private statusBar: StatusBar, private event: Events, private ds: DataService, private sql: SqliteService, private meta: MetaService, private ionicApp: IonicApp, private alert: AlertService, private app: App, private modalCtrl: ModalController, private ht: HttpServ, private local: LocalStorageService) {

        this.ht.testInternet();
        this.local.setBackButtonOverride(false);
        this.meta.getMetaService()
            .then((update) => {
                if (update != -1 && (update == true)) {

                    this.modalCtrl.create('UpdatePage').present();
                }
            });

        this.platform.ready().then(() => {
            // this.setupWidget();
            this.splashScreen.hide();
            //this.deepLinkSetup();
            if (this.local.getToken() == "" || this.local.getToken() == "undefined" || this.local.getToken() == "null" || this.local.getToken() == null) this.rootPage = 'SliderPage';
            else if ((this.local.getUser().homes && this.local.getUser().homes.length == 0) || !this.local.getUser().homes) {
                this.rootPage = 'SetupPage';
            }
            else this.rootPage = TabsPage;
            //StatusBar.styleDefault();
            if (this.rootPage == TabsPage) {
                if (!this.local.getDefaultHome()) {
                    if (this.local.getUser()) {
                        if (this.local.getUser().default_home) {
                            this.local.setDefaultHome(this.local.getUser().default_home);
                        }
                        else {
                            if (this.local.getUser().homes && this.local.getUser().homes.length) {
                                this.ds.putEditProfile({
                                    default_home: this.local.getUser().homes[0]
                                }, data => {
                                    console.log(data);
                                }, error => {
                                    console.log(error);
                                })
                                this.local.setDefaultHome(this.local.getUser().homes[0]);
                            }
                        }
                    }
                    else {
                        this.rootPage = LoginPage;
                    }
                }
                if (this.local.getCurrentHome() == 'undefined') this.local.setCurrentHome(this.local.getDefaultHome());
            }
            //  this.statusBar.overlaysWebView(false);
            this.statusBar.hide();
            // this.statusBar.backgroundColorByHexString('#313131');
            /*  if (platform.is('ios')) {
                  this.statusBar.styleLightContent();
              }
              if (platform.is('android')) {
                  this.statusBar.styleBlackTranslucent();
              }*/
            let popup = false;
            this.offlineBufferToServer();

            //   if (this.nav) {
            /*  this.app.viewDidLeave.subscribe((data:ViewController) => {
                  console.log(this.nav.getByIndex(data.index).name);
              });*/


            // }
            //this.connectMQTT('i_am_admin', 'No!&&No1@meBl0ck*M#');
            this.event.subscribe('relogin',
        data=>{
            if(data == false){
                this.local.logout();
                this.app.getRootNav().setRoot('LoginPage');
            }
        })
            this.event.subscribe('kiot_internet',
                (data) => {
                    if (data == 'offline') {
                        this.modalCtrl.create('OfflinePage').present();

                    }
                    if (data == 'online') {
                        this.offlineBufferToServer();

                    }
                    if (data == 'go_offline') {
                        if (this.ionicApp._modalPortal.getViews().length) {
                            this.ionicApp._modalPortal.popAll();
                        }
                        this.nav.setRoot(TabsPage, {
                            tab_index: 1
                        });
                    }
                    if(data == 'back_online'){
                        // let nav1 = app.getActiveNavs();
                        // console.log(nav1);
                        // //nav1.getActive()._didEnter();
                    }
                });
            platform.registerBackButtonAction(() => {
                let nav1 = app.getActiveNav();
                let x = local.getBackButtonOverride();
                let activePortal = this.ionicApp._loadingPortal.getActive() ||
                    this.ionicApp._modalPortal.getActive() ||
                    this.ionicApp._toastPortal.getActive() ||
                    this.ionicApp._overlayPortal.getActive();
                if (x == "true") {
                    console.log('do nothing');
                }
                else {
                    if (nav1.canGoBack()) {
                        nav1.pop();
                    }
                    else if (activePortal) {
                        activePortal.dismiss();
                    }
                    else {
                        this.alert.showConfirm("Exit KIOT?", "Press Yes to exit the app", 'No', 'Yes',
                            () => {

                            },
                            () => {
                                platform.exitApp();
                            })
                    }
                }
            });
            this.registerZeroConf();
            this.network.onConnect()
                .subscribe(() => {
                    this.registerZeroConf();
                    if (this.local.getInternetStatus() == 'online') {
                        this.offlineBufferToServer();
                    }


                })

            // Register Zeroconf PLugin


            // Activate this.oneSignal Notifications
            this.initiateNotifications();

        });
    }
    setupWidget() {
        /*
        ace.android.appWidget.clear();

        for (var i = 0; i < 10; i++) {
            ace.android.appWidget.add("Item with index " + i);
        }
        // Handle the app being resumed by a widget click:
        ace.addEventListener("android.intentchanged", () => {
            if (ace.platform != "Android") {
                return;
            }
        });
        ace.android.getIntent().invoke("getIntExtra", "widgetSelectionIndex", -1,
            (value) => {
                // value is the index of the item clicked
                // or -1 if no item has been clicked
                console.log('clicked');
                console.log(value);
            });
        */

    }
    registerZeroConf() {
        console.log('Zero conf is restarting');
        var zeroconf = cordova.plugins.zeroconf;
        zeroconf.registerAddressFamily = 'ipv4';
        zeroconf.watchAddressFamily = 'ipv4';
        try {
            zeroconf.close();
            // zeroconf.unwatch('_http._tcp.', 'local.');
        }
        catch (err) {
            console.log('no problem');
        }
        zeroconf.watch('_http._tcp.', 'local.', (data) => {
            var action = data.action;
            var service = data.service;

            if (action == "resolved") {
                console.log("ADDED");
                console.log(service);
                let ip = service.ipv4Addresses;
                let domain = service.hostname;
                if (ip.length > 0) {
                    //Take the last IP
                    if (ip[ip.length - 1]) {
                        ip = ip[ip.length - 1];
                    }
                } else {
                    //ip = "";
                    return;
                }
                this.local.setLocalIp(domain, ip);
                return;

            } else if (action == "removed") {
                console.log("REMOVED");
                console.log(service);
            }
            return;

        });
        //  console.log(zeroconf.getMyIp('kiot_switch_5ccf7fda57b4.local'));
    }
    connectMQTT(username, pass) {
        this._mqttService.connect({ username: username, password: pass });

    }
    initiateNotifications() {

        // Define settings for iOS
        var iosSettings = {};
        iosSettings["kOSSettingsKeyAutoPrompt"] = true;
        iosSettings["kOSSettingsKeyInAppLaunchURL"] = false;

        if (this.platform.is('ios')) {
            // Initialise plugin with this.oneSignal service
            this.oneSignal.startInit("fc79c843-b513-4ab0-bb14-dd72214478f9", "970544757572")
                .iosSettings(iosSettings);
            console.log('In Onesignal iOS');
        }
        else {
            this.oneSignal.startInit("fc79c843-b513-4ab0-bb14-dd72214478f9", "970544757572");
        }


        // Control how this.oneSignal notifications will be shown when
        // one is received while your app is in focus
        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);


        // Retrieve the this.oneSignal user id and the device token
        this.oneSignal.getIds()
            .then((ids) => {
                console.log('getIds: ' + JSON.stringify(ids));
                this.local.setOneSignalIds(ids);
            });


        // When a push notification is received handle
        // how the application will respond
        this.oneSignal.handleNotificationReceived()
            .subscribe((msg) => {
                // Log data received from the push notification service
                console.log('Notification received');
                console.dir(msg);
                console.dir(msg);
            });
        this.oneSignal.inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.Notification);
        // When a push notification is opened by the user
        // handle how the application will respond
        this.oneSignal.handleNotificationOpened()
            .subscribe((msg: any) => {
                // Log data received from the push notification service
                console.log('Notification opened');
                console.log(msg);
                /*  if (!msg.notification.isAppInFocus && msg.action.type != 0) {
                      // this.toast.nativeToast_Bottom_Short('Yolo Bro!!');
                      this.noti.invokeNotificationAction(msg.notification.payload.additionalData, msg.action);
                  }
                  else {*/
                // window.open(window.location.toString());
                this.modalCtrl.create(NotificationDispPage, {
                    msg: msg
                }).present();
                // }
                console.dir(msg);
            });


        // End plugin initialisation
        this.oneSignal.endInit();

    }



    offlineBufferToServer() {
        console.log('pushing offline buffer to server...');
        this.sql.openDb('kiot_ac.db')
            .then(data => {
                this.sql.checkTableExistence('KIOT_BUFFER')
                    .then((data) => {
                        if (data) {
                            this.sql.getATuple('KIOT_BUFFER', ' WHERE id=?', ['kiot_buffer'])
                                .then((data: any) => {
                                    if (data.rows.length != 0) {
                                        let obj;
                                        obj = {
                                            actions: JSON.parse(data.rows.item(0).buf_str)
                                        };
                                        this.ds.postOfflineSync(obj,
                                            data => {
                                                console.log(data);
                                                let buff_array = [];
                                                this.sql.updateData('KIOT_BUFFER', 'id=?,buf_str=?', ['kiot_buffer', JSON.stringify(buff_array)])
                                                    .then(data => { console.log('success in clearing data') })
                                                    .catch(error => { console.log(error) });
                                            }, error => { console.log(error); })
                                    }

                                }).catch(error => { console.log(error); })
                        }
                        else {
                            console.log('No table Bro!');
                        }
                    }).catch(err => { console.log(err) })

            }).catch(error => { console.log(error) });
    }
    deepLinkSetup() {
        /* this.deeplinks.routeWithNavController(this.nav, {
             '/login': LoginPage,
             '/dashboard': TabsPage
         }).subscribe((match) => {
             // match.$route - the route we matched, which is the matched entry from the arguments to route()
             // match.$args - the args passed in the link
             // match.$link - the full link data
             console.log('Successfully matched route', match);
         }, (nomatch) => {
             // nomatch.$link - the full link data
             console.error('Got a deeplink that didn\'t match', nomatch);
         });*/
    }

}