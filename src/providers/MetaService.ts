import { Injectable, Inject, forwardRef } from '@angular/core';
import { Platform } from 'ionic-angular';
import { DataService } from './DataService';
import { SqliteService } from './SqliteService';
import { LocalStorageService } from './LocalStorageService';
import { AppVersion } from '@ionic-native/app-version';


@Injectable()
export class MetaService {
    constructor( @Inject(forwardRef(() => DataService)) private ds: DataService, private platform: Platform, private appVersion: AppVersion, private sql: SqliteService, private local: LocalStorageService) {
    }
    getMetaService() {
        return new Promise((resolve, reject) => {
            this.ds.getMeta(
                (meta_data: any) => {
                    console.log(meta_data);
                    if (meta_data.update_mandate == true) {
                        this.updateAppCheck(meta_data)
                            .then(data => {
                                console.log(data);
                                resolve(data);
                            }).catch(err => {
                                resolve(-1);
                            });
                    }
                    else {
                        resolve(-1);
                    }
                    this.sql.openDb('kiot_ac.db')
                        .then(
                        data1 => {
                            this.upsertMeta(JSON.stringify({ meta: meta_data }));
                            this.local.setMetaData(JSON.stringify({ meta: meta_data }));

                        }).catch(
                        err => { console.log(err); }
                        );
                }, error => {
                    resolve(-1);
                    console.log(error);
                });
        })
    }
    updateAppCheck(meta_data) {

        return new Promise((resolve, reject) => {
            if (meta_data) {
                this.appVersion.getVersionCode().then((data) => {
                    console.log(data);
                    let current_version = data;
                    let update_version;
                    if (this.platform.is('android')) {
                        update_version = meta_data.android_app_version;
                    }
                    else if (this.platform.is('ios')) {
                        update_version = meta_data.ios_app_version;
                    }
                    if (update_version && current_version != undefined) {
                        resolve(this.compareVersions(update_version, current_version))
                    }
                    resolve(false);


                });
            }
            else {
                resolve(false);
            }
        })


    }

    compareVersions(update, current) {
        let update_ver = 0;
        let current_ver = current;
        let str = '';
        if (update.split('.')[0] && update.split('.')[1] && update.split('.')[2]) {
            if (parseInt(update.split('.')[0]) || parseInt(update.split('.')[0]) == 0) {
                str += (parseInt(update.split('.')[0])).toString();
            }
            if (parseInt(update.split('.')[1]) || parseInt(update.split('.')[1]) == 0) {
                if (parseInt(update.split('.')[1]) < 10) {
                    str += '0';
                }
                str += (parseInt(update.split('.')[1])).toString();
            }


            if (parseInt(update.split('.')[2]) || parseInt(update.split('.')[2]) == 0) {
                if (parseInt(update.split('.')[2]) < 10) {
                    str += '0';
                }
                str += parseInt(update.split('.')[2]).toString();
            }

        }
        update_ver = parseInt(str) || 0;
        if (update_ver > current_ver) {
            return true;
        }
        else {
            return false;
        }

    }

    upsertMeta(meta_data) {
        this.sql.createATable('CREATE TABLE IF NOT EXISTS KIOT_META (meta_str TEXT,id TEXT)')
            .then(data => {
                this.sql.getATuple('KIOT_META', ' WHERE id=?', ['1'])
                    .then((data: any) => {
                        if (data.rows.length == 0) {
                            this.sql.insertData('KIOT_META VALUES (?,?)', [meta_data, '1'])
                                .then(data => {
                                    console.log('success');
                                })
                                .catch(error => {
                                    console.log(error);
                                });
                        }
                        else {
                            this.sql.updateData(' KIOT_META ', ' meta_str=? WHERE id=?', [meta_data, '1'])
                                .then(data => {
                                    console.log('update success');
                                })
                                .catch(error => {
                                    console.log(error);
                                });
                        }
                    }).catch(error => {
                        console.log(error);
                    });
            }).catch(err => {
                console.log(err);
            })
    }

    getMetaData() {
        return this.sql.getATuple('KIOT_META', ' WHERE id=?', ['1']);
    }
    getMetaDataFromLocal() {
        let meta = this.local.getMetaData();
        if (meta) {
            return meta;
        }
        else {
            this.getMetaData()
                .then((data: any) => {
                    console.log(data.rows[0].meta_str);
                    if (data.rows.length == 0) {
                        this.getMetaService();
                        return 'NOT FOUND';
                    }
                    else {
                        return data.rows[0].meta_str;
                    }
                }).catch(error => {
                    console.log(error);
                });
        }
    }
}