import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import * as CryptoJS from 'crypto-js';


@Injectable()
export class Helper {

	constructor(private platform: Platform) {

	}

	isEmpty(param) {
		if (Array.isArray(param)) {
			if (param.length == 0) {
				return true;
			}
			else return false;
		}
		else {
			if (param == "" || param == "undefined" || param == "null" || param == null || JSON.stringify(param) == "[]") {
				return true;
			}
			else return false;
		}

	}
	//helper for switch appliance ON duration
	findDuration(obj, lastKnownStatus) {
		let current = false;
		let startTime;
		let totalHrsDuration = 0;
		let totalMinsDuration = 0;
		let progressbarDuration = 0;
		let mins;
		console.log(obj);
		if (obj.length == 0) {
			if (lastKnownStatus == 1 || lastKnownStatus) {
				var now = new Date(),
					then = new Date(
						now.getFullYear(),
						now.getMonth(),
						now.getDate(),
						0, 0, 0);
				totalMinsDuration = this.findMinsInterval(then, now);
			}

		}
		else {
			if (obj[0].event_type == 0) {
				let mid = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate(), 0, 0, 0);
				totalMinsDuration = this.findMinsInterval(mid, obj[0].timestamp);
			}
			for (let i of obj) {
				if (i.event_type == 1) {
					if (!current) {
						current = true;
						startTime = i.timestamp;
					}
				}
				if (i.event_type == 0) {
					if (current) {
						current = false;
						//  totalHrsDuration+= this.findHrsInterval(startTime,i.timestamp);
						totalMinsDuration += this.findMinsInterval(startTime, i.timestamp);
					}
				}
			}
			if (current) {
				//  totalHrsDuration+= Math.floor(this.findHrsInterval(startTime,Date.now()));
				totalMinsDuration += Math.floor(this.findMinsInterval(startTime, Date.now()));
			}
		}



		if (totalMinsDuration < 60) { mins = totalMinsDuration; }
		else {
			totalHrsDuration = Math.floor(totalMinsDuration / 60);
			mins = (totalMinsDuration % 60);
		}
		progressbarDuration = ((totalMinsDuration) / (24 * 60)) * 100;
		return [Math.floor(totalHrsDuration), mins ? Math.floor(mins) : 0, progressbarDuration];
	}
	findHrsInterval(startTime, endTime) {
		let diff, hrs;
		diff = endTime - startTime;
		hrs = diff / 1000 / 60 / 60;
		return hrs;
	}
	findMinsInterval(startTime, endTime) {
		let diff, mins;
		diff = endTime - startTime;
		mins = diff / 1000 / 60;
		//console.log(mins);
		return mins;
	}


	channel_num_finder(value, providerMap, channelMeta): any {
		let temp = channelMeta[value].base_channels;

		for (let i of temp) {
			if (providerMap[i]) {
				return providerMap[i];
			}
		}
		return 'unknown';

	}
	getCodesForChannel(channel_number, remote_keys) {
		let code_str = "";
		let channel_num_str = channel_number.toString();
		let x = 0;
		while (x < channel_num_str.length) {
			code_str += remote_keys[channel_num_str[x]];
			x++;
			if (x == channel_num_str.length) continue;
			code_str += ',';
		}
		return code_str;
	}
	switchToChannel(channelid, channelMetaData, providerMap, sb_id, device_id, STBRemote, channel_num_to_switch?) {

		let remote_keys = STBRemote[0].keys[0];
		let channel_number_to_switch;
		let flag = true;
		let temp;
		if (!channel_num_to_switch) {
			temp = channelMetaData[3].channels[channelid].base_channels;
			if (temp.length == 1) {
				channel_number_to_switch = providerMap[temp[0]];
			}
			else {
				for (let i of temp) {
					if (providerMap[i]) {
						channel_number_to_switch = providerMap[i];
						flag = true;
						break;
					}
					else {
						flag = false;
					}
				}
			}
		}
		else {
			channel_number_to_switch = channel_num_to_switch;
		}
		if (flag && channel_number_to_switch && channel_number_to_switch != 'unknown') {
			let obj = {
				"channelid": channel_number_to_switch,
				"ir_device": sb_id,
				"device": device_id,
				"protocol": STBRemote[0].protocol,
				"len": STBRemote[0].len,
				"proto_name": STBRemote[0].proto_name,
				"wave_gap": STBRemote[0].wave_gap,
				"codes": this.getCodesForChannel(channel_number_to_switch, remote_keys)
			}
			return obj;
		}
		else {
			return 'NO CHANNEL';
		}
	}



	getIrOfflineObject(irSignal, type) {
		if (type == 'ac') type = 2;
		switch (type) {
			case 1: {
				irSignal.type = 'protocol';
				let newString = JSON.stringify({
					code: irSignal.code,
					proto: irSignal.protocol,
					length: irSignal.len,
					repeat: 1
				});
				return `type=${irSignal.type}&irdata=${newString}`;
			}

			case 2: {
				irSignal.type = 'ircustom';
				let newString;
				newString = this.getTypeTwoString(irSignal);
				let anything = 'anything';
				return `type=${irSignal.type}&rawlen=${irSignal.len}&rawStr=${newString}&raw=${anything}`
			}
		}

	}

	getTypeTwoString(irSignal) {
		let msgString = "";
		let i;
		for (i = 0; i < 2 - String(irSignal.rpt_cnt).length; i++) {
			msgString += "0";
		}
		msgString += String(irSignal.rpt_cnt);
		msgString += ":";
		for (i = 0; i < 4 - String(irSignal.len).length; i++) {
			msgString += "0";
		}
		msgString += String(irSignal.len);
		msgString += ":";

		for (i = 0; i < 3 - String(irSignal.first_seq_len).length; i++) {
			msgString += "0";
		}
		msgString += String(irSignal.first_seq_len);
		msgString += ":";

		for (i = 0; i < 3 - String(irSignal.sec_seq_len).length; i++) {
			msgString += "0";
		}
		msgString += String(irSignal.sec_seq_len);
		msgString += ":";

		for (i = 0; i < 2 - String(irSignal.freq).length; i++) {
			msgString += "0";
		}
		msgString += String(irSignal.freq);
		msgString += ":";

		msgString += irSignal.code;
		return msgString;
	}


	getPlatform() {
		if (this.platform.is('ios')) {
			return 'ios'
		}
		else if (this.platform.is('android')) {
			return 'android';
		}
	}

	getDaysArray(arr) {
		let repeats = [];
		let c = 0;
		for (let i of arr) {
			if (i) {
				repeats.push(c);
			}
			c++;
		}
		return repeats;
	}

	sorter(array, key, override_key) {
		var swap = function (array, firstIndex, secondIndex) {
			var temp = array[firstIndex];
			array[firstIndex] = array[secondIndex];
			array[secondIndex] = temp;
		};
		var maxHeap = function (array, i) {
			var l = 2 * i;
			var r = l + 1;
			var largest;
			if (l < array.heapSize && array[l][key] > array[i][key]) {
				largest = l;
			}
			else if (l < array.heapSize && array[l][key] == array[i][key]) {
				if (array[l][override_key] > array[i][override_key]) {
					largest = l
				}
				else {
					largest = i;
				}
			} else {
				largest = i;
			}
			if (r < array.heapSize && array[r][key] > array[largest][key]) {
				largest = r;
			}
			else if (r < array.heapSize && array[r][key] == array[largest][key]) {
				if (array[r][override_key] > array[largest][override_key]) {
					largest = r
				}
			}
			if (largest != i) {
				swap(array, i, largest);
				maxHeap(array, largest);
			}
		};
		var buildHeap = function (array) {
			array.heapSize = array.length;
			for (var i = Math.floor(array.length / 2); i >= 0; i--) {
				maxHeap(array, i);
			}
		};
		buildHeap(array);
		for (var i = array.length - 1; i >= 1; i--) {
			swap(array, 0, i);
			array.heapSize--;
			maxHeap(array, 0);
		}
		return array;
	}

	timeFormatter(obj, hrs, mins, time_string_key, _break?) {
		let str = '';
		let hr = parseInt(obj[hrs]);
		let min = parseInt(obj[mins]);

		str += hr == 0 ? 12 : hr < 10 ? '0' + hr.toString() : hr > 12 ? (hr - 12).toString() : hr.toString();
		str += ':';
		str += min < 10 ? '0' + min.toString() : min.toString();
		if (_break) {
			str += '\n';
		}
		else {
			str += ' ';
		}
		str += hr < 12 ? 'AM' : 'PM';

		obj[time_string_key] = str;
	}
	arrayToCommaSeparatedString(arr) {
		let cs_str = '';
		if (arr) {
			for (let i of arr) {
				cs_str += i;
				cs_str += ','
			}
			if (cs_str != '') cs_str = cs_str.slice(0, cs_str.length - 1);
		}
		return cs_str;
	}

	getMqttPayload(message, iv, secretKey) {
		//let secretKey = "2B7E151628AED2A6ABF7158809CF4F3C";
		iv = new Buffer(iv, 'base64').toString('hex');
		iv = CryptoJS.enc.Hex.parse(iv);
		let key = CryptoJS.enc.Hex.parse(secretKey);
		var bytes = CryptoJS.AES.decrypt(message, key, { iv: iv });
		var plaintext = bytes.toString(CryptoJS.enc.Base64);
		var decoded_b64msg = new Buffer(plaintext, 'base64').toString('ascii');
		var decoded_msg = new Buffer(decoded_b64msg, 'base64').toString('ascii');
		if (decoded_msg.toString()[decoded_msg.toString().length - 1] == '}') {
			try {
				console.log(JSON.parse(decoded_msg.toString()));
			} catch (err) { }
			return decoded_msg.toString();
		}
		else {
			return decoded_msg.toString() + '}';
		}
	}

	arihantsTimeToString(time, for_form?) {
		let time_str = '';
		let hour;
		hour = Math.floor(time / 100);
		if (for_form) {
			hour = hour < 10 ? '0' + hour.toString() : hour.toString();
			time_str += hour;
			time_str += ':';
			let mins;
			mins = (time - Math.floor(time / 100) * 100);
			mins = mins < 10 ? '0' + mins.toString() : mins.toString();
			time_str += mins;
		}
		else {
			hour = hour > 12 ? hour - 12 : hour;
			time_str += hour.toString();
			time_str += ':';
			let mins;
			mins = (time - Math.floor(time / 100) * 100);
			mins = mins < 10 ? '0' + mins.toString() : mins.toString();
			time_str += mins;
			time_str += ' ';
			time_str += Math.floor(time / 100) >= 12 ? 'PM' : 'AM';
		}
		return time_str;
	}

	constructActionString(action) {
		let x = '';
		let clear = true;
		if (action.type == 'switch') {
			let act = action.sw_action ? 'ON' : 'OFF';
			if (action.app.switch_type == 'regulator') {
				if (!action.sw_dimm && action.sw_action != 0) {
					action.sw_dimm = 0;
				}
				x = 'Turn <span class="txt-orange">' + act + '</span> and set speed to <span class="txt-orange">' + action.sw_dimm + '</span> on ' + action.app.name + ' in ' + action.room.room_name;
			}
			else {
				x = 'Turn <span class="txt-orange">' + act + '</span> ' + action.app.name + ' in ' + action.room.room_name;
			}
		}
		else if (action.type == 'ir') {
			if (action.ir_action == 'POWER ON' || action.ir_action == 'POWER OFF') {
				x = '<span class="txt-orange">' + action.ir_action + '</span> ' + action.app.name + ' in ' + action.room.room_name;
			}
			else if (action.ir_action == 'temp') {
				if (!action.ir_action_value) {
					action.ir_action_value = 22;
				}
				x = 'Set Temperature to <span class="txt-orange">' + action.ir_action_value + '</span> on ' + action.app.name + ' in ' + action.room.room_name;
			}
			else {
				//this.alert.showAlert('Invalid', 'Action cannot be empty');
				clear = false;
			}
		}
		return {
			action_str: x,
			clear: clear,
			action: action
		};
	}

	motionToast(motion, last_motion_timestamp) {
		let msg = '';
		let last_motion_time;
		if (last_motion_timestamp) {
			last_motion_time = Math.ceil(this.findMinsInterval(last_motion_timestamp, Date.now()));
		}
		if (motion == 'unreachable') {
			msg = 'motion sensor is unreachable';
		}
		else {
			if (last_motion_time || last_motion_time == 0) {
				if (motion == 'detected') {
					if (last_motion_time == 0) {
						msg = 'Motion detected just now';
					}
					else {
						msg = 'Motion detected ' + last_motion_time + ' mins ago';
					}
				}
				else if (motion == 'not_detected') {
					if (last_motion_time == 0) {
						msg = 'No motion detected now';
					}
					else {
						msg = 'No motion detected since ' + last_motion_time + ' mins';
					}
				}
			}

		}

		return msg;
	}


	//EXTERNAL WEBSITE LINKS
	privacy_policy_link = "http://www.kiot.io/privacy-policy";
	legal_link = "http://www.kiot.io/privacy-policy";
	goToPrivacyPolicy() {
		window.open(this.privacy_policy_link, '_system', 'location=yes'); return false;
	  }
	  goToLegal() {
		window.open(this.legal_link, '_system', 'location=yes'); return false;
	  }


}

