import { ErrorCallback, SuccessCallback } from 'ionic-native/dist/es5';
import { AlertService } from './AlertService';
import { Injectable, Inject, forwardRef, Component, NgZone } from '@angular/core';
import { HttpServ } from './HttpServ';
import { Subject } from 'rxjs';
import { PouchService } from './PouchService';
import { LocalStorageService } from './LocalStorageService';
import { LoadingService } from './LoadingService';
import { Events, Platform } from 'ionic-angular';
import { LocalCommService } from './LocalCommunicationServ';
import { Vibration } from '@ionic-native/vibration';
import { NativeAudio } from '@ionic-native/native-audio';
import { Helper } from './Helper';
import { ToastService } from './ToastService';
import { GooglePlus } from '@ionic-native/google-plus';
import {
    MqttMessage,
    MqttModule,
    MqttService
} from 'ngx-mqtt';

@Injectable()
export class DataService {
    vibration;
    constructor( @Inject(forwardRef(() => HttpServ)) public ht: HttpServ, private gplus: GooglePlus, private toast: ToastService, private _mqttService: MqttService, private load1: LoadingService, public localComm: LocalCommService, private platform: Platform, public local: LocalStorageService, private event: Events, private ps: PouchService, private nAudio: NativeAudio, private load: LoadingService, private alert: AlertService, private help: Helper) {
        this.vibration = new Vibration();
        this.platform.ready().then(
            () => {
                this.nAudio.preloadSimple('irTrigger', 'assets/sounds/Click.mp3')
                    .then((data) => {
                        console.log(data);
                        console.log('success audio');
                    })
                    .catch(error => { console.log(error) });
            });


        // this.notLoggedIn=false;
        // let logsub = (loggedin)=>{
        //     if (!loggedin) {
        //         // nav.setRoot('LoginPage');
        //     }
        // }
        // authTracker.loginNotifier.subscribe(logsub);
        // this.ps.initDB('KIOT');
    }
    ping() {
        return this.ht.testInternet();
    }
    playClickAudio() {
        this.nAudio.play('irTrigger')
            .then(data => {
                console.log('playing...');
            }, error => {
                console.log(error);
                this.nAudio.preloadSimple('irTrigger', 'assets/sounds/Click.mp3')
                    .then(() => {
                        this.nAudio.play('irTrigger')
                            .then(() => {
                                console.log('playing...');
                            }).catch(error => {
                                console.log(error);
                            });
                    });
            });
    }
    getMeta(successCallBack, errorCallBack) {
        this.ht.getMeta()
            .subscribe(data => {
                successCallBack(data);
            }, error => {
                errorCallBack(error);
            })
    }
    tryRelogin(successCallback, errorCallBack) {
        let userData = this.local.getUserLoginObject();
        let engaged;
        engaged = this.local.getTemp();
        if(engaged != true){
            this.local.setTemp(false);
        }
        if (userData && engaged == false) {
            if (userData.type && userData.type == 'google') {
                this.local.setTemp(true);
                this.gplus.trySilentLogin({
                    scopes: 'profile email openid',
                    webClientId: '970544757572-aj26nhdjmv44f7f18btq7fjjrakgm080.apps.googleusercontent.com',
                    offline: true
                }).then((userData) => {
                    if (userData && userData.serverAuthCode) {
                        this.doGoogleLogin({ code: userData.serverAuthCode }, data => {
                            this.afterRelogin(true, data, userData);
                            return successCallback(data);
                        }, error => {
                            this.afterRelogin(false);
                        })
                    }
                    else {
                        this.afterRelogin(false);
                    }
                }).catch(err => {
                    this.afterRelogin(false);
                })

            }
            else {
                this.postLogin(userData, data => {
                    this.afterRelogin(true, data, userData);
                    return successCallback(data);
                }, error => {
                    this.afterRelogin(false);
                    // this.notLoggedIn= true;
                    //this.local.setToken(null);
                    // window.location.reload();
                    // this.appCtrl.getRootNav().push(LoginPage);
                    // HelperPage.changePage(LoginPage);
                    // navCtrl.push('LoginPage');
                    // return errorCallBack();
                }, true)
            }
        }



    }

    afterRelogin(success, data?, userData?) {
        if (success == true) {
            if (!data.token || data.token == undefined) {

            }
            // this.token = data.token;
            this.local.setToken(data.token);
            this.local.setUser(data.user);
            this.local.setUserLoginObject(userData);
            // console.log(JSON.parse(this.localService.getUser()));
            console.log(data.user.default_home);
            this.local.setDefaultHome(data.user.default_home);
            //this.local.setCurrentHome(this.local.getDefaultHome());
        }
        if (success == false) {
            this.local.setToken(null);
            this.event.publish('relogin',false);
            //window.location.reload();
        }
    }

    getDataFromDb(url1, url2, prefix, successCallBack, errorCallBack, dont_showLoader?, get_from_db?) {
        //dont_showLoader ? dont_showLoader : false;
        dont_showLoader = false;
        get_from_db = (get_from_db === undefined) ? false : get_from_db;



        if (this.local.getInternetStatus() == "online") {
            this.getData(url1 + url2,
                (data) => {
                    console.log(data);
                    if (data.switchAppliances) {
                        this.postDataToDb(url2, prefix, data.switchAppliances)
                            .then(data2 => {
                                console.log(data2);
                                let someArray2 = []; someArray2.push(data.devicesInfo);
                                this.postDataToDb(url2, 'switches_deviceMeta', someArray2)
                                    .then(() => { console.log('meta shift okay'); get_from_db = false; successCallBack(data); })
                                    .catch(err => { console.log(err) });
                            })
                            .catch(
                            err => {
                                errorCallBack(err);
                                console.log("an error is occuring");
                                console.log(err);
                            });

                    }
                    else if (data.appliances) {
                        this.postDataToDb(url2, prefix, data.appliances)
                            .then(data2 => {
                                console.log(data2);
                                let someArray2 = []; someArray2.push(data.devices);
                                this.postDataToDb(url2, 'remote_meta2', someArray2)
                                    .then(() => { console.log('meta2 shift okay'); successCallBack(data); })
                                    .catch(err => { console.log(err) });
                            })
                            .catch(
                            err => {
                                errorCallBack(err);
                                console.log("an error is occuring");
                                console.log(err);
                            });

                    }
                    else {
                        this.postDataToDb(url2, prefix, data)
                            .then(data2 => {
                                console.log(data2);
                                successCallBack(data);
                            })
                            .catch(
                            err => {
                                errorCallBack();
                                console.log("an error is occuring");
                                console.log(err);
                            });
                    }

                }, error => {
                    console.log(error);
                }, dont_showLoader);
            console.log("I am passed");
        }

        else {
            if (prefix == 'switches') {
                this.ps.getADoc(prefix, url2).then(
                    (data1) => {
                        this.ps.getADoc('switches_deviceMeta', url2)
                            .then(data2 => {
                                console.log(data1);
                                console.log(data2);
                                successCallBack({
                                    switchAppliances: data1,
                                    devicesInfo: data2[0]
                                })
                            }).catch(error => {
                                return errorCallBack(error);
                            })

                    },
                    (error) => {
                        return errorCallBack(error);
                    });
            }
            else if (prefix == 'remote_meta') {
                this.ps.getADoc(prefix, url2).then(
                    (data1) => {
                        this.ps.getADoc('remote_meta2', url2)
                            .then(data2 => {
                                console.log(data1);
                                console.log(data2);
                                successCallBack({
                                    appliances: data1,
                                    devices: data2[0]
                                })
                            }).catch(error => {
                                return errorCallBack(error);
                            })

                    },
                    (error) => {
                        return errorCallBack(error);
                    });
            }
            else {
                this.ps.getADoc(prefix, url2).then(
                    (data) => {
                        successCallBack(data);
                    },
                    (error) => {
                        errorCallBack();
                    });
            }
        }

        if (get_from_db) {
            if (prefix == 'switches') {
                this.ps.getADoc(prefix, url2).then(
                    (data1) => {
                        this.ps.getADoc('switches_deviceMeta', url2)
                            .then(data2 => {
                                console.log(data1);
                                console.log(data2);
                                if (data1 && data1.length) {
                                    successCallBack({
                                        switchAppliances: data1,
                                        devicesInfo: data2[0]
                                    })
                                }
                            }).catch(error => {
                                //return errorCallBack(error);
                            })

                    },
                    (error) => {
                        // return errorCallBack(error);
                    });
            }
            else if (prefix == 'remote_meta') {
                this.ps.getADoc(prefix, url2).then(
                    (data1) => {
                        this.ps.getADoc('remote_meta2', url2)
                            .then(data2 => {
                                console.log(data1);
                                console.log(data2);
                                if (data1 && data1.length) {
                                    successCallBack({
                                        appliances: data1,
                                        devices: data2[0]
                                    })
                                }
                            }).catch(error => {
                                return errorCallBack(error);
                            })

                    },
                    (error) => {
                        return errorCallBack(error);
                    });
            }

        }
    }
    postDataToDb(url, prefix, data) {
        let promiseArray = [];
        if (Array.isArray(data) == true) {
            this.ps.removeAll(prefix, url)
                .then(data5 => {
                    console.log(data5);
                    for (let i of data) {
                        if (i.__v || i.__v == 0) {
                            delete i['__v'];
                        }
                        promiseArray.push(this.ps.add(prefix, url, i, false));
                    }
                }
                ).catch(
                err => { console.log(err); return Promise.reject(err); }
                );
            /*
             let dataArray=[{dummy:"value"}];
                  dataArray.splice(0,1); 
             
           let j=0;
           for(let i of data){
               //this.ps.update(prefix,url,i);
               console.log('loop in postdatatodb');
                   this.ps.check(prefix,url,i)
                   .then( data2=>{
                         //  console.log(data2);
                         i._rev=data2._rev;
                         // data2=i;
                          //dataArray.push(data2);
                           // console.log(i._rev);
                          // dataArray[j]=i;
                        //  dataArray.push(i);
                          promiseArray.push(this.ps.add(prefix,url,i,data2._rev));
                        //  this.ps.add(prefix,url,i,data2._rev).then(data=>console.log(data));
                   }).catch(err=>{
                           if(err.name === 'not_found'){
                              // dataArray.push(i);
                              // dataArray[j]=i;
                             promiseArray.push(this.ps.add(prefix,url,i,false)); 
                            
                           }
                   })
                   
               //promiseArray.push (this.ps.update(prefix,url,i));
               j++;
              // console.log(data);
           }*/
            return Promise.all(promiseArray);
        }
        else {
            let emptyPromise = [];
            emptyPromise.push(Promise.resolve(1));
            return Promise.all(emptyPromise);
        }/*
        .then(
            (data)=>{
                successCallBack(data);
            },
            (error)=>{
                errorCallBack(error);
            });*/
        //return Promise.all(promiseArray);
    }

    getData(url, successCallBack, errorCallBack, showLoadingBar?: boolean, no_update?, dont_handle_error?: boolean) {
        if (typeof dont_handle_error == undefined) {
            dont_handle_error = false;
        }
        if (showLoadingBar != false) this.load.showLoading('loading');
        this.ht.getMe(url)
            .subscribe((data33) => {
                if (showLoadingBar != false) this.load.dismiss();
                //console.log(data33);
                successCallBack(data33);
                if (this.local.getInternetStatus() == 'offline') {
                    this.local.setInternetStatus("online");
                    this.event.publish('kiot_internet', 'online');
                }
            },
            (error) => {
                if ((error.status == 0 || error.name == 'TimeoutError') && !no_update) {
                    if (this.local.getInternetStatus() == 'online') {
                        this.local.setInternetStatus("offline");
                        this.event.publish('kiot_internet', 'offline');
                        dont_handle_error = true;
                    }
                    if (showLoadingBar != false) this.load.dismiss();
                }
                if (dont_handle_error) {
                    if (showLoadingBar) this.load.dismiss();
                }
                if (!dont_handle_error && error.status != 0) {
                    if (showLoadingBar != false) this.load.dismiss();
                    if (error.status == 401 || error.status == 403) {

                        console.log('yoooo');
                        this.tryRelogin(() => {
                            // successCallback
                            this.ht.getMe(url)
                                .subscribe((data) => {
                                    this.local.setInternetStatus("online");
                                    return successCallBack(data);
                                }, error => {
                                    // Error Again. Sorry man i can't do anything here
                                    this.alert.showAlert("Oops", "An unknown error occured, PLease restart the App.");
                                    return errorCallBack(error);
                                });
                        }, () => {
                            //errorCallBack
                            //if (showLoadingBar != false) this.load.dismiss();
                            this.alert.showAlert("Oops", "An unknown error occured, PLease restart the App.");
                            return errorCallBack(error);
                        });

                    } else if (error.status == 400 || error.status == 500) {
                        console.log(error);
                        try {
                            let x;
                            x = JSON.parse(error._body);
                            let msg;
                            msg = JSON.stringify(x.message);
                            this.alert.showAlert(error.statusText || "Oops!", msg || x.err || "Unknown Error Occured");
                        }
                        catch (err) {
                            console.log(err);
                        }

                        //if (showLoadingBar != false) this.load.dismiss();
                        return errorCallBack(error);
                    }
                    else if (error.name != "TimeoutError") {
                        this.alert.showAlert("Oops!", error.message || error.err || "Unknown Error Occured");
                        //if (showLoadingBar != false) this.load.dismiss();
                        return errorCallBack(error);
                    }
                } else {
                    if (showLoadingBar != false) this.load.dismiss();
                    return errorCallBack(error);
                }


            });
    }


    postData(url, jsonObj, successCallBack, errorCallBack, showLoader?: boolean, dont_handle_error?: boolean, no_update?) {
        if (typeof dont_handle_error == undefined) {
            dont_handle_error = false;
        }
        if (!showLoader) this.load.showLoading('loading');
        this.ht.postMe(url, jsonObj)
            .subscribe((data) => {
                if (!showLoader) this.load.dismiss();
                if (this.local.getInternetStatus() == 'offline') {
                    this.local.setInternetStatus("online");
                    this.event.publish('kiot_internet', 'online');
                }
                return successCallBack(data);

            }, (error) => {

                if ((error.status == 0 || error.name == 'TimeoutError') && !no_update) {
                    if (this.local.getInternetStatus() == 'online') {
                        this.local.setInternetStatus("offline");
                        this.event.publish('kiot_internet', 'offline');
                        dont_handle_error = true;
                    }
                    if (!showLoader) this.load.dismiss();
                }
                if (dont_handle_error) {
                    if (!showLoader) this.load.dismiss();
                    return errorCallBack(error);
                }
                if (!dont_handle_error && error.status != 0) {
                    if (error.status == 401 || error.status == 403) {
                        console.log('yoooo');
                        this.tryRelogin(() => {
                            // successCallback
                            this.ht.postMe(url, jsonObj)
                                .subscribe((data) => {
                                    this.load.dismiss();
                                    this.local.setInternetStatus("online");
                                    return successCallBack(data);

                                });
                        }, () => {
                            //errorCallBack
                            if (!showLoader != false) this.load.dismiss();
                            return errorCallBack(error);
                        });

                    } else if (error.status == 400 || error.status == 500) {
                        console.log(error);
                        try {
                            let x;
                            x = JSON.parse(error._body);
                            let msg;
                            msg = JSON.stringify(x.message);
                            this.alert.showAlert(error.statusText || "Oops!", msg || x.err || "Unknown Error Occured");
                        }
                        catch (err) {
                            console.log(err);
                        }
                        //this.alert.showAlert("Oops!", error.message || error.err || "Unknown Error Occured");
                        if (showLoader != false) this.load.dismiss();
                        return errorCallBack(error);
                    }
                    else if (error.name != "TimeoutError") {
                        this.alert.showAlert("Oops!", error.message || error.err || "Unknown Error Occured");
                        if (showLoader != false) this.load.dismiss();
                        return errorCallBack(error);
                    }

                }
                else {
                    if (!showLoader) this.load.dismiss();
                    return errorCallBack(error);
                }

            });
    }
    putData(url, jsonObj, successCallBack, errorCallBack, dont_handle_error?: boolean) {
        console.log(jsonObj);
        this.load.showLoading('loading');
        this.ht.putMe(url, jsonObj)
            .subscribe((data) => {
                this.load.dismiss();
                successCallBack(data);
                this.local.setInternetStatus("online");
            }, (error) => {
                if ((error.status == 0 || error.name == 'TimeoutError')) {
                    this.local.setInternetStatus("offline");
                    dont_handle_error = true;
                    this.load.dismiss();
                }
                else if (error.status == 401) {
                    this.tryRelogin(() => {
                        // successCallback
                        this.ht.putMe(url, jsonObj)
                            .subscribe((data) => {
                                this.load.dismiss();
                                this.local.setInternetStatus("online");
                                return successCallBack(data);

                            });
                    }, () => {
                        //errorCallBack
                        this.load.dismiss();
                        return errorCallBack(error);
                    });

                }
                else {
                    this.load.dismiss();
                    return errorCallBack(error);
                }
            });
    }




    //get REQUESTS go here
    getUser(successCallBack, errorCallBack) {
        this.getData('users/me',
            data => {
                return successCallBack(data);
            }, error => {
                return errorCallBack(error);
            })
    }
    getUserHomes(successCallBack, errorCallBack, dont_showLoader?) {
        let def = localStorage.getItem('default_home');
        this.getData('homes/myhomes?append=wifi_details,prim_user,other_users', (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        }, dont_showLoader);
    }
    getAHomeBasic(home_id, successCallBack, errorCallBack, dont_showLoader?) {
        this.getData('homes/myhomes?id=' + home_id, (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        }, dont_showLoader);
    }
    getUserHomeIdsOnly(successCallBack, errorCallBack, showLoader) {
        this.getData('homes/myhomes?only_ids=true', (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        }, showLoader);
    }
    getUserRooms(successCallBack, errorCallBack) {
        let def = localStorage.getItem('default_home');
        this.getData('rooms/allmyrooms', (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        });
    }
    getHomesRooms(str, successCallBack, errorCallBack) {
        let def = localStorage.getItem('default_home');
        this.getData('rooms/myrooms?home=' + str, (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        });
    }
    getJoinRequests(successCallBack, errorCallBack) {
        this.getData('homes/my_home_join_requests', (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        });
    }
    getSearchHome(str, successCallBack, errorCallBack, showLoader, handle401) {
        this.getData('homes/search_home_for_join?home=' + str, (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        }, showLoader, false, handle401);
    }
    getRooms(str, successCallBack, errorCallBack, dont_showLoader?) {
        this.getDataFromDb('rooms/home_rooms?home=', str, 'room', (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        }, dont_showLoader);
    }
    getSboards(home, successCallBack, errorCallBack, dont_showLoader?, append?) {
        this.getData('sboards/mysboards?home=' + home + '&append=' + append, (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        }, dont_showLoader);
    }
    getSboardsFromDb(str, successCallBack, errorCallBack) {
        this.getDataFromDb('sboards/mysboards?home=', str, 'room', (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        });
    }
    getSwitches(str, append, successCallBack, errorCallBack) {
        this.getData('sboards/mysboards?home=' + str + '&append=' + append, (data) => {

            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        });
    }
    getSwitchesInRoom(str, successCallBack, errorCallBack, from_db) {
        this.getDataFromDb('sboards/byroom?room=', str, 'switches', (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        }, true, from_db);
    }
    getSwitchHistory(switch_id, successCallBack, errorCallBack) {
        this.getData('sboards/history?switch_id=' + switch_id, (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        });
    }
    getIrAppliances(str, successCallBack, errorCallBack, dont_showLoader?, from_db?) {
        this.getDataFromDb('ir_appliances/myapps?append=paired_sb,room_id,paired_appliance&home=', str, 'remote_meta', (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        }, dont_showLoader, from_db);
    }
    getBrands(str, successCallBack, errorCallBack) {
        this.getData('appliances/type/' + str, (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        });
    }
    getCableProviders(searchStr, successCallBack, errorCallBack) {
        this.getData('epg/searchProviders/?searchString=' + searchStr,
            data => {
                return successCallBack(data);
            }, error => {
                return errorCallBack(error);
            }, false)
    }
    getTestCodeSets(type, successCallBack, errorCallBack, brand?, stb_provider?) {
        let url;
        url = brand ? 'appliances/test_codeset?brand=' + brand : 'appliances/test_codeset?stb_provider=' + stb_provider;
        this.getData(url + '&type=' + type, (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        });
    }
    getFullCodeSets(str1, successCallBack, errorCallBack, showLoader?) {
        this.getData('codesets?id=' + str1,
            (data) => {
                // this.ps.initDB('KIOT')
                this.ps.downloadRemote(data[0]);
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            }, showLoader);
    }
    getRemoteFromDb(str1, successCallBack, errorCallBack, showLoader?) {
        console.log(str1);
        let x = showLoader ? showLoader : true;
        this.ps.getADoc('codeset', str1)
            .then(data => {
                // this.ps.initDB('KIOT')
                if (data) {
                    if (data.length == 0) {
                        console.log('yoyo');
                        if (this.local.getInternetStatus() == 'offline') {
                            return errorCallBack({
                                status: 404,
                                message: 'Remote not found in database, please connect to internet for remote'
                            })
                        }
                        else {
                            this.getFullCodeSets(str1,
                                (data) => {
                                    return successCallBack(data);
                                },
                                (error) => {
                                    return errorCallBack(error);
                                }, x);
                        }
                    }
                    else {
                        return successCallBack(data);
                    }
                }

            }).catch(
            error => {
                return errorCallBack(error);
            });
    }
    getGeneralAppliances(successCallBack, errorCallBack) {
        this.getData('sboards/general_appliances',
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            });
    }
    getChannelGroupData(successCallBack, errorCallBack) {

        //this.load1.showLoading('yo');
        this.ps.getADoc('channel', '')
            .then(
            data => {

                if (!data || (data && data.length != 4)) {
                    console.log(data);
                    this.getData('epg/channelMeta?limit=1000&offset=2',
                        (data) => {
                            //this.load1.dismiss();
                            let channelData = {};
                            let promiseArray = [];
                            let dataToSendAsArray = [];
                            promiseArray.push(this.ps.add('channel', 'group', { channelGroups: data.channelGroups }, false));
                            promiseArray.push(this.ps.add('channel', 'Langs', data.channelLangs, false));
                            promiseArray.push(this.ps.add('channel', 'Types', data.channelTypes, false));
                            for (let j of data.channels) {
                                channelData[j.id] = j;
                            }
                            promiseArray.push(this.ps.add('channel', 'names', { channels: channelData }, false));
                            // console.log(channelData);

                            Promise.all(promiseArray).then(
                                doc => {
                                    //this.load1.dismiss();
                                    dataToSendAsArray.push(data.channelLangs);
                                    dataToSendAsArray.push(data.channelTypes);
                                    dataToSendAsArray.push({ channelGroups: data.channelGroups });
                                    dataToSendAsArray.push({ channels: channelData });
                                    return successCallBack(dataToSendAsArray);
                                }
                            ).catch(
                                err => {
                                    //this.load1.dismiss();
                                    return errorCallBack(err);
                                })

                        }, (error) => {
                            //this.load1.dismiss();
                            return errorCallBack(error);
                        }, false);
                }
                else {
                    //this.load1.dismiss();
                    console.log("Success 2");
                    return successCallBack(data);
                }
            }

            ).catch(
            err => {

                if (err.name == "not_found") {
                    this.getData('epg/channelGroups?limit=10&offset=2',
                        (data) => {
                            //this.load1.dismiss();
                            return successCallBack(data);
                        }, (error) => {
                            //this.load1.dismiss();
                            return errorCallBack(error);
                        });
                }
                console.log("Error 4");
                //this.load1.dismiss();
                return errorCallBack(err);

            }
            );
    }
    getPrograms(limit, offset, languages, types, showLoader, successCallBack, errorCallBack) {
        if (languages == null) languages = '';
        this.getData('epg/programs?limit=' + limit + '&offset=' + offset + '&languages=' + languages + '&programTypes=' + types + '&starttime=' + Date.now().toString() + '&queryType=current&sortby=popularity',
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            }, showLoader);
    }
    getProgramsForLater(limit, offset, languages, types, showLoader, successCallBack, errorCallBack) {
        if (languages == null) languages = '';
        this.getData('epg/programs?limit=' + limit + '&offset=' + offset + '&languages=' + languages + '&programTypes=' + types + '&starttime=' + Date.now().toString() + '&queryType=later',
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            }, showLoader);
    }
    getProgramDetails(upid, programid, mainprogramid, successCallBack, errorCallBack) {
        if (upid) {
            this.getData('epg/programs?upid=' + upid,
                (data) => {
                    return successCallBack(data);
                }, (error) => {
                    return errorCallBack(error);
                });
        }
        else {
            mainprogramid = mainprogramid ? mainprogramid : '';
            this.getData('epg/programs?programid=' + programid + '&mainprogramid=' + mainprogramid,
                (data) => {
                    return successCallBack(data);
                }, (error) => {
                    return errorCallBack(error);
                });
        }


    }
    getFavChannels(successCallBack, errorCallBack, showLoadingBar) {
        this.getData('users/fav_channels',
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            }, showLoadingBar);
    }
    getFavPrograms(type, successCallBack, errorCallBack, showLoadingBar) {
        this.getData('epg/program_fav?type=' + type,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            }, showLoadingBar);
    }
    getChannelPrograms(limit, offset, channelid, showLoader, successCallBack, errorCallBack) {
        this.getData('epg/programs?limit=' + limit + '&offset=' + offset + '&channelid=' + channelid + '&starttime=' + Date.now().toString() + '&queryType=all&sortby=starttime',
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            }, showLoader);
    }
    getCurrentProgramsOnFav(successCallBack, errorCallBack) {
        this.getData('epg/programs_playing_favch',
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            }, false);
    }

    getProviderMap(provider_id, successCallBack, errorCallBack, showLoadingBar) {

        this.ps.getADoc('provider', provider_id)
            .then(data => {
                if (data.length == 0) {
                    this.getData('epg/provider_channel_map?provider_id=' + provider_id,
                        data => {
                            let promiseArray = [];
                            promiseArray.push(this.ps.add('provider', provider_id, data.data_channels_map, false));
                            let rev = { _id: "xrev" };
                            for (var key in data.data_channels_map) {
                                rev[data.data_channels_map[key]] = key;
                            }
                            promiseArray.push(this.ps.add('provider', provider_id, rev, false));
                            Promise.all(promiseArray).then(() => {
                                let someArray = [];
                                someArray.push(data.data_channels_map);
                                someArray.push(rev);
                                return successCallBack(someArray);
                            })
                                .catch(error => {
                                    return errorCallBack(error);
                                })
                        },
                        error => {
                            return errorCallBack(error);
                        }, showLoadingBar);
                }
                else {
                    return successCallBack(data);
                }
            })
            .catch(error => {
                return errorCallBack(error);
            })

    }
    getDeviceDetails(deviceid, successCallBack, errorCallBack) {
        this.getData('kiotdevice/device_config?device_id=' + deviceid,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            });
    }
    getSearchPrograms(searchquery, successCallBack, errorCallBack, showLoader) {
        this.getData('epg/searchProgrammes/?searchString=' + searchquery,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            }, showLoader);
    }
    getReminders(successCallBack, errorCallBack, showLoader) {
        this.getData('epg/my_reminders',
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            }, showLoader);
    }
    getSchedulesOfASwitch(switch_id, successCallBack, errorCallBack) {
        this.getData('schedules/get?switch_appliance_id=' + switch_id,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            });
    }
    getAllScenes(home_id, successCallBack, errorCallBack, dont_showLoader?) {
        this.getData('scenes/get?homeId=' + home_id,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            }, !dont_showLoader)
    }
    getAcTimer(ir_appliance_id, successCallBack, errorCallBack) {
        this.getData('schedules/get?ir_appliance_id=' + ir_appliance_id,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }
    getDeviceAvailableOnline(successCallBack, errorCallBack, showLoadingBar) {
        this.getData('deviceevents/is_online',
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            }, showLoadingBar, true, true);


    }

    getNotificationTypes(successCallback, errorCallBack) {
        this.getData('users/get_notification_types',
            data => {
                return successCallback(data);
            },
            error => {
                return errorCallBack(error)
            });
    }
    getAllNotifications(offset, limit, successCallback, errorCallBack, showLoader?) {
        this.getData('notifications/get_all?offset=' + offset + '&limit=' + limit,
            data => {
                return successCallback(data);
            },
            error => {
                return errorCallBack(error)
            }, showLoader);
    }
    getUnseenNotifications(successCallback, errorCallBack) {
        this.getData('notifications/get_unseen_notif_count',
            data => {
                return successCallback(data);
            },
            error => {
                return errorCallBack(error)
            }, false);
    }
    getNotificationStatus(successCallback, errorCallBack, showLoader) {
        this.getData('users/get_show_notifications',
            data => {
                return successCallback(data);
            },
            error => {
                return errorCallBack(error)
            }, showLoader);
    }
    getUserNotificationTypes(successCallback, errorCallBack, showLoader) {
        this.getData('users/get_notification_pref',
            data => {
                return successCallback(data);
            },
            error => {
                return errorCallBack(error)
            }, showLoader);
    }
    getCustomRemote(home_id, successCallback, errorCallBack, showLoader) {
        this.getData('ir_appliances/customirremote?home=' + home_id,
            data => {
                return successCallback(data);
            },
            error => {
                return errorCallBack(error)
            }, false);
    }
    getCustomRemoteinDetail(home_id, remote_id, successCallback, errorCallBack, showLoader) {
        this.getData('ir_appliances/customirremote?home=' + home_id + '&id=' + remote_id,
            data => {
                return successCallback(data);
            },
            error => {
                return errorCallBack(error)
            }, showLoader);
    }
    getSwitchesDetails(switch_array, room_array, successCallback, errorCallBack) {

        this.getData('sboards/basic_switch_details?rooms=' + this.help.arrayToCommaSeparatedString(room_array) + '&switch_appliances=' + this.help.arrayToCommaSeparatedString(switch_array),
            data => {
                return successCallback(data);
            },
            error => {
                return errorCallBack(error)
            });
    }
    getUserShortcuts(home_id, successCallback, errorCallBack, showLoader) {
        this.getData('usershortcuts?home=' + home_id,
            data => {
                return successCallback(data);
            },
            error => {
                return errorCallBack(error)
            }, showLoader);
    }
    getSearchChannels(search_string, successCallback, errorCallBack, showLoader) {
        this.getData('epg/searchChannel?searchString=' + search_string,
            data => {
                return successCallback(data);
            },
            error => {
                return errorCallBack(error)
            }, showLoader);
    }
    getAllWorkflows(workflow_group, successCallback, errorCallBack) {
        this.getData('workflows?workflow_group_id=' + workflow_group,
            data => {
                return successCallback(data);
            },
            error => {
                return errorCallBack(error)
            });
    }
    getAllWorkflowsOnADevice(device_id, successCallback, errorCallBack) {
        this.getData('workflows?device_id=' + device_id,
            data => {
                return successCallback(data);
            },
            error => {
                return errorCallBack(error)
            });
    }
    getAllWorkflowGroups(home, successCallback, errorCallBack) {
        this.getData('workflows/get_groups?home_id=' + home,
            data => {
                return successCallback(data);
            },
            error => {
                return errorCallBack(error)
            });
    }
    getAllTickets(successCallback, errorCallBack) {
        this.getData('opsupport',
            data => {
                return successCallback(data);
            },
            error => {
                return errorCallBack(error)
            });
    }




    //post REQUESTS go here

    postLogin(obj, successCallBack, errorCallBack, loader?) {
        this.postData('user/authenticate', obj, (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        }, loader, true)
    }
    doGoogleLogin(obj, SuccessCallback, ErrorCallback) {
        let uri = 'user/authenticate/google';
        return this.postData(uri, obj, SuccessCallback, ErrorCallback);
    }
    postRequestSignUp(obj, successCallBack, errorCallBack) {
        this.postData('user/authenticate/request_registration', obj, (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        }, false, true)
    }
    postSignUp(obj, successCallBack, errorCallBack) {
        this.postData('user/authenticate/register', obj, (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        }, false, true)
    }

    postRequestOtp(obj, successCallBack, errorCallBack) {
        this.postData('user/authenticate/forgot_password', obj, (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        })
    }
    postUpdatePass(obj, successCallBack, errorCallBack) {
        this.postData('users/update_password', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }
    postResetPass(obj, successCallBack, errorCallBack) {
        this.postData('user/authenticate/reset_password', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }
    postCheckExisting(obj, successCallBack, errorCallBack) {
        this.postData('homes/check_existing', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            }, true)
    }
    postCheckExistingDevice(obj, successCallBack, errorCallBack) {
        this.postData('sboards/exists', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }
    postCreateHome(obj, successCallBack, errorCallBack) {
        this.postData('homes/new', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }
    postEditHome(obj, successCallBack, errorCallBack) {
        this.postData('homes/myhomes', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }
    postDeleteHome(obj, successCallBack, errorCallBack, showLoader, dont_handle_error) {
        this.postData('homes/myhomes/delete', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            }, showLoader, dont_handle_error)
    }
    postCreateWifiRouter(obj, successCallBack, errorCallBack) {
        this.postData('homes/wifi', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }
    postEditWifiRouter(obj, successCallBack, errorCallBack) {
        this.postData('homes/wifi', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }
    postDeleteWifiRouter(obj, successCallBack, errorCallBack) {
        this.postData('homes/wifi', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }
    postJoinRequest(obj, successCallBack, errorCallBack) {
        this.postData('homes/request_join', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }
    postApproveJoinRequest(obj, successCallBack, errorCallBack, dont_showLoader?) {
        this.postData('homes/my_home_join_requests', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            }, !dont_showLoader);
    }
    postRemoveUser(obj, successCallBack, errorCallBack, showLoader, not_handle401) {
        this.postData('homes/remove_user', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            }, showLoader, not_handle401);
    }
    postCreateRoom(obj, successCallBack, errorCallBack) {
        this.postData('rooms/new', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            });
    }
    postEditRoom(obj, successCallBack, errorCallBack) {
        this.postData('rooms/myroom', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            });
    }
    postDeleteRoom(obj, successCallBack, errorCallBack) {
        this.postData('rooms/myroom/delete', obj,
            (data) => {
                // this.ps.removeADoc(room,)
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            });
    }
    postCreateSboard(obj, successCallBack, errorCallBack) {
        this.postData('sboards/new', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            }, true);
    }
    postEditSwitchBoard(obj, successCallBack, errorCallBack) {
        this.postData('sboards/sboard', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            });
    }
    postDeleteSwitchBoard(obj, successCallBack, errorCallBack) {
        this.postData('sboards/sboard/delete', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            });
    }
    postCreateSwitch(obj, successCallBack, errorCallBack) {
        this.postData('sboards/addswitch', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            });
    }
    postEditSwitch(obj, successCallBack, errorCallBack) {
        this.postData('sboards/switch', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            });
    }
    postCreateIrApp(obj, successCallBack, errorCallBack) {
        this.postData('ir_appliances/new', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }
    postEditIrApp(obj, successCallBack, errorCallBack) {
        this.postData('ir_appliances/myapps', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }
    postDeleteIrApp(obj, successCallBack, errorCallBack) {
        this.postData('ir_appliances/myapps/delete', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }
    postTriggerIr(obj, successCallBack, errorCallBack, showLoader?: boolean) {
        if (this.platform.is('android')) {
            this.vibration.vibrate(30);
        }
        this.playClickAudio();
        this.postData('deviceevents/ircustom', obj,
            (data) => {
                return successCallBack(data);

            }, (error) => {
                return errorCallBack(error);
            }, !showLoader)
    }
    postTriggerSwitch(obj, successCallBack, errorCallBack) {
        this.playClickAudio();
        if (this.local.getInternetStatus() == 'online') {
            this.postData('deviceevents/switch', obj,
                (data) => {
                    return successCallBack(data);
                }, (error) => {
                    return errorCallBack(error);
                }, true);
        }
        else if (this.local.getInternetStatus() == 'offline') {
            console.log('offline comm')
            //call switch LOCALLY HERE-----------------------------
            this.localComm.triggerSwitch(obj,
                data => {
                    successCallBack();
                }, error => {
                    errorCallBack();
                })
        }
    }
    postTurnOffRoom(obj, successCallBack, errorCallBack) {
        this.postData('deviceevents/turnoff_room', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }
    postEditFavChannels(obj, successCallBack, errorCallBack) {
        this.postData('users/fav_channels', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            });
    }
    postEditFavPrograms(obj, successCallBack, errorCallBack) {
        this.postData('epg/program_fav', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            });
    }
    postSwitchToChannel(obj, successCallBack, errorCallBack) {
        if (this.platform.is('android')) {
            this.vibration.vibrate(30);
        }

        this.playClickAudio();
        this.postData('deviceevents/switch_to_channel', obj,
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            }, true)
    }
    postSwitchToChannelFromNotification(obj, successCallBack, errorCallBack) {
        if (this.platform.is('android')) {
            this.vibration.vibrate(30);
        }

        this.playClickAudio();
        this.postData('epg/watch_now', obj,
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            })
    }
    postSetReminder(obj, successCallBack, errorCallBack) {
        obj.action = "add";
        this.postData('epg/reminder', obj,
            data => {
                this.toast.bottomToast_Short('Reminder added for ' + obj.programname);
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            }, true)
    }
    postUnsetReminder(obj, successCallBack, errorCallBack) {
        obj.action = "remove";
        this.postData('epg/reminder', obj,
            data => {
                this.toast.bottomToast_Short('Reminder removed');
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            }, true)
    }
    postCreateSchedule(obj, successCallBack, errorCallBack) {
        this.postData('schedules/create', obj,
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            })
    }
    postEditSchedule(obj, successCallBack, errorCallBack) {
        this.postData('schedules/edit', obj,
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            })
    }
    postTriggerActiveSchedule(obj, successCallBack, errorCallBack) {
        this.postData('schedules/triggerActive', obj,
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            })
    }
    postDeleteSchedule(obj, successCallBack, errorCallBack) {
        this.postData('schedules/delete', obj,
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            })
    }
    postCreateScene(scheduled, obj, successCallBack, errorCallBack) {
        let url = scheduled ? 'scheduleScenes/create' : 'scenes/create'
        this.postData(url, obj,
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            })
    }
    postEditScene(scheduled, obj, successCallBack, errorCallBack) {
        let url = scheduled ? 'scheduleScenes/edit' : 'scenes/edit'
        this.postData(url, obj,
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            })
    }
    postDeleteScene(scheduled, obj, successCallBack, errorCallBack) {
        let url = scheduled ? 'scheduleScenes/delete' : 'scenes/delete'
        this.postData(url, obj,
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            })
    }
    postTriggerScene(scheduled, obj, successCallBack, errorCallBack) {
        let url = scheduled ? 'scheduleScenes/triggerActive' : 'scenes/trigger'
        this.postData(url, obj,
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            })
    }
    postOfflineSync(obj, successCallBack, errorCallBack) {
        if (obj.actions.length) {
            this.postData('deviceevents/offline_sync', obj,
                data => {
                    return successCallBack(data);
                },
                error => {
                    return errorCallBack(error);
                }, true, false, true)
        }
    }
    postCreateTimer(obj, successCallBack, errorCallBack) {
        this.postData('schedules/create', obj,
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            });
    }
    postDeleteTimer(obj, successCallBack, errorCallBack) {
        this.postData('schedules/delete', obj,
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            });
    }
    postLogout(obj, successCallBack, errorCallBack, showLoader, not_handle401) {
        this.postData('users/logout', obj,
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            }, !showLoader, not_handle401);
    }
    postDisableNotifications(successCallBack, errorCallBack, showLoader) {
        this.postData('users/stop_all_notifications', {},
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            }, !showLoader);
    }
    postEnableNotifications(successCallBack, errorCallBack, showLoader) {
        this.postData('users/start_all_notifications', {},
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            }, !showLoader);
    }
    postSetUserNotificationTypes(obj, successCallBack, errorCallBack) {
        this.postData('users/set_notification_pref', obj,
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            });
    }
    postSetNotificationSeen(obj, successCallBack, errorCallBack) {
        this.postData('notifications/change_notif_seen', obj,
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            }, !false);
    }
    postDismissSchedule(obj, successCallBack, errorCallBack) {
        this.postData('schedules/dismiss_schedule', obj,
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            }, !false);
    }
    postCreateCustomRemote(obj, successCallBack, errorCallBack) {
        this.postData('ir_appliances/customirremote/new', obj,
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            }, !false);
    }

    postEditCustomRemote(obj, successCallBack, errorCallBack) {
        this.postData('ir_appliances/customirremote', obj,
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            }, !false);
    }
    postCreateUserShortcuts(obj, successCallBack, errorCallBack) {
        this.postData('usershortcuts/new', obj,
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            });
    }

    postDeleteUserShortcuts(obj, successCallBack, errorCallBack) {
        this.postData('usershortcuts/delete', obj,
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            });
    }
    postCreateWorkflow(obj, successCallBack, errorCallBack) {
        this.postData('workflows/new', obj,
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            });
    }
    postEditWorkflow(obj, successCallBack, errorCallBack) {
        this.postData('workflows', obj,
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            });
    }
    postDeleteWorkflow(obj, successCallBack, errorCallBack) {
        this.postData('workflows/delete', obj,
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            });
    }
    postCreateWorkflowGroup(obj, successCallBack, errorCallBack) {
        this.postData('workflows/new_group', obj,
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            });
    }
    postEditWorkflowGroup(obj, successCallBack, errorCallBack) {
        this.postData('workflows/edit_group', obj,
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            });
    }
    postDeleteWorkflowGroup(obj, successCallBack, errorCallBack) {
        this.postData('workflows/delete_group', obj,
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            });
    }
    postTriggerLockHome(obj, successCallBack, errorCallBack) {
        this.postData('homes/trigger_away_mode', obj,
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            });
    }
    postGetPowerData(obj, successCallBack, errorCallBack) {
        if (obj && obj.device_id) {
            let str = ''
            let c = 0
            for (let i of obj.device_id) {
                c++;
                str += i;
                if (c < obj.device_id.length) {
                    str += ',';
                }
            }
            obj.device_id = str;
            if (obj.device_id) {
                this.postData('sboards/power_data', obj,
                    data => {
                        return successCallBack(data);
                    },
                    error => {
                        return errorCallBack(error);
                    }, !false);
            }
        }
    }
    postGetEnergyData(obj, successCallBack, errorCallBack) {
        if (obj && obj.device_id) {
            let str = ''
            let c = 0
            for (let i of obj.device_id) {
                c++;
                str += i;
                if (c < obj.device_id.length) {
                    str += ',';
                }
            }
            obj.device_id = str;
            if (obj.device_id) {
                this.postData('sboards/energy_data', obj,
                    data => {
                        return successCallBack(data);
                    },
                    error => {
                        return errorCallBack(error);
                    }, !false);
            }
        }
    }
    postCreateTicket(obj, successCallBack, errorCallBack) {
        this.postData('opsupport/new', obj,
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            });
    }
    postSendFeedback(obj, successCallBack, errorCallBack) {
        this.postData('feedback/new', obj,
            data => {
                return successCallBack(data);
            },
            error => {
                return errorCallBack(error);
            });
    }

    //put REQUESTS go here
    putEditProfile(obj, successCallBack, errorCallBack) {
        console.log(obj);
        this.putData('users/me', obj, (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        });
    }


    //Helper for Data
    testConnection(localIp, device_id, apikey, status_spec?) {
        let mqtt_reached = false;
        return new Promise((resolve, reject) => {
            let pubTpoic = '/KU/' + device_id + '/' + this.local.getCurrentHome() + '/ping/set';
            console.log(pubTpoic);
            this._mqttService.unsafePublish(pubTpoic, 'a');
            console.log('/KU/' + device_id + '/switch/get');
            this._mqttService.observe('/KU/' + device_id + '/' + this.local.getCurrentHome() + '/ping/get')
                .subscribe((data: MqttMessage) => {
                    console.log(device_id);
                    if (status_spec) {
                        status_spec.on_available = true;
                    }
                    mqtt_reached = true;
                    resolve(1);

                });
            this.localComm.pingDevice('http://', localIp + "/api/switch?apikey=" + apikey, device_id)
                .subscribe(data => {
                    if (status_spec) {
                        status_spec.off_available = true;
                    }
                    resolve(1);
                }, error => {
                    if (status_spec) {
                        status_spec.off_available = false;
                    }
                    console.log(error);
                });

            setTimeout(() => {
                if (status_spec && mqtt_reached) {
                    status_spec.on_available = false;
                }
                reject(-1);
            }, 40000);
        })

    }


}