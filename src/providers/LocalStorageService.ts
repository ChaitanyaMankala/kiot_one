import { Injectable } from '@angular/core';
import { Events, Platform } from 'ionic-angular';
// import { MetaService } from './MetaService';

@Injectable()
export class LocalStorageService {
    constructor(private event: Events, private platform: Platform) { }
    //default Home
    getDefaultHome() {
        return localStorage.getItem('default_home');
    }

    setDefaultHome(item: string) {
        localStorage.setItem('default_home', item);
        return;
    }

    //logout functions
    logout() {
        this.setDefaultHome('');
        this.setCurrentHome('');
        this.setToken('');
        this.setUser({});
        this.setUserLoginObject('');
    }

    //current Home
    getCurrentHome() {
        let x = localStorage.getItem('current_home');
        if (!x) {
            let user = this.getUser();
            if (user.default_home) {
                this.setCurrentHome(user.default_home);
                this.setDefaultHome(user.default_home);
                return user.default_home;
            }
            else if (user.homes && user.homes.length) {
                this.setCurrentHome(user.homes[0]._id);
                this.setDefaultHome(user.homes[0]._id);
                return user.homes[0]._id;
            }
        }
        else {
            return localStorage.getItem('current_home');
        }

    }
    setCurrentHome(item: string) {
        localStorage.setItem('current_home', item);
        return;

    }


    //Token
    getToken() {
        return localStorage.getItem('token');
    }

    setToken(token: string) {
        localStorage.setItem('token', token);
    }


    //User Details
    setUser(user) {
        localStorage.setItem('user', JSON.stringify(user));
    }
    getUser() {
        let user = localStorage.getItem('user');
        if (!user) {
            localStorage.setItem('user', JSON.stringify({}));
            user = "{}";
        }
        return JSON.parse(user);
    }

    //internetstatus
    getInternetStatus() {
        return localStorage.getItem("kiot_internet");
    }
    setInternetStatus(iStatus: string) {
        if (this.getInternetStatus() != 'online' && iStatus == 'online') {
            this.event.publish('kiot_internet', 'online');
        }
        localStorage.setItem("kiot_internet", iStatus);
        // this.event.publish('kiot_internet',iStatus);
    }

    //user login details
    setUserLoginObject(data: any) {
        localStorage.setItem("kiot_UserLoginObject", JSON.stringify(data));
    }
    getUserLoginObject() {
        let data = localStorage.getItem("kiot_UserLoginObject");
        return JSON.parse(data);
    }


    //languagePrefs
    setEpgLangs(data: string) {
        localStorage.setItem('kiot_epg_langs', data);
    }
    getEpgLangs() {
        return localStorage.getItem('kiot_epg_langs') == 'undefined' ? "" : localStorage.getItem('kiot_epg_langs');
    }

    //genrePrefs
    setEpgTypes(data: string) {
        localStorage.setItem('kiot_epg_types', data);
    }
    getEpgTypes() {
        try {
            return JSON.parse(localStorage.getItem('kiot_epg_types'));
        } catch (Err) {
            return null;
        }

    }

    //temp variable for various purposes

    setTemp(data) {
        localStorage.setItem('kiot_temp', data);
    }

    getTemp() {
        return localStorage.getItem('kiot_temp');
    }

    setLocalIp(domain, ip, override_flag?) {
        if (!override_flag && localStorage.getItem('kiot_domainMap')) {
            let all_devices = Object.keys(JSON.parse(localStorage.getItem('kiot_domainMap')));
            for (let i of all_devices) {
                if (ip == this.getLocalIp(i)) {
                    this.setLocalIp(i, '', true);
                }
            }
        }
        let domainMap: any = localStorage.getItem("kiot_domainMap");
        if (domain[domain.length - 1] == ".") {
            domain = domain.slice(0, -1);
        }
        console.log(domain);
        if (!domainMap) {
            domainMap = JSON.stringify({});
        }
        domainMap = JSON.parse(domainMap);
        domainMap[domain] = ip;

        localStorage.setItem("kiot_domainMap", JSON.stringify(domainMap));
    }

    getLocalIp(domain) {

        let domainMap: any = localStorage.getItem("kiot_domainMap");
        try {
            domainMap = JSON.parse(domainMap);
        } catch (err) {
            domainMap = {};
        }
        if (!domainMap) { domainMap = {}; }
        if (!domainMap[domain] && this.platform.is('ios')) {
            return domain;
        }
        return domainMap[domain];
    }


    //AC Status
    setCurrentAcStatus(ac_id, status) {
        localStorage.setItem('kiot_ac' + ac_id, JSON.stringify(status));
    }
    getCurrentAcStatus(ac_id) {
        try {
            return JSON.parse(localStorage.getItem('kiot_ac' + ac_id));
        } catch (Err) {
            return null;
        }
    }

    //Meta Data
    getMetaData() {
        return localStorage.getItem('kiot_meta');
        /*  if(meta){
              return meta;
          }
          else{
          this.meta.getMetaData()
          .then((data:any)=>{
              console.log(data.rows);
              if(data.rows.length==0){
                  this.meta.getMetaService();
              }
          }).catch(error=>{
              console.log(error);
          });
          }*/
    }
    setMetaData(data) {
        localStorage.setItem('kiot_meta', data);
    }

    getOneSignalIds() {
        return JSON.parse(localStorage.getItem('kiot_noti_ids'));
    }
    setOneSignalIds(data) {
        localStorage.setItem('kiot_noti_ids', JSON.stringify(data));
    }

    //introData
    getIntroData() {
        let x;
        x = localStorage.getItem('intro');
        if (!x || x === undefined || x == "" || x === null) {
            x = {};
            this.setIntroData(x);
            return {};
        }
        return JSON.parse(x);
    }

    setIntroData(obj) {
        localStorage.setItem('intro', JSON.stringify(obj));
    }

    getBackButtonOverride() {
        let x = localStorage.getItem('back_override');
        if (!x) {
            this.setBackButtonOverride(false);
            return false;
        }
        return x;
    }
    setBackButtonOverride(state) {
        localStorage.setItem('back_override', state);
    }

    setSecret(device_id, secret, override_flag?) {
        if (!override_flag && localStorage.getItem('kiot_deviceMap')) {
            let all_devices = Object.keys(JSON.parse(localStorage.getItem('kiot_deviceMap')));
            for (let i of all_devices) {
                if (secret == this.getSecret(i)) {
                    this.setSecret(i, '', true);
                }
            }
        }
        let device_map: any = localStorage.getItem("kiot_deviceMap");
        if (device_id[device_id.length - 1] == ".") {
            device_id = device_id.slice(0, -1);
        }
        console.log(device_id);
        if (!device_map) {
            device_map = JSON.stringify({});
        }
        device_map = JSON.parse(device_map);
        device_map[device_id] = secret;

        localStorage.setItem("kiot_deviceMap", JSON.stringify(device_map));
    }

    getSecret(device_id) {

        let device_map: any = localStorage.getItem("kiot_deviceMap");
        try {
            device_map = JSON.parse(device_map);
        } catch (err) {
            device_map = {};
        }
        if (!device_map) { device_map = {}; }

        return device_map[device_id];
    }

}