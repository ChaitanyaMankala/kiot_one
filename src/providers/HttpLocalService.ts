import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import 'rxjs/Rx';
import { Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';
import { LocalStorageService } from './LocalStorageService';
import { Helper } from './Helper';
import * as CryptoJS from 'crypto-js';

@Injectable()
export class HttpLocalServ {
    timeout = 15000;
    constructor(private http: Http, private local: LocalStorageService, private help: Helper) {


    }
    getMe(host: string, path: string, params: any, device_id) {
        let ip = this.local.getLocalIp(host);
        if (!ip) {
            let x = Observable.create(observer => {
                return observer.error("null");
            });
            return x;
        }
        // Parameters obj-
        let parameters: URLSearchParams = new URLSearchParams();
        for (let param in params) {
            if (params.hasOwnProperty(param)) {
                parameters.set(param, params[param]);
            }
        }
        ip = "http://" + ip;
        let url = ip + path;
        url += this.attachMeta(url, device_id);
        return this.http.get(url, {
            search: parameters
        }).timeout(this.timeout);
    }
    ping(address, device_id) {
        let req_opt;
        let myHeaders: Headers = new Headers();
        myHeaders.set('Content-Type', 'application/x-www-form-urlencoded');
        req_opt = new RequestOptions({
            headers: myHeaders
        });
        address += this.attachMeta(address, device_id);
        return this.http.get(address, req_opt).timeout(this.timeout);
    }

    postMe(host: string, path: string, req_body_encoded, device_id, for_config?) {
        let url;
        if (!for_config) {
            let ip = this.local.getLocalIp(host);
            if (!ip) {
                let x = Observable.create(observer => {
                    return observer.error("null");
                });
                return x;
            }
            // Parameters obj-
            /*    let parameters: URLSearchParams = new URLSearchParams();
                for(let param in params){
                    if(params.hasOwnProperty(param)){
                        parameters.set(param, params[param]);
                    }
                }*/
            ip = "http://" + ip;
            url = ip + path;
        }
        else {
            url = host + path;
        }
        url += this.attachMeta(url, device_id);
        let req_opt;
        let myHeaders: Headers = new Headers();
        myHeaders.set('Content-Type', 'application/x-www-form-urlencoded');
        req_opt = new RequestOptions({
            headers: myHeaders
        });
        console.log('In Last step');
        console.log(url);
        console.log(req_body_encoded);

        return this.http.post(url, req_body_encoded, req_opt).timeout(this.timeout);
    }

    attachMeta(url, device_id) {
        let c = url;
        c = c.slice(c.lastIndexOf('apikey')).split('&')[0].split('=')[1];
        if (c) {
            let x = this.offlineQueryMeta(c, device_id);
            return '&meta=' + x.meta + '&meta1=' + x.meta1;
        }
        else {
            return '';
        }
    }


    offlineQueryMeta(apikey, device_id) {
        if (apikey && device_id) {
            let key = this.local.getSecret(device_id);
            if (key) {
                let x;
                x = Date.now();
                /*x = Math.floor(x / 100);
                x = x % 10000000000;*/
                x = x.toString();
                x = x.slice(2, 11);
                let x2;

                x2 = CryptoJS.HmacSHA256(x + ',' + apikey.toString(), key);
                x2.toString(CryptoJS.enc.Hex)
                console.log(x2);
                return {
                    meta: x2,
                    meta1: x
                }
            }
            else {
                return {
                    meta: -1,
                    meta1: -1
                }
            }
        }
        else {
            return {
                meta: -1,
                meta1: -1
            }
        }

    }

}