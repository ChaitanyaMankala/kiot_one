import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/Rx';
import { Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';
import { LocalStorageService } from './LocalStorageService';

@Injectable()
export class HttpServ {
    timeout = 40000;
    token: string;
    url: string;
    baseUrl: string;
    opt1: RequestOptions;
    headers;
    myHeaders: Headers = new Headers;
    public API_ENDPOINT: string;
    constructor(private http: Http, private local: LocalStorageService) {
        this.API_ENDPOINT = 'http://staging.feturtles.com:3002/';
        //this.API_ENDPOINT  ='http://staging.feturtles.com:3002/';

        this.baseUrl = 'http://staging.feturtles.com:3002/api/v1/';
        //this.baseUrl = 'http://staging.feturtles.com:3002/api/v1/';

        this.myHeaders.append('Content-type', 'application/json');
        // this.token = this.ls.getToken();
        // console.log(this.token);
        // this.myHeaders.set('Authorization', 'Bearer '+this.token);
        // this.myHeaders.append('Content-type', 'application/json');
        // console.log(this.myHeaders);
        // this.opt1 = new RequestOptions({
        //     headers: this.myHeaders
        // });
    }

    except_url = {
        "user/authenticate": 1,
        "user/authenticate/forgot_password": 1,
        "user/authenticate/reset_password": 1,
        "user/authenticate/register": 1,
        "user/authenticate/request_registration": 1,
        "user/authenticate/google":1
    }

    getMe(url: any) {
        let x = this.getTokens(url);
        return this.http.get(x + url, this.opt1).map((res: Response) => res.json()).timeout(this.timeout);
    }

    postMe(url, jsonObj) {
        let x = this.getTokens(url);
        return this.http.post(x + url, jsonObj, this.opt1).map((res: Response) => {
            console.log('error message');
            console.log(res);
            return res.json();
        }).timeout(this.timeout);
    }
    putMe(url, jsonObj) {
        let x = this.getTokens(url);

        return this.http.put(x + url, jsonObj, this.opt1).map((res: Response) => res.json()).timeout(this.timeout);
    }
    getTokens(url) {
        if (this.except_url[url]) {
            this.headers = new Headers();
            this.headers.append('Content-Type', 'application/json');
            this.opt1 = new RequestOptions({
                headers: this.headers
            });
            return this.API_ENDPOINT;
        }
        else {
            let token = this.local.getToken();
            this.myHeaders.set('Authorization', 'Bearer ' + token);
            this.opt1 = new RequestOptions({
                headers: this.myHeaders
            });
            return this.baseUrl;
        }
    }
    /*testInternet(){
        return this.http.get(this.baseUrl+"homes/myhomes")
            .timeout(3000);
                
    }*/



    // http://staging.feturtles.com:3002/api/v1/sboards/addswitch

    testInternet() {
        return new Promise((resolve, reject) => {
            this.http.get(' http://staging.feturtles.com:3002/ping')
                .subscribe(
                data => {
                    this.local.setInternetStatus('online');
                    resolve('online');
                },
                error => {

                    if (error.status == 0)
                        this.local.setInternetStatus('offline');
                    resolve('offline');
                })
        })

    }
    getMeta() {
        return this.http.get(' http://staging.feturtles.com:3002/ping/meta').map((response: Response) => response.json());
    }

}
