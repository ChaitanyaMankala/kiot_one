import { Injectable } from '@angular/core';
import { HttpLocalServ } from "./HttpLocalService";
import { Observable } from 'rxjs/Observable';
import { SqliteService } from './SqliteService';
import { NativeAudio } from '@ionic-native/native-audio';

@Injectable()
export class LocalCommService {
    SwitchActions: Object = {
        "s1": "state_01",
        "s2": "state_02",
        "s3": "state_03",
        "s4": "state_04",
        "s5": "state_05",
        "s6": "state_06",
        "s7": "state_07",
        "s8": "state_08",
        "dimmer": "state_dimmer"
    }
    DeviceTestPath = "/test_kiot";
    constructor(private httpLocal: HttpLocalServ, private sql: SqliteService, private nAudio: NativeAudio) {

    }

    sendSwitchSignal(host, sno, action, apikey, is_dimmer, device_id) {
        this.nAudio.play('irTrigger')
            .then(data => {
                console.log('playing...');
            }, error => {
                console.log(error);
            });
        let path: string = "/api/switch?apikey=" + apikey;
        let params = {};
        /*  params["action"] = "switch";
          if (actions.length > 10) {
              let x = Observable.create(observer => {
                  observer.error(new Error("Invalid Action"));
              });
              return x;
          }
          for (let act in actions) {
              if (actions.hasOwnProperty(act)) {
                  console.log(act);
                  params[this.SwitchActions[act]] = actions[act];
              }
          }*/
        console.log(host);
        console.log(path);
        console.log(sno);
        console.log(action);
        let always_true = true;
        let req_body_encoded = is_dimmer ? `key=${sno}&value=${action}&is_dimmer=${always_true}` : `key=${sno}&value=${action}`;

        return this.httpLocal.postMe(host, path, req_body_encoded, device_id);

    }
    pingDevice(host, path: string, device_id) {
        /*  if (path.includes('undefined')) {
              let x = Observable.create(observer => {
                  return observer.error("null");
              });
              return x;
          }*/
        return this.httpLocal.ping(host + path, device_id);
    }

    getConf(host, path, device_id) {

        return this.httpLocal.getMe(host, path, '', device_id);
    }
    sendConf(host, path, conf, device_id) {
        let conf_encoded = `body=${conf}`;
        return this.httpLocal.postMe(host, path, conf_encoded, device_id, true);
    }


    triggerSwitch(obj, successCallBack, errorCallBack) {
        return new Promise((resolve, reject) => {
            this.pushInOfflineBuffer(obj);
            resolve(successCallBack);
        });
    }
    pushInOfflineBuffer(obj) {
        this.sql.openDb('kiot_ac.db')
            .then(data => {
                this.sql.createATable("CREATE TABLE IF NOT EXISTS KIOT_BUFFER (id TEXT,buf_str TEXT)")
                    .then(data => {
                        this.sql.getATuple(' KIOT_BUFFER', ' WHERE id=?', ['kiot_buffer'])
                            .then((data: any) => {
                                console.log(data);
                                if (data.rows.length == 0) {
                                    let buff_array = [];
                                    buff_array.push(obj);
                                    this.sql.insertData(' KIOT_BUFFER (id,buf_str) VALUES (?,?)', ['kiot_buffer', JSON.stringify(buff_array)])
                                        .then(data => { console.log('success in inserting') })
                                        .catch(error => { console.log(error) });
                                }
                                else {
                                    console.log(JSON.parse(data.rows.item(0).buf_str));
                                    let buff_array = JSON.parse(data.rows.item(0).buf_str);
                                    buff_array.push(obj);
                                    this.sql.updateData('KIOT_BUFFER', 'id=?,buf_str=?', ['kiot_buffer', JSON.stringify(buff_array)])
                                        .then(data => { console.log('success in inserting') })
                                        .catch(error => { console.log(error) });
                                }


                            }).catch(error => { console.log(error); })
                    }
                    ).catch(error => { console.log(error) })
            }).catch(error => { console.log(error) });

    }



    sendRemoteSignal(host, apikey, req_body_encoded, device_id) {
        this.nAudio.play('irTrigger')
            .then(data => {
                console.log('playing...');
            }, error => {
                console.log(error);
            });
        let path: string = "/custom/ir?apikey=" + apikey;
        let params = {};

        console.log(host);
        console.log(path);
        console.log(req_body_encoded);
        return this.httpLocal.postMe(host, path, req_body_encoded, device_id);


    }
}