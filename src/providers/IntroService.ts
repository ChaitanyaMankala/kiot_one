import { Injectable } from '@angular/core';

//services
import { LocalStorageService } from './LocalStorageService';

const threshold = 1;

@Injectable()
export class IntroService {

    introObj;
    constructor(private ls: LocalStorageService) {
        this.introObj = ls.getIntroData();
    }

    setIntroSeen(page) {
        this.introObj = this.ls.getIntroData();
        if (page) {
            let x;
            x = this.introObj[page];
            if (x) {
                x.seen = parseInt(x.seen) + 1;
            }
            else {
                x = {
                    "seen": 1
                };
            }
            this.introObj[page] = x;
            this.ls.setIntroData(this.introObj);
        }

    }

    hasSeen(page, threshold_override?) {
        this.introObj = this.ls.getIntroData();
        if (page) {
            let x;
            x = this.introObj[page];
            if (x) {
                if (x.seen && parseInt(x.seen) >=  threshold) {
                    return true;
                }
                return false;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }



}