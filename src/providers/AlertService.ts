import { Injectable } from "@angular/core";
import { AlertController } from "ionic-angular";


@Injectable()
export class AlertService{

        constructor(private alertCtrl:AlertController){

        }
showAlert(title:string,message:string){

         let alert = this.alertCtrl.create({
          title: title,
          message: message,
          buttons: [
            {
              text: 'Okay'
          }]

       });
       alert.present();
}

showConfirm(title:string,message:string,cancelText,okText,cancelCallBack,okCallBack){
        let confirm = this.alertCtrl.create({
          title: title,
          message: message,
          buttons: [
            {
              text: cancelText,
              handler: ()=> cancelCallBack()
            },
            {
              text: okText,
              handler:() =>  okCallBack()
            }]
        });
        confirm.present();
}
showConfirmWithCustomButtons(title:string,message:string,buttonsArray){
        let confirm = this.alertCtrl.create({
          title: title,
          message: message,
          buttons: buttonsArray
        });
        confirm.present();
}
showPrompt(title,message,inputsAsArray,cancelText,cancelCallBack,okText,okCallBack){
      let prompt = this.alertCtrl.create({
      title: title,
      message: message,
      inputs: inputsAsArray,
      buttons: [
        {
          text: cancelText,
          handler: data=>  cancelCallBack(data)
        },
        {
          text: okText,
          handler: data => okCallBack(data)
        }
      ]
    });
    prompt.present();
}
}