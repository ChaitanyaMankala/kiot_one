import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Platform } from 'ionic-angular';
import { LoadingService } from './LoadingService';

declare var window;
@Injectable()
export class SqliteService {
    _db:SQLiteObject;
    constructor(private platform: Platform, private load: LoadingService, private sqlite:SQLite) {
    }
    openDb(db_name) {
        let promise = new Promise((resolve,reject)=>{
            if (window.sqlitePlugin) {
            console.log('Im here');
            this.sqlite.create({
                name: db_name,
                location: 'default'
            }).then( (db: SQLiteObject)=>{
                this._db=db;
                return resolve(db);
            }).catch(err=>{
                return reject("Could not create database");
            })
            
        }
        else {
            this._db = window.openDatabase(db_name, '1.0', 'KIOT', 5 * 1024 * 1024);
            if (this._db) {
                return resolve('Created');
            }
            else {
                return reject('cannot create');
            }
        }
    });
    return promise;
        

    }

    moveRemoteToDb() {
        /*  return this._db.transaction((tx:any)=>{
            tx.executeSql('CREATE TABLE IF NOT EXISTS AC_REMOTE' +
              ' (codeset TEXT,code TEXT,fan_set TEXT,first_seq_len TEXT,func TEXT,mode_set TEXT,power_set TEXT,sec_seq_len TEXT,swing_set TEXT,temp_set TEXT)', [])
              .then(() => {
                  console.log('table created/opened');
                  return this.insertDataInTable(id,data,codeset);
              }).catch(error => {
                  console.log(error);
                  return Promise.reject(new Error(error));
              });
          })*/

        let query = 'CREATE TABLE IF NOT EXISTS AC_REMOTE' +
            ' (codeset TEXT,code TEXT,fan_set TEXT,first_seq_len TEXT,func TEXT,mode_set TEXT,power_set TEXT,sec_seq_len TEXT,swing_set TEXT,temp_set TEXT)';
        let params = [];
        return new Promise((resolve, reject) => {
            try {
                this._db.transaction((tx: any) => {
                    tx.executeSql(query, [],
                        (tx: any, res: any) => { resolve({ tx: tx, res: res }); },
                        (tx: any, err: any) => reject({ tx: tx, err: err }));
                }
                   );
            } catch (err) {
                reject({ err: err });
            }
        });

    }
    insertDataInTable(id, data, codeset,progress) {
        /*   return this._db.transaction((tx: any) => {
               let promiseArray = [];
               return tx.executeSql('SELECT * FROM  AC_REMOTE WHERE codeset=\'' + codeset + '\'', {})
                   .then(data => {
                       if (data.rows.length == 0) {
   
                           for (let i of data[0].keys) {
   
                               promiseArray.push(tx.executeSql("INSERT INTO AC_REMOTE (codeset,code,fan_set,first_seq_len,func,mode_set,power_set,sec_seq_len,swing_set,temp_set) VALUES (?,?,?,?,?,?,?,?,?,?)", [codeset, i.code, i.fan_set, i.first_seq_len, i.func, i.mode_set, i.power_set, i.sec_seq_len, i.swing_set, i.temp_set]));
                           }
                       }
                       else {
                           promiseArray.push(Promise.resolve(1));
                       }
                       return Promise.all(promiseArray);
                   }
                   ).catch(error => {
                       console.log(error);
                       return Promise.reject(new Error(error));
                   });
           });*/

        let query1 = "SELECT * FROM  AC_REMOTE WHERE codeset=?";
        let query2 = "INSERT INTO AC_REMOTE (codeset,code,fan_set,first_seq_len,func,mode_set,power_set,sec_seq_len,swing_set,temp_set) VALUES (?,?,?,?,?,?,?,?,?,?)"

        let params1 = [codeset];
        return new Promise((resolve, reject) => {
            try {
                this._db.transaction((tx: any) => {
                    tx.executeSql(query1, params1,
                        (tx: any, res: any) => {
                            let promiseArray = [];
                            if (res.rows.length == 0) {

                                for (let i of data[0].keys) {

                                    promiseArray.push(new Promise((resolve, reject) => {
                                        try {
                                            this._db.transaction((tx: any) => {
                                                tx.executeSql(query2, [codeset, i.code, i.fan_set, i.first_seq_len, i.func, i.mode_set, i.power_set, i.sec_seq_len, i.swing_set, i.temp_set],
                                                    (tx: any, res: any) => { console.log('INSERTED');progress[0]++; resolve({ tx: tx, res }) },
                                                    (tx: any, err: any) => { console.log('ERROR' + JSON.stringify(err)); reject({ tx: tx, err: err }) });
                                            });
                                        } catch (err) {
                                            reject({ err: err });
                                        }
                                    }));
                                }
                            }
                            else {
                                console.log('Im Here Atleast');
                                // promiseArray = [];
                                // promiseArray.push(Promise.resolve(1));
                                resolve('DONE BRO!');
                            }
                            Promise.all(promiseArray).then(data=>{
                                resolve(data);
                            });

                        },
                        (tx: any, err: any) => { console.log(err); reject({ tx: tx, err: err }) });
                });
            } catch (err) {
                reject({ err: err });
            }
        });
    }


    getATuple(table_name, query, params) {
        return new Promise((resolve, reject) => {
            window.something = this._db;
            try {
                this._db.transaction((tx: any) => {
                    tx.executeSql('SELECT *FROM ' + table_name + query, params,
                        (tx: any, res: any) => resolve(res),
                        (tx: any, err: any) => reject({ tx: tx, err: err }));
                });
            } catch (err) {
                console.log("I am sql error");
                reject({ err: err });
            }
        });

    }
    checkTableExistence(table_name) {
        return new Promise((resolve, reject) => {
            try {
                this._db.transaction((tx: any) => {
                    tx.executeSql("SELECT name FROM sqlite_master WHERE type='table' AND name=?", [table_name],
                        function (tx, results) {
                            if (results.rows.length == 0) {
                                return resolve(0)
                            } else {
                                return resolve(1)
                            }
                        });
                });
            } catch (err) {
                reject({ err: err });
            }
        });
    }
    getModes(table_name, query, params) {
        return new Promise((resolve, reject) => {
            try {
                this._db.transaction((tx: any) => {
                    tx.executeSql('SELECT DISTINCT func FROM ' + table_name + query, params,
                        (tx: any, res: any) => resolve(res),
                        (tx: any, err: any) => reject({ tx: tx, err: err }));
                });
            } catch (err) {
                reject({ err: err });
            }
        });

    }

    createATable(query) {
        let params = [];
        return new Promise((resolve, reject) => {
            try {
                this._db.transaction((tx: any) => {
                    tx.executeSql(query, [],
                        (tx: any, res: any) => { resolve({ tx: tx, res: res }); },
                        (tx: any, err: any) => reject({ tx: tx, err: err }));
                });
            } catch (err) {
                reject({ err: err });
            }
        });
    }
    insertData(query1, params) {
        let query = 'INSERT INTO ' + query1;
        //let params = [];
        return new Promise((resolve, reject) => {
            try {
                this._db.transaction((tx: any) => {
                    tx.executeSql(query, params,
                        (tx: any, res: any) => { resolve({ tx: tx, res: res }); },
                        (tx: any, err: any) => reject({ tx: tx, err: err }));
                });
            } catch (err) {
                reject({ err: err });
            }
        });
    }
    updateData(table_name, query1, params) {
        let query = 'UPDATE ' + table_name + ' SET ' + query1;
        //let params = [];
        return new Promise((resolve, reject) => {
            try {
                this._db.transaction((tx: any) => {
                    tx.executeSql(query, params,
                        (tx: any, res: any) => { resolve({ tx: tx, res: res }); },
                        (tx: any, err: any) => reject({ tx: tx, err: err }));
                });
            } catch (err) {
                reject({ err: err });
            }
        });
    }

}