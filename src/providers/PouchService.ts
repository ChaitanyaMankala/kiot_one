import { Injectable,Inject,forwardRef } from '@angular/core';  
import PouchDB from 'pouchdb';
//import { Network } from 'ionic-native';
import { LocalStorageService } from './LocalStorageService';
import { HttpServ } from './HttpServ';


@Injectable()
export class PouchService{
    public _db;
    private _homes;
    public url:string;
    connected:boolean; 
    constructor(@Inject(forwardRef(() => LocalStorageService))private local:LocalStorageService){ 
          //this._db = new PouchDB(url, { adapter: 'idb' });
                  window["PouchDB"] = PouchDB;
                //   PouchDB.plugin(require('pouchdb-adapter-idb'));
                //  this._db = new PouchDB("KIOT");
                  this._db = new PouchDB("KIOT", {auto_compaction: true });
                 // PouchDB.plugin(require('pouchdb-upsert'));
    }

    initDB(url) {
        console.log(this._db.adapter);
        this.url = url;

    }
    bulkAdd(docsArray){
          console.log(docsArray);
         let promiseArray=[];
          for(let i of docsArray){
              promiseArray.push(this._db.put(i));
          }
          console.log(promiseArray);
          return Promise.all(promiseArray);
          //return this._db.bulkDocs(docsArray);
         // return Promise.reject(1);
    }
    add(prefix,nameOfDoc,data,update_flag) { 
    
    if(data.room_id && !data.room_id._id){
        data._id = prefix + '_'+ nameOfDoc + '_' + data.room_id;
    }
    else
     data._id = prefix + '_'+ nameOfDoc + '_' + data._id;
         if(update_flag!=false){ 
             data._rev = update_flag;
         }
        return this._db.put(data);
    }  
    removeADoc(prefix,nameOfDoc,data_id){
        data_id = prefix + '_'+ nameOfDoc + '_' + data_id;
        console.log(data_id);
            this._db.get(data_id)
            .then(
                doc=>{
                    console.log('deleted!!');
                    return this._db.remove(doc);
                })
            .catch(
                err=>{
                    console.log(err);
                    return err;
            });
    }
    
 update(prefix,nameOfDoc,data){  
        console.log(data._id);
        if(data!=null){
            data._id = prefix + '_'+ nameOfDoc + '_' + data._id;
            
           // console.log(data._id);
            this._db.get(data._id).
            then(function(doc){
                data._rev = doc._rev;
                console.log(data);
                 return this._db.put(data);
                //return this._db.put(data);
            }).catch(err=>{
               //console.log("I am an error"); 
               if(err.name === 'not_found'){
                   return this._db.put(data);
               }
                return this._db.put(data);
            });
        
    }


}

check(prefix,nameOfDoc,data){
    if(data.room_id){
        data._id = prefix + '_'+ nameOfDoc + '_' + data.room_id;
    }
    else
     data._id = prefix + '_'+ nameOfDoc + '_' + data._id;

    console.log(data);
     return this._db.get(data._id);
           /* then(function(doc){
                console.log(doc._rev);
                return doc._rev;
            }).catch(err=>{
               console.log(err); 
               if(err.name === 'not_found'){
                   return false;
               }
                return false;
            });*/
}

downloadRemote(remote){
        if(remote!=null){
            remote._id ="codeset_"+remote._id;
            this._db.get(remote._id,
            (doc)=> 
            {

                if(doc==null||doc._id == null){
                     console.log(remote); 
                     console.log(doc);
                    // return Promise.resolve(remote); 
                    return this._db.put(remote);
                }
                else{
                   // return this._db.put(remote);
                   return Promise.resolve(remote);
                }
                                
            },
            (err)=> { 
                if(err.name=='not_found'){
                    return this._db.put(remote);
                }
                return console.log(err);
            });
        
    }
}

getADoc(prefix,nameOfDoc){

    let startkey = prefix+'_'+nameOfDoc; 
    let endkey = startkey+"\ufff0";
    window.someVar = this._db;
     return this._db.allDocs({startkey:startkey,endkey:endkey, include_docs: true})
            .then(docs => {
                console.log(docs.rows);
                this._homes = docs.rows.map(row => {
                   return row.doc;
                });
                return this._homes;
            }).catch(err=>{
                console.log(err);
            }); 
}
removeAll(prefix,nameOfDoc){
    let startkey = prefix+'_'+nameOfDoc; 
    let endkey = startkey+"\ufff0";
    let promiseArray=[];
     return this._db.allDocs({startkey:startkey,endkey:endkey, include_docs: true})
            .then(docs => {
                //console.log(docs);
                let rows= docs.rows.map( (row)=> {
                            return{
                            _id: row.id, 
                            _rev: row.doc._rev, 
                            _deleted: true }
                     });
                  return this._db.bulkDocs(rows);
            }).catch(err=>{
                console.log(err);
                if(err.name=='not_found'){
                    return Promise.resolve('yess');
                }
                return Promise.reject(err);
            }); 
}


getAll() { 

    //checks for internet connection- if yes fetch from Server or from db 
    // if(this.local.getInternetStatus()=="online"){

    //   this.ht.getMe(this.url)
    //     .subscribe((data)=>{
    //             console.log("ch1");
    //        if(Array.isArray(data)==true){
    //                console.log("ch2");
    //             for(let i of data){

    //                 //if(i!=null)
    //                // this.update(i);
    //             }
    //        }
    //        else{
    //                console.log("ch3");
    //            //this.update(data);
    //        }
    //    },(error)=>{

    //    });
    // }

    //fetch from db now
    if (!this._homes) {
        return this._db.allDocs({ include_docs: true})
            .then(docs => {

                // Each row has a .doc object and we just want to send an 
                // array of home objects back to the calling controller,
                // so let's map the array to contain just the .doc objects.

                this._homes = docs.rows.map(row => {
                    // Dates are not automatically converted from a string.
                   // row.doc.Date = new Date(row.doc.Date);
                    return row.doc;
                });

                // Listen for changes on the database.
                this._db.changes({ live: true, since: 'now', include_docs: true})
                    .on('change', this.onDatabaseChange);

                return this._homes;
            });
    } else {
        // Return cached data as a promise
        return Promise.resolve(this._homes);
    }


}
private onDatabaseChange = (change) => {  
    var index = this.findIndex(this._homes, change.id);
    var home = this._homes[index];

    if (change.deleted) {
        if (home) {
            this._homes.splice(index, 1); // delete
        }
    } else {
       // change.doc.Date = new Date(change.doc.Date);
        if (home && home._id === change.id) {

            this._homes[index] = change.doc; // update
            console.log(this._homes);
        } else {
            this._homes.splice(index, 0, change.doc) // insert
        }
    }
}

// Binary search, the array is by default sorted by _id.
private findIndex(array, id) {  
    var low = 0, high = array.length, mid;
    while (low < high) {
    mid = (low + high) >>> 1;
    array[mid]._id < id ? low = mid + 1 : high = mid
    }
    return low;
}
}

/*export class urlToDb{
        data={
            "homes/myhomes":"UserHomes",
        }
}*/