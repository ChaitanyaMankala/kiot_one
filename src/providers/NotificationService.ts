import { Injectable } from '@angular/core';
import { DataService } from './DataService';

@Injectable()
export class NotificationService {
    constructor(private ds: DataService) {

    }

    invokeNotificationAction(add_data, _act?) {
        console.log(add_data.type);
        switch (add_data.type) {
            case 'JOIN_HOME_REQUEST': {

                return this.joinHomeAction(add_data, _act);
                // break;
            }
            case 'SCHEDULE': {
                return this.scheduleAction(add_data, _act);
            }

            case 'REMINDER': {
                return this.reminderAction(add_data, _act);
            }
        }
    }

    joinHomeAction(add_data, _act?) {
        if (_act) {
            if (_act.actionID == 'yes' || _act.actionID == 1) {
                return new Promise((resolve, reject) => {
                    this.ds.postApproveJoinRequest({
                        status: 'confirm',
                        req_id: add_data.data.join_home_request,
                        notification_id: add_data.data.noti_id
                    }, data => {
                        console.log(data);
                        return resolve(data);
                    }, error => {
                        console.log(error);
                        return reject(error);
                    }, true);
                })
            }
            else if (_act.actionID == 'no' || _act.actionID == 0) {
                return new Promise((resolve, reject) => {
                    this.ds.postApproveJoinRequest({
                        status: 'deleted',
                        req_id: add_data.data.join_home_request,
                        notification_id: add_data.data.noti_id
                    }, data => {
                        console.log(data);
                        return resolve(data);
                    }, error => {
                        console.log(error);
                        return reject(error);
                    }, true);
                })

            }
        }
    }

    scheduleAction(add_data, _act) {
        if (_act) {
            if (_act.actionID == 1) {
                return new Promise((resolve, reject) => {
                    this.ds.postDismissSchedule({
                        notification_id: add_data.data.noti_id,
                        is_cancelled: false,
                        schedule_id: add_data.data.schedule.scheduleId
                    }, data => {
                        resolve(data);
                    }, error => {
                        reject(error);
                    });
                });
            }
            else if (_act.actionID == 0) {
                return new Promise((resolve, reject) => {
                    this.ds.postDismissSchedule({
                        notification_id: add_data.data.noti_id,
                        is_cancelled: true,
                        schedule_id: add_data.data.schedule.scheduleId
                    }, data => {
                        resolve(data);
                    }, error => {
                        reject(error);
                    });
                });
            }
        }

    }

    reminderAction(add_data, _act) {
        if (_act) {
            if (_act.actionID == 1) {
                return new Promise((resolve, reject) => {
                    if (!add_data.data.reminder.codes) {
                        reject(-1);
                    }
                    else {
                        this.ds.postSwitchToChannelFromNotification({
                            "epg_reminder_id":add_data.data.epg_reminder_id,
                            "channelid": add_data.data.reminder.channelno,
                            "ir_device": add_data.data.reminder.ir_appliance_id,
                            "device": add_data.data.reminder.device_id,
                            "protocol": add_data.data.reminder.protocol,
                            "len": add_data.data.reminder.len,
                            "proto_name": add_data.data.reminder.proto_name,
                            "wave_gap": add_data.data.reminder.wave_gap,
                            "codes": add_data.data.reminder.codes,
                            "notification_id":add_data.data.noti_id
                        }, data => {
                                console.log(data);
                                resolve(1);
                        }, error => {
                                console.log(error);
                                reject(0);
                        })
                    }
                })
            }
            else if (_act.actionID == 0) {
                return new Promise((resolve, reject) => {

                    this.ds.postUnsetReminder({
                        notification_id: add_data.data.noti_id,
                        action: "remove",
                        programid: add_data.data.reminder.programid
                    }, data => {
                        resolve(data);
                    }, error => {
                        reject(error);
                    });
                });
            }
        }

    }

}

