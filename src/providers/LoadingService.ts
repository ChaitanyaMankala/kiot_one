import { Injectable } from '@angular/core';
import { LoadingController } from 'ionic-angular';

@Injectable()
export class LoadingService {
    loading;
    constructor(private loadingCtrl: LoadingController) {

    }
    showLoading(show) {

        this.loading = this.loadingCtrl.create({
            //spinner: 'dots',
            // content: `<ion-spinner name="dots" style="spinner-ios-dots-color:#FF9500;spinner-md-dots-color:#FF9500"></ion-spinner>`,
            showBackdrop: true
        });


        this.loading.onDidDismiss(() => {
            console.log('Dismissed loading');
        });

        this.loading.present();
        setTimeout(() => {
            if (this.loading) {
                if (this.loading.instance) {
                    this.dismiss();
                }
            }
        }, 40000);
        return this.loading;

    }
    dismiss() {
        if (this.loading) {
            try {
                this.loading.dismiss();

            }
            catch (exception) {
                console.log(exception);
            }
            this.loading = null;
        }
    }
    showTimeOutLoading(timeout) {
        this.loading = this.loadingCtrl.create({
            spinner: 'crescent',
            content: `<ion-spinner name="crescent" style="spinner-ios-crescent-color:#FF9500;spinner-md-crescent-color:#FF9500"></ion-spinner>`,
            showBackdrop: false,
            duration: timeout
        });


        this.loading.present();

    }



}