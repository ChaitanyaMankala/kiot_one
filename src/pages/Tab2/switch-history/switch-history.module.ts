import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SwitchHistoryPage } from './switch-history';
import { PipesModule } from '../../../pipes/pipes.module';

@NgModule({
    declarations: [
        SwitchHistoryPage,
    ],
    imports: [
        IonicPageModule.forChild(SwitchHistoryPage),
        PipesModule
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class SwitchHistoryPageModule { }