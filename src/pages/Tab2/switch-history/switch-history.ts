import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../providers/DataService';


@IonicPage()
@Component({
  selector: 'page-switch-history',
  templateUrl: 'switch-history.html'
})
export class SwitchHistoryPage {
  switchData; history_data = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, private ds: DataService) {
    this.switchData = this.navParams.get('switch');
    this.ds.getSwitchHistory(this.switchData._id.split('_')[2] || this.switchData._id,
      data => {
        for (let i of data) {
          if (i.event_type != 0 && i.event_type != 1) {
            // i.showHistory = false;
            continue;
          }
          this.history_data.push(i);
        }
      }, error => {
        console.log(error);
      })


  }

  ionViewWillEnter() {
    console.log('ionViewDidLoad SwitchHistoryPage');




  }

}
