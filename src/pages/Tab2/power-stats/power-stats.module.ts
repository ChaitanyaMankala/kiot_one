import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PowerStatsPage } from './power-stats';

@NgModule({
    declarations: [
        PowerStatsPage,
    ],
    imports: [
        IonicPageModule.forChild(PowerStatsPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class PowerStatsPageModule { }