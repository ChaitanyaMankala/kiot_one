import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController, IonicPage } from 'ionic-angular';
import { Chart } from 'chart.js';

//providers
import { DataService } from '../../../providers/DataService';
import { Helper } from '../../../providers/Helper';

const MONTH_NAMES = {
  0: 'January',
  1: 'February',
  2: 'March',
  3: 'April',
  4: 'May',
  5: 'June',
  6: 'July',
  7: 'August',
  8: 'September',
  9: 'October',
  10: 'November',
  11: 'December'
}

@IonicPage()
@Component({
  selector: 'page-power-stats',
  templateUrl: 'power-stats.html',
})
export class PowerStatsPage {
  devices; show_chart = false; all_months = []; current;

  @ViewChild('lineCanvas') lineCanvas;

  lineChart: any;
  show_spinner;

  constructor(public navCtrl: NavController, private ds: DataService, private help: Helper, public navParams: NavParams, private viewCtrl: ViewController) {
    this.show_spinner = true;
    this.devices = this.navParams.get('devices');
    let c = new Date(Date.now());
    this.all_months.push({
      name: MONTH_NAMES[c.getMonth()],
      month: c.getMonth()
    })
    this.current = c.getMonth();
    this.initialiseChart(c, 0);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PowerStatsPage');
  }
  dismiss(data) {
    this.viewCtrl.dismiss(data);
  }

  initialiseChart(month, index) {
    month = new Date(month);
    let d = new Date(month);
    d.setHours(0, 0, 0, 0);
    d.setDate(1);
    this.ds.postGetPowerData({
      "device_id": this.devices,
      "timeto": month.getTime(),
      "timefrom": d.getTime(),
      "interval": "month"
    }, data => {

      this.show_chart = true;

      if (data.length) {
        if (index != 0) {
          this.all_months.push({
            name: MONTH_NAMES[month.getMonth()],
            month: month.getMonth()
          })
        }


        this.show_chart = true;
        let chartData = [];
        let labels = [];
        let sorted_data;
        sorted_data = this.help.sorter(data, "_id", "");
        for (let i of sorted_data) {
          chartData.push(i.avg_power);
          labels.push(i._id);
        }

        this.ds.postGetEnergyData({
          "device_id": this.devices,
          "timeto": month.getTime(),
          "timefrom": d.getTime(),
          "interval": "month"
        }, data => {
          this.show_spinner = false;
          this.all_months[index].data = {
            chartData: chartData,
            labels: labels,
            energyData: data
          }
          if (index == 0) {
            this.setupChart(chartData, labels);
            this.setupDayWise(0);
          }
          month = new Date(month.getFullYear(), month.getMonth(), 0);
          this.initialiseChart(month, index + 1);
        }, error => {
          console.log(error);
        })


      }
    }, error => {
      console.log(error);
    })
  }
  day_wise = [];
  monthSelected(month) {
    this.current = this.all_months[month].month;
    this.setupChart(this.all_months[month].data.chartData, this.all_months[month].data.labels);
    this.setupDayWise(month);
  }

  setupDayWise(month) {
    this.day_wise = [];
    let ind = 0;
    for (let i of this.all_months[month].data.chartData) {
      let date = this.all_months[month].data.labels[ind];
      let sup_txt = 'th'
      if (date == 1 || date == 21 || date == 31) {
        sup_txt = 'st'
      }
      else if (date == 2 || date == 22) {
        sup_txt = 'nd'
      }
      else if (date == 3 || date == 23) {
        sup_txt = 'rd'
      }

      this.day_wise.push({
        power: Math.round(i * 100) / 100,
        date: date,
        sup_txt: sup_txt
      })
      ind++;
    }
    for (let i of this.all_months[month].data.energyData) {
      for (let j of this.day_wise) {
        if (j.date == i._id) {
          if (i.avg_energy) {
            j.energy = Math.round(i.avg_energy * 100) / 100;
            break;
          }
        }
      }
    }
  }

  setupChart(data, labels) {
    if (this.lineChart != undefined) {
      this.lineChart.destroy();
    }
    let gradient = this.lineCanvas.nativeElement.getContext('2d').createLinearGradient(0, 0, 0, 450);

    gradient.addColorStop(0, '#ff9500');
    gradient.addColorStop(0.4, '#1c1c1d');
    gradient.addColorStop(1, '#1c1c1d');
    this.lineChart = new Chart(this.lineCanvas.nativeElement, {

      type: 'line',
      data: {
        layout: {
          padding: {
            left: 10,
            right: 15,
            top: 20,
            bottom: 10
          }
        },
        labels: labels,
        datasets: [
          {
            label: 'Power Consumed (W)',
            backgroundColor: gradient,
            borderColor: 'rgba(255,255,255,0)',
            pointBackgroundColor: 'transparent',
            borderWidth: 1,
            pointBorderColor: "transparent",
            data: data,
            lineTension: 0
          }
        ]
      },
      options: {
        legend: {
          display: false
        },
        scales: {
          xAxes: [{
            display: true,
            gridLines: {
              color: 'rgba(255,255,255,0.03)'
            },
            ticks: {
              autoSkip: true,
              maxTicksLimit: 6,
              maxRotation: 0,
              minRotation: 0,
              fontFamily: "SanFranciscoLight"
            }
          }],
          yAxes: [{
            display: true,
            gridLines: {
              color: 'rgba(255,255,255,0.03)'
            },
            ticks: {

              autoSkip: true,
              maxTicksLimit: 4,
              maxRotation: 0,
              minRotation: 0,
              fontFamily: "SanFranciscoLight"
            }
          }]
        }
      }


    });
  }

}
