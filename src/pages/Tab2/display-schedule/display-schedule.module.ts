import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DisplaySchedulePage } from './display-schedule';
import { PipesModule } from '../../../pipes/pipes.module';

@NgModule({
    declarations: [
        DisplaySchedulePage,
    ],
    imports: [
        IonicPageModule.forChild(DisplaySchedulePage),
        PipesModule
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class DisplaySchedulePageModule { }