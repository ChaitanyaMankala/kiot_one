import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';

//pages
import { CreateSchedulePage } from '../create-schedule/create-schedule';
import { ScheduleDetailsPage } from '../schedule-details/schedule-details';

//providers
import { DataService } from '../../../providers/DataService';


@IonicPage()
@Component({
  selector: 'page-display-schedule',
  templateUrl: 'display-schedule.html',
})
export class DisplaySchedulePage {
  switch; schedules = []; appliance_name;
  constructor(public navCtrl: NavController, public navParams: NavParams, private ds: DataService) {
    this.switch = this.navParams.get('switch');
    console.log(this.switch);
    this.appliance_name = this.switch.name;
  }
  ionViewWillEnter() {
    if (this.schedules.length) {
      for (let i of this.schedules) {
        try {
          i.mins = parseInt(i.mins);
        }
        catch (err) {
          console.log(err);
        }
      }
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad DisplaySchedule');
    this.ds.getSchedulesOfASwitch(this.switch._id.split('_')[2],
      data => {
        console.log(data);
        this.schedules = data;
        for (let i of this.schedules) {
          i.hrs = i.hour;
          i.am_pm = 'AM';
          if (i.hour == 12) {
            i.am_pm = 'PM';
          }
          else if (i.hour > 12) {
            i.hrs = i.hour - 12;
            i.am_pm = 'PM';
          }
        }
      }, error => {
        console.log(error);
      })

  }
  goToCreateSchedule() {
    this.navCtrl.push('CreateSchedulePage', {
      switch: this.switch,
      current_schedules: this.schedules
    });
  }
  goToScheduleDetails(ind) {
    this.navCtrl.push('ScheduleDetailsPage', {
      schedules: this.schedules,
      ind: ind,
      switch: this.switch
    })
  }
  triggerActive(eve, i) {
    console.log(eve);
    this.ds.postTriggerActiveSchedule({
      scheduleId: i._id,
      isActive: eve
    }, data => {
      console.log('YOLO It works');
    }, error => {
      console.log(error);
      i.isActive = !i.isActive;
    });
  }

}
