import { Component } from '@angular/core';
import { NavController, NavParams,IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../providers/DataService';
import { ToastService } from '../../../providers/ToastService';
import { Helper } from '../../../providers/Helper';

@IonicPage()
@Component({
  selector: 'page-create-schedule',
  templateUrl: 'create-schedule.html',
})
export class CreateSchedulePage {
  schedule_time; switch_action; schedule_name; switch; schedule_repeats = [false, false, false, false, false, false, false]; repeat_toggle; current_schedules; dimm_value;
  constructor(public navCtrl: NavController, public navParams: NavParams, private help: Helper, private ds: DataService, private toast: ToastService) {
    /*  let x = new Date(Date.now());
      let mins = x.getMinutes();
      let hrs = x.getHours();
      this.schedule_time = hrs + ':' + mins;*/
    this.schedule_time = 'Select';
    this.switch_action = true;
    this.switch = this.navParams.get('switch');
    console.log(this.switch);
    this.current_schedules = this.navParams.get('current_schedules');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateSchedule');
  }

  createSchedule() {
    if (this.validateSchedule()) {
      console.log(this.schedule_repeats);
      let obj;
      obj = {
        "SBid": this.switch.sboard_id,
        "deviceId": this.switch.device_id,
        "actionType": "switchAction",
        "isActive": true,
        "days": this.help.getDaysArray(this.schedule_repeats),
        "hour": this.schedule_time.slice(0, 2),
        "mins": this.schedule_time.slice(3, 5),
        "id": this.switch._id.split('_')[2],
        "act_type": "switch",
        "device": this.switch.device_id,
        "key": Date.now(),
        "isRepeat": this.repeat_toggle,
        "mobId": "98239023",
        "switch_no": this.switch.switch_no,
        "switch_state": this.switch_action ? 1 : 0
      }
      if (this.dimm_value) {
        obj.dimm_value = this.dimm_value;
      }
      this.ds.postCreateSchedule(obj,
        data => {
          console.log(data);
          let hrs = parseInt(this.schedule_time.slice(0, 2));
          let obj =
            {

              "actionType": "switchAction",
              "isActive": true,
              "isRepeat": this.repeat_toggle,
              "hour": this.schedule_time.slice(0, 2),
              "hrs": hrs > 12 ? hrs - 12 : hrs,
              "mins": this.schedule_time.slice(3, 5),
              "am_pm": hrs > 12 ? 'PM' : 'AM',
              "args": {
                "switch_state": this.switch_action ? 1 : 0,
                "dimm_value": this.dimm_value
              },
              "days": this.help.getDaysArray(this.schedule_repeats),
              "_id": data.scheduleId
            };
          obj.mins = parseInt(obj.mins);
          this.current_schedules.push(obj);
          this.navCtrl.pop();
          this.toast.bottomToast_Short('Schedule Added');
        }, error => {
          console.log(error);
        })
    }
    else {
      this.toast.bottomToast_Short('Schedule at ' + this.schedule_time + ' already exists');
    }
    //console.log(this.schedule_time);
  }
  validateSchedule() {
    for (let i of this.current_schedules) {
      if (parseInt(i.hour) == parseInt(this.schedule_time.slice(0, 2)) && (parseInt(i.mins) == parseInt(this.schedule_time.slice(3, 5)))) {
        return false;
      }
    }
    return true;
  }

}
