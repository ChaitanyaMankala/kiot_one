import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateSchedulePage } from './create-schedule';
import { PipesModule } from '../../../pipes/pipes.module';

@NgModule({
    declarations: [
        CreateSchedulePage,
    ],
    imports: [
        IonicPageModule.forChild(CreateSchedulePage),
        PipesModule
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class CreateSchedulePageModule { }