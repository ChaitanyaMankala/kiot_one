import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScheduleDetailsPage } from './schedule-details';
import { PipesModule } from '../../../pipes/pipes.module';

@NgModule({
    declarations: [
        ScheduleDetailsPage,
    ],
    imports: [
        IonicPageModule.forChild(ScheduleDetailsPage),
        PipesModule
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class ScheduleDetailsPageModule { }