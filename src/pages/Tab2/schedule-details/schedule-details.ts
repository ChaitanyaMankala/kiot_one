import { Component } from '@angular/core';
import { NavController, NavParams,IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../providers/DataService';
import { AlertService } from '../../../providers/AlertService';

//pages
import { EditSchedulePage } from '../edit-schedule/edit-schedule';


@IonicPage()
@Component({
  selector: 'page-schedule-details',
  templateUrl: 'schedule-details.html',
})
export class ScheduleDetailsPage {
  schedules; ind; schedule_details; repeats_string = "";flag=false;
  constructor(public navCtrl: NavController, public navParams: NavParams, private ds: DataService, private alert: AlertService) {
    this.initialise();
    this.flag = true;
  }

  ionViewWillEnter() {
    if(this.flag){
        this.initialise();
    }

  }
  initialise() {
    this.repeats_string = "";
    this.schedules = this.navParams.get('schedules');
    this.ind = this.navParams.get('ind');
    this.schedule_details = this.schedules[this.ind];
    let days = ['M ', 'T ', 'W ', 'Th ', 'F ', 'Sa ', 'Su '];
    if (this.schedule_details.days.length) {
      for (let i of this.schedule_details.days) {
        this.repeats_string += days[i];
      }
    }
  }
  deleteSchedule() {
    this.alert.showConfirm('Are you Sure', 'This deletes the schedule made for switch', 'Cancel', 'Delete',
      () => {

      }, () => {
        this.ds.postDeleteSchedule({
          "scheduleId": this.schedule_details._id
        }, data => {
          console.log(data);
          this.schedules.splice(this.ind, 1);
          this.navCtrl.pop();
        }, error => {
          console.log(error);
        })
      })
  }

  goToEditSchedule() {
    this.navCtrl.push('EditSchedulePage', {
      schedules: this.schedules,
      ind: this.ind,
      switch: this.navParams.get('switch'),
      scheduleId: this.schedule_details._id
    });
  }
}
