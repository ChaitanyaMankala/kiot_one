import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditSchedulePage } from './edit-schedule';
import { PipesModule } from '../../../pipes/pipes.module';

@NgModule({
    declarations: [
        EditSchedulePage,
    ],
    imports: [
        IonicPageModule.forChild(EditSchedulePage),
        PipesModule
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class EditSchedulePageModule { }