import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../providers/DataService';
import { ToastService } from '../../../providers/ToastService';
import { Helper } from '../../../providers/Helper';

@IonicPage()
@Component({
  selector: 'page-edit-schedule',
  templateUrl: 'edit-schedule.html',
})
export class EditSchedulePage {
  current_schedule;
  schedule_time; switch_action; schedule_name; switch; schedule_repeats = [false, false, false, false, false, false, false]; dimm_value; repeat_toggle; switch_type; ind; schedules;
  constructor(public navCtrl: NavController, private help: Helper, public navParams: NavParams, private ds: DataService, private toast: ToastService) {
    this.switch = this.navParams.get('switch');
    this.schedules = this.navParams.get('schedules');
    this.ind = this.navParams.get('ind');
    this.current_schedule = this.schedules[this.ind];
    this.switch_type = this.navParams.get('switch_type');
    console.log(this.current_schedule);
    this.schedule_name = this.current_schedule.scheduleName;
    this.switch_action = this.current_schedule.args.switch_state;
    this.repeat_toggle = this.current_schedule.days.length ? true : false;
    //this.schedule_repeats = this.current_schedule.days;
    this.schedule_time = this.current_schedule.hour.toString() + ':' + (this.current_schedule.mins < 10 ? '0' : '') + (this.current_schedule.mins.toString());
    this.dimm_value = this.current_schedule.args.dimm_value;
    if (this.current_schedule.days) {
      for (let i of this.current_schedule.days) {
        this.schedule_repeats[i] = true;
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditSchedule');
  }
  createSchedule() {

    console.log(this.schedule_repeats);
    let obj;
    obj = {
      "scheduleId": this.navParams.get('scheduleId'),
      "SBid": this.switch.sboard_id,
      "deviceId": this.switch.device_id,
      "actionType": "switchAction",
      "isActive": this.current_schedule.isActive,
      "days": this.repeat_toggle ? this.help.getDaysArray(this.schedule_repeats) : [],
      "hour": this.schedule_time.slice(0, 2),
      "mins": this.schedule_time.slice(3, 5),
      "id": this.switch._id.split('_')[2],
      "act_type": "switch",
      "device": this.switch.device_id,
      "key": Date.now(),
      "isRepeat": this.repeat_toggle,
      "mobId": "98239023",
      "switch_no": this.switch.switch_no,
      "switch_state": this.switch_action ? 1 : 0,
      "dimm_value": this.dimm_value
    }
    this.ds.postEditSchedule(obj,
      data => {
        console.log(data);
        let hrs = parseInt(this.schedule_time.slice(0, 2));
        let obj =
          {
            "_id": this.navParams.get('scheduleId'),
            "actionType": "switchAction",
            "isActive": true,
            "isRepeat": this.repeat_toggle,
            "hour": this.schedule_time.slice(0, 2),
            "hrs": hrs > 12 ? hrs - 12 : hrs,
            "mins": parseInt(this.schedule_time.slice(3, 5)),
            "am_pm": hrs > 12 ? 'PM' : 'AM',
            "args": {
              "switch_state": this.switch_action ? 1 : 0
            },
            "days": this.help.getDaysArray(this.schedule_repeats),
            "dimm_value": this.dimm_value
          };
        this.schedules[this.ind] = obj;
        this.navCtrl.pop();
        this.toast.bottomToast_Short('Schedule Edited');
      }, error => {
        console.log(error);
      })
    //console.log(this.schedule_time);
  }
}
