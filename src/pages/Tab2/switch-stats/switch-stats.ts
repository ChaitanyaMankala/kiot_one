import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';

//pages
import { SwitchHistoryPage } from '../switch-history/switch-history';
import { DisplaySchedulePage } from '../display-schedule/display-schedule';

//providers
import { DataService } from '../../../providers/DataService';
import { LocalCommService } from "../../../providers/LocalCommunicationServ";
import { LocalStorageService } from '../../../providers/LocalStorageService';
import { AlertService } from '../../../providers/AlertService';

@IonicPage()
@Component({
  selector: 'page-switch-stats',
  templateUrl: 'switch-stats.html'
})
export class SwitchStatsPage {
  switch; name; durationHrs; durationMins; sboards1; devicesInfo;
  constructor(public navCtrl: NavController, public navParams: NavParams, private alert: AlertService, private local: LocalStorageService, private LocalComm: LocalCommService, private ds: DataService) {
    this.switch = this.navParams.get('switch');
    console.log(this.switch);
    this.name = this.switch.name;
    this.durationHrs = this.switch.durationHrs;
    this.durationMins = this.switch.durationMins;
    this.sboards1 = this.navParams.get('sboards1');
    this.devicesInfo = this.navParams.get('devicesInfo');
  }

  ionViewWillEnter() {
    console.log('ionViewDidLoad SwitchStatsPage');

  }
  goToHistory() {
    this.navCtrl.push('SwitchHistoryPage', {
      switch: this.switch
    });
  }
  goToSchedules() {
    this.navCtrl.push('DisplaySchedulePage', {
      switch: this.switch
    });
  }
  testConnection() {/*
    this.ds.getDeviceAvailableOnline(data => {
      this.sboards1[this.switch.sboard_id].on_available = data.status;
      if (data.status) this.showOKAlert();
      this.LocalComm.pingDevice('http://', this.local.getLocalIp(this.devicesInfo[this.switch.device_id].wifi_local_name) + "/api/switch?apikey=" + this.devicesInfo[this.switch.device_id].apikey)
        .subscribe(data2 => {
          if (!data.status) this.showOKAlert();
          this.sboards1[this.switch.sboard_id].off_available = true;
        }, error => {
          if (!data.status) this.showErrAlert();
          console.log(error);
          this.sboards1[this.switch.sboard_id].off_available = false;
        });
    }, error => {
      console.log(error);
      this.sboards1[this.switch.sboard_id].on_available = false;
      this.LocalComm.pingDevice('http://', this.local.getLocalIp(this.devicesInfo[this.switch.device_id].wifi_local_name) + "/api/switch?apikey=" + this.devicesInfo[this.switch.device_id].apikey)
        .subscribe(data => {
          this.showOKAlert();
          this.sboards1[this.switch.sboard_id].off_available = true;
        }, error => {
          console.log(error);
          this.showErrAlert();
          this.sboards1[this.switch.sboard_id].off_available = false;
        });
    }, true);
*/  this.show_spinner = true;
    this.startTimer();
    this.ds.testConnection(
      this.local.getLocalIp(this.devicesInfo[this.switch.device_id].wifi_local_name),
      this.switch.device_id,
      this.devicesInfo[this.switch.device_id].apikey,
      this.sboards1[this.switch.sboard_id])
      .then(() => {
        this.showOKAlert();
      }).catch(() => {
        this.showErrAlert();
      })

  }

  show_spinner = false; timer = 40; timer_interval;
  showErrAlert() {
    this.show_spinner = false;
    this.alert.showAlert('Connection Failed', 'Unable to connect to the switch');
  }
  showOKAlert() {
    this.show_spinner = false;
    this.alert.showAlert('Connection Success', 'Switch responded');

  }
  startTimer() {
    this.timer = 40;
    this.timer_interval = setInterval(() => {
      this.timer--;
      if (this.timer == 0) {
        clearInterval(this.timer_interval);
      }
    }, 1000)
  }

  ionViewWillLeave() {
    clearInterval(this.timer_interval);
  }

}
