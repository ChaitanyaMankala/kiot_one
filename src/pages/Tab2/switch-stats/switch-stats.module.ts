import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SwitchStatsPage } from './switch-stats';
import { PipesModule } from '../../../pipes/pipes.module';

@NgModule({
    declarations: [
        SwitchStatsPage,
    ],
    imports: [
        IonicPageModule.forChild(SwitchStatsPage),
        PipesModule
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class SwitchStatsPageModule { }