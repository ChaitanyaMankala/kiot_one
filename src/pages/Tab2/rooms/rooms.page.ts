import { Component, OnInit, NgZone } from '@angular/core';
import { NavController, App, AlertController, ModalController, IonicPage } from 'ionic-angular';
import {
  MqttMessage,
  MqttModule,
  MqttService
} from 'ngx-mqtt';


//providers
import { DataService } from '../../../providers/DataService';
import { LocalStorageService } from '../../../providers/LocalStorageService';
import { AlertService } from '../../../providers/AlertService';
import { ToastService } from '../../../providers/ToastService';
import { LocalCommService } from "../../../providers/LocalCommunicationServ";
import { IntroService } from '../../../providers/IntroService';
import { Helper } from '../../../providers/Helper';

//pages
import { SwitchesInRoomPage } from '../switches-in-room/switches-in-room';
import { NotificationDispPage } from '../../Tab1/notification-disp/notification-disp';

const introJs = require('../../../../node_modules/intro.js/intro.js');

//Models
import { getSboards } from '../../../models/getSboards';

@IonicPage()
@Component({
  selector: 'page-rooms',
  templateUrl: 'rooms.page.html',
  providers: []
})
export class RoomsPage {
  data = [];
  def;
  switches = [];
  kiot_internet;
  dont_show_loader;
  device_info = [];
  observableArray = [];
  observableArray2 = [];
  _topics; _suback; no_rooms_loader = true;
  intro = new introJs.introJs();
  constructor(public navCtrl: NavController, private help: Helper, private is: IntroService, private LocalComm: LocalCommService, private _mqttService: MqttService, private modalCtrl: ModalController, private toast: ToastService, private alert: AlertService, private zone: NgZone, private ds: DataService, public alertCtrl: AlertController, private app: App, private local: LocalStorageService) {
    this._topics = {};
    this.device_statuses = {};

  }

  retry_loading = false;
  retry() {
    this.retry_loading = true;
    this.ds.ping()
      .then(data => {
        console.log(data);
        this.kiot_internet = this.local.getInternetStatus();
        this.retry_loading = false;
        if (this.kiot_internet == 'online') {
          this.ionViewWillEnter();
          this.toast.bottomToast_Short('kiot is back Online!');
        }
      });
  }
  num_of_notifications;
  goToNotifications() {
    let noti_modal = this.modalCtrl.create('NotificationDispPage');
    noti_modal.present();
    noti_modal.onWillDismiss((data) => {
      if (data) {
        this.num_of_notifications = 0;
      }
    });

  }

  ionViewDidLoad() {
    this.setupIntro();
  }
  setupIntro() {
    setTimeout(() => {
      this.intro.addSteps([
        {
          element: '#room_step_one',
          intro: "<ul class='no-bullet'><li><i class='total'></i>Total number of switches</li><li class='unavailable'>Total number of Unreachable switches</li><li class='turned-on'>Total number of switches(Turned ON)</li><li class='turned-off'>Total number of switches(Turned OFF)</li></ul>",
          position: 'bottom'
        },
        {
          element: '#room_step_two',
          intro: "All switches in room can be turned off in a Tap!",
          position: 'bottom'
        },
        {
          element: '#room_step_three',
          intro: "Number of switches, which are turned On",
          position: 'left'
        }
      ]);
    }, 500);
  }

  startIntro(delay?) {
    if (this.intro._introItems.length) {
      this.intro.start();
    }
    else {
      setTimeout(() => {
        this.intro.start();
      }, delay ? delay : 1500);
    }

  }
  device_statuses;
  ionViewWillEnter() {
    this.kiot_internet = this.local.getInternetStatus();
    this.device_statuses = {};
    this.def = this.local.getCurrentHome();
    if (this.kiot_internet == 'online') {
      this._suback = this._mqttService.onSuback.subscribe((e) => {
        console.log(e);
        if (e.filter) {
          if (this._topics[e.filter]) {
            this._topics[e.filter].granted = e.granted || false;
            if (this._topics[e.filter].granted) {
              this._mqttService.unsafePublish(this._topics[e.filter].pubTpoic, 'a');
              console.log(this._topics[e.filter].pubTpoic);
            }
          }
        }
      })
      this.ds.getUnseenNotifications(
        data => {
          this.num_of_notifications = data;
          console.log(data);
        }, error => {
          console.log(error);
        });
    }

    /*
  this.zone.run(() => {
      this.ds.getSboards(this.def,
      data => {
            console.log(data);
            this.data = data;
            for(let x of data){
              x.switches = [];
              x.active_count = 0;
                let k=0;
                for(let i of x.devices){
                  for(let j of i.switchappliances){
                    x.switches[k]=j;
                    x.switches[k].sb_id=i._id;
                    x.switches[k].device_id=i.device_id;
                    if(j.switch_type=='regulator') x.switches[k].isRegulator = true;
                    else x.switches[k].isRegulator = false;
                    if(j.current_status == 0){
                      x.switches[k].isActive = false;
                    //  console.log(x.switches[k].active_count);
                    }
                    else if(j.current_status == 1){
                      x.switches[k].isActive = true;
                      x.active_count++;
                    } 
                    k++;
                    
                  }
                }
                console.log(x.active_count);
            }
            //console.log(this.items);
          }, error => {
              console.log(error);
              
      });
  });*/

    if (this.def) {
      this.dont_show_loader = this.data ? false : true;
      if (this.data && !this.data.length) {
        this.no_rooms_loader = true;
      }
      this.ds.getRooms(this.def,
        (data) => {
          this.no_rooms_loader = false;
          this.data = [];
          for (let i of data) {
            if (i.totalAppliances != 0)
              this.data.push(i)
          }
          //this.data = data;
          if (!this.is.hasSeen('RoomsPage') && this.data.length) {
            this.startIntro();
            this.is.setIntroSeen('RoomsPage');
          }
          for (let i of this.data) {
            i.show_loader = true;
            if (i.device_info.length) {
              let ind = -1;
              for (let j of i.device_info) {
                ind++;
                this.calculateActive(ind);

                if (j) {
                  if (!this.device_statuses[j.device_id]) {
                    this.device_statuses[j.device_id] = {
                      on_available: false,
                      off_available: false
                    };
                  }
                  if (this.local.getLocalIp(j.wifi_local_name)) {
                    this.local.setSecret(j.device_id, j.apiSecret);
                    this.LocalComm.pingDevice('http://', this.local.getLocalIp(j.wifi_local_name) + "/api/switch?apikey=" + j.apikey, j.device_id)
                      .subscribe((status_data: any) => {
                        let body = JSON.parse(status_data._body)
                        if (body.relays) {
                          this.device_statuses[j.device_id].off_available = true;
                          this.device_statuses[j.device_id].relays = body.relays;
                          if (ind == i.device_info.length - 1) {
                            i.show_loader = false;
                          }
                          j.activeAppliances = 0;
                          for (let k of body.relays) {
                            if (k) {
                              j.activeAppliances++;
                            }
                          }
                        }
                        this.calculateActive(ind);
                      }, error => {
                        console.log(error);
                       
                      });

                  }
                  else {
                    i.show_loader = false;
                  }

                  //  console.log(this._mqttService.observables);

                  let pubTpoic = '/KU/' + j.device_id + '/' + this.local.getCurrentHome() + '/ping/set';
                  let subTpoic = '/KU/' + j.device_id + '/' + this.local.getCurrentHome() + '/ping/get';
                  if (!this._topics[subTpoic]) {
                    this._topics[subTpoic] = {
                      pubTpoic: pubTpoic,
                      granted: false
                    }
                  }

                  this.observableArray.push(this._mqttService.observe(subTpoic)
                    .subscribe((data: MqttMessage) => {
                      console.log(j.device_id);
                      // console.log(data);
                      j.online_status = true;
                      i.show_loader = false;
                      j.activeAppliances = 0;
                      let x;
                      x = data.payload.toString();
                      x = JSON.parse(x);
                      let body = JSON.parse(this.help.getMqttPayload(x.payload, x.meta, j.encryptKey));
                      if (body.switch) {
                        let y = JSON.parse(body.switch);
                        if (y && y.relays) {
                          this.device_statuses[j.device_id].on_available = true;
                          this.device_statuses[j.device_id].relays = y.relays;
                          for (let k of y.relays) {
                            if (k) {
                              j.activeAppliances = parseInt(j.activeAppliances);
                              j.activeAppliances++;
                            }
                          }
                        }
                      }
                      if (body.sns) {
                        let y = JSON.parse(body.sns);
                        if (y && y.temphumid) {
                          if (y.temphumid.split(',')[0] != 0) {
                            j.tempr = y.temphumid.split(',')[0];
                            this.calculateTempr(ind, 'tempr');
                          }
                        }
                      }
                      if (body.power) {
                        let y = JSON.parse(body.power);
                        if (y && y.power != undefined) {
                          j.power = y.power;
                          this.calculateTempr(ind, 'power');
                        }
                      }

                      this.calculateActive(ind);
                    }));


                  /*
                  let pubTpoic2 = '/KU/' + j.device_id + '/sns/set';
                  console.log(pubTpoic2);

                  this._mqttService.unsafePublish(pubTpoic2, JSON.stringify({ "q": "temp" }), { qos: 1 });

                  this.observableArray.push(this._mqttService.observe('/KU/' + j.device_id + '/sns/get')
                    .subscribe((data: MqttMessage) => {
                      console.log(j.device_id);
                      let x;
                      x = data.payload.toString();
                      x = JSON.parse(x);
                      let body = JSON.parse(this.help.getMqttPayload(x.payload, x.meta, j.encryptKey));
                      console.log(body);
                      j.tempr = body.temphumid.split(',')[0];
                      this.calculateTempr(ind, 'tempr');
                    }));
                  this.observableArray.push(this._mqttService.observe('/KU/' + j.device_id + '/power/get')
                    .subscribe((data: MqttMessage) => {

                      let x;
                      x = data.payload.toString();
                      x = JSON.parse(x);
                      let body = JSON.parse(this.help.getMqttPayload(x.payload, x.meta, j.encryptKey));
                      console.log(body);
                      j.power = body.power;
                      this.calculateTempr(ind, 'power');
                    }));*/



                } else {
                  i.show_loader = false;
                }
              }
            }
            else {
              i.show_loader = false;
            }

          }

        },
        (error) => {
          this.no_rooms_loader = false;
          console.log(error);
        }, this.dont_show_loader);

    }

  }
  ionViewWillLeave() {
    this.unsubscribeAll();

  }
  unsubscribeAll(refresh?) {
    try {
      if (!refresh) this._suback.unsubscribe();
      for (let i of this.observableArray) {
        i.unsubscribe();
      }
    }
    catch (err) {
      console.log(err);
    }
  }
  calculateActive(ind) {
    let flag;
    for (let i of this.data) {
      flag = false;
      i.activeAppliance = 0;
      for (let j of i.device_info) {
        if (j && (j.activeAppliances || j.activeAppliances == 0)) {
          flag = true;
          i.activeAppliance = parseInt(j.activeAppliances) + i.activeAppliance;
        }
      }
      if (!flag) {
        i.activeAppliance = '-';
      }
    }
  }
  calculateTempr(ind, key) {
    let flag;
    let c = 0;
    for (let i of this.data) {
      flag = false;
      i[key] = 0;
      for (let j of i.device_info) {
        if ((j) && (j[key] || (key == 'power' && j[key] == 0))) {
          flag = true;
          i[key] += parseFloat(j[key]);
          c++;
        }
      }
      if (!flag) {
        i[key] = null;
      }
      else {
        if (c != 0) {
          i[key] = i[key] / c;
        }
      }
    }

  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    //this.ionViewWillLeave();
    this.unsubscribeAll(true);
    this.ionViewWillEnter();
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 99);
  }
  goToSwitchesinTheRoom(room) {
    this.navCtrl.push('SwitchesInRoomPage', {
      room_id: room.room_id,
      def: this.def,
      room_name: room.room_name,
      device_statuses: this.device_statuses
    }, { animate: false });
  }
  turnOffRoom(i) {
    if (i.activeAppliance != '-' && i.activeAppliance != 0) {
      this.alert.showConfirm('Switch Off?',
        'this switches off all the appliances in ' + i.room_name, 'Cancel', 'Switch Off', () => { },
        () => {
          this.turnOffRoomAction(i)
            .then((data) => {
              console.log(data);
              this.ionViewWillEnter();
            })
            .catch(error => {
              console.log(error);
              this.alert.showAlert('Error', 'Unable to communicate with one/more switchboard(s)');
              this.ionViewWillEnter();
            });
        })

    }
  }
  turnOffRoomAction(i) {
    if (this.kiot_internet == 'online') {
      return new Promise((resolve, reject) => {
        this.ds.postTurnOffRoom(
          {
            "room": i.room_id
          },
          (data) => {
            console.log(data);
            i.activeAppliances = 0;
            resolve(1);
          },
          (error) => {
            console.log(error);
            //this.alert.showAlert('Cannot Switch Off', error + 'Please Try Again');
            reject(error);
          });
      });
    }
    else if (this.kiot_internet == 'offline' && i.device_info[0]) {
      let promiseArray = [];
      for (let j of i.device_info) {
        this.LocalComm.sendSwitchSignal(j.wifi_local_name, 0, 0, j.apikey, false, j.device_id)
          .subscribe(data => {
            promiseArray.push(Promise.resolve(1));
          }, error => {
            promiseArray.push(Promise.reject(error));
          });
      }
      return Promise.all(promiseArray);
    }
  }

}