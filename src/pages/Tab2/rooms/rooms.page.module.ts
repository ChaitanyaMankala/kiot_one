import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RoomsPage } from './rooms.page';

@NgModule({
    declarations: [
        RoomsPage,
    ],
    imports: [
        IonicPageModule.forChild(RoomsPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class RoomsPageModule { }