import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SwitchesInRoomPage } from './switches-in-room';

import { PipesModule } from '../../../pipes/pipes.module';
import { RoundProgressModule } from 'angular-svg-round-progressbar';

@NgModule({
    declarations: [
        SwitchesInRoomPage,
    ],
    imports: [
        IonicPageModule.forChild(SwitchesInRoomPage),
        PipesModule,
        RoundProgressModule
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class SwitchesInRoomPageModule { }