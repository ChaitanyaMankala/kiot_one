import { Component, ViewChild, EventEmitter } from '@angular/core';
import { NavController, NavParams, AlertController, Nav, App, ModalController, IonicPage } from 'ionic-angular';
import {
  MqttMessage,
  MqttModule,
  MqttService
} from 'ngx-mqtt';
//import { Chart } from 'chart.js';

import { Chart } from 'chart.js';

//providers
import { DataService } from '../../../providers/DataService';
import { Helper } from '../../../providers/Helper';
import { AlertService } from '../../../providers/AlertService';
import { LocalStorageService } from '../../../providers/LocalStorageService';
import { MetaService } from '../../../providers/MetaService';
import { LocalCommService } from "../../../providers/LocalCommunicationServ";
import { ToastService } from '../../../providers/ToastService';

//pages
import { SwitchStatsPage } from '../switch-stats/switch-stats';
import { RealRemotePage } from '../../Tab4/real-remote/real-remote';
import { HomeListPage } from '../../Tab5/4sboards/home-list/home-list';
import { PowerStatsPage } from '../power-stats/power-stats';

var ICO = require('./icon-config.json');

@IonicPage()
@Component({
  selector: 'page-switches-in-room',
  templateUrl: 'switches-in-room.html'
})
export class SwitchesInRoomPage {

  @ViewChild('lineCanvas') lineCanvas;

  lineChart: any;


  room_id; app_icons = ICO.icons; prev_statuses; analog; last_motion_time; _topics; _suback; switches = []; motion = 'unreachable'; room_energy; switchShow; observableArray = []; show_chart = false; room_name; isActive; data = []; devicesInfo; def; display_colors; sboards1; phone_online; on_off_string; room_meta;
  constructor(public navCtrl: NavController, private _mqttService: MqttService, private toast: ToastService, private modalCtrl: ModalController, private app: App, private nav: Nav, private meta: MetaService, private LocalComm: LocalCommService, public navParams: NavParams, private local: LocalStorageService, private alertCtrl: AlertController, private ds: DataService, private help: Helper, private alert: AlertService) {
    this.room_name = this.navParams.get('room_name');
    this._topics = {};
    let x = this.navParams.get('device_statuses');
    if (x) {
      this.prev_statuses = this.clone(x);
    }
    this.sboards1 = {};
    console.log(this.prev_statuses);

    //console.log(this.subackListen.granted);
    //this.app_icons = ICO.icons;

  }
  clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
      if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
  }

  mqttPublish(one?) {
    if (one) {
      this._mqttService.unsafePublish(this._topics[one].pubTpoic, 'a');
      console.log(this._topics[one].pubTpoic);
    }
    else if (this._topics && this._topics != {}) {
      for (let i of Object.keys(this._topics)) {
        this._mqttService.unsafePublish(this._topics[i].pubTpoic, 'a');
      }
    }
  }

  ionViewDidLoad() {
    this.unsubscribeAll();
    this._suback = this._mqttService.onSuback.subscribe((e) => {
      console.log(e);
      if (e.filter) {
        if (this._topics[e.filter]) {
          this._topics[e.filter].granted = e.granted || false;
          if (this._topics[e.filter].granted) {
            this.mqttPublish(e.filter);
          }
        }
      }
    })
    this.def = this.navParams.get('def');
    if (this.meta.getMetaDataFromLocal()) {
      this.display_colors = JSON.parse(this.meta.getMetaDataFromLocal()).meta.display_colors;
    }
    console.log(this.display_colors);
    this.room_id = this.navParams.get('room_id');
    this.phone_online = this.local.getInternetStatus() == 'online' ? true : false;
    this.sboards1 = [];
    this.ds.getSwitchesInRoom(this.room_id,
      (data) => {
        console.log(data);

        if (this.data && data.switchAppliances && (this.data.length == 0 || this.data.length < data.switchAppliances.length)) {
          this.data = data.switchAppliances;
        }
        else if (this.data && data.appliances && this.data.length != 0 && this.data.length == data.appliances.length) {

          /*this.data = this.help.sorter(this.data, '_id', 'sboard_id');
          let sorting = this.data.slice();
          let result = []

          sorting.forEach(function (key) {
            var found = false;
            this.data = this.data.filter(function (item) {
              if (!found && item._id == key._id) {
                result.push(item);
                found = true;
                return false;
              } else
                return true;
            })
          })
          this.data = result;
          */


        }
        if (1) {
          let x;
          x = {};
          for (let i of this.data) {
            if (!x[i.sboard_id]) {
              x[i.sboard_id] = [];
            }
            x[i.sboard_id].push(i);
          }
          let result = [];
          for (let i of Object.keys(x)) {
            x[i] = this.help.sorter(x[i], 'switch_no', '');
            result = result.concat(x[i]);
          }
          this.data = result;
        }
        this.devicesInfo = data.devicesInfo;
        let x = false;
        if (this.device_list && this.device_list.length == 0) {
          x = true;
        }
        this.device_list = [];
        for (let i of Object.keys(this.devicesInfo)) {
          if (this.devicesInfo[i]._id) {
            this.device_list.push(this.devicesInfo[i]._id);
          }
        }
        if (x) {
          this.startChart();
        }
        if (this.last_ping == -1 || (this.last_ping != -1 && (Date.now() - this.last_ping > 1500))) {
          this.last_ping = Date.now();
          this.pingAllDevices(false, this.prev_statuses);

        }

        this.prev_statuses = {};

      },
      (error) => {
        console.log(error);
      }, true);
    if (this.phone_online == 'offline') {
      this.ds.getRooms(this.def,
        data => {
          this.room_meta = data;
        }, error => {
          console.log(error);
        })
    }
    console.log(this.sboards1);
  }
  device_list = [];
  last_ping = -1;
  pingAllDevices(no_online?, prev_stat?) {
    let duration = [];
    let ind = 0;
    let buffer;
    buffer = {};
    for (let j of this.data) {
      if (!this.sboards1[j.device_id]) {
        this.sboards1[j.device_id] = {};
      }
      if (prev_stat && prev_stat[j.device_id]) {

        this.sboards1[j.device_id].on_available = prev_stat[j.device_id].on_available;
        this.sboards1[j.device_id].off_available = prev_stat[j.device_id].off_available;
        this.calculateActive({ relays: prev_stat[j.device_id].relays, dimmer: null }, j);

      }
      let x = this.sboards1[j.device_id]
      if (!buffer[j.sboard_id.split('_')[2] || j.sboard_id]) {
        buffer[j.sboard_id.split('_')[2] || j.sboard_id] = 1;
        //this.sboards1[j.device_id] = {};

        let pubTpoic = '/KU/' + j.device_id + '/' + this.local.getCurrentHome() + '/ping/set';
        let subTpoic = '/KU/' + j.device_id + '/' + this.local.getCurrentHome() + '/ping/get';
        if (!this._topics[subTpoic]) {
          this._topics[subTpoic] = {
            pubTpoic: pubTpoic,
            granted: false
          }
        }

        if (!no_online) {
          this.observableArray.push(this._mqttService.observe(subTpoic)

            .subscribe((data: MqttMessage) => {
              console.log(j.device_id);
              let x;
              x = data.payload.toString();
              x = JSON.parse(x);
              let body = JSON.parse(this.help.getMqttPayload(x.payload, x.meta, this.devicesInfo[j.device_id].encryptKey));
              this.sboards1[j.device_id].on_available = true;
              if (body.switch) {
                let x = JSON.parse(body.switch);
                if (x) {
                  this.calculateActive(x, j);
                }
              }
              if (body.sns) {
                let y = JSON.parse(body.sns);
                if (y && y.temphumid) {
                  this.devicesInfo[j.device_id].tempr = y.temphumid.split(',')[0];
                  this.devicesInfo[j.device_id].hum = y.temphumid.split(',')[1];
                  this.calculateTempr(ind);
                }
                if (y && (y.motion || y.motion == 0)) {
                  this.last_motion_time = Date.now();
                  if (y.motion != 0) {
                    this.motion = "detected";
                  }
                  else {
                    this.motion = "not_detected";
                  }
                }
                if (y && (y.analog && y.analog != 0)) {
                  this.analog = y.analog;
                }
              }
              //              console.log(JSON.parse(data.payload.toString()));
            }));
        }
        //setTimeout(() => { this._mqttService.unsafePublish(pubTpoic, 'a'); }, 1000);

        /*
        let pubTpoic2 = '/KU/' + j.device_id + '/sns/set';
        console.log(pubTpoic2);
  
        this._mqttService.unsafePublish(pubTpoic2, "", { qos: 1 });
  
        this.observableArray.push(this._mqttService.observe('/KU/' + j.device_id + '/sns/get')
          .subscribe((data: MqttMessage) => {
            console.log(j.device_id);
            let x;
            x = data.payload.toString();
            x = JSON.parse(x);
            let body = JSON.parse(this.help.getMqttPayload(x.payload, x.meta, this.devicesInfo[j.device_id].encryptKey));
            console.log(body);
            this.devicesInfo[j.device_id].tempr = body.temphumid.split(',')[0];
            this.devicesInfo[j.device_id].hum = body.temphumid.split(',')[1];
            this.calculateTempr(ind);
          }));
          */


        if (this.devicesInfo[j.device_id]) {
          this.local.setSecret(j.device_id, this.devicesInfo[j.device_id].apiSecret);
          this.LocalComm.pingDevice('http://', this.local.getLocalIp(this.devicesInfo[j.device_id].wifi_local_name) + "/api/switch?apikey=" + this.devicesInfo[j.device_id].apikey, j.device_id)
            .subscribe((data11: any) => {
              this.sboards1[j.device_id].off_available = true;
              let y = 0;

              let body = JSON.parse(data11._body);
              this.calculateActive(body, j);
            }, error => {
              console.log(error);
              this.sboards1[j.device_id].off_available = false;
            });
        }
      }
      ind++;
      if (j.current_status == 1) {
        j.isActive = true;
      }
      else if (j.current_status == 0) {
        j.isActive = false;
      }

      if (j.switch_type == 'regulator') j.isRegulator = true;
      else {
        j.isRegulator = false;
      }
      //duration = this.help.findDuration(j.switch_events, j.isActive1);
      duration = j.runn_hr;
      j.durationHrs = duration[0];
      j.durationMins = duration[1];
      j.progressBarDuration = duration[2];
      if (j.progressBarDuration == 0) j.isRounded = false;
      else j.isRounded = true;
      j.wattage = Math.floor((parseInt(j.active_wattage) * duration[0]) + (j.active_wattage * (duration[1] / 60)));
      //console.log(j.duration);
      if (j.appliance_type == 'TV' || j.appliance_type == 'Set Top Box' || j.appliance_type == 'Air Conditioner') {
        j.show_remote = true;
      }
      else {
        j.show_remote = false;
      }

    }
  }
  motionToast() {
    let msg = '';
    msg = this.help.motionToast(this.motion, this.last_motion_time);
    if (msg != '') {
      this.toast.bottomToast_Short(msg);
    }
  }
  ionViewWillLeave() {
    this.unsubscribeAll();
  }
  unsubscribeAll() {
    if (this._suback) {
      this._suback.unsubscribe();
    }
    for (let i of this.observableArray) {
      i.unsubscribe();
    }
  }

  startChart() {
    let c = new Date(Date.now());
    let d = new Date(c);
    d.setHours(0, 0, 0, 0);
    this.ds.postGetPowerData({
      "device_id": this.device_list,
      "timeto": c.getTime(),
      "timefrom": d.getTime(),
      "interval": "today"
    }, data => {
      console.log(data);
      if (data.length) {
        this.show_chart = true;
        let chartData = [];
        let labels = [];
        let sorted_data;
        sorted_data = this.help.sorter(data, "_id", "");
        for (let i of sorted_data) {
          chartData.push(i.avg_power);
          if (i._id < 12) {
            if (i._id == 0) {
              labels.push('12AM');
            }
            else {
              labels.push(i._id + 'AM');
            }
          }
          else {
            if (i._id == 12) {
              labels.push('12PM');
            }
            else {
              labels.push((parseInt(i._id) - 12).toString() + 'PM');
            }
          }
        }
        this.setupChart(chartData, labels);
      }
    }, error => {
      console.log(error);
    });
    this.ds.postGetEnergyData({
      "device_id": this.device_list,
      "timeto": c.getTime(),
      "timefrom": d.getTime(),
      "interval": "today"
    }, data => {
      if (data.length) {
        // data = this.help.sorter(data, 'avg_energy', '')
        let x = 0;
        for (let i of data) {
          x += parseFloat(i.avg_energy);
        }
        if (x) {
          this.room_energy = x;
          this.room_energy = Math.round(this.room_energy * 100) / 100;
        }
        /*if (data[data.length - 1].avg_energy && parseInt(data[data.length - 1].avg_energy)) {
          this.room_energy = Math.round(data[data.length - 1].avg_energy * 100) / 100;
        }*/
      }
    }, error => {
      console.log(error);
    })
  }

  setupChart(data, labels) {
    let gradient = this.lineCanvas.nativeElement.getContext('2d').createLinearGradient(0, 0, 0, 450);

    gradient.addColorStop(0, '#ff9500');
    gradient.addColorStop(0.4, '#1c1c1d');
    gradient.addColorStop(1, '#1c1c1d');
    this.lineChart = new Chart(this.lineCanvas.nativeElement, {

      type: 'line',
      data: {
        labels: labels,
        datasets: [
          {
            label: 'Power Consumed (W)',
            backgroundColor: gradient,
            borderColor: 'rgba(255,255,255,0)',
            pointBackgroundColor: 'transparent',
            borderWidth: 1,
            pointBorderColor: "transparent",
            data: data,
            lineTension: 0
          }
        ]
      },
      options: {
        layout: {
          padding: {
            left: 10,
            right: 15,
            top: 20,
            bottom: 10
          }
        },
        legend: {
          display: false
        },
        scales: {
          xAxes: [{
            display: true,
            gridLines: {
              color: 'rgba(255,255,255,0.03)'
            },
            ticks: {
              autoSkip: true,
              maxTicksLimit: 6,
              maxRotation: 0,
              minRotation: 0,
              fontFamily: "SanFranciscoLight"
            }
          }],
          yAxes: [{
            display: true,
            gridLines: {
              color: 'rgba(255,255,255,0.03)'
            },
            ticks: {
              fontFamily: "SanFranciscoLight",
              maxTicksLimit: 4
            }
          }]
        }
      }


    });

  }
  goToChartDetails() {
    if (Object.keys(this.sboards1).length) {
      this.modalCtrl.create('PowerStatsPage', {
        devices: this.device_list
      }).present();
    }
  }

  calculateActive(body, j) {
    if (!body) {
      body = {
        relays: [],
        dimmer: null
      };
    }
    let relayStatus = body.relays;
    let dimmerStatus = body.dimmer;

    if (!relayStatus || !(relayStatus.constructor === Array)) {
      relayStatus = [];
    }
    for (let x of this.data) {
      if (x.device_id == j.device_id) {
        x.current_status = -1;
        relayStatus.forEach((val, index) => {
          if (index + 1 == x.switch_no) {
            x.isActive1 = val;
          }
        });
        if (x.switch_type == "regulator" && dimmerStatus) {
          x.current_dimm = dimmerStatus;
        }
      }
    }
  }
  room_tempr; room_hum;
  calculateTempr(ind) {
    let flag = false;
    let c = 0;
    this.room_tempr = 0;
    this.room_hum = 0;
    for (let i of this.data) {
      if (this.devicesInfo[i.device_id].tempr) {
        flag = true;
        this.room_tempr += parseInt(this.devicesInfo[i.device_id].tempr);
        this.room_hum += parseInt(this.devicesInfo[i.device_id].hum)
        this.devicesInfo[i.device_id].tempr = null;
        this.devicesInfo[i.device_id].hum = null;
        c++;
      }
      if (!flag) {
        //this.room_tempr = 27;
        //this.room_hum = 75;
      }
      else {
        if (c != 0) {
          this.room_tempr = this.room_tempr / c;
          this.room_hum = this.room_hum / c;
        }
      }
    }

  }
  act; sw_no; on_off;
  trigger(i, ind, obj_for_dimm?) {
    if (!i.show_loader) {
      i.show_loader = true;
      this.on_off_string = "";
      this.on_off_string = this.local.getInternetStatus() == 'online' ? '1' : this.local.getInternetStatus() == 'offline' ? '0' : '-1';
      this.on_off_string += this.sboards1[i.device_id].on_available ? '1' : '0';
      this.on_off_string += this.sboards1[i.device_id].off_available ? '1' : '0';
      this.sw_no = i.switch_no;
      if (i.isActive1 == true) this.on_off = 0;
      else if (i.isActive1 == false) this.on_off = 1;
      let time = Date.now();
      let obj = obj_for_dimm ? obj_for_dimm : {
        "device": i.device_id,
        "sb": i.sboard_id,
        "switch_no": this.sw_no.toString(),
        "switch_state": this.on_off,
        "key": time,
        "mobId": "98239023",
        "act_type": "switch",
        "id": (i._id).split("_")[2]
      };

      console.log(obj);
      switch (this.on_off_string) {
        case '111': {
          this.sendOnlineSignal(ind, obj, time, obj_for_dimm ? true : false);
          break;
        }
        case '110': {
          this.sendOnlineSignal(ind, obj, time, obj_for_dimm ? true : false);
          break;
        }
        case '101': {
          this.sendLocalSignal(i.device_id, time, i.isActive1, obj, parseInt(this.sw_no), this.on_off, ind, obj_for_dimm ? true : false);
          break;
        }
        case '011': {
          this.sendLocalSignal(i.device_id, time, i.isActive1, obj, parseInt(this.sw_no), this.on_off, ind, obj_for_dimm ? true : false);
          break;
        }
        case '001': {
          this.sendLocalSignal(i.device_id, time, i.isActive1, obj, parseInt(this.sw_no), this.on_off, ind, obj_for_dimm ? true : false);
          break;
        }
        case '000': {
          i.show_loader = false;
          this.alert.showAlert('Unable to Communicate', 'We cannot communicate with the KIOT Device');
          break;
        }
        case '100': {
          i.show_loader = false;
          this.alert.showAlert('Unable to Communicate', 'We cannot communicate with the KIOT Device');
          break;
        }
        case '010': {
          i.show_loader = false;
          this.alert.showAlert('Unable to Communicate', 'We cannot communicate with the KIOT Device');
          break;
        }
        default: {
          i.show_loader = false;
          console.log(this.on_off_string);
          break;
        }
      }
    }
  }

  sendLocalSignal(device_id, time, now, obj, sw, act, ind, is_dimm?) {
    let device_url = "kiot" + this.data[ind].device_id + ".local";
    // let actions = [];
    let switchAction = {};
    switchAction[sw] = act;
    if (is_dimm) {
      act = obj.dimm_value;
    }
    this.LocalComm.sendSwitchSignal(this.devicesInfo[device_id].wifi_local_name, sw, act, this.devicesInfo[device_id].apikey, is_dimm, device_id)
      .subscribe(res => {
        this.data[ind].show_loader = false;
        console.log(res);
        this.LocalComm.pushInOfflineBuffer(obj);
        if (!is_dimm) {
          this.data[ind].isActive1 = !this.data[ind].isActive1;
        }
        if (now != this.data[ind].isActive1) {
          let obj2 = {
            timestamp: time,
            event_type: this.on_off,
            username: this.local.getUser().name
          }
          //this.data[ind].switch_events.push(obj2);
        }
        //return this.on_off ? true : false;
        if (this.room_meta) {

        }

      }, err => {
        this.data[ind].show_loader = false;
        console.log("I am error");
        console.log(err);
      });
  }
  sendOnlineSignal(ind, obj, time, is_dimm?) {
    this.ds.postTriggerSwitch(obj,
      (data) => {
        this.data[ind].show_loader = false;
        console.log(data);
        if (data.status == 1 && !is_dimm) {
          this.data[ind].isActive1 = !this.data[ind].isActive1;
          let obj2 = {
            timestamp: time,
            event_type: this.on_off,
            username: this.local.getUser().name
          }
          //this.data[ind].switch_events.push(obj2);
        }
      },
      (error) => {
        this.data[ind].show_loader = false;
        console.log(error);
      });
  }
  updateDimm(i, ind) {
    let obj = {
      "device": i.sboard_id,
      "sb": i.sboard_id,
      "switch_no": i.switch_no.toString(),
      "key": Date.now(),
      "mobId": "98239023",
      "act_type": "regulate",
      "id": (i._id).split("_")[2],
      "dimm_value": i.current_dimm
    };
    this.trigger(i, ind, obj);
    /*
    if (this.local.getInternetStatus() == 'online') {
      console.log(obj);
      this.ds.postTriggerSwitch(obj,
        (data) => {
          console.log(data);
        },
        (error) => {
          console.log(error);
        });
    }
    else if (this.local.getInternetStatus() == 'offline') {
      this.sendLocalSignal(null,null,obj, 'dimmer', i.current_dimm, ind, true);
    }*/
  }
  roundProgressClick(switchData: any, roomDetails: any) {

    for (let i = 0; i < this.switches.length; i++) {
      if (switchData._id == this.switches[i]._id) {
        this.switchShow = switchData._id;
        this.switches[i].isActive1 = !this.switches[i].isActive1;

      }
    }
    let obj = {
      "device": roomDetails.device_id,
      "sb": switchData.sboard_id,
      "act": "21",
      "key": "12345",
      "mobId": "9015555",
      "act_type": "switch"
    }

    /*this.ds.postDeviceevents(obj,
       data => {
             console.log(data);
           }, error => {
               console.log(error);            
       });*/




  }
  doRefresh(refresher) {
    // console.log('Begin async operation', refresher);
    //this.ionViewWillLeave();
    // this.ionViewDidLoad();
    this.pingAllDevices(true);
    this.mqttPublish();
    setTimeout(() => {
      refresher.complete();
    }, 200);
  }
  goToStats(item) {
    console.log(item);
    this.navCtrl.push('SwitchStatsPage', {
      switch: item,
      sboards1: this.sboards1,
      devicesInfo: this.devicesInfo
    });
  }
  goToRemote(i) {
    let someArray = [];

    this.ds.getIrAppliances(this.def,
      data => {
        for (let j of data.appliances) {
          if (j.room_id._id == this.room_id && j.type == i.appliance_type) {
            someArray.push(j);
          }
        }
        if (someArray.length == 1) {
          console.log(someArray[0]);
          this.navCtrl.push('RealRemotePage', {
            ir_appliance_name: someArray[0].name,
            codeset: someArray[0].remote_codeset,
            device_id: someArray[0].paired_sb.device_id,
            sb_id: someArray[0].paired_sb._id,
            type: i.appliance_type
          });
        }
        else if (someArray.length == 0) {
          this.alert.showAlert('Not Available', 'No registered remote for this ' + i.appliance_type + ' is available in the room')
        }
        else {
          let alert = this.alertCtrl.create();
          alert.setTitle('Select a ' + i.appliance_type);
          let c = 0;
          for (let k of someArray) {
            alert.addInput({
              type: 'radio',
              label: k.name,
              value: k,
              checked: c == 0 ? true : false
            })
            c++;
          }
          alert.addButton({
            text: 'Cancel'
          });
          alert.addButton({
            text: 'Go',
            handler: (data) => {
              console.log(data);
              this.navCtrl.push('RealRemotePage', {
                codeset: data.remote_codeset,
                device_id: data.paired_sb.device_id,
                sb_id: data.paired_sb._id,
                type: i.appliance_type
              });
            }
          });
          alert.present();
        }
      },
      error => {
        console.log(error);
      }, false)
  }
  goToSettings() {
    console.log('yolo');
    this.app.getRootNav().getActiveChildNav().select(4);
  }
}