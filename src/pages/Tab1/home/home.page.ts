import { Component, ChangeDetectorRef, ViewChild, ElementRef, NgZone, ChangeDetectionStrategy } from '@angular/core';

import { NavController, Platform, ModalController,IonicPage } from 'ionic-angular';
import {
	MqttMessage,
	MqttModule,
	MqttService

} from 'ngx-mqtt';
//import { Chart } from 'chart.js';


import { Chart } from 'chart.js';


//providers
import { LocalStorageService } from '../../../providers/LocalStorageService';
import { DataService } from '../../../providers/DataService';
import { AlertService } from '../../../providers/AlertService';
import { LocalCommService } from "../../../providers/LocalCommunicationServ";
import { ToastService } from '../../../providers/ToastService';
import { IntroService } from '../../../providers/IntroService';
import { Helper } from '../../../providers/Helper';

//pages
import { NotificationDispPage } from '../notification-disp/notification-disp';
import { SwitchesListPage } from '../switches-list/switches-list';
import { PowerStatsPage } from '../../Tab2/power-stats/power-stats';

const introJs = require('../../../../node_modules/intro.js/intro.js');
var ICO = require('../../Tab2/switches-in-room/icon-config.json');

declare var cordova;
declare var WifiWizard: any;

@IonicPage()
@Component({
	selector: 'page-home',
	templateUrl: 'home.page.html'
})
export class HomePage {
	shortcuts_loading = true;
	overlayHidden: boolean = true;
	dont_show_lock = true;
	isActive: Boolean = false; kiot_internet; all_sboards = []; home_energy = 0; dev_avail; observableArray = [];
	show_home_loader = true;
	total_switches = 0;
	active_switches = 0;
	inactive_switches = 0;
	colData = [];
	home_name; home_lock = false;
	app_icons = ICO.icons;
	username; retry_loading = false; no_of_shortcuts = -1; switch_shortcuts = []; switch_shortcuts1; is_edit;
	intro = new introJs.introJs();
	_topics; _suback;
	constructor(private local: LocalStorageService, private help: Helper, private is: IntroService, private toast: ToastService, private LocalComm: LocalCommService, private _mqttService: MqttService, private ds: DataService, private modalCtrl: ModalController, private alert: AlertService, private platform: Platform, public navCtrl: NavController) {
		this.dev_avail = {};
		this.username = this.local.getUser().name;
		console.log(this._mqttService.state);
		this.is_edit = false;
		// this.getHome();
		this.room_sensor_data = {};
		this._topics = {};



	}
	@ViewChild('lineCanvas') lineCanvas;

	lineChart: any;
	ionViewDidLoad() {
		if (this.kiot_internet == 'online') {
			this.setupIntro();
		}

	}
	mqtt_connected = false;
	connectMQTT(username, pass) {
		this._mqttService.connect({ username: username, password: pass });
		this.mqtt_connected = true;
	}
	setupIntro() {
		setTimeout(() => {
			this.intro.addSteps([
				{
					element: '#step_one',
					intro: "Shortcuts to switch on/off any switch in home",
					position: 'bottom'
				},
				{
					element: '#step_two',
					intro: "<ul class='no-bullet'><li><i class='total'></i>Total number of switches</li><li class='unavailable'>Total number of Unreachable switches</li><li class='turned-on'>Total number of switches(Turned ON)</li><li class='turned-off'>Total number of switches(Turned OFF)</li></ul>",
					position: 'bottom'
				},
				{
					element: '#step_three',
					intro: "Here notifications can be seen",
					position: 'left'
				}
			]);
			if (!this.is.hasSeen('HomePage')) {
				this.startIntro();
			}
		}, 700);
	}
	setActive() {
		this.isActive = true;

	}
	startIntro() {
		this.intro.start();
		this.is.setIntroSeen('HomePage');
	}

	getHome() {
		this.show_home_loader = this.home_name ? false : true;
		this.ds.getAHomeBasic(this.local.getCurrentHome(),
			data => {
				if (!this.mqtt_connected) {
					if (data.mqttPasswordu) {
						this.connectMQTT(this.local.getCurrentHome(), data.mqttPassword);
					}
					else {
						this.connectMQTT('i_am_admin', 'No!&&No1@meBl0ck*M#');
					}
					
				}
				this.show_home_loader = false;
				this.home_name = data.display_name;

				this.dont_show_lock = false;
				let x = this.home_lock;
				this.home_lock = data.away_mode;
				this.unlockHome(x);
				console.log(data);
			}, error => {
				this.show_home_loader = false;
				console.log(error);
			}, false);
	}
	getShortcuts() {
		//this.switch_shortcuts = [];
		this.shortcuts_loading = true;
		this.ds.getUserShortcuts(this.local.getCurrentHome(),
			data => {
				this.shortcuts_loading = false;
				console.log(data);
				this.switch_shortcuts1 = data;
				let ind = 0;
				let x = [];
				for (let i of this.switch_shortcuts1) {
					if (i.switch_appliance.sboard_id == null) {
						this.deleteShortCut(i);
						continue;
					}
					x.push(i);
					ind++;
				}
				this.switch_shortcuts = x;
				this.no_of_shortcuts = data.length;
			}, error => {
				this.shortcuts_loading = false;
				console.log(error);
			}, false);
	}

	num_of_notifications;
	goToNotifications() {
		let noti_modal = this.modalCtrl.create('NotificationDispPage');
		noti_modal.present();
		noti_modal.onWillDismiss((data) => {
			if (data) {
				this.num_of_notifications = 0;
			}
		});

	}
	show_chart = false;
	startChart() {
		let c = new Date(Date.now());
		let d = new Date(c);
		d.setHours(0, 0, 0, 0);
		this.ds.postGetPowerData({
			"device_id": this.deviceid_list,
			"timeto": c.getTime(),
			"timefrom": d.getTime(),
			"interval": "today"
		}, data => {
			console.log(data);
			if (data.length) {
				this.show_chart = true;
				let chartData = [];
				let labels = [];
				let sorted_data;
				sorted_data = this.help.sorter(data, "_id", "");
				for (let i of sorted_data) {
					chartData.push(i.avg_power);
					if (i._id < 12) {
						if (i._id == 0) {
							labels.push('12AM');
						}
						else {
							labels.push(i._id + 'AM');
						}
					}
					else {
						if (i._id == 12) {
							labels.push('12PM');
						}
						else {
							labels.push((parseInt(i._id) - 12).toString() + 'PM');
						}
					}
				}
				this.setupChart(chartData, labels);
			}
		}, error => {
			console.log(error);
		});
		this.ds.postGetEnergyData({
			"device_id": this.deviceid_list,
			"timeto": c.getTime(),
			"timefrom": d.getTime(),
			"interval": "today"
		}, data => {
			if (data.length) {

				//data = this.help.sorter(data, 'avg_energy', '')
				let x = 0;
				for (let i of data) {
					x += parseFloat(i.avg_energy);
				}
				if (x) {
					this.home_energy = x;
					this.home_energy = Math.round(this.home_energy * 100) / 100;
				}
			}
		}, error => {
			console.log(error);
		})
	}

	setupChart(data, labels) {
		let gradient = this.lineCanvas.nativeElement.getContext('2d').createLinearGradient(0, 0, 0, 450);

		gradient.addColorStop(0, '#ff9500');
		gradient.addColorStop(0.4, '#1c1c1d');
		gradient.addColorStop(1, '#1c1c1d');

		this.lineChart = new Chart(this.lineCanvas.nativeElement, {

			type: 'line',
			data: {
				labels: labels,
				datasets: [
					{
						label: 'Power Consumed (W)',
						backgroundColor: gradient,
						borderColor: 'rgba(255,255,255,0)',
						pointBackgroundColor: 'transparent',
						borderWidth: 1,
						pointBorderColor: "transparent",
						data: data,
						lineTension: 0
					}
				]
			},
			options: {
				responsive: true,
				maintainAspectRatio: false,
				layout: {
					padding: {
						left: 10,
						right: 15,
						top: 20,
						bottom: 10
					}
				},
				legend: {
					display: false
				},
				scales: {
					xAxes: [{
						display: true,
						gridLines: {
							color: 'rgba(255,255,255,0.03)'
						},
						ticks: {

							autoSkip: true,
							maxTicksLimit: 5,
							maxRotation: 0,
							minRotation: 0,
							fontFamily: "SanFranciscoLight"
						}
					}],
					yAxes: [{
						display: true,
						gridLines: {
							color: 'rgba(255,255,255,0.03)'
						},
						ticks: {
							beginAtZero: true,
							fontFamily: "SanFranciscoLight"
						}
					}]
				}
			}


		});

	}
	goToPowerStats() {
		if (this.deviceid_list.length) {
			this.modalCtrl.create('PowerStatsPage', {
				devices: this.deviceid_list
			}).present();
		}
	}
	ionViewWillEnter() {

		this.kiot_internet = this.local.getInternetStatus();
		if (this.kiot_internet == 'online') {
			this.getHome();
			this._suback = this._mqttService.onSuback.subscribe((e) => {
				console.log(e);
				if (e.filter) {
					if (this._topics[e.filter]) {
						this._topics[e.filter].granted = e.granted || false;
						if (this._topics[e.filter].granted) {
							this._mqttService.unsafePublish(this._topics[e.filter].pubTpoic, 'a');
							console.log(this._topics[e.filter].pubTpoic);
						}
					}
				}
			})

			this.ds.getUnseenNotifications(
				data => {
					this.num_of_notifications = data;
					console.log(data);
				}, error => {
					console.log(error);
				});
		}
		this.getSboards();
		this.getFavouritePrograms();
	}

	getFavouritePrograms() {
		this.ds.getChannelGroupData(channelMetaData => {
			this.ds.getCurrentProgramsOnFav(
				(data) => {
					if (data && data.length) {
						let x = [];
						for (let i of data) {
							i.channel_name = channelMetaData[3].channels[i.channelid].channelname;
							i.duration = (this.help.findHrsInterval(i.starttime, Date.now())) / (this.help.findHrsInterval(i.starttime, i.endtime));
							i.duration = (i.duration * 100).toString() + '%';
							x.push(i);
						}
						this.colData = x;
					}
				}, err => {
					console.log(err);
				})
		}, error => {
			console.log(error);
		})


		/*
		this.ds.getFavChannels(
				 (data) => {
						 this.colData = [];
						 for (let i of Object.keys(data.fav_channels)) {
								 this.ds.getChannelPrograms(1, 0, i, false,
										 data => {
												 if (data[0]) {
														 data[0].channel_name = channelMetaData[3].channels[data[0].channelid].channelname;
														 data[0].duration = (this.help.findHrsInterval(data[0].starttime, Date.now())) / (this.help.findHrsInterval(data[0].starttime, data[0].endtime));
														 data[0].duration = (data[0].duration * 100).toString() + '%';
														 this.colData.push(data[0]);
												 }
										 }, error => {
												 console.log(error);
										 })
						 }
				 },
				 error => {
						 console.log(error);
				 }, false
		 )*/

	}
	appliance_types; deviceid_list = []; room_sensor_data; all_rooms;
	getSboards() {
		this.total_switches = 0;
		this.all_sboards = [];
		this.deviceid_list = [];

		//append = kiotdevice,swappliance
		this.ds.getSboards(this.local.getCurrentHome(),
			data => {
				console.log(data);
				this.appliance_types = {};
				this.all_rooms = data;
				for (let i of data) {
					for (let j of i.devices) {
						if (j.device_info) {
							this.deviceid_list.push(j.device_info._id);
						}
						this.all_sboards.push(j);
						this.dev_avail[j.device_id] = j;
						this.total_switches += j.switch_appliances.length;
						for (let k of j.switch_appliances) {
							this.appliance_types[k._id] = k.appliance_type;
						}
					}
					if (i.room_id && !this.room_sensor_data[i.room_id]) {
						this.room_sensor_data[i.room_id] = {
							tempr: '',
							hum: '',
							motion: 'unreachable'
						};
					}

				}
				this.pingDevices();
				this.getShortcuts();
				this.startChart();
				console.log(this.all_sboards);

			}, error => {
				console.log(error);
				this.getShortcuts();
			}, false, 'kiotdevice,swappliance,room');
	}

	lockHome() {
		this.alert.showConfirm('Lock Home?', 'Locking home can notify every switch action and considerable motion in home'
			, 'Cancel', 'Lock', () => {

			}, () => {
				//lock here
				this.triggerLockUnlock(true);
			})
	}

	triggerLockUnlock(lock) {

		let text = lock ? 'Locked' : 'Unlocked';
		this.ds.postTriggerLockHome({
			home_id: this.local.getCurrentHome(),
			mode: lock
		}, data => {
			//this.toast.bottomToast_Short(this.home_name + ' ' + text);
			let x = this.home_lock;
			this.home_lock = lock;
			if (this.dont_show_lock) {
				this.dont_show_lock = false;
			}
			this.unlockHome(x);

		}, error => {
			console.log(error);
		})

	}

	pingDevices() {
		for (let i of this.all_sboards) {
			this.dev_avail[i.device_id].relays = [];
			let pubTpoic = '/KU/' + i.device_id + '/' + this.local.getCurrentHome() + '/ping/set';

			let subTpoic = '/KU/' + i.device_id + '/' + this.local.getCurrentHome() + '/ping/get';
			if (!this._topics[subTpoic]) {
				this._topics[subTpoic] = {
					pubTpoic: pubTpoic,
					granted: false
				}
			}

			this.observableArray.push(this._mqttService.observe(subTpoic)
				.subscribe((data: MqttMessage) => {
					console.log(i.device_id);
					let x;
					x = data.payload.toString();
					x = JSON.parse(x);
					let body = JSON.parse(this.help.getMqttPayload(x.payload, x.meta, i.device_info.encryptKey));
					if (body.switch) {
						try {
							this.dev_avail[i.device_id].relays = JSON.parse(body.switch).relays;
						}
						catch (error) {
							console.log(error);
						}
					}
					this.dev_avail[i.device_id].on_available = true;
					//					console.log(JSON.parse(data.payload.toString()));
					this.calculateStatuses1();
					if (body.sns && i.room_id && i.room_id._id) {
						let y = JSON.parse(body.sns);
						if (y && y.temphumid) {
							this.room_sensor_data[i.room_id._id].tempr = parseInt(y.temphumid.split(',')[0]);
							this.room_sensor_data[i.room_id._id].hum = parseInt(y.temphumid.split(',')[1]);
							//this.calculateTempr(ind);
						}
						if (y && (y.pir || y.pir == 0)) {
							let motion = 'not_detected';
							if (y.pir > 0) {
								motion = 'detected';
							}
							this.room_sensor_data[i.room_id._id].pir = motion;
						}
						if (y && (y.analog && y.analog != 0)) {
							this.room_sensor_data[i.room_id._id].analog = y.analog;
						}
					}

				}));
			if (i.device_info && this.local.getLocalIp(i.device_info.wifi_local_name)) {
				this.local.setSecret(i.device_info.device_id, i.device_info.apiSecret);
				this.LocalComm.pingDevice('http://', this.local.getLocalIp(i.device_info.wifi_local_name) + "/api/switch?apikey=" + i.device_info.apikey, i.device_info.device_id)
					.subscribe((data11: any) => {
						this.dev_avail[i.device_id].relays = JSON.parse(data11._body).relays;
						this.dev_avail[i.device_id].off_available = true;
						this.calculateStatuses1();
					}, error => {
						this.dev_avail[i.device_id].off_available = false;
						console.log(error);
					});
			}

		}

		console.log(this.dev_avail);
	}
	calculateStatuses1() {
		this.active_switches = 0;
		this.inactive_switches = 0;
		for (let i of this.all_sboards) {
			if (this.dev_avail[i.device_id].relays) {
				for (let j of this.dev_avail[i.device_id].relays) {
					if (j) {
						this.active_switches++;
					}
					else if (!j) {
						this.inactive_switches++;
					}
				}
			}
		}
	}





	retry() {
		this.retry_loading = true;
		this.ds.ping()
			.then(data => {
				console.log(data);
				this.kiot_internet = this.local.getInternetStatus();
				if (this.kiot_internet == 'online') {
					this.ionViewWillEnter();
				}
				this.retry_loading = false;
			});


	}


	doAction(i, ind) {

		if (this.is_edit == true) {
			this.deleteShortCut(i);
			this.switch_shortcuts.splice(ind, 1);
			if (!this.switch_shortcuts.length) {
				this.is_edit = false;
			}
		}
		else if (this.is_edit == false) {
			this.trigger(i, ind);
		}
	}
	doAddShortcuts() {
		if (this.is_edit == false) {
			let modalCtrl1 = this.modalCtrl.create('SwitchesListPage', {
				switch_shortcuts: this.switch_shortcuts
			});
			modalCtrl1.present();
		}
	}
	editShortcuts() {
		this.is_edit = !this.is_edit;
	}

	ionViewWillLeave() {
		this.is_edit = false;
		try {
			this._suback.unsubscribe();
			for (let i of this.observableArray) {
				i.unsubscribe();
			}
		}
		catch (err) {
			console.log(err);
		}
	}
	deleteShortCut(i) {
		this.ds.postDeleteUserShortcuts({
			id: i._id
		}, data => {
			console.log(data);
		}, error => {
			console.log(error);
		})
	}


	on_off_string; sw_no; on_off;
	trigger(i, ind) {
		if (!i.show_loader) {
			let device_id = i.switch_appliance.sboard_id.device_id;
			i.show_loader = true;
			this.on_off_string = "";
			this.on_off_string = this.local.getInternetStatus() == 'online' ? '1' : this.local.getInternetStatus() == 'offline' ? '0' : '-1';
			this.on_off_string += this.dev_avail[device_id].on_available ? '1' : '0';
			this.on_off_string += this.dev_avail[device_id].off_available ? '1' : '0';
			this.sw_no = i.switch_appliance.switch_no;
			if (this.dev_avail[device_id].relays[i.switch_appliance.switch_no - 1]) this.on_off = 0;
			else this.on_off = 1;
			let time = Date.now();

			let obj = {
				"device": device_id,
				"sb": i.switch_appliance.sboard_id._id,
				"switch_no": this.sw_no.toString(),
				"switch_state": this.on_off,
				"key": time,
				"mobId": "98239023",
				"act_type": "switch",
				"id": i.switch_appliance._id
			};
			let curr_status = this.dev_avail[device_id].relays[i.switch_appliance.switch_no - 1];
			console.log(obj);
			switch (this.on_off_string) {
				case '111': {
					this.sendOnlineSignal(ind, obj, time, device_id, parseInt(this.sw_no));
					break;
				}
				case '110': {
					this.sendOnlineSignal(ind, obj, time, device_id, parseInt(this.sw_no));
					break;
				}
				case '101': {
					this.sendLocalSignal(device_id, time, curr_status, obj, parseInt(this.sw_no), this.on_off, ind);
					break;
				}
				case '011': {
					this.sendLocalSignal(device_id, time, curr_status, obj, parseInt(this.sw_no), this.on_off, ind);
					break;
				}
				case '001': {
					this.sendLocalSignal(device_id, time, curr_status, obj, parseInt(this.sw_no), this.on_off, ind);
					break;
				}
				case '000': {
					i.show_loader = false;
					this.alert.showAlert('Unable to Communicate', 'We cannot communicate with the KIOT Switchboard');
					break;
				}
				case '100': {
					i.show_loader = false;
					this.alert.showAlert('Unable to Communicate', 'We cannot communicate with the KIOT Switchboard');
					break;
				}
				default: {
					i.show_loader = false;
					console.log(this.on_off_string);
					break;
				}
			}
		}
	}

	sendLocalSignal(device_id, time, now, obj, sw, act, ind, is_dimm?) {
		let device_url = this.dev_avail[device_id].device_info.wifi_local_name;
		// let actions = [];
		let switchAction = {};
		switchAction[sw] = act;
		if (is_dimm) {
			act = obj.dimm_value;
		}
		this.LocalComm.sendSwitchSignal(device_url, sw, act, this.dev_avail[device_id].device_info.apikey, is_dimm, device_id)
			.subscribe(res => {
				this.switch_shortcuts[ind].show_loader = false;
				console.log(res);
				this.LocalComm.pushInOfflineBuffer(obj);
				if (!is_dimm) {
					this.dev_avail[device_id].relays[sw - 1] = !this.dev_avail[device_id].relays[sw - 1];
					this.setStatuses(this.dev_avail[device_id].relays[sw - 1]);
				}
				//return this.on_off ? true : false;

			}, err => {
				this.switch_shortcuts[ind].show_loader = false;
				console.log(err);
			});
	}
	sendOnlineSignal(ind, obj, time, device_id, sw, is_dimm?) {
		this.ds.postTriggerSwitch(obj,
			(data) => {
				this.switch_shortcuts[ind].show_loader = false;
				console.log(data);
				if (data.status == 1 && !is_dimm) {
					this.dev_avail[device_id].relays[sw - 1] = !this.dev_avail[device_id].relays[sw - 1];
					this.setStatuses(this.dev_avail[device_id].relays[sw - 1]);
				}
			},
			(error) => {
				this.switch_shortcuts[ind].show_loader = false;
				console.log(error);
			});
	}
	setStatuses(curr_status) {
		if (curr_status) {
			this.active_switches++;
			this.inactive_switches--;
		}
		else {
			this.active_switches--;
			this.inactive_switches++;
		}
	}

	showToastForInfo(stringToToast) {
		this.toast.bottomToast_Short(stringToToast);
	}
	hideOverlay() {
		this.overlayHidden = true;
	}



	@ViewChild('lockerTop') lockerTop;
	@ViewChild('lockerBottom') lockerBottom;
	@ViewChild('lockerLock') lockerLock;



	unlockHome(prev_status) {
		let unlockTop = 'slideOutUp';
		let unlockBottom = 'slideOutDown';
		let lockTop = 'slideInDown';
		let lockBottom = 'slideInUp';
		let unlockLock = 'fast';
		if (this.home_lock != prev_status) {
			if (!this.home_lock) {
				this.lockerLock.nativeElement.classList.add(unlockLock);
				this.lockerTop.nativeElement.classList.remove(lockTop);
				this.lockerTop.nativeElement.classList.add(unlockTop);
				this.lockerBottom.nativeElement.classList.remove(lockBottom);
				this.lockerBottom.nativeElement.classList.add(unlockBottom);
				//this.home_lock = false;

			} else {
				this.lockerTop.nativeElement.classList.remove(unlockTop);
				this.lockerTop.nativeElement.classList.add(lockTop);
				this.lockerBottom.nativeElement.classList.remove(unlockBottom);
				this.lockerBottom.nativeElement.classList.add(lockBottom);

			}
		}
		else {
			this.lockerTop.nativeElement.classList.remove(unlockTop);
			this.lockerBottom.nativeElement.classList.remove(unlockBottom);
			this.lockerBottom.nativeElement.classList.remove(lockBottom);
			this.lockerTop.nativeElement.classList.remove(lockTop);
		}
	}
}
