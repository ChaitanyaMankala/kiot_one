import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../providers/DataService';
import { LocalStorageService } from '../../../providers/LocalStorageService';
import { AlertService } from '../../../providers/AlertService';

@IonicPage()
@Component({
    selector: 'page-switches-list',
    templateUrl: 'switches-list.html',
})
export class SwitchesListPage {
    items; switch_shortcuts; shortcut_dictionary;
    constructor(public navCtrl: NavController, private alert: AlertService, private local: LocalStorageService, private ds: DataService, public navParams: NavParams, private viewCtrl: ViewController) {
        this.shortcut_dictionary = {};
        this.switch_shortcuts = this.navParams.get('switch_shortcuts');
        for (let i of this.switch_shortcuts) {
            this.shortcut_dictionary[i.switch_appliance._id] = 1;
        }
        this.ds.getSwitches(this.local.getCurrentHome(), 'room,swappliance',
            data => {
                this.items = data;
                console.log(this.items);
            }, error => {
                console.log(error);
            });
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad SwitchesListPage');

    }
    dismiss(data) {
        this.viewCtrl.dismiss(data);
    }
    addShortcut(k, room, sboard) {
        if (!k.is_configured) {
            this.alert.showAlert('Please Configure', 'Unconfigured switches can not be added as shortcuts, configureation can be done in Settings Tab');
        }
        else {
            console.log(k);
            this.ds.postCreateUserShortcuts({
                "type": "switch",
                "home": this.local.getCurrentHome(),
                "switch_appliance": k._id
            }, data => {
                if (!this.switch_shortcuts) {
                    this.switch_shortcuts = [];
                }
                this.switch_shortcuts.push({
                    home: this.local.getCurrentHome(),
                    switch_appliance: {
                        is_configured: k.is_configured,
                        name: k.name,
                        switch_no: k.switch_no,
                        switch_type: k.switch_type,
                        _id: k._id,
                        room_id: {
                            _id: room.room_id,
                            name: room.room_name
                        },
                        sboard_id: {
                            _id: sboard._id,
                            device_id: sboard.device_id
                        }

                    },
                    type: "switch",
                    _id: data._id
                })
                this.dismiss(0);
            }, error => {
                console.log(error);
            });
        }
    }
}
