import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SwitchesListPage } from './switches-list';

@NgModule({
    declarations: [
        SwitchesListPage,
    ],
    imports: [
        IonicPageModule.forChild(SwitchesListPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class SwitchesListPageModule { }