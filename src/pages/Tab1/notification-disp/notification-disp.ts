import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, Platform,ModalController,IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../providers/DataService';
import { ToastService } from '../../../providers/ToastService';
import { NotificationService } from '../../../providers/NotificationService';
import { AlertService } from '../../../providers/AlertService';
import { Helper } from '../../../providers/Helper';

//pages
import { ProgramDetailsPage } from '../../Tab4/program-details/program-details';

@IonicPage()
@Component({
  selector: 'page-notification-disp',
  templateUrl: 'notification-disp.html',
})
export class NotificationDispPage {
  msg; btn_types; all_notifications = [];icons; no_more_notifications; offset; today_notifications = []; current_platform; earlier_notifications = []; seen = false;
  constructor(public navCtrl: NavController, private help: Helper,private modalCtrl:ModalController, private platform: Platform, private alert: AlertService, private noti: NotificationService, private toast: ToastService, private ds: DataService, public navParams: NavParams, private viewCtrl: ViewController) {
    this.current_platform = this.help.getPlatform();
    this.offset = 0;
    this.no_more_notifications = false;
    this.msg = this.navParams.get('msg');

    this.icons={
      'Settings':'settings',
      "Remotes" : "desktop",
      "Dashboard":"home",
      "Rooms":"cube",
      "Scenes":"alarm"
    }

    this.btn_types = {
      "JOIN_HOME_REQUEST": [

        {
          name: 'Reject',
          icon: 'close'
        },
        {
          name: 'Accept',
          icon: 'checkmark'
        },
      ],
      "SCHEDULE": [
        {
          name: 'Cancel',
          icon: 'close'
        },
        {
          name: 'continue',
          icon: 'checkmark'
        }
      ],
      "REMINDER": [
        {
          name: 'Cancel',
          icon: 'close'
        },
        {
          name: 'Watch Now',
          icon: 'desktop'
        }
      ]
    };
    this.getNotifs(this.offset);
  }
  getNotifs(offset, ev?) {
    if (offset == 0) {
      this.all_notifications = [];
      this.today_notifications = [];
      this.earlier_notifications = [];
    }
    let now = new Date(Date.now());
    now.setHours(0, 1, 1, 1);
    let some_time;
    let show_loader = ev ? false : true;
    this.ds.getAllNotifications(offset, 20,
      data => {
        this.offset += 20;
        if (ev) {
          ev.complete();
        }
        if (data.length < 20 || data.length == 0) {
          this.no_more_notifications = true;
        }
        this.all_notifications = this.all_notifications.concat(data);
        this.seen = true;
        let all_notification_ids = [];
        for (let i of this.all_notifications) {
          if (!i.seen) {
            all_notification_ids.push(i._id);
          }
          some_time = new Date(i.updatedAt);
          if (!i.added) {
            if (some_time > now) {
              i.added = true;
              this.today_notifications.push(i);
            }
            else {
              i.added = true;
              this.earlier_notifications.push(i);
            }
          }
        }
        this.ds.postSetNotificationSeen({
          notifications: all_notification_ids
        }, data => {
          console.log(data);
        }, error => {
          console.log(error);
        })
        console.log(this.all_notifications);
      }, error => {
        if (ev) {
          ev.complete();
        }
        console.log(error);
      }, show_loader);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationDispPage');


  }
  moreDetails(item) {
    if (item.type == 'REMINDER') {
      this.modalCtrl.create('ProgramDetailsPage',{
        upid:item.data.reminder.upid,
        programid:item.data.reminder.programid,
        channel_name:item.data.reminder.channelname,
        channel_number:item.data.reminder.channelno||'unknown',
        reminders:{
          upid:{
            _id:item.data.reminder.upid
          }
        }
      }).present();
    }
    else {
      let str;
      let noti_time = new Date(item.updatedAt);
      str = item.data.notif_message + ' on ' + noti_time.toDateString() + ', ' + (noti_time.getHours() > 12 ? noti_time.getHours() - 12 : noti_time.getHours()) + ':' + noti_time.toLocaleTimeString().split(':')[1] + ' ' + (noti_time.getHours() > 12 ? 'PM' : 'AM');
      this.alert.showAlert(
        item.data.notif_heading,
        str);
    }
  }

  dismiss(data) {
    this.viewCtrl.dismiss(this.seen);
  }
  showToast(i) {
    if (this.btn_types[i.type] && !i.action_taken) {
      this.toast.bottomToast_Short('Swipe left for more actions');
    }
    else {
      this.moreDetails(i);
    }
  }
  notifAction(item, val) {
    item.data.noti_id = item._id;
    this.noti.invokeNotificationAction({
      type: item.type,
      data: item.data
    }, {
        actionID: val
      }).then(data => {
        this.doRefresh();
      }).catch(err => {
        console.log(err);
        if (err == -1 && item.type == 'REMINDER') {
          this.alert.showAlert('Sorry', 'Channel number not available for ' + item.data.reminder.channelname);
        }
      });
  }
  doRefresh(ev?) {
    this.getNotifs(0);
  }
  doInfinite(ev) {

  }
}
