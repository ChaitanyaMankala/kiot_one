import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotificationDispPage } from './notification-disp';

@NgModule({
    declarations: [
        NotificationDispPage,
    ],
    imports: [
        IonicPageModule.forChild(NotificationDispPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class NotificationDispPageModule { }