import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AllAccountsPage } from './all-accounts';

@NgModule({
    declarations: [
        AllAccountsPage,
    ],
    imports: [
        IonicPageModule.forChild(AllAccountsPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class AllAccountsPageModule { }