import { Component } from '@angular/core';
import { NavController, NavParams,IonicPage } from 'ionic-angular';


//pages
import { SettingsAlexaPage } from '../../7accounts/settings-alexa/settings-alexa';
import { SettingsGooglePage } from '../../7accounts/settings-google/settings-google';

@IonicPage()
@Component({
  selector: 'page-all-accounts',
  templateUrl: 'all-accounts.html',
})
export class AllAccountsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AllAccountsPage');
  }
  goToGoole() {
    this.navCtrl.push('SettingsGooglePage');
  }
  goToAlexa() {
    this.navCtrl.push('SettingsAlexaPage');
  }
  goToIFTTT() {
    console.log('Coming Soon!');
  }

}
