import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfigureAlexaPage } from './configure-alexa';

@NgModule({
    declarations: [
        ConfigureAlexaPage,
    ],
    imports: [
        IonicPageModule.forChild(ConfigureAlexaPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class ConfigureAlexaPageModule { }