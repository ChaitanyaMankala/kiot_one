import { Component } from '@angular/core';
import { NavController, NavParams,IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../../providers/DataService';
import { LocalStorageService } from '../../../../providers/LocalStorageService';
import { LocalCommService } from '../../../../providers/LocalCommunicationServ';

@IonicPage()
@Component({
  selector: 'page-configure-alexa',
  templateUrl: 'configure-alexa.html',
})
export class ConfigureAlexaPage {
  all_sboards = []; total_count; disable_all = true;
  constructor(public navCtrl: NavController, private LocalComm: LocalCommService, private local: LocalStorageService, public navParams: NavParams, private ds: DataService) {

    this.ds.getSboards(this.local.getCurrentHome(),
      data => {
        this.all_sboards = data;
        if (data.length) {
          this.total_count = 0;
          this.disable_all = false;
        }
      }, error => {
        console.log(error);
      },false,'room,swappliance,kiotdevice');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfigureAlexaPage');
  }

  configureDevices() {
    this.count_total();
    this.disable_all = true;
    for (let i of this.all_sboards) {
      for (let j of i.devices) {
        if (j.is_checked) {
          j.show_success = false;
          j.show_failure = false;
          j.show_loader = true;
          this.local.setSecret(j.device_info.device_id,j.device_info.apiSecret);
          this.LocalComm.sendConf('http://' + this.local.getLocalIp(j.device_info.wifi_local_name), '/config', JSON.stringify({
            config: this.getConfObject(j.switchappliances, i.room_name)
          }),j.device_info.device_id)
            .subscribe(data => {
              j.show_loader = false;
              j.show_success = true;
              j.show_failure = false;
              this.total_count--;
              if (this.total_count == 0) {
                this.disable_all = false;
              }
            }, error => {
              j.show_loader = false;
              j.show_success = false;
              j.show_failure = true;
              this.total_count--;
              if (this.total_count == 0) {
                this.disable_all = false;
              }
            })
        }
      }
    }
  }
  count_total() {
    this.total_count=0;
    for (let i of this.all_sboards) {
      for (let j of i.devices) { 
        if(j.is_checked){
          this.total_count++;
        }
      }
    }
  }
  getConfObject(switches_list, room_name) {
    let list = [];
    let obj;
    for (let i of switches_list) {
      list.push({
        name: 'alexa_device' + i.switch_no,
        value: i.name + ' in ' + room_name
      });
    }
    return list;
  }

  sboard_checked(item) {
    item.show_success = false;
    item.show_failure = false;

  }

}
