import { Component } from '@angular/core';
import { NavController, NavParams,IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-instructions-alexa',
  templateUrl: 'instructions-alexa.html',
})
export class InstructionsAlexaPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InstructionsAlexaPage');
  }

}
