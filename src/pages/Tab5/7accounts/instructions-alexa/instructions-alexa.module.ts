import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InstructionsAlexaPage } from './instructions-alexa';

@NgModule({
    declarations: [
        InstructionsAlexaPage,
    ],
    imports: [
        IonicPageModule.forChild(InstructionsAlexaPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class InstructionsAlexaPageModule { }