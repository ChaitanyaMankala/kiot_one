import { Component } from '@angular/core';
import { NavController, NavParams,IonicPage } from 'ionic-angular';

//pages
import { ConfigureAlexaPage } from '../configure-alexa/configure-alexa';
import { InstructionsAlexaPage } from '../instructions-alexa/instructions-alexa';

@IonicPage()
@Component({
  selector: 'page-settings-alexa',
  templateUrl: 'settings-alexa.html',
})
export class SettingsAlexaPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsAlexaPage');
  }
  goToConfigureAlexa(){
    this.navCtrl.push('ConfigureAlexaPage');
  }
  goToHowToConfigure(){
      this.navCtrl.push('InstructionsAlexaPage');
  }

}
