import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SettingsAlexaPage } from './settings-alexa';

@NgModule({
    declarations: [
        SettingsAlexaPage,
    ],
    imports: [
        IonicPageModule.forChild(SettingsAlexaPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class SettingsAlexaPageModule { }