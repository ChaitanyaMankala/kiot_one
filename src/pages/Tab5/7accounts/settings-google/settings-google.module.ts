import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SettingsGooglePage } from './settings-google';

@NgModule({
    declarations: [
        SettingsGooglePage,
    ],
    imports: [
        IonicPageModule.forChild(SettingsGooglePage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class SettingsGooglePageModule { }