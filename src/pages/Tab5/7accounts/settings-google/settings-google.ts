import { Component } from '@angular/core';
import {  NavController, NavParams,IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-settings-google',
  templateUrl: 'settings-google.html',
})
export class SettingsGooglePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsGooglePage');
  }

}
