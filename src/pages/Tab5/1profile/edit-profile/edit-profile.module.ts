import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditProfilePage } from './edit-profile';

@NgModule({
    declarations: [
        EditProfilePage,
    ],
    imports: [
        IonicPageModule.forChild(EditProfilePage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class EditProfilePageModule { }