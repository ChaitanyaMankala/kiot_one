import { Component, ViewChild } from '@angular/core';
import { AlertController, NavController, NavParams, Platform, Select,IonicPage } from 'ionic-angular';



//providers
import { DataService } from '../../../../providers/DataService';
import { LocalStorageService } from '../../../../providers/LocalStorageService';
import { AlertService } from '../../../../providers/AlertService';
import { ToastService } from '../../../../providers/ToastService';

@IonicPage()
@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html'
})
export class EditProfilePage {
  name; phone; gender; def; items; data; obj; defDisable; disUpdate; otp; no_otp; defaults; all_apps = []; apps_set = []; apps_tv = [];

  @ViewChild('defset') def_set: Select;
  @ViewChild('deftv') def_tv: Select;


  constructor(public navCtrl: NavController, public navParams: NavParams, private toast: ToastService, private ds: DataService, private local: LocalStorageService, public alertCtrl: AlertController, private alert: AlertService) {

    this.defaults = this.navParams.get('default_appliances') || {};
    this.obj = this.local.getUser();
    this.name = this.obj.name;
    if (this.name.toString().length > 2) this.disUpdate = false;
    this.phone = this.obj.phone;
    this.def = this.obj.default_home;
    this.gender = this.obj.gender;
    this.defDisable = false;
    this.no_otp = false;
    /*   for(let i of this.items){
         if(i._id ==  this.def) i.selected=true;
         else i.selected=false;
         }*/
  }

  ionViewDidLoad() {
    this.ds.getIrAppliances(this.local.getCurrentHome(),
      data => {
        this.all_apps = data.appliances;
        for (let i of this.all_apps) {
          switch (i.type) {
            case 'Set Top Box': {
              this.apps_set.push(i);
              break;
            }
            case 'TV': {
              this.apps_tv.push(i);
              break;
            }
          }
        }
      }, error => {
        console.log(error);
      })
  }
  openSelect(ele) {
    switch (ele) {
      case 'set': {
        this.def_set.open();
        break;
      }
      case 'tv': {
        this.def_tv.open();
        break;
      }
    }
  }
  updateProfile() {
    /*let obj = {
        "default_home": this.def,
        "name": this.name,
        "gender": this.gender
    };*/

    if (this.name.length >= 3) {

      this.obj.gender = this.gender;
      this.obj.name = this.name;
      let x = {
        "tv": "",
        "stb": ""
      };

      x.tv = this.defaults.tv ? this.defaults.tv._id.split('_')[3] || this.defaults.tv._id : null;
      x.stb = this.defaults.stb ? this.defaults.stb._id.split('_')[3] || this.defaults.stb._id : null;
      this.obj.default_appliances = x;
      let some_obj = this.clone_an_obj(this.obj);
      delete some_obj["default_home"];
      delete some_obj["email"];
      this.ds.putEditProfile(some_obj, data => {
        console.log(data);
        this.toast.bottomToast_Short('Edit successful');
        this.navCtrl.pop();
        this.local.setUser(this.obj);
        this.local.setDefaultHome(this.def);
      }, error => {
        console.log(error);
        console.log(JSON.stringify(error.json()));
      });

    }
    else {
      console.log('Invalid');
      this.alert.showAlert("Error", "Name must be longer than 3 characters");
    }




  }

  clone_an_obj(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
      if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
  }

  onChange() {
    this.disUpdate = this.name.length > 2 ? false : true;
  }

  checknum() {

    this.obj.phone = this.phone;
    let re = new RegExp(/^[0-9]*$/);
    if (re.test(this.phone) && this.phone.length == 10 && this.name.length >= 3) {
      this.disUpdate = false;
      this.ds.putEditProfile(this.obj, data => {
        console.log(data);
        this.toast.bottomToast_Short('invalid phone number');

        this.local.setUser(this.obj);
        this.local.setDefaultHome(this.def);
      },

        error => {

          console.log(error);
          console.log(JSON.stringify(error.json()));
        });

    }
    else {

      this.disUpdate = true;
      console.log('Invalid');
      ;
    }
  }

  getOtpPassword() {
    let alert = this.alertCtrl.create({
      title: 'Verify Number',


      inputs: [

        {
          name: 'otp',
          placeholder: 'Enter the OTP Number',
          type: 'number'
        }
      ],
      buttons: [
        {
          text: 'Resend',
          role: 'cancel',
          handler: data => {
            console.log('Resend clicked');
          }
        },
        {
          text: 'Verify',

          handler: data => {
            console.log('Saved OTP')

          }
        }
      ]
    });
    alert.present();
  }

}





