import { Component } from '@angular/core';
import { AlertController, NavController, NavParams,IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../../providers/DataService';
import { AlertService } from '../../../../providers/AlertService';
import { ToastService } from '../../../../providers/ToastService';

@IonicPage()
@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html'
})
export class ChangePasswordPage {
  new1; new2; old; otp; disUpdate; defDisable;
  constructor(public navCtrl: NavController, public navParams: NavParams, private toast: ToastService, private ds: DataService, public alertCtrl: AlertController, private alert: AlertService) {

    this.defDisable = false;
    this.disUpdate = true;

  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePasswordPage');
  }




  checkPassword() {
    if (this.new1 != this.new2) {
      this.toast.bottomToast_Short("please enter the same new password in both fields");
      // this.alert.showAlert("Passwords do not match","please enter the same new password in both fields");
    }
    else {
      this.disUpdate = false;

    }
  }
  onChange() {
    this.disUpdate = false;
  }
  getOtpPassword() {
    /* let alert = this.alertCtrl.create({
     title: 'Verify Number',
      
 
     inputs: [
      
       {
         name: 'otp',
         placeholder: 'Enter the OTP Number',
         type: 'number'
       }
     ],
     buttons: [
       {
         text: 'Resend',
         role: 'cancel',
         handler: data => {
           console.log('Resend clicked');
         }
       },
       {
         text: 'Verify',
 
         handler: data => {
         console.log('Saved OTP');*/
    if (this.new1 != this.new2) {
      //toastHere
      this.alert.showAlert("Passwords do not match", "please enter the same new password in both fields");
    }
    else {
      let obj = {
        "old_password": this.old,
        "new_password": this.new1
      }
      this.ds.postUpdatePass(obj,
        data => {
          console.log(data);
          if (data.status == 0) {
            this.alert.showAlert("Error", data.err);
          }
          else {
            this.toast.bottomToast_Short('Password Updated');
            this.navCtrl.pop();
          }


        }, error => {
          this.toast.bottomToast_Long('Failed,Please try again later')
          this.navCtrl.pop();
          console.log(error);
          console.log(JSON.stringify(error.json()));
        });
    }

  }
}
