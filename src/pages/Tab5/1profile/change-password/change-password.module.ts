import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChangePasswordPage } from './change-password';

@NgModule({
    declarations: [
        ChangePasswordPage,
    ],
    imports: [
        IonicPageModule.forChild(ChangePasswordPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class ChangePasswordPageModule { }