import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotificationSettingsPage } from './notification-settings';

@NgModule({
    declarations: [
        NotificationSettingsPage,
    ],
    imports: [
        IonicPageModule.forChild(NotificationSettingsPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class NotificationSettingsPageModule { }