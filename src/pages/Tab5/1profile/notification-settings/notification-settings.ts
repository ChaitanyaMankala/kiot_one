import { Component } from '@angular/core';
import { NavController, NavParams,IonicPage } from 'ionic-angular';
// Providers
import { DataService } from '../../../../providers/DataService';
import { ToastService } from '../../../../providers/ToastService';


@IonicPage()
@Component({
  selector: 'page-notification-settings',
  templateUrl: 'notification-settings.html',
})

export class NotificationSettingsPage {
  receive_noti; types_noti;
  constructor(public navCtrl: NavController, public navParams: NavParams, private ds: DataService, private toast: ToastService) {
    this.ds.getNotificationTypes(data => {
      console.log(data);
      this.types_noti = data;
      this.ds.getUserNotificationTypes(
        data => {
          console.log(data);
          for (let i of this.types_noti) {
            for (let j of data.notification_pref) {
              if (i.value == j) i.selected = true;
            }
          }
        }, error => {
          console.log(error);
        }, false);
    }, error => {
      console.log(error);
    });
    this.ds.getNotificationStatus(
      data => {
        console.log(data);
        this.receive_noti = data.show_notifications;
      }, error => {
        console.log(error);
      }, false);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationSettings');
  }
  enableNotifications(ev) {
    //console.log(ev.checked);
    if (ev.checked) {
      console.log('enabled');
      this.ds.postEnableNotifications(data => {
        console.log(data);
      }, error => {
        console.log(error);
        this.receive_noti = !this.receive_noti;
      }, false);
    }
    else {
      this.ds.postDisableNotifications(data => {
        console.log(data);
      }, error => {
        console.log(error);
        this.receive_noti = !this.receive_noti;
      }, false);
    }
  }

  saveNotiTypes() {
    let arr = [];
    for (let i of this.types_noti) {
      if (i.selected) arr.push(i.value);
    }
    this.ds.postSetUserNotificationTypes({ notification_pref: arr }, data => {
      console.log(data);
      this.toast.bottomToast_Short('Settings saved')
    }, error => {
      console.log(error);
      this.toast.bottomToast_Short('error occurred');
    })
  }

}
