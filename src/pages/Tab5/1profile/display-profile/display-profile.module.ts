import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DisplayProfilePage } from './display-profile';

@NgModule({
    declarations: [
        DisplayProfilePage,
    ],
    imports: [
        IonicPageModule.forChild(DisplayProfilePage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class DisplayProfilePageModule { }