import { Component } from '@angular/core';
import { NavController, NavParams,IonicPage } from 'ionic-angular';

//pages
import { ChangePasswordPage } from '../change-password/change-password';
import { EditProfilePage } from '../edit-profile/edit-profile';

//provider
import { LocalStorageService } from '../../../../providers/LocalStorageService';
import { DataService } from '../../../../providers/DataService';

@IonicPage()
@Component({
  selector: 'page-display-profile',
  templateUrl: 'display-profile.html'
})
export class DisplayProfilePage {
  profile; username; mobile; email; gender; defaults;
  constructor(public navCtrl: NavController, public navParams: NavParams, private local: LocalStorageService, private ds: DataService) {


  }

  ionViewWillEnter() {
    //console.log('ionViewDidLoad DisplayProfilePage');
    // this.profile = JSON.parse(localStorage.getItem('user'));
    this.profile = this.local.getUser();


  }
  ionViewDidLoad() {
    this.ds.getUser(
      data => {

        this.defaults = {};
        this.defaults = data.default_appliances;
        if (!this.profile || !this.profile.name || !this.profile.phone || !this.profile.email) {

          this.profile = data;
          this.local.setUser(data);
          this.username = this.profile.name;
          this.mobile = this.profile.phone;
          this.gender = this.profile.gender;
          this.email = this.profile.email;

        }
        else {
          this.username = this.profile.name;
          this.mobile = this.profile.phone;
          this.gender = this.profile.gender;
          this.email = this.profile.email;
        }
      }, error => {
        console.log(error);
      }
    )
  }
  goToChangePassword() {
    this.navCtrl.push('ChangePasswordPage');
  }
  goToEditProfile() {
    this.navCtrl.push('EditProfilePage', {
      user: this.profile,
      default_appliances: this.defaults
    });
  }

}
