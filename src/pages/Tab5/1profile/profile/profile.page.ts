import { Component } from '@angular/core';
import { NavController, NavParams, App, AlertController, ModalController, IonicPage } from 'ionic-angular';
import { GooglePlus } from '@ionic-native/google-plus';

//pages
import { LoginPage } from '../../../Start/login/login';
import { RegisterHomePage } from '../../2homes/registerhome/registerhome';
import { DisplayProfilePage } from '../display-profile/display-profile';
import { HomeListPage } from '../../4sboards/home-list/home-list';
import { DisplayRoomsPage } from '../../3rooms/display-rooms/display-rooms';
import { SboardListPage } from '../../4sboards/sboard-list/sboard-list';
import { DisplaySwitchesPage } from '../../5switches/display-switches/display-switches';
import { DisplayIrAppsPage } from '../../6irAppliances/display-ir-apps/display-ir-apps';
import { NotificationSettingsPage } from '../notification-settings/notification-settings';
import { NotificationDispPage } from '../../../Tab1/notification-disp/notification-disp';
import { AllAccountsPage } from '../../7accounts/all-accounts/all-accounts';
import { SupportMainPage } from '../../8support/support-main/support-main';

//providers
import { LocalStorageService } from '../../../../providers/LocalStorageService';
import { DataService } from '../../../../providers/DataService';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.page.html'
})
export class ProfilePage {
  kiot_internet; one_home_only; home;
  constructor(public navCtrl: NavController, private modalCtrl: ModalController, private gplus: GooglePlus, public navParams: NavParams, private ds: DataService, private app: App, private local: LocalStorageService, private alertCtrl: AlertController) {
    this.home = [];
  }

  ionViewWillEnter() {
    // console.log(this.home);
    this.kiot_internet = this.local.getInternetStatus();
    if (this.kiot_internet == 'online') {
      this.ds.getUnseenNotifications(
        data => {
          this.num_of_notifications = data;
          console.log(data);
        }, error => {
          console.log(error);
        });
    }
    if (this.home.length == 1) {
      this.one_home_only = true;
    }
    else {
      this.one_home_only = false;
    }
  }
  ionViewDidLoad() {
    this.ds.getUserHomeIdsOnly(
      data => {
        console.log(data);
        if (data.length == 1) {
          this.one_home_only = true;
          this.ds.getUserHomes(
            data => {
              this.home = data;
              console.log(this.home);
            }, error => {
              console.log(error);
            }, false
          )
        }
        else {
          this.one_home_only = false;
        }
      },
      error => {
        console.log(error);
      },
      true);
  }

  logout() {
    let alert = this.alertCtrl.create({
      title: 'Confirm Logout',
      message: 'Do you want to logout?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            //console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            console.log(localStorage.getItem("token"));
            this.serverLogout()
              .then(() => {
                this.local.logout();
                this.app.getRootNav().setRoot('LoginPage');
              })
              .catch(err => {
                console.log(err);
              }
              );
          }
        }
      ]
    });
    alert.present();
  }

  serverLogout() {
    return new Promise((resolve, reject) => {
      let x = this.local.getUserLoginObject();
      if (x.type && x.type == 'google') {
        this.gplus.trySilentLogin({
          scopes: 'profile email openid',
          webClientId: '970544757572-aj26nhdjmv44f7f18btq7fjjrakgm080.apps.googleusercontent.com',
          offline: true
        }).then((data) => {
          console.log(data);
          this.gplus.disconnect()
            .then(() => {
              resolve(1);
            })
            .catch((err) => {
              console.log(err);
              reject('from disconnect');
            })
        }).catch(err => {
          console.log(err);
          reject('from relogin')
        })


      }

      this.ds.postLogout({
        notification_id: this.local.getOneSignalIds() ? this.local.getOneSignalIds().userId : undefined
      }, data => {
        console.log(data);
        resolve(1);
      }, error => {
        resolve(1);
      }, true, true);

    })

  }


  goTomyHomes() {
    this.navCtrl.push('RegisterHomePage', {
      show: "1",
      home: this.home
    });
  }
  goTomyHomesForRooms() {
    this.navCtrl.push('DisplayRoomsPage', {
      show: "0"
    });
  }
  goToDisplayProfile() {
    this.navCtrl.push('DisplayProfilePage');
  }
  goTomyHomesForSboards() {
    if (this.one_home_only) {
      this.go(this.home[0], 'sboards');
    }
    else {
      this.navCtrl.push('HomeListPage', {
        for: "sboards"
      });
    }
  }
  goToHelp() {
    this.navCtrl.push('SupportMainPage');
  }
  goToSupport() {
    this.navCtrl.push('ContactUsPage');
  }
  goToAbout() {
    this.navCtrl.push('AboutPage');
  }
  goToNotificationsSettings() {
    this.navCtrl.push('NotificationSettingsPage');
  }
  num_of_notifications;
  goToNotifications() {
    let noti_modal = this.modalCtrl.create('NotificationDispPage');
    noti_modal.present();
    noti_modal.onWillDismiss((data) => {
      if (data) {
        this.num_of_notifications = 0;
      }
    });

  }

  goTomyHomesForSwitches() {
    if (this.one_home_only) {
      this.go(this.home[0], 'switches');
    }
    else {
      this.navCtrl.push('HomeListPage', {
        for: "switches"
      });
    }
  }
  goToHomeListForIR() {
    if (this.one_home_only) {
      this.go(this.home[0], 'ir');
    }
    else {
      this.navCtrl.push('HomeListPage', {
        for: "ir"
      })
    }
  }
  go(item, for1) {
    try {
      switch (for1) {
        case 'sboards':
          this.navCtrl.push('SboardListPage', {
            home: item,
            homeName: item.display_name,
            for: for1
          });
          break;

        case 'switches':
          this.navCtrl.push('DisplaySwitchesPage', {
            home: item,
            homeName: item.display_name,
            for: for1
          });
          break;

        case 'ir':
          this.navCtrl.push('DisplayIrAppsPage', {
            home: item
          });
          break;

      }
    }
    catch (err) {

    }

  }
  feedbackalert() {
    let alert = this.alertCtrl.create({
      title: 'Compose Feedback',
      message: 'Hi, thanks for feedback',


      inputs: [

        {
          name: 'feedback',
          placeholder: 'e.g. Remote is not working',
          type: 'Feedback',

        }
      ],
      buttons: [
        {
          text: 'Send',
          handler: data => {

          }
        },

        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        }

      ]
    });
    alert.present();
  }
  retry_loading = false;
  retry() {
    this.retry_loading = true;
    this.ds.ping()
      .then(data => {
        console.log(data);
        this.kiot_internet = this.local.getInternetStatus();
        this.retry_loading = false;
      });
  }
  goToAccounts() {
    this.navCtrl.push('AllAccountsPage');
  }

}
