import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../../providers/DataService';


@IonicPage()
@Component({
  selector: 'page-tickets-list',
  templateUrl: 'tickets-list.html',
})
export class TicketsListPage {
  all_tickets = []; wait = true;
  constructor(public navCtrl: NavController, public navParams: NavParams, private ds: DataService) {
    this.getTickets();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TicketsListPage');
  }


  getTickets() {
    this.ds.getAllTickets(
      data => {
        this.wait = false;
        this.all_tickets = data;
      },
      error => {
        this.wait = false;
        console.log(error);
      });

  }
  goToNewTicket() {
    this.navCtrl.push('CreateTicketPage', {
      all_tickets: this.all_tickets
    });
  }



}
