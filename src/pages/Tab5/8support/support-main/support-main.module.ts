import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SupportMainPage } from './support-main';

@NgModule({
    declarations: [
        SupportMainPage,
    ],
    imports: [
        IonicPageModule.forChild(SupportMainPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class SupportMainPageModule { }