import { Component } from '@angular/core';
import { NavController, NavParams,IonicPage } from 'ionic-angular';

//pages
import { ContactUsPage } from '../contact-us/contact-us';


@IonicPage()
@Component({
  selector: 'page-support-main',
  templateUrl: 'support-main.html',
})
export class SupportMainPage {
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SupportMainPage');
  }


  goToContactUs() {
    this.navCtrl.push('ContactUsPage');
  }
}
