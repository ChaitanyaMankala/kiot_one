import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateTicketPage } from './create-ticket';

@NgModule({
    declarations: [
        CreateTicketPage,
    ],
    imports: [
        IonicPageModule.forChild(CreateTicketPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class CreateTicketPageModule { }