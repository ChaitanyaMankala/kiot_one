import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';


//providers
import { DataService } from '../../../../providers/DataService';
import { LocalStorageService } from '../../../../providers/LocalStorageService';
import { ToastService } from '../../../../providers/ToastService';

var TICK_CONF = require('../ticket-config.json');

@IonicPage()
@Component({
  selector: 'page-create-ticket',
  templateUrl: 'create-ticket.html',
})
export class CreateTicketPage {
  maxChar = 300;
  conf; all_tickets; issueType; issueTitle = ''; wait_for_devices = true; issueTitle2; issueDesc = ''; cust_address; devices_in_rooms = []; deviceObj;
  constructor(public navCtrl: NavController, public navParams: NavParams, private toast: ToastService, private local: LocalStorageService, private ds: DataService) {
    this.all_tickets = this.navParams.get('all_tickets') || [];
    this.conf = TICK_CONF;
    this.getAllDevices();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateTicketPage');
  }
  getAllDevices() {
    this.ds.getSboards(this.local.getCurrentHome(),
      data => {
        this.wait_for_devices = false;
        for (let room of data) {
          for (let device of room.devices) {
            this.devices_in_rooms.push({
              str: device.sboard_name + " in " + room.room_name,
              obj: device
            });

          }
        }
      }, err => {
        this.wait_for_devices = false;
        console.log(err);
      }, false)
  }
  createTicket() {
    if (this.validate()) {
      let pr = 3;
      if (this.issueType != 'other') {

        for (let i of TICK_CONF.queries[this.issueType]) {
          if (i.title == this.issueTitle) {
            pr = i.priority;
            break;
          }
        }
      }
      let dev_obj = this.issueType == 'hardware' ? this.deviceObj ? this.deviceObj : {} : {};
      let payload = {
        "deviceObj": dev_obj,
        "title": this.issueTitle == 'other' || this.issueType == 'other' ? this.issueTitle2 : this.issueTitle,
        "priority": pr,
        "ticket_type": this.issueType,
        "customer_comments": this.issueDesc,
        "status": "pending",
        "createdAt":Date.now()
      };
      this.ds.postCreateTicket(payload, data => {
        this.all_tickets.push(payload);
        this.navCtrl.pop();
        this.toast.bottomToast_Short('Ticket registered');
      }, error => {
        console.log(error);
      })
    }
  }

  validate() {
    if (this.issueType == 'other' && !this.issueTitle2) {
      this.toast.bottomToast_Short('please fill the title');
      return false;
    }
    if (this.issueType == 'hardware' && (this.deviceObj == {} || !this.deviceObj)) {
      this.toast.bottomToast_Short('please select the device');
      return false;
    }
    if (!this.issueTitle && !this.issueTitle2) {
      this.toast.bottomToast_Short('Please enter issue title');
      return false;
    }
    if (this.issueDesc && this.issueDesc.length > this.maxChar) {
      this.toast.bottomToast_Short('Description cannot exceed ' + this.maxChar + ' chracters');
      return false;
    }
    return true;
  }

}
