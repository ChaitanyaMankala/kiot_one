import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';
import { AndroidPermissions } from '@ionic-native/android-permissions';

//pages
import { CreateTicketPage } from '../create-ticket/create-ticket';

//providers
import { DataService } from '../../../../providers/DataService';
import { AlertService } from '../../../../providers/AlertService';
import { ToastService } from '../../../../providers/ToastService';
import { LocalStorageService } from '../../../../providers/LocalStorageService';

@IonicPage()
@Component({
  selector: 'page-contact-us',
  templateUrl: 'contact-us.html',
})
export class ContactUsPage {
  all_tickets = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, private perm: AndroidPermissions, private callNumber: CallNumber, private local: LocalStorageService, private toast: ToastService, private alert: AlertService, private ds: DataService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactUsPage');
  }

  goToNewTicket() {
    this.navCtrl.push('CreateTicketPage');
  }
  goToTickets() {
    this.navCtrl.push('TicketsListPage')
  }
  callCustCare() {
    this.callNumber.callNumber("+918885864318", true)
      .then(() => console.log('Launched dialer!'))
      .catch((err) => {
        console.log(err);
        if (err == 20) {
          this.perm.requestPermission('android.permission.CALL_PHONE')
            .then(data => {
              this.callCustCare();
            }).catch(err => {
              this.toast.bottomToast_Short(err);
            })
          this.toast.bottomToast_Short('Please enable permission to make calls from settings')
        }
      });
  }
  feedbackAlert() {
    this.alert.showPrompt('Feedback', 'Please report the issue here',
      [

        {
          name: 'feedback',
          placeholder: 'e.g. Remote is not working',
          type: 'Feedback',

        }
      ], 'Cancel', () => { }, 'Submit', (data) => {
        let x = this.local.getUser();
        this.ds.postSendFeedback({
          "name": x.name,
          "email": x.email,
          "mobile": x.phone,
          "user_comment": data.feedback
        }, data => {
          this.toast.bottomToast_Short('Thanks for your feedback');
        }, error => {
          console.log(error);
        })
      })
  }

}
