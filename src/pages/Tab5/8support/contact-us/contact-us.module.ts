import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContactUsPage } from './contact-us';

import { CallNumber } from '@ionic-native/call-number';

@NgModule({
    declarations: [
        ContactUsPage,
    ],
    imports: [
        IonicPageModule.forChild(ContactUsPage),
    ],
    providers:[
        CallNumber
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class ContactUsPageModule { }