import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular'

//providers
import { Helper } from '../../../../providers/Helper';

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private help: Helper) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');
  }
  goToPrivacyPolicy() {
    this.help.goToPrivacyPolicy();
  }
  goToLegal() {
    this.help.goToLegal();
  }

}
