import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditSwitchPage } from './edit-switch';

@NgModule({
    declarations: [
        EditSwitchPage,
    ],
    imports: [
        IonicPageModule.forChild(EditSwitchPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class EditSwitchPageModule { }