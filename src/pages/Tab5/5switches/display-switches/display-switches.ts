import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController,IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../../providers/DataService';

//pages
import { SwitchDetailsPage } from '../../5switches/switch-details/switchDetails';
import { EditSwitchPage } from '../../5switches/edit-switch/edit-switch';


@IonicPage()
@Component({
    selector: 'page-display-switches',
    templateUrl: 'display-switches.html',
    providers: []
})
export class DisplaySwitchesPage implements OnInit {
    sboard_id; items;
    public homeData: any;
    public homeName: string;
    constructor(public navCtrl: NavController, public navParams: NavParams, private ds: DataService, private modalCtrl: ModalController) {
        // this.sboard_id = this.navParams.get('sboard_id');
        //this.items = this.sObj.switch_appialnces;
        this.homeData = this.navParams.get('home');
        this.homeName = this.homeData.display_name;
    }
    ionViewWillEnter() {
        this.ds.getSwitches(this.homeData._id,'room,swappliance',
            data => {
                this.items = data;
                console.log(this.items);
                // this.items = JSON.parse(this.items._body);
                // this.items = this.items.switch_appialnces; 
            }, error => {
                console.log(error);
            });

    }

    ngOnInit() {
    }

   

    public goToSwitchDetails(value, sbNmae, rmName, isConigured) {

        if (!isConigured) {
            /*   this.navCtrl.push('SwitchDetailsPage',{
               editSwitch:value,
                homeName: this.homeName,
                sbNmae:sbNmae,
                rmName:rmName,
                for:this.navParams.get('for'),
                is_configured:isConigured
      
            });*/
            this.modalCtrl.create('SwitchDetailsPage', {
                editSwitch: value,
                homeName: this.homeName,
                sbNmae: sbNmae,
                rmName: rmName,
                for: this.navParams.get('for'),
                is_configured: isConigured
            }).present();

        } else {
            this.navCtrl.push('EditSwitchPage', {
                switch: value,
                homeName: this.homeName,
                sbNmae: sbNmae,
                rmName: rmName,
                for: this.navParams.get('for'),
                is_configured: isConigured

            });
        }
    }

}
