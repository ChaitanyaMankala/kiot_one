import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DisplaySwitchesPage } from './display-switches';

@NgModule({
    declarations: [
        DisplaySwitchesPage,
    ],
    imports: [
        IonicPageModule.forChild(DisplaySwitchesPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class DisplaySwitchesPageModule { }