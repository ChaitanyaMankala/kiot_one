import { Component } from '@angular/core';
import { NavController, NavParams, ViewController,IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../../providers/DataService';
import { Helper } from '../../../../providers/Helper';
import { ToastService } from '../../../../providers/ToastService';
import { AlertService } from '../../../../providers/AlertService';

@IonicPage()
@Component({
  selector: 'page-switch-details',
  templateUrl: 'switchDetails.html',
  providers: []
})
export class SwitchDetailsPage {
  sboard_id; items;
  public homeData: any;
  public switchDetailsData: any;
  public homeName: string;
  public sbNmae: string;
  public rmName: string;
  data; data2; disUpdate; appliance_subtype_exists; appliance_type_exists; name;
  appliance_type; sub_appliance_type; appliance_changed; sub_appliance_changed;
  constructor(public navCtrl: NavController, private alert: AlertService, private toast: ToastService, public navParams: NavParams, public viewCtrl: ViewController, private ds: DataService, private help: Helper) {
    this.switchDetailsData = this.navParams.get('editSwitch');
    this.homeName = this.navParams.get('homeName');
    this.sbNmae = this.navParams.get('sbNmae');
    this.rmName = this.navParams.get('rmName');
    this.name = this.switchDetailsData.name;
    (this.switchDetailsData.appliance_type) ? this.appliance_type_exists = true : this.appliance_type_exists = false;
    (this.switchDetailsData.appliance_subtype) ? this.appliance_subtype_exists = true : this.appliance_subtype_exists = false;
    this.sub_appliance_type = {};
    this.sub_appliance_type.name = this.switchDetailsData.appliance_subtype;
    this.sub_appliance_type.wattage = this.switchDetailsData.active_wattage;
    this.appliance_changed = false; this.sub_appliance_changed = false;
    // console.log(this.appliance_type);
  }

  ionViewWillEnter() {
    console.log(this.switchDetailsData);
    this.ds.getGeneralAppliances(
      (data) => {
        this.data = data;
        this.data = this.help.sorter(this.data, 'appname', '');
        for (let x of this.data) {
          if (x.appname == this.switchDetailsData.appliance_type) {
            x.selected = true;
            this.data2 = x.subtypes;
            for (let y of this.data2) {
              (y.name == this.switchDetailsData.appliance_subtype) ? y.selected = true : y.selected = false;
            }
          }
          else {
            x.selected = false;
          }
        }
        console.log(this.data);
      },
      (error) => {

      }
    )
    // if(this.appliance_subtype_exists)this.subType();
  }
  isEmpty() {
    console.log(this.name);
    if (this.help.isEmpty(this.name) || !this.appliance_type_exists) {
      console.log('im here insidee');
      return true;
    }
    else return false;
  }
  dismiss(data) {
    this.viewCtrl.dismiss(data);
  }

  subType() {
    console.log(this.appliance_type);
    this.appliance_type_exists = true;
    this.appliance_subtype_exists = true;
    this.appliance_changed = true;
    this.data2 = this.appliance_type.subtypes;
  }
  public updateSwitch() {
    if (this.isEmpty()) {
      this.alert.showAlert("Please fill all fields", "Cannot configure unless all the fields are filled");
    }
    else {
      console.log(this.sub_appliance_type);
      this.switchDetailsData.name = this.name;
      if (this.appliance_type) this.switchDetailsData.appliance_type = this.appliance_type.appname;
      if (this.sub_appliance_type) this.switchDetailsData.sub_appliance_type = this.sub_appliance_type.name;
      let obj = {
        "id": this.switchDetailsData._id,
        "name": this.switchDetailsData.name,
        "sboard_id": this.switchDetailsData.sboard_id,
        "appliance_type": this.switchDetailsData.appliance_type,
        "appliance_subtype": this.switchDetailsData.sub_appliance_type,
        "is_configured": true,
        "active_wattage": ""
      }
      if (this.sub_appliance_type) obj.active_wattage = this.sub_appliance_type.wattage;
      let is_configured = this.navParams.get('is_configured');
      this.ds.postEditSwitch(obj,
        data => {
          this.switchDetailsData.is_configured = true;
          this.toast.bottomToast_Short('Switch edited');
          this.navCtrl.pop();
        }, error => {
          console.log(JSON.stringify(error.json()));
        });

    }

  }

  checkSubType() {
    for (let i of this.data2) {
      if (this.sub_appliance_type.name == i.name) {
        return true;
      }
    }
    return false;
  }

}
