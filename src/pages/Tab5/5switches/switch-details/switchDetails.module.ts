import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SwitchDetailsPage } from './switchDetails';

@NgModule({
    declarations: [
        SwitchDetailsPage,
    ],
    imports: [
        IonicPageModule.forChild(SwitchDetailsPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class SwitchDetailsPageModule { }