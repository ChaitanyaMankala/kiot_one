import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateRoomPage } from './createroom';

@NgModule({
    declarations: [
        CreateRoomPage,
    ],
    imports: [
        IonicPageModule.forChild(CreateRoomPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class CreateRoomPageModule { }