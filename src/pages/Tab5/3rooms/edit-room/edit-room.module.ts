import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditRoomPage } from './edit-room';

@NgModule({
    declarations: [
        EditRoomPage,
    ],
    imports: [
        IonicPageModule.forChild(EditRoomPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class EditRoomPageModule { }