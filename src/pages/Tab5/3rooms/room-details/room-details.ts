import { Component } from '@angular/core';
import { NavController, NavParams,ModalController,IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../../providers/DataService';
import { AlertService } from '../../../../providers/AlertService';
import { PouchService } from '../../../../providers/PouchService';
import { ToastService } from '../../../../providers/ToastService';

//pages
import { EditRoomPage } from '../edit-room/edit-room';

@IonicPage()
@Component({
  selector: 'page-room-details',
  templateUrl: 'room-details.html'
})
export class RoomDetailsPage {
  room;
  constructor(public navCtrl: NavController, public navParams: NavParams,private toast:ToastService,public modalCtrl:ModalController,private ps:PouchService, private ds: DataService,private alert:AlertService) {

    this.room = this.navParams.get('room');
    console.log(this.room);
  }

  ionViewWillEnter() {
    console.log(this.navParams.get('home'));
  }

  deleteRoom() {
    this.alert.showConfirm("Are you sure?","this deletes the room and all switchboards in it","Cancel","Delete",
      ()=>{
        console.log('cancelled');
      },
      ()=>{
           let obj =  { room: this.room._id };
            this.ds.postDeleteRoom(obj,
            data => {
              this.ps.removeADoc('room',this.navParams.get('home'),this.room._id);
              console.log(data + "Successfully Deleted");
              // this.toaster("Edit Successful");
              this.toast.bottomToast_Short('Room deleted');
              this.navCtrl.pop();
            },
              error => {
                // this.toaster("error, cannot edit home");
              });

      });
 

  }
  goToEditRoom() {
  /*  this.navCtrl.push('EditRoomPage', {
      room: this.room
    });*/
   let editRoomModal =  this.modalCtrl.create('EditRoomPage',{
     room: this.room
   });
   editRoomModal.present();

  }

}
