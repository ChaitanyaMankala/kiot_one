import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RoomDetailsPage } from './room-details';

@NgModule({
    declarations: [
        RoomDetailsPage,
    ],
    imports: [
        IonicPageModule.forChild(RoomDetailsPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class RoomDetailsPageModule { }