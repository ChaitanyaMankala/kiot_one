import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DisplayRoomsPage } from './display-rooms';

@NgModule({
    declarations: [
        DisplayRoomsPage,
    ],
    imports: [
        IonicPageModule.forChild(DisplayRoomsPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class DisplayRoomsPageModule { }