import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IrAppDetailsPage } from './ir-app-details';

@NgModule({
    declarations: [
        IrAppDetailsPage,
    ],
    imports: [
        IonicPageModule.forChild(IrAppDetailsPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class IrAppDetailsPageModule { }