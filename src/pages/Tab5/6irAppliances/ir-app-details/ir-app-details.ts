import { Component } from '@angular/core';
import { NavController, NavParams, Events, ModalController,IonicPage } from 'ionic-angular';

//pages
import { EditIrAppPage } from '../edit-ir-app/edit-ir-app';
import { ConfigureRemotePage } from '../configure-remote/configure-remote';

//providers
import { DataService } from '../../../../providers/DataService';
import { AlertService } from '../../../../providers/AlertService';
import { LocalStorageService } from '../../../../providers/LocalStorageService';
import { ToastService } from '../../../../providers/ToastService';

@IonicPage()
@Component({
  selector: 'page-ir-app-details',
  templateUrl: 'ir-app-details.html'
})
export class IrAppDetailsPage {
  appliance; name; type; brand; room_id; home_name; paired_sb; edit_obj; devicesInfo; devices_loaded = false;
  constructor(public navCtrl: NavController, public navParams: NavParams, private toast: ToastService, public modalCtrl: ModalController, private local: LocalStorageService, private ds: DataService, private alert: AlertService, private ev: Events) {
    this.appliance = this.navParams.get('appliance');
    this.local.setTemp(this.appliance.stb_provider_id);
  }

  ionViewWillEnter() {
    console.log(this.appliance);
    this.in_page = true;
    this.name = this.appliance.name;
    this.type = this.appliance.type;
    this.brand = this.appliance.brand;
    this.room_id = this.appliance.room_id.name;
    this.paired_sb = this.appliance.paired_sb.sboard_name;
    this.home_name = this.navParams.get('home').display_name;
    console.log(this.appliance._id);
    console.log(this.edit_obj);
    if (this.edit_obj) {
      this.brand = this.edit_obj.brand;
    }

  }
  goToEditIrApp() {
    this.navCtrl.push('EditIrAppPage', {
      appliance: this.appliance,
      home_id: this.navParams.get('home')._id
    })
    /* this.modalCtrl.create('EditIrAppPage',{
       appliance:this.appliance,
       home_id:this.navParams.get('home')._id
     }).present();*/
  }

  deleteIr() {
    this.alert.showConfirm("Are you Sure?", "this deletes IR Appliance and corresponding remote", "No", "Delete",
      () => {
        console.log('cancel!!');
      }, () => {
        if (this.appliance._id.split('_')[0] == 'remote') {
          this.appliance._id = this.appliance._id.split('_')[3];
        }
        let obj = {
          "id": this.appliance._id
        };
        this.ds.postDeleteIrApp(obj,
          (data) => {
            console.log(data);
            this.toast.bottomToast_Short('IR Appliance Deleted');
            this.navCtrl.pop();
          },
          (error) => {
            console.log(error);
          });

      })
  }
  goToConfigureRemote() {
    if (this.appliance._id.split('_')[0] == 'remote') {
      this.appliance._id = this.appliance._id.split('_')[3];
    }
    this.navCtrl.push('ConfigureRemotePage', {
      _id: this.appliance._id,
      name: this.appliance.name,
      room: this.appliance.room_id._id,
      sb: this.appliance.paired_sb._id,
      device: this.appliance.paired_sb.device_id,
      // device:this.sb_id.device_id,
      home_id: this.navParams.get('home')._id,
      type: this.type,
      mode: "edit",
      codeset: this.appliance.remote_codeset,
      brand: this.appliance.brand,
      edit_obj: this.edit_obj
    })
  }

  next_spinner = false;
  testConnection() {
    if (!this.paired_sb) {
      this.alert.showAlert('No Paired Device', 'The device paired with this remote is either deleted or does not exist, Plaese change it in Edit on top right corner');
    }
    else {
      this.getSboards()
        .then(() => {
          //console.log(this.paired_sb);
          this.next_spinner = true;
          this.ds.testConnection(
            this.local.getLocalIp(this.devicesInfo[this.appliance.paired_sb.device_id].wifi_local_name),
            this.appliance.paired_sb.device_id,
            this.devicesInfo[this.appliance.paired_sb.device_id].apikey)
            .then(() => {
              this.showOKAlert();
            }).catch(() => {
              this.showErrAlert();
            })
        })
    }
  }

  getSboards() {
    return new Promise((resolve, reject) => {
      if (this.devices_loaded) {
        resolve(1);
      }
      else {
        this.ds.getSboards(this.navParams.get('home')._id,
          (data) => {
            this.devices_loaded = true;
            this.devicesInfo = {};
            for (let i of data) {
              for (let j of i.devices) {
                this.devicesInfo[j.device_id] = j.device_info;
              }
            }
            resolve(1);
          },
          (error) => {
            reject(-1);
            console.log(error);
            console.log(JSON.stringify(error.json()));

          }, false, 'room,swappliance,kiotdevice');
      }
    })

  }

  showOKAlert() {
    this.next_spinner = false;
    this.goToConfigureRemote();
  }
  in_page = true;
  showErrAlert() {
    this.next_spinner = false;
    if (this.in_page) {
      this.alert.showAlert('Unable to reach', 'Make sure that the device (' + this.paired_sb + ') is reachable or change the paired device');
    }
  }
  ionViewWillLeave() {
    this.in_page = false;
  }
  
}
