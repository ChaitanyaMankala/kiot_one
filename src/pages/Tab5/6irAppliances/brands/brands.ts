import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, Events,IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../../providers/DataService';
import { LocalStorageService } from '../../../../providers/LocalStorageService';

//pages
// import { RemoteSelectPage } from '../remote-select/remote-select';

@IonicPage()
@Component({
  selector: 'page-brands',
  templateUrl: 'brands.html',
  providers: []
})
export class BrandsPage {
  type: any; data: any; data2 = []; showList: any; codeset: any; select; brand; searchInput = "";show_loader;
  constructor(public navCtrl: NavController, public navParams: NavParams, private local: LocalStorageService, private alertCtrl: AlertController, private loading: LoadingController, private ds: DataService, private ev: Events) {

    this.brand = this.navParams.get('brand');
    this.select = this.navParams.get('select');
  }
  i: any; loader: any;
  ionViewDidLoad() {
    this.initializeItems();
  }
  providers;
  initializeItems() {
    this.type = this.navParams.get('type');
    if (this.type == 'Set Top Box') {
      this.show_loader=true;
      this.ds.getCableProviders(this.searchInput,
        data => {
          this.show_loader=false;
          this.data = data;
          this.providers = data;
          this.data2 = [];
          console.log(this.data);
          for (let i of this.data) {
            this.data2.push(i.title)
          }
          this.data2.sort(function (a: any, b: any) {
            var x = a < b ? -1 : 1;
            return x;
          });
          console.log(this.data2);
          this.data = this.data2;
        },
        error => {
          this.show_loader=false;
          console.log(error);
        }
      )
    }
    else {
      this.ds.getBrands(this.type,
        data => {
          this.data = data;
          console.log(this.type);
          this.data = this.data[0].brand_codeset[0];
          this.codeset = this.data;
          this.data = Object.keys(this.data);
          this.data.sort(function (a: any, b: any) {
            var x = a < b ? -1 : 1;
            return x;
          });
          this.data2 = this.data;
          console.log(this.data2);
        },
        (error) => {
          console.log(error);
        })
    }

  }

  filterItems(ev: any) {
    if (this.type != 'Set Top Box' && this.searchInput) {
      this.data2 = this.data;
      let val = ev.target.value;
      let result_data = this.data2.slice();
      if (val && val.trim() != '') {
        this.data2 = result_data.filter((item: any) => {
          return (item.toLowerCase().lastIndexOf(val.toLowerCase(), 0) === 0);
        })
      }
      else {
        this.data2 = result_data;
      }

    }
    else if (this.type == 'Set Top Box'){
      this.initializeItems();
    }
  }

  codeDisplay(brand: any) {
    this.brand = brand;
    if (this.type == 'Set Top Box') {
      for (let i of this.providers) {
        if (i.title == this.brand) {
          console.log(i);
          this.local.setTemp(i.id);
        }
      }
    }
    this.select = true;
    this.ev.publish('brand', this.brand);
    console.log(this.brand);
    this.navCtrl.pop();
  }

}
