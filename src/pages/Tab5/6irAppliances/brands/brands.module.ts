import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BrandsPage } from './brands';

@NgModule({
    declarations: [
        BrandsPage,
    ],
    imports: [
        IonicPageModule.forChild(BrandsPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class BrandsPageModule { }