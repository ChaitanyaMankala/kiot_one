import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfigureRemotePage } from './configure-remote';

@NgModule({
    declarations: [
        ConfigureRemotePage,
    ],
    imports: [
        IonicPageModule.forChild(ConfigureRemotePage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class ConfigureRemotePageModule { }