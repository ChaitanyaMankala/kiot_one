import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditIrAppPage } from './edit-ir-app';

@NgModule({
    declarations: [
        EditIrAppPage,
    ],
    imports: [
        IonicPageModule.forChild(EditIrAppPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class EditIrAppPageModule { }