import { Component } from '@angular/core';
import { NavController, NavParams,IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../../providers/DataService';
import { LoadingService } from '../../../../providers/LoadingService';
import { LocalStorageService } from '../../../../providers/LocalStorageService';
import { AlertService } from '../../../../providers/AlertService';

@IonicPage()
@Component({
  selector: 'page-set-up-remote',
  templateUrl: 'set-up-remote.html'
})
export class SetUpRemotePage {
  data;
  brand;
  type;
  number_of_remotes;
  row1_keys = [];
  row2_keys = [];
  keys = [];
  present_remote;
  waiting;
  disable_next;
  disable_prev;
  moreThanOne;
  codeset = [];
  switchboard_id;
  is_ac; remote_anim;
    icons;
  constructor(public navCtrl: NavController, public navParams: NavParams, private alert: AlertService, private local: LocalStorageService, private ds: DataService, private load: LoadingService) {
    this.brand = this.navParams.get('brand');
    this.type = this.navParams.get('type');
    this.waiting = false;
    this.icons={};
    this.disable_next = false;
    this.disable_prev = true;
    this.moreThanOne = false;
    this.codeset = this.navParams.get('codeset');
    this.switchboard_id = this.navParams.get('sb');
  }
  no_remotes;
  ionViewDidLoad() {
    let provider = null; let brand = null;
    (this.type == 'Set Top Box') ? provider = this.local.getTemp() : brand = this.brand;
    this.ds.getTestCodeSets(this.type,
      (data) => {
        this.no_remotes = false;
        console.log(data);

        if (data[0]) {
          this.data = data;
          this.number_of_remotes = this.data.length;
          this.is_ac = data[0].is_ac;
          if (this.number_of_remotes > 1) this.moreThanOne = true;
          this.present_remote = 1;
          this.remoteConfig();
        }
        else {
          this.no_remotes = true;
          this.alert.showAlert('Sorry', 'No remotes are available in our database. But You can still use Entertainment Guide!');
          this.codeset[0] = 'not available';
          this.navCtrl.pop();
        }
      },
      (error) => {

      }, brand, provider);
  }

  remoteConfig() {
    if (this.is_ac) {
      this.keys = [];
      for (let i of this.data[this.present_remote - 1].keys) {
        this.keys.push(i.func);
        //this.icons[i] = i.toLowerCase().split(' ').join('-');
      }
    }
    else if (this.type == 'Set Top Box') {
      this.keys = Object.keys(this.data[this.present_remote - 1].keys[0]);
      for (let i of this.keys) {
        this.icons[i] = i.toLowerCase().split(' ').join('-');
      }
    }
    else {
      this.keys = Object.keys(this.data[this.present_remote - 1].keys);
      for (let i of this.keys) {
        this.icons[i] = i.toLowerCase().split(' ').join('-');
      }
    }

  }
  triggerIr(key, ind) {
    this.waiting = true;
    let keyHere = this.data[this.present_remote - 1].keys;
    let len;
    let obj;
    let protocol; let proto_name; let wave_gap;
    if (this.is_ac) {
      console.log('im here at ACs');
      len = parseInt(keyHere[ind].first_seq_len) + parseInt(keyHere[ind].sec_seq_len);
      obj = {
        "code": keyHere[ind].code,
        "device": this.navParams.get('device'),
        "freq": this.data[this.present_remote - 1].freq,
        "len": len,
        "first_seq_len": keyHere[ind].first_seq_len,
        "sec_seq_len": keyHere[ind].sec_seq_len,
        "rpt_cnt": this.data[this.present_remote - 1].rpt_cnt,
        "switchboard": this.switchboard_id
      };


    }
    else {
      switch (this.data[this.present_remote - 1].type) {
        case 1: {
          let keyHere = this.data[this.present_remote - 1].keys[0];
          console.log('im here at 1');
          len = this.data[this.present_remote - 1].len;
          protocol = this.data[this.present_remote - 1].protocol;
          proto_name = this.data[this.present_remote - 1].proto_name;
          wave_gap = this.data[this.present_remote - 1].wave_gap;
          obj = {
            "device": this.navParams.get('device'),
            "code": keyHere[key],
            "len": len,
            "proto_name": proto_name,
            "protocol": protocol,
            "wave_gap": wave_gap,
            "switchboard": this.switchboard_id
          };
          break;
        }
        case 2: {
          console.log('im here at 2');
          len = parseInt(keyHere[key].first_seq_len) + parseInt(keyHere[key].sec_seq_len);
          obj = {
            "device": this.navParams.get('device'),
            "freq": this.data[this.present_remote - 1].freq,
            "code": keyHere[key].code,
            "len": len,
            "first_seq_len": keyHere[key].first_seq_len,
            "sec_seq_len": keyHere[key].sec_seq_len,
            "rpt_cnt": this.data[this.present_remote - 1].rpt_cnt,
            "switchboard": this.switchboard_id

          };
          break;
        }
        default: {
          console.log('error');
          break;
        }
      }



    }
    // console.log(obj);
    this.ds.postTriggerIr(obj,
      (data) => {
        console.log(data);
        this.waiting = false;
      },
      (error) => {
        console.log(error);
        this.waiting = false;
      });
  }
  nextRemote() {
    console.log(this.number_of_remotes);
    console.log(this.present_remote);
    this.present_remote++;
    this.remoteConfig();
    console.log(this.number_of_remotes);
    console.log(this.present_remote);
    if (this.present_remote == this.number_of_remotes) {
      this.disable_next = true;
      this.disable_prev = false;
    }
    else if (this.present_remote < this.number_of_remotes) {
      this.disable_prev = false;
    }
    else {
      this.disable_next = true;
    }

    setTimeout(() => {
      this.remote_anim = 'sliding-remote';
    }, 10);
    this.remote_anim = '';

  }

  prevRemote() {
    this.remote_anim = '';
    this.present_remote--;
    this.remoteConfig();
    if (this.present_remote == 1) {
      this.disable_next = false;
      this.disable_prev = true;
    }
    else if (this.present_remote > 1) {
      this.disable_next = false;
    }
    else {
      this.disable_prev = true;
    }
    this.remote_anim = 'sliding-remote';
  }
  setRemote() {
    this.codeset.splice(0, 2);
    this.codeset.push(this.data[this.present_remote - 1]._id);
    this.codeset.push(this.data[this.present_remote - 1].name);
    //this.codeset[0] = this.data[this.present_remote - 1].name;
    console.log(this.codeset);
    this.navCtrl.pop();
  }
}
