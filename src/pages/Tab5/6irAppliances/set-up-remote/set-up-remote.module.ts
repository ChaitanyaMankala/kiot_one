import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SetUpRemotePage } from './set-up-remote';

@NgModule({
    declarations: [
        SetUpRemotePage,
    ],
    imports: [
        IonicPageModule.forChild(SetUpRemotePage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class SetUpRemotePageModule { }