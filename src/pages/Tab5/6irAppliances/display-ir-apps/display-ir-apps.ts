import { Component } from '@angular/core';
import { NavController, NavParams,IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../../providers/DataService';

//pages
import { CreateIrAppPage } from '../create-ir-app/create-ir-app';
import { IrAppDetailsPage } from '../ir-app-details/ir-app-details';

@IonicPage()
@Component({
  selector: 'page-display-ir-apps',
  templateUrl: 'display-ir-apps.html'
})
export class DisplayIrAppsPage {
  home; data; no_remotes_loader = true;
  constructor(public navCtrl: NavController, public navParams: NavParams, private ds: DataService) {
    this.home = this.navParams.get('home');
  }

  ionViewWillEnter() {
    this.ds.getIrAppliances(this.home._id,
      (data) => {
        this.no_remotes_loader = false;
        console.log(data);
        this.data = data.appliances;
      },
      (error) => {
        this.no_remotes_loader = false;
        console.log(error);
      });



  }
  goToIrDetails(item) {
    this.navCtrl.push('IrAppDetailsPage', {
      appliance: item,
      home: this.home
    })

  }

  addCustomRemote() {
    /*  this.navCtrl.push('RemotesListPage',{
        list:this.data,
        home_id:this.home._id
      });*/
  }
  addIrAppliance() {
    this.navCtrl.push('CreateIrAppPage', {
      home_id: this.home._id
    });
  }

}
