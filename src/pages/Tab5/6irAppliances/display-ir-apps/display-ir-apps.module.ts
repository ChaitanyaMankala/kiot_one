import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DisplayIrAppsPage } from './display-ir-apps';

@NgModule({
    declarations: [
        DisplayIrAppsPage,
    ],
    imports: [
        IonicPageModule.forChild(DisplayIrAppsPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class DisplayIrAppsPageModule { }