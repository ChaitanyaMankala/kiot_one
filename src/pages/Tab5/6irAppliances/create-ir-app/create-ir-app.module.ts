import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateIrAppPage } from './create-ir-app';

@NgModule({
    declarations: [
        CreateIrAppPage,
    ],
    imports: [
        IonicPageModule.forChild(CreateIrAppPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class CreateIrAppPageModule { }