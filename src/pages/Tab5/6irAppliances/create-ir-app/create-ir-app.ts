import { Component } from '@angular/core';
import { NavController, NavParams,IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../../providers/DataService';
import { AlertService } from '../../../../providers/AlertService';
import { Helper } from '../../../../providers/Helper';
import { LocalStorageService } from '../../../../providers/LocalStorageService';

//pages
import { ConfigureRemotePage } from '../configure-remote/configure-remote';

@IonicPage()
@Component({
  selector: 'page-create-ir-app',
  templateUrl: 'create-ir-app.html'
})
export class CreateIrAppPage {
  data; name; room_id; type; items = []; items1 = []; disSb; sb = []; room; sb_id; devicesInfo;
  constructor(public navCtrl: NavController, public navParams: NavParams, private ds: DataService, private local: LocalStorageService, private alert: AlertService, private help: Helper) {
    this.disSb = true;
    this.local.setTemp('');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateIrAppPage');
    this.ds.getSboards(this.navParams.get('home_id'),
      (data) => {
        console.log(data);
        this.items = data;
        this.devicesInfo = {};
        for (let i of this.items) {
          for (let j of i.devices) {
            this.devicesInfo[j.device_id] = j.device_info;
          }
        }
      },
      (error) => {
        console.log(error);
        console.log(JSON.stringify(error.json()));

      }, false, 'room,swappliance,kiotdevice');
  }
  goToConfigure() {

    this.navCtrl.push('ConfigureRemotePage', {
      name: this.name,
      room_id: this.room,
      sb: this.sb_id._id,
      device: this.sb_id.device_id,
      home_id: this.navParams.get('home'),
      type: this.type,
      mode: "create"
    });

  }
  onRoomChange() {
    console.log(this.room);
    for (let i of this.items) {
      if (i.room_id == this.room) {
        this.sb = i.devices;
        console.log(this.sb);
      }
    }
    this.disSb = false;


  }

  testConnection() {
    if (this.help.isEmpty(this.room) == false && this.help.isEmpty(this.sb_id) == false && this.help.isEmpty(this.name) == false && this.help.isEmpty(this.type) == false) {
      this.next_spinner = true;
      this.ds.testConnection(
        this.local.getLocalIp(this.devicesInfo[this.sb_id.device_id].wifi_local_name),
        this.sb_id.device_id,
        this.devicesInfo[this.sb_id.device_id].apikey)
        .then(() => {
          this.showOKAlert();
        }).catch(() => {
          this.showErrAlert();
        })
    }
    else {
      this.alert.showAlert("Fill the details", "please fill all the fields to continue");
    }

  }

  next_spinner = false;
  showOKAlert() {
    this.next_spinner = false;
    this.goToConfigure();
  }
  showErrAlert() {
    this.next_spinner = false;
    this.alert.showAlert('Unable to reach', 'Make sure that the device (' + this.sb_id.sboard_name + ') is reachable or change the paired device');
  }
}


