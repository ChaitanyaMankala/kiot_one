import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrimeUserDetailsPage } from './prime-user-details';

@NgModule({
    declarations: [
        PrimeUserDetailsPage,
    ],
    imports: [
        IonicPageModule.forChild(PrimeUserDetailsPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class PrimeUserDetailsPageModule { }