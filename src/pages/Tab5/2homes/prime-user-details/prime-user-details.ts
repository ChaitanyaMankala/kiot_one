import { Component } from '@angular/core';
import { NavController, NavParams,IonicPage } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-prime-user-details',
  templateUrl: 'prime-user-details.html'
})
export class PrimeUserDetailsPage {
home;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.home = this.navParams.get('home');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PrimeUserDetailsPage');
  }

}
