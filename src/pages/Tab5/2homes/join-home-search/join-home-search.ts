import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, ViewController,IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../../providers/DataService';
import { LocalStorageService } from '../../../../providers/LocalStorageService';
import { ToastService } from '../../../../providers/ToastService';


@IonicPage()
@Component({
  selector: 'page-join-home-search',
  templateUrl: 'join-home-search.html'
})
export class JoinHomeSearchPage {
  show; searchStr; data; home; user; showNotFound; home_id;
  constructor(public navCtrl: NavController, public navParams: NavParams, private toast: ToastService, private viewCtrl: ViewController, private ds: DataService, private toastCtrl: ToastController) {
    this.show = false;
    this.showNotFound = false;
    this.searchStr = "";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad JoinHomeSearchPage');
  }
  dismiss(data) {
    this.viewCtrl.dismiss(data);
  }
  search(str) {
    console.log(str);
    this.ds.getSearchHome(str,
      data => {
        console.log(data);
        this.data = data;
        //this.data = JSON.parse(this.data._body);
        console.log(this.data);
        this.data = this.data[0];
        this.home = this.data.name;
        this.home_id = this.data._id;
        this.user = this.data.prim_user;
        this.user = this.user.name;
        this.showNotFound = false;
        this.show = true;
      },
      error => {
        this.show = false;
        try {
          this.showNotFound = JSON.parse(error._body).message;
        }
        catch (err) { }
        //this.toaster("error, cannot edit home");toastHere
      }, true, true);

  }
  sendJoinRequest() {
    this.ds.postJoinRequest({
      name: this.home,
      home: this.home_id
    },
      data => {
        console.log(data);
        this.toast.bottomToast_Short("Join request sent to " + this.user);
        this.dismiss(0);
      },
      error => {
        console.log(error);
        //this.toaster("error, cannot edit home");//toastHere
      });

  }

}
