import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { JoinHomeSearchPage } from './join-home-search';

@NgModule({
    declarations: [
        JoinHomeSearchPage,
    ],
    imports: [
        IonicPageModule.forChild(JoinHomeSearchPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class JoinHomeSearchPageModule { }