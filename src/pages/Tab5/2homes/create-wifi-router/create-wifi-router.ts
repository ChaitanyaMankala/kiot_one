import { Component } from '@angular/core';
import { NavController, NavParams,ViewController,IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../../providers/DataService';
import { ToastService } from '../../../../providers/ToastService';

@IonicPage()
@Component({
  selector: 'page-create-wifi-router',
  templateUrl: 'create-wifi-router.html'
})
export class CreateWifiRouterPage {
ssid;pass;routers;disCreate=true;
  constructor(public navCtrl: NavController, public navParams: NavParams,private toast:ToastService,private ds:DataService,public viewCtrl:ViewController) {
        this.routers = this.navParams.get('routers');
  }
dismiss(data){
  this.viewCtrl.dismiss(data);
}
  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateWifiRouterPage');
  }
  createWifiRouter(){
    console.log(this.navParams.get('home_id'));
        let obj={
          "action":"add",
          "ssid": this.ssid,
          "password": this.pass,
          "home": this.navParams.get('home_id')
        }
        let obj2={
          "name": this.ssid,
          "password": this.pass
        }
        this.ds.postCreateWifiRouter(obj,
        data => {
                console.log(data);
                 this.toast.bottomToast_Short('Wi-Fi Router Added');
                 this.routers.push(obj2);
                  this.navCtrl.pop();
        
         },error=>{
            console.log(error);
        });
        
  }
  onChangeSSID(){
      if(this.ssid.length>3){
        this.disCreate = false;
      }
      else{
        this.disCreate = true;
      }
  }

}
