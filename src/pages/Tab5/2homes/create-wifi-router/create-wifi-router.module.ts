import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateWifiRouterPage } from './create-wifi-router';

@NgModule({
    declarations: [
        CreateWifiRouterPage,
    ],
    imports: [
        IonicPageModule.forChild(CreateWifiRouterPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class CreateWifiRouterPageModule { }