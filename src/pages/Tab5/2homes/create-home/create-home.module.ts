import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateHomePage } from './create-home';

@NgModule({
    declarations: [
        CreateHomePage,
    ],
    imports: [
        IonicPageModule.forChild(CreateHomePage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class CreateHomePageModule { }