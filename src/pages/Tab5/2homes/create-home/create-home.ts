import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, Platform,IonicPage } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { AndroidPermissions } from '@ionic-native/android-permissions';

//providers
import { DataService } from '../../../../providers/DataService';
import { LocalStorageService } from '../../../../providers/LocalStorageService';
import { AlertService } from '../../../../providers/AlertService';
import { ToastService } from '../../../../providers/ToastService';

//pages
import { TabsPage } from '../../../../pages/tabs/tabs.page';
import { LoginPage } from '../../../../pages/Start/login/login';

@IonicPage()
@Component({
  selector: 'page-create-home',
  templateUrl: 'create-home.html',
  providers: [Geolocation]
})
export class CreateHomePage {
  data; showError; response; latlong = []; def; show; grab; disable_create; from_setup;
  constructor(public navCtrl: NavController, private platform: Platform, private geolocation: Geolocation, public perm: AndroidPermissions, public navParams: NavParams, private ds: DataService, private toast: ToastService, private toastCtrl: ToastController, private local: LocalStorageService, private alert: AlertService) {
    this.from_setup = this.navParams.get('from_setup') || false;
    this.disable_create = true;
    this.response = {
      name: "",
      dName: ""
    };
    this.show = this.navParams.get('show');
    this.grab = false;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateHomePage');
  }
  checkNameAvailablitiy(checkName) {

    this.ds.postCheckExisting({
      "name": checkName //object to send on post request

    }, data => {

      this.data = data;

      //this.data = JSON.parse(this.data._body);
      console.log(data);
      if (this.data.availaible == false) {
        this.showError = true;
      } else if (this.data.availaible == true) {
        this.showError = false;
      }
    },
      error => {
        //   //   let alert1 = this.alertCtrl.create({
        //   //     title: 'Registration Failed',
        //   //     subTitle: 'Check',
        //   //     buttons: ['OK']
        //   //   });
        //   // alert1.present();
        //   // console.log(error);
        //   //   console.log(JSON.stringify(error.json()));
      });

  }


  createHome() {
    if (this.validate()) {
      if (this.show == false) this.def = true;
      this.checkNameAvailablitiy(this.response.name);
      let obj = {
        "name": this.response.name,
        "display_name": this.response.dName,
        "is_default": this.def,
        "lati": this.latlong[0],
        "longi": this.latlong[1]
      };

      this.ds.postCreateHome(obj,
        data => {
          console.log(data);
          this.data = data;
          if (!this.showError) {
            this.toast.bottomToast_Short("home added successfully!");
            if (this.def == true || this.from_setup) {
              this.ds.putEditProfile({ "default_home": this.data.home._id, new: true },
                (data) => {
                  console.log("default home set");
                },
                (error) => {
                  console.log('error');
                })
              this.local.setDefaultHome(this.data.home._id);
            }
            if (!this.from_setup) {
              this.navCtrl.pop();
            }
            else {
              this.local.setCurrentHome(this.data.home._id);
              this.navCtrl.setRoot(TabsPage);
            }
          }

        }, error => {
          this.toast.bottomToast_Long("Home Name already exists, please try another name");
          console.log(error);
        });
    }
  }
  validate() {
    let regexp = /^\S*$/;
    let no_spaces = new RegExp(regexp);
    if (this.response.dName.length > 15 || this.response.name.length > 15) {
      this.toast.bottomToast_Long("name must not exceed 15 characters");
      return false;
    }
    else if (!no_spaces.test(this.response.name)) {
      this.toast.bottomToast_Long("Home name (Unique) must not contain 'Spaces'");
      return false;
    }
    else return true;
  }

  locate() {

    this.alert.showConfirm("Grab current location?", "Your current location is taken as home's location", "Cancel", "Okay",
      () => {
        console.log("Cancel");
      }, () => {
        if (this.platform.is('android') && 0) {
          this.perm.checkPermission(this.perm.PERMISSION.ACCESS_FINE_LOCATION).then(
            success => {
              this.grabLocation();
            },
            err => {
              this.toast.bottomToast_Short('Please grant persmssion to access location and try again');
              this.perm.requestPermission(this.perm.PERMISSION.ACCESS_FINE_LOCATION);
            }
          );
        }
        else {
          this.grabLocation();
        }


      });
  }
  grabLocation() {
    this.geolocation.getCurrentPosition().then(res => {
      this.latlong[0] = JSON.stringify(res.coords.latitude);
      this.latlong[1] = JSON.stringify(res.coords.longitude);
      console.log(this.latlong);
      this.grab = true;
    }).catch((error) => {
      console.log('Error getting location', error);
      this.alert.showAlert("Error", error);
    });
  }
  enableCreate() {
    this.disable_create = this.response.name && this.response.dName ? false : true;
  }

}
