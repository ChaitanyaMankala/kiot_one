import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomeUsersPage } from './home-users';

@NgModule({
    declarations: [
        HomeUsersPage,
    ],
    imports: [
        IonicPageModule.forChild(HomeUsersPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class HomeUsersPageModule { }