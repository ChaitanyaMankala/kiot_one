import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DisplayWifiRoutersPage } from './display-wifi-routers';

@NgModule({
    declarations: [
        DisplayWifiRoutersPage,
    ],
    imports: [
        IonicPageModule.forChild(DisplayWifiRoutersPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class DisplayWifiRoutersPageModule { }