import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WifiDetailsPage } from './wifi-details';

@NgModule({
    declarations: [
        WifiDetailsPage,
    ],
    imports: [
        IonicPageModule.forChild(WifiDetailsPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class WifiDetailsPageModule { }