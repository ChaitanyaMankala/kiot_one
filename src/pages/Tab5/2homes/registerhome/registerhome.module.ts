import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterHomePage } from './registerhome';

@NgModule({
    declarations: [
        RegisterHomePage,
    ],
    imports: [
        IonicPageModule.forChild(RegisterHomePage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class RegisterHomePageModule { }