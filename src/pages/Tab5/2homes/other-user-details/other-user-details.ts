import { Component } from '@angular/core';
import { NavController, NavParams,IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../../providers/DataService';
import { AlertService } from '../../../../providers/AlertService';
import { LocalStorageService } from '../../../../providers/LocalStorageService';

@IonicPage()
@Component({
  selector: 'page-other-user-details',
  templateUrl: 'other-user-details.html'
})
export class OtherUserDetailsPage {
  name; phone; email; other; ind; show_delete_user; prim_email; my_email;
  constructor(public navCtrl: NavController, private alert: AlertService, private local: LocalStorageService, public navParams: NavParams, private ds: DataService) {
    this.name = this.navParams.get('name');
    this.phone = this.navParams.get('phone');
    this.email = this.navParams.get('email');
    this.other = this.navParams.get('other');
    this.ind = this.navParams.get('ind');
    this.prim_email = this.navParams.get('prim_email');
    this.my_email = this.local.getUser().email;
    console.log(this.local.getUser().email);
    console.log(this.email);
     this.show_delete_user = this.my_email == this.email ? false : true;
     this.show_delete_user = this.my_email == this.prim_email ? true : false;
    console.log(this.show_delete_user);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OtherUserDetailsPage');
  }
  deleteUser() {
    let obj = {
      "user": this.navParams.get('_id'),
      "home": this.navParams.get('home')
    }

    this.ds.postRemoveUser(obj,
      data => {
        console.log(data);
        /* let j=0;
         for(let i of this.other){
           if(i._id = this.navParams.get('_id')){
             this.other.splice(j,1);
             console.log(this.other);
           }JSON.parse(
           j++;
         }*/
        this.other.splice(this.ind, 1);
        //alertHere
        this.navCtrl.pop();
      }, error => {
        console.log(error);
        console.log(JSON.parse(error._body).err);
        this.alert.showAlert('Cannot delete', JSON.parse(error._body).err);
      }, true, true);
  }
}
