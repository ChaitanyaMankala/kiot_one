import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OtherUserDetailsPage } from './other-user-details';

@NgModule({
    declarations: [
        OtherUserDetailsPage,
    ],
    imports: [
        IonicPageModule.forChild(OtherUserDetailsPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class OtherUserDetailsPageModule { }