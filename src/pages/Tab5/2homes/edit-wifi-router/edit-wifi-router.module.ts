import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditWifiRouterPage } from './edit-wifi-router';

@NgModule({
    declarations: [
        EditWifiRouterPage,
    ],
    imports: [
        IonicPageModule.forChild(EditWifiRouterPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class EditWifiRouterPageModule { }