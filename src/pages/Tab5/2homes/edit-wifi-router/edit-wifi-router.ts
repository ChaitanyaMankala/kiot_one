import { Component } from '@angular/core';
import { NavController, NavParams,ViewController,IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../../providers/DataService';
import { ToastService } from '../../../../providers/ToastService';


@IonicPage()
@Component({
  selector: 'page-edit-wifi-router',
  templateUrl: 'edit-wifi-router.html'
})
export class EditWifiRouterPage {
ssid;pass;wifi;
  constructor(public navCtrl: NavController,private toast:ToastService, public navParams: NavParams,private ds:DataService,public viewCtrl:ViewController) {
    this.wifi= this.navParams.get('wifi');
   // console.log(this.wifi);

  }

  ionViewWillEnter() {
        this.ssid=this.wifi.name;
    this.pass =  this.wifi.password;
    console.log('ionViewDidLoad EditWifiRouterPage');
  }
  dismiss(data){
    this.viewCtrl.dismiss(data);
  }
 editWifiRouter(){
   if(this.ssid.length>3){
        let obj={
          "action":"update",
          "ssid": this.ssid,
          "password": this.pass,
          "home": this.navParams.get('home')
        }
        this.ds.postEditWifiRouter(obj,
        data => {
                console.log(data);
                this.wifi.password = this.pass;
                 this.toast.bottomToast_Short("Edited successfully!");
                  this.navCtrl.pop();
        },error=>{
            console.log(error);
        });
   }
   else{
     this.toast.bottomToast_Short('Wi-Fi SSID must be alteast 3 characters long');
   }
  }
}
