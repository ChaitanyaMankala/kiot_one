import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditHomePage } from './edit-home';

@NgModule({
    declarations: [
        EditHomePage,
    ],
    imports: [
        IonicPageModule.forChild(EditHomePage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class EditHomePageModule { }