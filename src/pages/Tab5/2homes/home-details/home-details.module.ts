import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomeDetailsPage } from './home-details';

@NgModule({
    declarations: [
        HomeDetailsPage,
    ],
    imports: [
        IonicPageModule.forChild(HomeDetailsPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class HomeDetailsPageModule { }