import { Component } from '@angular/core';
import { Nav, NavController, App, NavParams, AlertController, ToastController, ModalController,IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../../providers/DataService';
import { LocalStorageService } from '../../../../providers/LocalStorageService';
import { AlertService } from '../../../../providers/AlertService';
import { ToastService } from '../../../../providers/ToastService';

//pages
import { HomeUsersPage } from '../home-users/home-users';
import { EditHomePage } from '../edit-home/edit-home';
import { DisplayWifiRoutersPage } from '../display-wifi-routers/display-wifi-routers';
import { TabsPage } from '../../../tabs/tabs.page';
import { LoginPage } from '../../../Start/login/login';

@IonicPage()
@Component({
  selector: 'page-home-details',
  templateUrl: 'home-details.html'
})
export class HomeDetailsPage {
  home; home_name; home_dName; latlong; def; lat: string; long: string; showEnterHome; obj; isDefault;
  constructor(public navCtrl: NavController, public nav: Nav, public app: App, public navParams: NavParams, private toast: ToastService, private alertCtrl: AlertController, public modalCtrl: ModalController, private ds: DataService, private toastCtrl: ToastController, private local: LocalStorageService, private alert: AlertService) {
    this.home = this.navParams.get('home');
    if (this.home._id == this.local.getDefaultHome()) {
      this.def = "yes";
      this.isDefault = true;
    }
    else {
      this.def = "no";
      this.isDefault = false;
    }

    this.obj = this.local.getUser();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomeDetailsPage');

  }
  ionViewWillEnter() {
    if (this.local.getCurrentHome() == this.home._id) this.showEnterHome = false;
    else this.showEnterHome = true;
    this.home_dName = this.home.display_name;
    this.home_name = this.home.name;
    if (this.home.lati && this.home.longi) {
      this.lat = this.home.lati.slice(0, 5);
      this.long = this.home.longi.slice(0, 5);
    }
  }

  deleteHome() {
    if (this.local.getDefaultHome() != this.home._id) {
      /* let confirm = this.alertCtrl.create({
         title: 'Delete Home?',
         message: 'Deleting a home deletes all the rooms and settings from your profile',
         buttons: [
           {
             text: 'Cancel',
             handler: () => {
                           console.log('Disagree clicked');
             }
           },
           {
             text: 'Delete',
             handler: () => {
               console.log('Agree clicked');
               this.ds.postDeleteHome({
                 'home':this.home._id
                 },data => {
                   console.log(data);
                     this.toaster("Home Deleted");
                   // this.toaster("home added successfully!");
                     this.navCtrl.pop();
                 },error=>{

                   });
             
               //this.navCtrl.pop();
             }
           }
         ]
       });
       confirm.present();*/
      this.alert.showConfirm("Delete Home?", "Deleting a home deletes all the rooms and settings from your profile", "Cancel", "Delete",
        () => {
          console.log('Disagree clicked');
        }, () => {
          console.log('Agree clicked');
          this.ds.postDeleteHome({
            'home': this.home._id
          }, data => {
            console.log(data);
            this.local.setCurrentHome(this.local.getDefaultHome());
            this.toast.bottomToast_Short('Home Deleted');
            this.navCtrl.pop();
          }, error => {
            console.log(error);
            if (error.status == 403) {
              this.alert.showAlert('Cannot delete', 'You are not the Prime User to delete the home');
            }
          }, false, true);

        });


    }
    else {
      this.alert.showAlert('Cannot Delete', 'Default Home can not be deleted, please change your default home');

    }
  }
  tabs: TabsPage;
  enterHome() {
    this.alert.showConfirm("Are you sure?", "Entering home allows you to control the appliances of this home", "Cancel", "Enter",
      () => {
        console.log('cancelled');
      },
      () => {
        this.showEnterHome = false;
        this.local.setCurrentHome(this.home._id);
        // this.navCtrl.parent.select(1);
        // window["myVar"] = this.navCtrl.parent;
        this.toast.bottomToast_Short('Entered into ' + this.home_dName);
        let v = this.nav.getActive();
        //this.nav.pop();
        this.nav.setRoot(TabsPage);
        // this.app.getRootNav().getActiveChildNav().select(0);
        //this.navCtrl.parent.setFunc();
        // console.log(this.navCtrl.getViews());
        // window.location.reload();
        // document.location.href = 'index.html';
        //this.navCtrl.setPages([TabsPage]);
        //this.navCtrl.setRoot(TabsPage);

      });

  }
  goToWifiRouters() {
    this.navCtrl.push('DisplayWifiRoutersPage',
      {
        home: this.navParams.get('home')
      });
  }
  goToHomeUsers() {
    this.navCtrl.push('HomeUsersPage', {
      home: this.navParams.get('home')
    })
  }
  goToEditHome() {
    this.navCtrl.push('EditHomePage', {
      home: this.navParams.get('home')
    })
    /*
    this.modalCtrl.create('EditHomePage',{
      home:this.navParams.get('home')
    }).present();*/
  }
  makeDefaultHome() {
    let obj = {
      "default_home": this.home._id,
      new: false
    }
    this.ds.putEditProfile(obj, data => {
      console.log(data);
      this.isDefault = true;
      this.toast.bottomToast_Short('Default Home Changed to ' + this.home_dName);
      this.local.setUser(this.obj);
      this.local.setDefaultHome(this.home._id);
    }, error => {
      console.log(error);
      console.log(JSON.stringify(error.json()));
    });

  }
}
