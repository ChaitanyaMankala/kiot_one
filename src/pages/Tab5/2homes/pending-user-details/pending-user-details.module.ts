import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PendingUserDetailsPage } from './pending-user-details';

@NgModule({
    declarations: [
        PendingUserDetailsPage,
    ],
    imports: [
        IonicPageModule.forChild(PendingUserDetailsPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class PendingUserDetailsPageModule { }