import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomeListPage } from './home-list';

@NgModule({
    declarations: [
        HomeListPage,
    ],
    imports: [
        IonicPageModule.forChild(HomeListPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class HomeListPageModule { }