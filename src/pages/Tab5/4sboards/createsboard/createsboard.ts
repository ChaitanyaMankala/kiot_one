import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Component } from '@angular/core';
import { NavController, NavParams, Platform, IonicPage } from 'ionic-angular';
// import { BarcodeScanner } from '@ionic-native/barcode-scanner';

//providers
import { DataService } from '../../../../providers/DataService';
import { ToastService } from '../../../../providers/ToastService';
import { AlertService } from '../../../../providers/AlertService';

//pages
import { DisplaySwitchesPage } from '../../5switches/display-switches/display-switches';

declare var cordova;

@IonicPage()
@Component({
  selector: 'page-createsboard',
  templateUrl: 'createsboard.html',
  providers: []
})
export class CreatesboardPage {
  public roomArray: [{ room_name: '', room_id: '' }];
  public roomArrayData: [{ room_name: '', room_id: '' }];
  public newRoomdata: [{ roomName: '', roomId: '' }];

  public registerSwitchBoardCredentials = {
    switchBoardName: '',
    device_id: '',
    device_type: '',
    sType: '',
    roomId: '',
    device_pin: ''
  }
  public data;
  showLoader_forCheck = false;
  constructor(public navCtrl: NavController, private bScanner: BarcodeScanner, public navParams: NavParams, private alert: AlertService, public platform: Platform, private toast: ToastService, public ds: DataService) {
    this.getRoomsList(this.navParams.get('homeId'))

  }
  ionViewWillEnter() {
    this.registerSwitchBoardCredentials.device_type = this.navParams.get('device_type');
  }
  public getRoomsList(id: any) {
    this.ds.getHomesRooms(id,
      (data) => {
        this.roomArrayData = data.rooms;

      },
      (error) => {
        console.log(JSON.stringify(error.json()));

      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreatesboardPage');
  }

  registerSwitchBoard() {
    console.log(this.navParams.get('room'));
    if (this.validate()) {
      let obj2 = {
        "device": this.registerSwitchBoardCredentials.device_id,
        "room": this.registerSwitchBoardCredentials.roomId,
        "home": this.navParams.get('homeId'),
        "device_type": this.registerSwitchBoardCredentials.device_type,
        "name": this.registerSwitchBoardCredentials.switchBoardName,
        "s_type": "ir",
        "device_pin": this.registerSwitchBoardCredentials.device_pin
      }
      this.ds.postCreateSboard(obj2,
        data => {
          console.log(data);
          this.data = data;
          this.toast.bottomToast_Short('Device added');
          this.navCtrl.pop();
          this.navCtrl.pop();
          if (this.registerSwitchBoardCredentials.device_type != 'kiot_red') {
            this.navCtrl.push('DisplaySwitchesPage', {
              home: this.navParams.get('home')
            });
          }

        }, error => {
          //     let alert1 = this.alertCtrl.create({
          //       title: 'Registration Failed',
          //       subTitle: 'Check',
          //       buttons: ['OK']
          //     });
          //   alert1.present();
          console.log(error);
          console.log(JSON.stringify(error.json()));
        });
    }
    else {
      this.toast.bottomToast_Short('Please enter all fields');
    }
  }
  scan(pin) {
    this.platform.ready().then(() => {

      this.bScanner.scan().then(result => {
        console.log(result);
        if (pin) {
          this.registerSwitchBoardCredentials.device_pin = result.text;
        }
        else {
          this.checkDeviceExisting(result.text);
        }
      }, (error) => {
        this.toast.bottomToast_Short(error);
        console.log(error);
      });

    });
  }
  deleteDeviceId(pin?) {

    if (pin) {
      this.registerSwitchBoardCredentials.device_pin = '';
    }
    else {
      this.registerSwitchBoardCredentials.device_id = null;
    }
  }
  enterId(pin?) {
    let title = 'Enter Device Id';
    let name = 'device_id';

    if (pin) {
      title = 'Enter Device pin';
      name = 'device_pin';
    }
    this.alert.showPrompt(title, "",
      [{
        name: name,
      }], 'Cancel',
      () => {
        if (!pin)
          this.registerSwitchBoardCredentials.device_id = null;
      },
      'Ok', (data) => {
        if (pin) {
          this.registerSwitchBoardCredentials.device_pin = data[name];
        }
        else {
          this.checkDeviceExisting(data[name]);
        }
      }

    )
  }
  checkDeviceExisting(dev_id) {
    this.showLoader_forCheck = true;
    this.ds.postCheckExistingDevice(
      {
        device_id: dev_id
      }, data => {
        this.showLoader_forCheck = false;
        if (data.status == 1) {
          this.registerSwitchBoardCredentials.device_id = dev_id;
        }
        else if (data.status == 0) {
          this.toast.bottomToast_Long(data.message);
        }
      }, error => {
        this.showLoader_forCheck = false;
        console.log(error);
        //this.toast.bottomToast_Long("Error occurred");
      })

  }
  validate() {
    if (!this.registerSwitchBoardCredentials.device_id || !this.registerSwitchBoardCredentials.device_type
      || !this.registerSwitchBoardCredentials.roomId
      || !this.registerSwitchBoardCredentials.switchBoardName) {
      return false;
    }
    else {
      return true;
    }
  }

}
