import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, reorderArray, ModalController,IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../../providers/DataService';
import { AlertService } from '../../../../providers/AlertService';
import { ToastService } from '../../../../providers/ToastService';
import { LocalCommService } from '../../../../providers/LocalCommunicationServ';
import { LocalStorageService } from '../../../../providers/LocalStorageService';

//pages
import { CreatesboardPage } from '../createsboard/createsboard';
import { DisplaySwitchesPage } from '../../5switches/display-switches/display-switches';
import { CreateRoomPage } from '../../3rooms/createroom/createroom';
import { switchBoardModal } from '../switchBoardModal';
import { SwitchBoardEditPage } from '../../4sboards/edit-switch-board/edit-switch-board';
import { SwitchBoardConfigPage } from '../../4sboards/configure-switch-board/config-switch-board';

declare var WifiWizard;

@IonicPage()
@Component({
    selector: 'page-switch-board-display',
    templateUrl: 'switch-board-display.html',
    providers: []
})
export class SwitchBoardDisplayPage {
    public switchboard: any;
    public homeName: string;
    public roomName: string;
    public homeId: string;
    home_id;
    room_id;
    show_loader = true;
    public switchBoardDetailsData: switchBoardModal;

    sboardConfig = {
        app: "",
        current_wifi: ""
    }

    constructor(public navCtrl: NavController, public navParams: NavParams, private local: LocalStorageService, private LocalComm: LocalCommService, private toast: ToastService, public modalCtrl: ModalController, private ds: DataService, private alert: AlertService) { }

    ionViewDidLoad() {
        this.device_details = {};
        // this.switchBoardDetailsData.is_configured = true;
        this.switchboard = this.switchBoardDetailsData = this.navParams.get('switchboard');
        this.homeName = this.navParams.get('homeName');
        this.roomName = this.navParams.get('roomName');
        this.home_id = this.navParams.get('homeId');
        console.log(this.switchboard);
        this.switchBoardDetailsData.sboard_name = this.switchboard.sboard_name;
        this.switchBoardDetailsData.device_type = this.switchboard.device_type;
        this.switchBoardDetailsData.home_type = this.homeName;
        this.switchBoardDetailsData.room_type = this.roomName;
        this.switchBoardDetailsData.device_id = this.switchboard.device_id;

        this.local.setSecret(this.switchBoardDetailsData.device_id, this.switchboard.device_info.apiSecret);
        this.LocalComm.getConf(this.switchboard.device_info.wifi_local_name, '/info?apikey=' + this.switchboard.device_info.apikey, this.switchBoardDetailsData.device_id)
            .subscribe((data) => {
                this.show_loader = false;
                this.sboardConfig.app = data.app;
                this.sboardConfig.current_wifi = data.network;
            }, error => {
                this.show_loader = false;
                console.log(error);
            })
    }

    ngOnInit() { }
    addSwitchBoard() {
        this.navCtrl.push('CreatesboardPage', {
            room: this.room_id,
            home: this.home_id,
        });
    }
    doRefresh(event) {
        this.ngOnInit();
        event.complete();
        console.log(event);
    }
    goToSwitches(item) {
        this.navCtrl.push('DisplaySwitchesPage', {
            sboard_id: item._id
        });
    }
    //edit switch board 
    public goToEditSwitchBoard() {
        /* this.navCtrl.push('SwitchBoardEditPage', {
             editSwitchboard: this.switchBoardDetailsData,
             homeName: this.homeName,
             homeId: this.homeId,
             room_id: this.navParams.get('room_id'),
             switchBoardId: this.switchBoardDetailsData._id,
             for: this.navParams.get('for'),
 
         });*/
        this.modalCtrl.create('SwitchBoardEditPage', {
            editSwitchboard: this.switchBoardDetailsData,
            homeName: this.homeName,
            homeId: this.home_id,
            room_id: this.navParams.get('room_id'),
            switchBoardId: this.switchBoardDetailsData._id,
            for: this.navParams.get('for')
        }).present();
    }
    //configuring Switch Board
    device_details;
    public configureSwitchBoard() {
        this.ds.getDeviceDetails(this.switchboard.device_id,
            data => {
                console.log(data);
                this.device_details = data;
                this.local.setSecret(this.device_details.device_id, this.device_details.apiSecret);
                this.local.setLocalIp(this.device_details.wifi_hot_name, '');
                try {
                    WifiWizard.setWifiEnabled(true, () => {
                        this.goToConfigPage();
                    }, () => {
                        this.alert.showAlert('Not Connected', 'Please turn on Wi-Fi to proceed');
                    });
                }
                catch (err) {
                    this.goToConfigPage();
                }
                // this.findSSID();
            }, error => {
                console.log(error);
            })



    }

    goToConfigPage() {
        this.navCtrl.push('SwitchBoardConfigPage', {
            editSwitchboard: this.switchBoardDetailsData,
            homeName: this.homeName,
            switchBoardId: this.switchBoardDetailsData._id,
            for: this.navParams.get('for'),
            wifi_details: this.navParams.get('wifi_details'),
            home_id: this.home_id,
            device_details: this.device_details
        });
    }
    //switch board delete based on Id
    public deleteSwitchBoard() {
        let obj = {
            id: this.switchBoardDetailsData._id
        }
        this.alert.showConfirm("Are you sure?", "this deletes all switchboard details and configuration", "Cancel", "Delete",
            () => {
                console.log('cancelled');
            }, () => {

                this.ds.postDeleteSwitchBoard(obj,
                    data => {
                        this.navCtrl.pop();
                        this.toast.bottomToast_Short('Switch board deleted');
                    }, error => {
                        //this.navCtrl.pop();
                        console.log(JSON.stringify(error.json()));
                    });
            })

    }

}