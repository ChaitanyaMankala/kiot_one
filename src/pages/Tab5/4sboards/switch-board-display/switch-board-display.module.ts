import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SwitchBoardDisplayPage } from './switch-board-display';

@NgModule({
    declarations: [
        SwitchBoardDisplayPage,
    ],
    imports: [
        IonicPageModule.forChild(SwitchBoardDisplayPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class SwitchBoardDisplayPageModule { }