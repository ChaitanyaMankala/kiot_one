import { Component, OnInit } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { NavController, NavParams, reorderArray, Events, Nav, Platform,IonicPage } from 'ionic-angular';
import { Network } from '@ionic-native/network';

//providers
import { DataService } from '../../../../providers/DataService';
import { AlertService } from '../../../../providers/AlertService';
import { LocalCommService } from '../../../../providers/LocalCommunicationServ';
import { LocalStorageService } from '../../../../providers/LocalStorageService';

//pages
import { CreatesboardPage } from '../createsboard/createsboard';
import { DisplaySwitchesPage } from '../../5switches/display-switches/display-switches';
import { CreateRoomPage } from '../../3rooms/createroom/createroom';
import { switchBoardModal } from '../switchBoardModal';
import { Configure2SwitchBoardPage } from '../configure2-switch-board/configure2-switch-board';
declare var WifiWizard;

@IonicPage()
@Component({
  selector: 'page-switch-board-config',
  templateUrl: 'config-switch-board.html',
  providers: [Network]
})
export class SwitchBoardConfigPage {
  step; stepCondition; stepDefaultCondition;
  public switchboard: any;
  public homeName: string;
  public switchBoardId: string;
  home_id; text;
  room_id; current_wifi = "";
  public switchBoardDetailsData = new switchBoardModal();
  wifi_details;
  selected_router;
  device_details; net_sub;
  _recursionVarfindSSID; proceed_next = false;

  videoUrl: SafeResourceUrl;
  constructor(public navCtrl: NavController, private domSanitizer: DomSanitizer, private platform: Platform, private network: Network, public nav: Nav, private events: Events, private localComm: LocalCommService, private local: LocalStorageService, public navParams: NavParams, private ds: DataService, private alert: AlertService) {
    this.step = 1;
    this.stepCondition = this.selected_router;
    this.stepDefaultCondition = this.stepCondition;
    this.videoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/aw5pMBeOWM0')
    /* this.net_sub = this.network.onConnect()
       .subscribe(data => {
         console.log(data);
         this.findSSID();
       });*/
    this._recursionVarfindSSID = setInterval(() => {
      this.findSSID();
    }, 2000);

    this.events.subscribe('step:changed', () => {
      console.log(this.step);
      if (this.step == 1 && this.selected_router) {
        this.stepCondition = true;
      }
      if (this.step == 2) {
        if (this.device_details.wifi_hot_name != this.current_wifi) {
          this.stepCondition = false;
        }
        if (this.platform.is('android')) {
          let x = "\"" + this.device_details.wifi_hot_name + "\"";
          console.log(x);
          WifiWizard.connectNetwork(x, () => {
            console.log('connected');
          }, (error) => {
            console.log(error);
          });
        }
        // this.findSSID();
      }
      if (this.step == 3) {
        let time = new Date(Date.now());
        this.connectToDevice(time.setMinutes(time.getMinutes() + 1));
      }
    });
  }
  /* _recursionfindSSID() {
     setTimeout(() => {
       if (!this.findSSID()) {
         this._recursionfindSSID();
       }
     }, 2000);
 
   }*/
  routerSelect(event) {
    console.log(event);
    if (this.selected_router) {
      this.alert.showPrompt('Password', 'Please enter the Wi-Fi password',
        [
          {
            name: 'pswd',
            placeholder: 'Password',
            type: 'password'
          }

        ], 'Cancel', () => {

        }, 'Okay', (pswd) => {
          this.selected_router.password = pswd.pswd;
          this.stepCondition = true;
        }
      )

    }
  }
  ionViewDidLeave() {
    console.log('left');
    try {
      clearInterval(this._recursionVarfindSSID);
    }
    catch (err) {
      console.log(err);
    }
  }
  goToNextStep() {
    let clear = true;
    if (this.platform.is('android')) {
      let x = "\"" + this.device_details.wifi_hot_name + "\"";
      WifiWizard.getCurrentSSID(
        (s) => {
          console.log(s);
          this.current_wifi = s;
          if (s.toString() === x.toString()) {
            this.goToNextPage();
          }
          else {
            this.alert.showAlert('Not Connected', 'Please connect your phone to the given Wi-Fi ' + x.toString());
          }
        },
        (e) => {
          this.alert.showAlert('Error', e);
          console.log(e);
          return 0;
        });

    }
    else {
      this.goToNextPage();
    }
  }
  findSSID() {
    if (this.step == 2) {
      let x = "\"" + this.device_details.wifi_hot_name + "\"";
      let this1 = this;
      WifiWizard.getCurrentSSID(
        (s) => {
          console.log(s);
          this.current_wifi = s;
          if (s.toString() === x.toString()) {
            //  this.net_sub.unsubscribe();
            console.log('Im In Bro!');
            this.goToNextPage();
            clearInterval(this._recursionVarfindSSID);
          }

        },
        (e) => {
          console.log(e);
          return 0;
        });
    }
  }
  ionViewDidLoad() {
    this.wifi_details = this.navParams.get('wifi_details');
    this.switchBoardDetailsData.is_configured = true;
    this.switchboard = this.navParams.get('editSwitchboard');
    this.homeName = this.navParams.get('homeName');
    this.switchBoardId = this.navParams.get('switchBoardId');
    console.log(this.wifi_details);
    console.log(JSON.stringify(this.switchboard));
    this.switchBoardDetailsData.sboard_name = this.switchboard.sboard_name;
    this.switchBoardDetailsData.device_type = this.switchboard.device_type;
    this.switchBoardDetailsData.home_type = this.homeName;
    this.switchBoardDetailsData.room_type = 'Room Name';
    this.switchBoardDetailsData.device_id = this.switchboard.device_id;
    this.home_id = this.navParams.get('home_id');
    this.device_details = this.navParams.get('device_details');
    console.log(this.switchboard);
    /*
    this.ds.getDeviceDetails(this.switchboard.device_id,
      data => {
        console.log(data);
        this.device_details = data;
        this.local.setSecret(this.device_details.device_id, this.device_details.apiSecret);
        this.local.setLocalIp(this.device_details.wifi_hot_name, '');
        // this.findSSID();
      }, error => {
        console.log(error);
      })*/


  }

  public updateSwitchBoard(item: any) {
    let obj = {
      "id": this.switchBoardId,
      "sboard_name": this.switchBoardDetailsData.sboard_name,
      "display_color": "584e4757ae82ee51fc44a0ca"
    }
    console.log(JSON.stringify(item))
    this.ds.postEditSwitchBoard(obj,
      data => {
        //  alert(JSON.stringify(data))
        //  this.data = data;      
        this.navCtrl.pop();
        //  this.navCtrl.push('TabsPage');
      }, error => {
        this.navCtrl.pop();
        console.log(JSON.stringify(error.json()));
      });
  }
  addRouter() {
    let inputArray = [
      {
        name: 'name',
        placeholder: 'Name/SSID'
      }
    ]
    console.log('add new router!!');
    this.alert.showPrompt('New Wi-Fi Router',
      'Pleas provide Wi-Fi Name and Password',
      inputArray, 'Cancel',
      () => {

      },
      'Add', (data) => {
        console.log(data);
        this.ds.postCreateWifiRouter(
          {
            'action': 'add',
            'ssid': data.name,
            'home': this.home_id
          },
          (data1) => {
            console.log('Success Bro!');
            this.wifi_details.push({
              'name': data.name,
              'password': ''
            })
          },
          error => {
            console.log(error);
          }
        )
      })
  }
  goToNextPage() {
    console.log(this.selected_router);
    this.navCtrl.pop();
    this.navCtrl.push('Configure2SwitchBoardPage', {
      device_details: this.device_details,
      wifi_details: this.selected_router,
      home_id: this.navParams.get('home_id')
    })
  }




  data;
  connectToDevice(end) {
    this.text = 'Trying to contact device/switchboard...';

    this.localComm.pingDevice('http://192.168.4.1', '/test_kiot', this.device_details.device_id)
      .subscribe(data => {
        console.log(data);
        this.data = data;
        if (this.data._body == 'success') {
          this.text = 'connected to KIOT!';
          this.sendDataToDevice();
          return;
        }
        else {
          if (Date.now() < end) {
            return this.connectToDevice(end);
          }
          else {
            this.throwErrorAlert();
          }
        }
        //alert(data);
      },
      error => {
        console.log(error);
        if (Date.now() < end) {
          return this.connectToDevice(end);
        }
        else {
          this.throwErrorAlert();
        }
      });
  }

  throwErrorAlert() {
    this.text = 'Unable to connect :( Are you connected the phone to ' + this.device_details.wifi_hot_name + '?';

  }
  sendDataToDevice() {
    this.text = 'Connection Established...Sending configuration data to device...';
    console.log('sending...');
    this.localComm.sendConf('http://192.168.4.1', '/wifisave?',
      'ssid=' + this.wifi_details.name + '&' +
      'pass=' + this.wifi_details.password + '&' +
      'iot=1&' +
      'mqtt_server=139.59.30.136&mqtt_port=24236&' +
      'mqtt_user=' + this.device_details.mqtt_user + '&' +
      'mqtt_pass=' + this.device_details.mqtt_pass + '&' +
      'pubtop=' + this.device_details.mqtt_pubtopic + '&' +
      'subtop=' + this.device_details.mqtt_subtopic1 + '&' +
      'subtop2=' + this.device_details.mqtt_subtopic2, this.device_details.device_id).subscribe(
      data => {
        this.text = 'Data sent...';
        let t = new Date(Date.now());
        this.checkOfflineConnection(t.setMinutes(t.getMinutes() + 4));
      },
      error => {
        this.text = error;
      }
      );

  }
  checkOfflineConnection(endtime, recursion?) {
    if (!recursion && 0) {
      var zeroconf = cordova.plugins.zeroconf;
      zeroconf.watch('_http._tcp.', 'local.', (data) => {
        var action = data.action;
        var service = data.service;

        if (action == "added") {
          console.log("ADDED");
          console.log(service);
          let ip = service.ipv4Addresses;
          let domain = service.hostname;
          if (ip.length > 0) {
            //Take the last IP
            ip = ip[ip.length - 1];
          } else {
            ip = "";
            return;
          }
          this.local.setLocalIp(domain, ip);
          return;

        } else if (action == "removed") {
          console.log("REMOVED");
          console.log(service);
        }
        return;
      });
    }
    let temp = this.local.getLocalIp(this.device_details.wifi_local_name);
    // temp = temp[this.wifi_details.wifi_local_name];
    console.log(temp);
    console.log(this.device_details.wifi_local_name);
    console.log(localStorage.getItem('kiot_domainMap'));
    if (temp) {
      zeroconf.unwatch('_http._tcp.', 'local.');
      this.pingDevice(temp, endtime);

    }
    else {
      if (Date.now() < endtime) {
        return this.checkOfflineConnection(endtime, true);
      }
      else {
        this.text = 'Unable to connect to device, please reconfigure! :('
      }
    }
  }
  pingDevice(temp, endtime) {
    this.localComm.pingDevice('http://' + temp, '/test_kiot', this.device_details.device_id)
      .subscribe(data => {
        this.text = 'Configuration successful';

      }, error => {
        if (Date.now() < endtime) {
          return this.pingDevice(temp, endtime);
        }
        else {
          this.text = 'unable to contact KIOT. Please reconfigure';
          return;
        }
      });
  }
  ionViewWillLeave() {
    this.events.unsubscribe('step:changed', () => {
      console.log('unsubscribed');
    })
  }

}
