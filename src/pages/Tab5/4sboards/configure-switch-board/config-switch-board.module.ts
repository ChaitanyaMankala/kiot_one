import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SwitchBoardConfigPage } from './config-switch-board';

import { IonSimpleWizard } from '../../ion-simple-wizard/ion-simple-wizard.component';
import { IonSimpleWizardStep } from '../../ion-simple-wizard/ion-simple-wizard.step.component';


@NgModule({
    declarations: [
        SwitchBoardConfigPage,
        IonSimpleWizard,
        IonSimpleWizardStep
    ],
    imports: [
        IonicPageModule.forChild(SwitchBoardConfigPage)
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SwitchBoardConfigPageModule { }