import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SwitchBoardEditPage } from './edit-switch-board';

@NgModule({
    declarations: [
        SwitchBoardEditPage,
    ],
    imports: [
        IonicPageModule.forChild(SwitchBoardEditPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class SwitchBoardEditPageModule { }