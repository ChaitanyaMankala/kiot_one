import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, reorderArray,ViewController,IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../../providers/DataService';
import { ToastService } from '../../../../providers/ToastService';

//pages
import { CreatesboardPage } from '../createsboard/createsboard';
import { DisplaySwitchesPage } from '../../5switches/display-switches/display-switches';
import { CreateRoomPage } from '../../3rooms/createroom/createroom';
import { switchBoardModal } from '../switchBoardModal';
import { SwitchBoardDisplayPage } from '../../4sboards/switch-board-display/switch-board-display';

@IonicPage()
@Component({
    selector: 'page-switch-board-edit',
    templateUrl: 'edit-switch-board.html',
    providers: []
})
export class SwitchBoardEditPage {
    public switchboard: any;
    public homeName: string;
    public switchBoardId: string;
    public roomName: string;
    public roomArrayData: [{ room_name: '', _id: '' ,selected:boolean}];
    home_id;
    room_id;data;
    public switchBoardDetailsData: switchBoardModal;
    disUpdate;

    constructor(public navCtrl: NavController, public navParams: NavParams,private toast:ToastService, public ds: DataService,private viewCtrl:ViewController) {
        

    }
dismiss(data){
    this.viewCtrl.dismiss(data);
}
    ngOnInit() {
        this.switchBoardDetailsData = this.switchboard = this.navParams.get('editSwitchboard');
        this.homeName = this.navParams.get('homeName');
        this.switchBoardId = this.navParams.get('switchBoardId');
        this.roomName = this.navParams.get('roomName');
        this.switchBoardDetailsData.sboard_name = this.switchboard.sboard_name;
        this.switchBoardDetailsData.device_type = this.switchboard.device_type;
        this.switchBoardDetailsData.home_type = this.homeName;
          this.room_id = this.navParams.get('room_id');
        this.switchBoardDetailsData.device_id = this.switchboard.device_id;
        this.switchBoardDetailsData.roomId = this.room_id;
        this.disUpdate = true;
      

    }

    ionViewWillEnter() {
        this.ds.getHomesRooms(this.navParams.get('homeId'),
            (data) => {
                this.data = data.rooms;
                for(let i of this.data){
                    if(i._id == this.room_id){
                        i.selected = true;
                    }
                    else i.selected=false;
                }
            },
            (error) => {
                console.log(JSON.stringify(error.json()));

            });
    }


    public updateSwitchBoard(item: any) {
        let obj = {
            "id": this.switchBoardId,
            "sboard_name": this.switchBoardDetailsData.sboard_name,
            "room_id":this.switchBoardDetailsData.roomId,
            "display_color": "584e4757ae82ee51fc44a0ca"
        }

        this.switchBoardDetailsData.sboard_name = this.switchBoardDetailsData.sboard_name;

        console.log(JSON.stringify(item))
        this.ds.postEditSwitchBoard(obj,
        data => {
            this.navCtrl.pop();
            this.toast.bottomToast_Short('Switch board edited');
            //  this.navCtrl.push('TabsPage');
        }, error => {
            // this.navCtrl.pop();
            console.log(JSON.stringify(error.json()));
        });
    }



    addSwitchBoard() {
        this.navCtrl.push('CreatesboardPage', {
            room: this.room_id,
            home: this.home_id,
        });
    }
    doRefresh(event) {
        this.ngOnInit();
        event.complete();
        console.log(event);
    }
    goToSwitches(item) {
        this.navCtrl.push('DisplaySwitchesPage', {
            sboard_id: item._id
        });
    }
    onChange(){
            this.disUpdate = false;
    }

}
