import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SboardListPage } from './sboard-list';

@NgModule({
    declarations: [
        SboardListPage,
    ],
    imports: [
        IonicPageModule.forChild(SboardListPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class SboardListPageModule { }