import { Component } from '@angular/core';
import { NavController, NavParams, Platform,IonicPage } from 'ionic-angular';

//providers
import { LocalCommService } from '../../../../providers/LocalCommunicationServ';
import { LocalStorageService } from '../../../../providers/LocalStorageService';
import { AlertService } from '../../../../providers/AlertService';
declare var WifiWizard;

@IonicPage()
@Component({
  selector: 'page-configure2-switch-board',
  templateUrl: 'configure2-switch-board.html'
})
export class Configure2SwitchBoardPage {
  device_details; data; text;
  progress_array = {
    1: null,
    2: null,
    3: null,
    4: null,
    5: null
  }; wifi_details; kill_4_1 = false; timeout_forPing; timeout_forPing2; home_id;
  constructor(public navCtrl: NavController, private platform: Platform, public navParams: NavParams, private alert: AlertService, private local: LocalStorageService, private localComm: LocalCommService) {
    this.device_details = this.navParams.get('device_details');
    this.home_id = this.navParams.get('home_id');
    this.wifi_details = this.navParams.get('wifi_details');
    let time = new Date(Date.now());
    this.connectToDevice(time.setMinutes(time.getMinutes() + 1));

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Configure2SwitchBoardPage');

  }
  connectToDevice(end) {
    this.text = 'Trying to contact device/switchboard...';
    this.progress_array[1] = true;
    this.localComm.pingDevice('http://192.168.4.1', '/api/switch?apikey=' + this.device_details.apikey, this.device_details.device_id)
      .subscribe(data => {
        console.log(data);
        this.data = data;
        this.progress_array[2] = true;
        this.text = 'connected to KIOT!';
        this.sendDataToDevice();
        return;
        //alert(data);
      },
      error => {
        console.log(error);
        if (Date.now() < end) {
          return this.connectToDevice(end);
        }
        else {
          this.throwErrorAlert(2);
        }
      });
  }

  throwErrorAlert(ind) {
    this.progress_array[ind] = -1;
    this.text = 'Unable to connect :( Is the phone connected to ' + this.device_details.wifi_hot_name + '?';

  }
  sendDataToDevice() {
    this.progress_array[3] = true;
    this.text = 'Connection Established...Sending configuration data to device...';
    console.log('sending...');
    let req_obj = {
      "config": [
        {
          "name": "manufacturer",
          "value": "KIOT_PVT_LTD"
        },
        {
          "name": "mqttServer",
          "value": "139.59.30.136"
        },
        {
          "name": "mqttPort",
          "value": "24236"
        },
        {
          "name": "relayMode",
          "value": "2"
        },
        {
          "name": "multirelayVisible",
          "value": 1
        },
        {
          "name": "relaySync",
          "value": "0"
        },
        {
          "name": "fauxmoVisible",
          "value": 1
        },
        {
          "name": "fauxmoEnabled",
          "value": true
        },
        {
          "name": "maxNetworks",
          "value": 5
        },
        {
          "name": "ssid",
          "value": encodeURIComponent(this.wifi_details.name)
        },
        {
          "name": "pass",
          "value": encodeURIComponent(this.wifi_details.password)
        },
        {
          "name": "dns",
          "value": ""
        },
        {
          "name": "homeId",
          "value": this.home_id
        }
      ]
    };
    if (this.device_details.mqtt_pass && this.device_details.mqtt_user) {
      req_obj.config.push({
        "name": "mqttUser",
        "value": encodeURIComponent(this.device_details.mqtt_user)
      },
        {
          "name": "mqttPassword",
          "value": encodeURIComponent(this.device_details.mqtt_pass)
        });
    }
    let req_str = JSON.stringify(req_obj);
    /*
          'ssid=' + this.wifi_details.name + '&' +
      'pass=' + this.wifi_details.password + '&' +
      'iot=1&' +
      'mqtt_server=139.59.30.136&mqtt_port=24236&' +
      'mqtt_user=' + this.device_details.mqtt_user + '&' +
      'mqtt_pass=' + this.device_details.mqtt_pass + '&' +
      'pubtop=' + this.device_details.mqtt_pubtopic + '&' +
      'subtop=' + this.device_details.mqtt_subtopic1 + '&' +
      'subtop2=' + this.device_details.mqtt_subtopic2
      */
    this.local.setSecret(this.device_details.device_id, this.device_details.apiSecret);
    this.localComm.sendConf('http://192.168.4.1', '/config?apikey=' + this.device_details.apikey, req_str, this.device_details.device_id)
      .subscribe(
      data => {
        this.progress_array[4] = true;
        this.text = 'Data sent... Waiting for confirmation';
        this.kill_4_1 = true;
        let t = new Date(Date.now());
        this.checkOfflineConnection(t.setMinutes(t.getMinutes() + 4));
      },
      error => {
        console.log(error);
        if (error.status == 0) {
          this.progress_array[4] = true;
          this.text = 'Data sent...';
          this.kill_4_1 = true;
          let t = new Date(Date.now());
          this.checkOfflineConnection(t.setMinutes(t.getMinutes() + 4));
        }
        else {
          //this.text = 'yolo';
          WifiWizard.disconnectNetwork(this.device_details.wifi_hot_name, () => {
            this.failedRollBack();
          }, () => {
            this.failedRollBack();
          });

        }
      }
      );

  }
  failedRollBack() {
    this.navCtrl.pop();
    this.progress_array[4] = -1;
    this.alert.showAlert('Configuration Failed', 'Unable to communicate with the device, please try again');
  }
  checkOfflineConnection(endtime) {
    if (0) {
      var zeroconf = cordova.plugins.zeroconf;
      zeroconf.watch('_http._tcp.', 'local.', (data) => {
        var action = data.action;
        var service = data.service;

        if (action == "added") {
          console.log("ADDED");
          console.log(service);
          let ip = service.ipv4Addresses;
          let domain = service.hostname;
          if (ip.length > 0) {
            //Take the last IP
            if (ip[ip.length - 1]) {
              ip = ip[ip.length - 1];
            }
          } else {
            ip = "";
            return;
          }
          this.local.setLocalIp(domain, ip);
          return;

        } else if (action == "removed") {
          console.log("REMOVED");
          console.log(service);
        }
        return;
      });
    }
    let temp = this.local.getLocalIp(this.device_details.wifi_local_name);
    // temp = temp[this.wifi_details.wifi_local_name];
    console.log(temp);
    console.log(this.device_details.wifi_local_name);
    console.log(localStorage.getItem('kiot_domainMap'));
    if (temp) {
      // zeroconf.unwatch('_http._tcp.', 'local.');
      this.pingDevice(temp, endtime);
      clearInterval(this.timeout_forPing2);
    }
    else {
      if (Date.now() < endtime) {
        if (!this.timeout_forPing2) {
          this.timeout_forPing2 = setInterval(() => {
            this._recursionCheckOfflineConnection(endtime);
          }, 3000);
        }
        return;
      }
      else {
        clearInterval(this.timeout_forPing2);
        this.progress_array[5] = -1;
        this.text = 'Unable to connect to device, please reconfigure! :(';
      }
    }
  }
  _recursionCheckOfflineConnection(endtime) {
    return this.checkOfflineConnection(endtime);
  }
  pingDevice(temp, endtime) {
    this.localComm.pingDevice('http://' + this.local.getLocalIp(this.device_details.wifi_local_name), '/api/switch?apikey=' + this.device_details.apikey, this.device_details.device_id)
      .subscribe(data => {
        clearInterval(this.timeout_forPing);
        this.progress_array[5] = true;
        this.text = 'Configuration successful';
        this.navCtrl.pop();
        this.alert.showConfirmWithCustomButtons('Success', 'Your device is configured successfully', [{
          text: 'Okay',
          handler: () => {
          }
        }]);

      }, error => {
        if (Date.now() < endtime) {
          if (!this.timeout_forPing) {
            this.timeout_forPing = setInterval(() => {
              this.pingDevice(temp, endtime);
            }, 2000);
          }
        }
        else {
          clearInterval(this.timeout_forPing);
          this.progress_array[5] = -1;
          this.text = 'unable to contact KIOT. Please reconfigure';
          return;
        }
      });
  }
  ionViewDidLeave() {

    try {
      clearInterval(this.timeout_forPing2);
    }
    catch (err) {
      console.log(err);
    }
    try {
      clearInterval(this.timeout_forPing);
    }
    catch (err) {
      console.log(err);
    }
  }

}
