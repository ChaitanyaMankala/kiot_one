import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Configure2SwitchBoardPage } from './configure2-switch-board';

@NgModule({
    declarations: [
        Configure2SwitchBoardPage,
    ],
    imports: [
        IonicPageModule.forChild(Configure2SwitchBoardPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class Configure2SwitchBoardPageModule { }