import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeviceCatalogPage } from './device-catalog';

@NgModule({
  declarations: [
    DeviceCatalogPage,
  ],
  imports: [
    IonicPageModule.forChild(DeviceCatalogPage),
  ],
})
export class DeviceCatalogPageModule {}
