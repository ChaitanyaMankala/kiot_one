import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-device-catalog',
  templateUrl: 'device-catalog.html',
})
export class DeviceCatalogPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DeviceCatalogPage');
  }

  goToCreateDevice(device_type) {
    this.navCtrl.push('CreatesboardPage', {
      device_type: device_type,
      homeId: this.navParams.get('homeId'),
      roomData: this.navParams.get('roomData'),
      home: this.navParams.get('home')
    });
  }

}
