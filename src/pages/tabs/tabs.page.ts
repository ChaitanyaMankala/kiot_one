import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

//pages
import { HomePage } from '../Tab1/home/home.page';
import { RoomsPage } from '../Tab2/rooms/rooms.page';
import { ScenesPage } from '../Tab3/scenes/scenes.page';
import { RemotePage } from '../Tab4/remote/remote.page';
import { ProfilePage } from '../Tab5/1profile/profile/profile.page';

@Component({
  templateUrl: 'tabs.page.html',
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = 'HomePage';
  tab2Root: any = 'RoomsPage';
  tab3Root: any = 'ScenesPage';
  tab4Root: any = 'RemotePage';
  tab5Root: any = 'ProfilePage';
  tab_index;
  nav: NavController;
  constructor(private navCtrl: NavController, private navParams: NavParams) {

    this.tab_index = this.navParams.get('tab_index');
  }
  logIt() {
    console.log('yolo');
  }
  /* onPageWillEnter(){
     if(localStorage.getItem('popper')=="yes"){
               this.tab.select(1);
               this.nav = this.tab.getActiveChildNav();
                this.navCtrl.popToRoot();
     }
   }*/
  setFunc() {
    this.navCtrl.setRoot(TabsPage);
  }

}

