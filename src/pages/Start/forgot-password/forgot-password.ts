import { Component } from '@angular/core';
import { NavController, NavParams,IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../providers/DataService';
import { ToastService } from '../../../providers/ToastService';

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html'
})
export class ForgotPasswordPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private toast: ToastService, private ds: DataService) { }
  otp = ''; pass = ''; email; wait = false;
  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotPasswordPage');
    this.email = this.navParams.get('email');
  }

  resetPass() {
    if (this.validate()) {
      this.wait = true;
      this.ds.postResetPass({
        otp: this.otp,
        password: this.pass,
        email: this.email
      }, data => {
        this.wait = false;
        this.toast.bottomToast_Long('Success, your password is reset');
        this.navCtrl.pop();
        this.navCtrl.pop();
      }, err => {
        this.wait = false;
        console.log(err);
      });
    }
  }

  validate() {
    if (this.wait) {
      return false;
    }
    if (!this.otp || this.otp == '') {
      this.toast.bottomToast_Short('Enter OTP');
      return false;
    }
    if (!this.pass || this.pass == '') {
      this.toast.bottomToast_Short('Enter password');
      return false;
    }
    if (this.pass && this.pass.length < 7) {
      this.toast.bottomToast_Short('password must be atleast 6 characters');
      return false;
    }
    return true;
  }

}
