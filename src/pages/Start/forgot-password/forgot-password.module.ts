import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ForgotPasswordPage } from './forgot-password';

@NgModule({
    declarations: [
        ForgotPasswordPage,
    ],
    imports: [
        IonicPageModule.forChild(ForgotPasswordPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class ForgotPasswordPageModule { }