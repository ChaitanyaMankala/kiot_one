import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, Platform,IonicPage } from 'ionic-angular';
import { GooglePlus } from '@ionic-native/google-plus';


import { authToken } from '../../../providers/app/authToken/authToken';
//import { Facebook, NativeStorage } from 'ionic-native';
import { DataService } from '../../../providers/DataService';

//providers
import { LocalStorageService } from '../../../providers/LocalStorageService';
import { LoadingService } from '../../../providers/LoadingService';
import { AlertService } from '../../../providers/AlertService';

//pages
import { ForgotPasswordDetailsPage } from '../forgot-password-details/forgot-password-details';
import { SignupPage } from '../signup/signup';
import { HomePage } from '../../Tab1/home/home.page';
import { TabsPage } from '../../tabs/tabs.page';
import { SetupPage } from '../setup/setup';
declare var cordova;

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [authToken]
})
export class LoginPage {
  FB_APP_ID: number = 361624727529364;
  registerCredentials = { email: '', password: '' };
  em: string;
  pass: string;
  data;
  token;
  icon1;
  fb; email; name; id;


  constructor(public navCtrl: NavController, private platform: Platform, private alert: AlertService, private gplus: GooglePlus, private load: LoadingService, public navParams: NavParams, public alertCtrl: AlertController, private a: authToken, private localService: LocalStorageService, private ds: DataService) {
    // Facebook.browserInit(this.FB_APP_ID, "v2.8");
    // this.loginWithGoogle();
    //this.loginWebView();

  }



  resp;
  uid;
  /* fbLogin(): void {
     let permissions = new Array();
     let nav = this.navCtrl;
     let al = this.alertCtrl;
     //the permissions your facebook app needs from the user
     permissions = ["public_profile"];
 
 
     Facebook.login(['email'])
       .then(function (response) {
         this.resp = response;
         let userId = response.authResponse.userID;
         let params = new Array();
         this.uid = userId;
         localStorage.setItem("token", JSON.stringify(this.resp));
         nav.push(TabsPage);
         //Getting name and gender properties
           Facebook.api("/me?fields=name,gender", params)
           .then(function(user) {
               
             user.picture = "https://graph.facebook.com/" + userId + "/picture?type=large";
             //now we have the users info, let's save it in the NativeStorage
             NativeStorage.setItem('user',
             {
               name: user.name,
               gender: user.gender,
               picture: user.picture
             })
             .then(function(){
               
               console.log(user.name);
             }, function (error) {
               console.log(error);
              
             })
           })
       }, function (error) {
         console.log(error);
       });
 
 
     // this.navCtrl.push('TabsPage');
   }*/

  login() {
    this.em = this.registerCredentials.email;
    this.pass = this.registerCredentials.password;

    let userLoginObject = {
      "email": this.em,
      "password": this.pass,
      "notification_id": this.localService.getOneSignalIds(),
      "loginType": "password"
    };
    //this.load.showLoading('Logging in...');
    this.ds.postLogin(userLoginObject
      , data => {
        // this.load.dismiss();
        /* let alert = this.alertCtrl.create({
         title: 'Login successful',
         subTitle: 'the username and password are authenticated on REST API!',
         buttons: ['OK']
       });*/
        //alert.present();
        // this.data = data;
        this.afterLogin(data, userLoginObject);
        // this.navCtrl.pop();

      }, error => {
        let x;
        x = JSON.parse(error._body);
        let msg;
        msg = JSON.stringify(x.message);
        this.alert.showAlert(error.statusText, msg || x.err || "Unknown Error Occured")
        //console.log(JSON.stringify(error.json()));
      });


    //console.log(this.resp.token);

  }
  afterLogin(data, userLoginObject) {
    if (!data.token || data.token == undefined) {

    }
    // this.token = data.token;
    this.localService.setToken(data.token);
    this.localService.setUser(data.user);
    this.localService.setUserLoginObject(userLoginObject);
    // console.log(JSON.parse(this.localService.getUser()));
    console.log(data.user.default_home);
    if (!data.user.homes) {
      this.navCtrl.setRoot('SetupPage');
    }
    else if (data.user.homes && data.user.homes.length == 0) {
      this.navCtrl.setRoot('SetupPage');
    }
    else {
      if (data.user.default_home) {
        this.localService.setDefaultHome(data.user.default_home);
        this.localService.setCurrentHome(this.localService.getDefaultHome());
      }
      else if (!data.user.default_home && data.user.homes.length) {
        this.localService.setDefaultHome(data.user.homes[0]);
        this.localService.setCurrentHome(this.localService.getDefaultHome());
        this.ds.putEditProfile({
          default_home: data.user.homes[0]
        }, data => {

        }, error => { })
      }
      this.localService.setInternetStatus('online');
      this.navCtrl.setRoot(TabsPage);
    }
  }

  loginWithGoogle() {
    this.platform.ready().then(
      () => {
        let userLoginObject = {
          type: "google"
        }
        this.gplus.login({
          scopes: 'profile email openid',
          webClientId: '970544757572-aj26nhdjmv44f7f18btq7fjjrakgm080.apps.googleusercontent.com',
          offline: true
        }).then(data => {
          console.log(data);
          userLoginObject["email"] = data.email;
          if (!data.serverAuthCode) {
            this.alert.showAlert("Oops!", "Error occured in google login, Can you try again. ");
            return;
          }
          this.ds.doGoogleLogin({ code: data.serverAuthCode, "notification_id": this.localService.getOneSignalIds() }, (data) => {
            this.afterLogin(data, userLoginObject);
          }, error => {
            let x;
            x = JSON.parse(error._body);
            let msg;
            msg = JSON.stringify(x.message);
            this.alert.showAlert(error.statusText, msg || x.err || "Unknown Error Occured")
          });
        }).catch(error => {
          console.log(error);
          this.alert.showAlert("Oops!", "Error occured in google login, Can you try again. ");
        });

      }).catch(error => {
        console.log(error);
        this.alert.showAlert("Oops!", "Error occured in google login, Can you try again. ");
      });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    //loconsole.log(this.a.checker());
  }
  goToSignUp() {
    this.navCtrl.push('SignupPage');
  }
  goToForgotPassword() {
    this.navCtrl.push('ForgotPasswordDetailsPage');
  }
}
