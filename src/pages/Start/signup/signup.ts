import { Component } from '@angular/core';
import { NavController, NavParams,IonicPage } from 'ionic-angular';


//pages
import { TabsPage } from '../../tabs/tabs.page'
import { SetupPage } from '../setup/setup';

//providers
import { DataService } from '../../../providers/DataService';
import { ToastService } from '../../../providers/ToastService';
import { AlertService } from '../../../providers/AlertService';
import { LocalStorageService } from '../../../providers/LocalStorageService';
import { Helper } from '../../../providers/Helper';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,private help:Helper, private ls: LocalStorageService, private ds: DataService, public alert: AlertService, private toast: ToastService) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }
  registerCredentials1 = { name: '', email: '', pass: '', pass1: '', num: '', gender: '' };
  em: string;
  pass: string;
  nam: string;
  num: string;
  pass1: string;
  gender;
  data;
  token;
  signupp() {

    this.em = this.registerCredentials1.email;
    this.pass = this.registerCredentials1.pass;
    this.pass1 = this.registerCredentials1.pass1;
    this.nam = this.registerCredentials1.name;
    this.num = this.registerCredentials1.num;
    this.gender = this.registerCredentials1.gender;

    if (this.validate()) {
      this.ds.postRequestSignUp(
        {
          "email": this.em,
          "password": this.pass,
          "name": this.nam,
          "phone": this.num,
          "gender": this.gender
        },
        data => {
          this.askForOTP();
        }, error => {
          this.errHandle(error);

        });
    }

    //console.log(this.resp.token);

  }
  askForOTP() {
    this.alert.showPrompt('Enter OTP', 'An OTP is sent to ' + this.em,
      [

        {
          name: 'otp',
          placeholder: 'Enter the OTP'
        }
      ], 'Cancel', () => {

      }, 'Submit', (data) => {
        if (data.otp) {
          let x = {
            "email": this.em,
            "password": this.pass,
            "name": this.nam,
            "phone": this.num,
            "gender": this.gender,
            otp: data.otp,
            "notification_id": this.ls.getOneSignalIds()
          };
          this.ds.postSignUp(x, data => {
            this.loginHere(data, x);
          }, error => {
            this.errHandle(error);
          })
        }
        else {
          this.toast.bottomToast_Long('Invalid OTP');
        }
      })
  }
  goToPrivacyPolicy(){
    this.help.goToPrivacyPolicy();
  }
  
  validate() {
    if (!this.em) {
      this.toast.bottomToast_Short('Invalid email');
      return false;
    }
    if (!this.pass || !this.pass1) {
      this.toast.bottomToast_Short('enter same password in both fields');
      return false;
    }
    if (this.pass != this.pass1) {
      this.toast.bottomToast_Short('Repeat password didnot match with password');
      return false;
    }
    if (!this.nam || this.nam.length < 5) {
      this.toast.bottomToast_Short('Name must be atleast 4 characters long');
      return false;
    }
    if (this.pass && this.pass.length < 7) {
      this.toast.bottomToast_Short('Password must be atleast 6 characters long');
      return false;
    }
    if ((this.num && this.num.length != 10)) {
      this.toast.bottomToast_Short('Invald phone number');
      return false;
    }
    return true;
  }
  errHandle(error) {
    let x;
    x = JSON.parse(error._body);
    let msg;
    msg = JSON.stringify(x.message);
    this.alert.showAlert(error.statusText, msg || x.err || "Unknown Error Occured")
  }
  loginHere(data, userLoginObject) {

    if (!data.token || data.token == undefined) {

    }
    // this.token = data.token;
    this.ls.setToken(data.token);
    this.ls.setUser(data.user);
    this.ls.setUserLoginObject(userLoginObject);
    this.navCtrl.setRoot('SetupPage');
    // console.log(JSON.parse(this.ls.getUser()));
    console.log(data.user.default_home);
  }
  goBack() {
    this.navCtrl.pop();
  }
}
