import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SliderPage } from './slider.page';

@NgModule({
    declarations: [
        SliderPage,
    ],
    imports: [
        IonicPageModule.forChild(SliderPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class SliderPageModule { }