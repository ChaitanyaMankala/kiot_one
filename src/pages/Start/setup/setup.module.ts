import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SetupPage } from './setup';

@NgModule({
    declarations: [
        SetupPage,
    ],
    imports: [
        IonicPageModule.forChild(SetupPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class SetupPageModule { }