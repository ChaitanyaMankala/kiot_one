import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ModalController, Slides, App,IonicPage } from 'ionic-angular';
import { GooglePlus } from '@ionic-native/google-plus';

//pages
import { CreateHomePage } from '../../Tab5/2homes/create-home/create-home';
import { JoinHomeSearchPage } from '../../Tab5/2homes/join-home-search/join-home-search';
import { TabsPage } from '../../tabs/tabs.page';
import { LoginPage } from '../login/login';

//providers
import { DataService } from '../../../providers/DataService';
import { LocalStorageService } from '../../../providers/LocalStorageService';
import { AlertService } from '../../../providers/AlertService';
import { ToastService } from '../../../providers/ToastService';

@IonicPage()
@Component({
  selector: 'page-setup',
  templateUrl: 'setup.html',
})
export class SetupPage {
  current_slide = 0;
  constructor(public navCtrl: NavController, private gplus: GooglePlus, private ds: DataService, private app: App, private toast: ToastService, private alert: AlertService, private local: LocalStorageService, public navParams: NavParams, private modalCtrl: ModalController) {
    //this.checkForHomes(false);
  }
  @ViewChild(Slides) slides: Slides;

  ionViewDidLoad() {
    console.log('ionViewDidLoad SetupPage');

  }
  slideChanged() {
    this.current_slide = this.slides.getActiveIndex();
  }
  createHome() {
    this.navCtrl.push('CreateHomePage', {
      from_setup: true
    });
  }

  checkForHomes() {
    /*this.ds.getUserHomeIdsOnly((data) => {
      if (data && data.length) {
        let x = this.local.getUser();
        if (x) {
          if (!x.homes) {
            x.homes = [];
          }
          x.homes.push(data);
          if (!x.default_home) {
            x.default_home = data[0]._id;
          }
          if (!x.current_home) {
            x.current_home = x.default_home;
          }
          this.local.setUser(x);
          this.navCtrl.setRoot(TabsPage);
        }
        else {
          this.navCtrl.setRoot('LoginPage');
        }

      }
      else if (data && data.length == 0) {
        this.toast.topToast_Short('No homes joined')
      }
    }, error => {
      console.log(error);
    }, true)*/
    this.ds.tryRelogin(
      total_data => {
        let data = total_data.user;
        if (data && data.homes && data.homes.length) {

          if (!data.homes) {
            data.homes = [];
          }
          data.homes.push(data);
          if (!data.default_home) {
            data.default_home = data.homes[0];
            this.ds.putEditProfile({
              default_home: data.homes[0]
            }, () => { }, () => { });
          }
          if (!data.current_home) {
            data.current_home = data.default_home;
            this.local.setCurrentHome(data.current_home);
          }
          //this.local.setUser(data);
          this.navCtrl.setRoot(TabsPage);


        }
        else if (data && (!data.homes || data.homes.length == 0)) {
          this.toast.topToast_Short('No homes joined')
        }
        else {
          this.toast.topToast_Short('Some error occurred')
          this.navCtrl.setRoot('LoginPage');
        }
      }, error => {
        console.log(error);
      }
    )
  }

  joinHome() {
    this.modalCtrl.create('JoinHomeSearchPage').present();
  }
  /*logout() {
    this.alert.showConfirm('Confirm logout?', '',
      'Cancel', 'Logout', () => { }, () => {
        this.ds.postLogout({
          notification_id: this.local.getOneSignalIds() ? this.local.getOneSignalIds().userId : undefined
        }, data => {
          console.log(data);
          this.local.logout();
        }, error => {
          console.log(error);
          this.local.logout();
        }, false, true);
        this.navCtrl.setRoot('LoginPage');
      })
  }*/




  logout() {
    this.alert.showConfirm('Confirm logout?', '',
      'Cancel', 'Logout', () => { }, () => {
        this.serverLogout()
          .then(() => {
            this.local.logout();
            this.app.getRootNav().setRoot('LoginPage');
          })
          .catch(err => {
            console.log(err);
          });

      })


  }

  serverLogout() {
    return new Promise((resolve, reject) => {
      let x = this.local.getUserLoginObject();
      if (x.type && x.type == 'google') {
        this.gplus.trySilentLogin({
          scopes: 'profile email openid',
          webClientId: '970544757572-aj26nhdjmv44f7f18btq7fjjrakgm080.apps.googleusercontent.com',
          offline: true
        }).then((data) => {
          console.log(data);
          this.gplus.disconnect()
            .then(() => {
              resolve(1);
            })
            .catch((err) => {
              console.log(err);
              reject('from disconnect');
            })
        }).catch(err => {
          console.log(err);
          reject('from relogin')
        })


      }
      this.ds.postLogout({
        notification_id: this.local.getOneSignalIds() ? this.local.getOneSignalIds().userId : undefined
      }, data => {
        console.log(data);
        resolve(1);
      }, error => {
        resolve(1);
      }, true, true);
    })

  }

}
