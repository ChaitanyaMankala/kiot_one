import { Component } from '@angular/core';
import { NavController, NavParams,IonicPage } from 'ionic-angular';



//providers
import { DataService } from '../../../providers/DataService';
import { ToastService } from '../../../providers/ToastService';

//pages
import { ForgotPasswordPage } from '../forgot-password/forgot-password';

@IonicPage()
@Component({
    selector: 'page-forgot-password-details',
    templateUrl: 'forgot-password-details.html'
})
export class ForgotPasswordDetailsPage {

    constructor(public navCtrl: NavController, public navParams: NavParams, private ds: DataService, private toast: ToastService) { }

    ionViewDidLoad() {
        console.log('ionViewDidLoad ForgotPasswordDetailsPage');
    }
    email; wait = false;
    goToOTP() {
        if (this.email && this.email != '' && !this.wait) {
            let obj = {
                "email": this.email
            };
            this.wait = true;
            this.ds.postRequestOtp(obj
                , data => {
                    this.wait = false;
                    this.navCtrl.push('ForgotPasswordPage', {
                        email: this.email
                    });
                }, err => {
                    this.wait = false;
                    console.log(err);
                })

        }
        else {
            this.toast.bottomToast_Short('please enter the registered email');
        }
    }

}
