import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ForgotPasswordDetailsPage } from './forgot-password-details';

@NgModule({
    declarations: [
        ForgotPasswordDetailsPage,
    ],
    imports: [
        IonicPageModule.forChild(ForgotPasswordDetailsPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class ForgotPasswordDetailsPageModule { }