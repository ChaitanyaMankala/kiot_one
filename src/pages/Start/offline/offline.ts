import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController, Events,IonicPage } from 'ionic-angular';

//providers
import { LocalStorageService } from '../../../providers/LocalStorageService';
import { DataService } from '../../../providers/DataService';
import { ToastService } from '../../../providers/ToastService';

//pages
import { TabsPage } from '../../tabs/tabs.page'

@IonicPage()
@Component({
  selector: 'page-offline',
  templateUrl: 'offline.html',
})
export class OfflinePage {

  retry_loading = false;
  kiot_internet; show_offline_mode;
  constructor(public navCtrl: NavController, public viewCtrl: ViewController, private event: Events, private toast: ToastService, public navParams: NavParams, private ds: DataService, private local: LocalStorageService) {
    this.kiot_internet = this.local.getInternetStatus();
    this.show_offline_mode = this.local.getToken() ? true : false;
    this.local.setBackButtonOverride(true);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OfflinePage');
  }
  goOffline() {
    this.dismiss();
    this.event.publish('kiot_internet', 'go_offline');
  }
  retry() {
    this.retry_loading = true;
    this.ds.ping()
      .then(data => {
        this.kiot_internet = this.local.getInternetStatus();
        if (this.kiot_internet == 'online') {
          this.dismiss();
          this.toast.bottomToast_Short('Kiot is back Online!');
        }
        this.retry_loading = false;
      });


  }
  ionViewWillLeave(){
    this.local.setBackButtonOverride(false);
  }
  ionViewDidLeave(){
   // this.event.publish('kiot_internet','back_online');
  }
  dismiss() {
    this.local.setBackButtonOverride(false);
    this.viewCtrl.dismiss(0);
  }
}
