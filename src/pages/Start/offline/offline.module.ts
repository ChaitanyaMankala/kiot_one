import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OfflinePage } from './offline';

@NgModule({
    declarations: [
        OfflinePage,
    ],
    imports: [
        IonicPageModule.forChild(OfflinePage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class OfflinePageModule { }