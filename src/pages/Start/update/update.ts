import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController, Platform,IonicPage } from 'ionic-angular';

//providers
import { LocalStorageService } from '../../../providers/LocalStorageService';
import { DataService } from '../../../providers/DataService';
import { ToastService } from '../../../providers/ToastService';

//pages
import { TabsPage } from '../../tabs/tabs.page'

@IonicPage()
@Component({
  selector: 'page-update',
  templateUrl: 'update.html',
})
export class UpdatePage {

  retry_loading = false;
  store_link = "#";

  constructor(public navCtrl: NavController, public viewCtrl: ViewController, private platform: Platform, private toast: ToastService, public navParams: NavParams, private ds: DataService, private local: LocalStorageService) {

    if (this.platform.is('android')) {
      this.store_link = "market://details?id=com.kiot.io";
    }
    this.local.setBackButtonOverride(true);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OfflinePage');
  }

  goToStore() {
    window.open(this.store_link, '_system'); return false;
  }

}
