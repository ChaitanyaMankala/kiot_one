import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpdatePage } from './update';

@NgModule({
    declarations: [
        UpdatePage,
    ],
    imports: [
        IonicPageModule.forChild(UpdatePage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class UpdatePageModule { }