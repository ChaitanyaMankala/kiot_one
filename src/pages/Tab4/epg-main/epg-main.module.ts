import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EpgMainPage } from './epg-main';
import { PipesModule } from '../../../pipes/pipes.module';

@NgModule({
    declarations: [
        EpgMainPage,
    ],
    imports: [
        IonicPageModule.forChild(EpgMainPage),
        PipesModule
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class EpgMainPageModule { }