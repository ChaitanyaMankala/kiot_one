import { ViewAllNowPage } from './../view-all-now/view-all-now';
import { Component, ViewChild, Query, ViewChildren, QueryList, EventEmitter, Input, Output, ElementRef, Renderer } from '@angular/core';
import { NavController, ViewController, NavParams, AlertController, PopoverController, ModalController, FabContainer, Platform,IonicPage } from 'ionic-angular';
//import { Vibration } from 'ionic-native';

//providers
import { LocalStorageService } from '../../../providers/LocalStorageService';
import { DataService } from '../../../providers/DataService';
import { AlertService } from '../../../providers/AlertService';
import { Helper } from '../../../providers/Helper';
import { ToastService } from '../../../providers/ToastService';

//pages
import { ProgramDetailsPage } from '../program-details/program-details';
import { LaterThisWeekPage } from '../later-this-week/later-this-week';
import { ChannelProgramsPage } from '../channel-programs/channel-programs';
import { RealRemotePage } from '../real-remote/real-remote';
import { NumPadPage } from '../num-pad/num-pad';
import { SearchModalPage } from '../search-modal/search-modal';
import { ChannelListPage } from '../channel-list/channel-list';


//modals
import { RemoteModalPage } from '../remote-modal/remote-modal';

@IonicPage()
@Component({
  selector: 'page-epg-main',
  templateUrl: 'epg-main.html'
})
export class EpgMainPage {

  show;
  channelMetaData;
  langs;
  prog_types;
  just_loaded;
  rowData = new Array();
  colData = [];
  blurred = [];
  types;
  rowCount;
  noMoreRows;
  current;
  currentActive;
  providerMap;
  rev_providerMap;
  rev_providerMap2 = {};
  fav_toggle;
  loaderGif;
  show_tab1; show_tab2; show_tab3; show_tab4;
  reminders = [];
  ir_appliance_name;
  kiot_internet;
  noMoreRowsForLater = false;

  @ViewChild('fab') fab1: FabContainer;

  constructor(private _elementRef: ElementRef,
    private _renderer: Renderer, public navCtrl: NavController, public modalCtrl: ModalController, private platform: Platform, private toast: ToastService, private navParams: NavParams, private help: Helper, private popCtrl: PopoverController, private alert: AlertService, private ds: DataService, private alertCtrl: AlertController, private local: LocalStorageService) {
    this.kiot_internet = this.local.getInternetStatus();
    if (this.kiot_internet == 'online') {
      platform.is('ios') ? this.loaderGif = 'ios-loader.gif' : '';
      platform.is('android') ? this.loaderGif = 'md-loader.gif' : '';
      this.noMoreRows = false;
      this.ir_appliance_name = this.navParams.get('ir_appliance_name');
      this.current = 'now';
      this.show = true;
      this.show_hide(1);
      this.types = [
        {
          "key": "Movie",
          "value": "Movies"
        },
        {
          "key": "TV-Series",
          "value": "TV Shows"
        },
        {
          "key": "Music",
          "value": "Music"
        },
        {
          "key": "News",
          "value": "News"
        },
        {
          "key": "Sports",
          "value": "Sports"
        },
        {
          "key": "Health",
          "value": "Health"
        },
        {
          "key": "Travel",
          "value": "Travel"
        },
        {
          "key": "Spiritual",
          "value": "Spiritual"
        },
        {
          "key": "Astrology",
          "value": "Astrology"
        }
      ];
      this.just_loaded = true;
      this.channelMetaData = this.navParams.get('channelGroupData');
      this.langs = this.local.getEpgLangs();
      if (!this.langs) {
        this.langs = '';
        this.doCheck(true);
      }

    }
    this.getSTBRemote();
    this.ds.getProviderMap(this.navParams.get('stb_provider_id'),
      data => {
        this.providerMap = data[0];
        this.rev_providerMap = data[1];
        let temp = this.channelMetaData[3].channels;
        for (let i in temp) {
          for (let j of temp[i].base_channels) {
            this.rev_providerMap2[j] = temp[i].channelname;
          }
        }
        console.log(this.rev_providerMap2);
        console.log(data);
      },
      error => {
        console.log(error);
      }, false)
  }
  remote_keys; remote_keys1; STBRemote;
  getSTBRemote() {
    this.ds.getRemoteFromDb(this.navParams.get('codeset'),
      (data: any) => {
        this.STBRemote = data;
        this.remote_keys = data[0].keys[0];
        this.remote_keys1 = Object.keys(this.remote_keys);
        console.log(this.remote_keys1);
      }, error => {
        console.log(error);
      }, false);

  }
  ionViewWillEnter() {
    this.currentActive = 'NONE';
    this.noMoreRows = false;
  }
  ionViewDidLoad() {

    if (this.kiot_internet == 'online') {
      this.currentActive = 'NONE';
      this.noMoreRows = false;
      this.rowData = [];
      if (this.local.getEpgTypes() === undefined || this.local.getEpgTypes() == null) {
        this.prog_types = this.types;
        this.local.setEpgTypes(JSON.stringify(this.types));
        //console.log("Bello");

      }
      else {
        this.prog_types = this.local.getEpgTypes();
      }


      this.rowData = this.prog_types;
      this.langs = this.local.getEpgLangs();
      console.log(this.colData);
      if (this.just_loaded) {
        this.tabOneFunction();
        this.just_loaded = false;
      }
    }
  }
  show_hide(num) {
    this.show_tab1 = false;
    this.show_tab2 = false; this.show_tab3 = false; this.show_tab4 = false;
    switch (num) {
      case 1: {
        this.show_tab1 = true;
        break;
      }
      case 2: {
        this.show_tab2 = true;
        break;
      }
      case 3: {
        this.show_tab3 = true;
        break;
      }
      case 4: {
        this.show_tab4 = true;
        break;
      }
    }
  }
  tabOneFunction() {
    this.currentActive = 'NONE';
    if (this.local.getEpgTypes() === undefined || this.local.getEpgTypes() == null) {
      this.prog_types = this.types;
      this.local.setEpgTypes(JSON.stringify(this.types));
      //console.log("Bello");

    }
    else {
      this.prog_types = this.local.getEpgTypes();
    }


    this.rowData = this.prog_types;
    this.current = "now";
    this.show_hide(1);
    this.colData = [];
    this.noMoreRows = false;
    if (this.prog_types.length == 1) {

      this.rowCount = 2;
      this.getARow(this.prog_types[0].key.split('_')[0], 1, this.langs, 0);
    }
    else {

      this.rowCount = 5;
      this.getARow(this.prog_types[0].key.split('_')[0], 3, this.langs, 0);
      this.getARow(this.prog_types[1].key.split('_')[0], 3, this.langs, 0);
      this.getARow(this.prog_types[2].key.split('_')[0], 3, this.langs, 0);
      this.getARow(this.prog_types[3].key.split('_')[0], 3, this.langs, 0);
      /*for(let i of this.prog_types){
        this.colData[i.key]=[];
      }*/
    }

  }
  doRefresh(eve) {
    setTimeout(() => {
      switch (this.current) {
        case 'now': {
          this.ionViewDidLoad();
          eve.complete();
          break;
        }
        case 'later': {
          this.ionViewDidLoad();
          this.tabTwoFunction();
          eve.complete();
          break;
        }
        case 'guide': {
          this.tabThreeFunction(0);
          eve.complete();
          break;
        }
        case 'Favourites': {
          this.tabFourFunction();
          eve.complete();
          break;
        }
      }
    }, 500)
  }
  goToRemote(fab: FabContainer) {
    /*   this.navCtrl.push('RealRemotePage',{
            codeset:this.navParams.get('codeset'),
            device_id:this.navParams.get('device_id'),
            sb_id:this.navParams.get('sb_id')
    });*/
    fab.close();
    this.blurred[0] = 'blur';
    console.log(this.navParams.get('tv_data'));
    let remoteModal = this.modalCtrl.create('RemoteModalPage', {
      ir_appliance: this.navParams.get('ir_appliance'),
      codeset: this.navParams.get('codeset'),
      device_id: this.navParams.get('device_id'),
      sb_id: this.navParams.get('sb_id'),
      tv_data: this.navParams.get('tv_data'),
      channel_meta: this.channelMetaData[3].channels,
      providerMap: this.providerMap,
      rev_providerMap: this.rev_providerMap,
      rev_providerMap2: this.rev_providerMap2,
      STBRemote: this.STBRemote,
      paired_tv: this.navParams.get('paired_tv')
    });
    remoteModal.present();
    remoteModal.onDidDismiss((data) => {
      if (data == 'fav' && this.current != 'Favourites') {
        this.current = 'Favourites';
        this.tabFourFunction();
      }
    })


  }
  goToNumPad(fab: FabContainer) {
    fab.close();
    this.blurred[0] = 'blur';
    let numPadModal = this.modalCtrl.create('NumPadPage', {
      channel_meta: this.channelMetaData[3].channels,
      providerMap: this.providerMap,
      rev_providerMap: this.rev_providerMap,
      rev_providerMap2: this.rev_providerMap2,
      device_id: this.navParams.get('device_id'),
      sb_id: this.navParams.get('sb_id'),
      STBRemote: this.STBRemote
    });
    numPadModal.present();
  }

  getARow(rows, num, lang, offset, eve?) {

    this.ds.getPrograms(10, offset, lang, rows, false,
      (data) => {
        if (eve) eve.complete();
        if (data.length == 0 && !eve && this.colData[num == -1 ? rows + '_' + lang : rows]) {
          this.colData[num == -1 ? rows + '_' + lang : rows].current_offset = -1;
        }

        for (let i of data) {
          console.log('yo');
          let pType = num == -1 ? i.program.type + '_' + lang : i.program.type;
          i.channel_name = this.channelMetaData[3].channels[i.channelid].channelname;
          i.duration = (this.help.findHrsInterval(i.starttime, Date.now())) / (this.help.findHrsInterval(i.starttime, i.endtime));
          i.duration = (i.duration * 100).toString() + '%';
          if (!this.colData[pType]) this.colData[pType] = [];
          this.colData[pType].push(i);
          this.colData[pType].current_offset = offset + 10;
        }
        if (num == 1) {  //for single category like Music,Movies,News etc

          let c = 1;
          let currentType = this.prog_types[0].key.toString();
          let langsArray = [];
          langsArray = this.langs.toString().split(',');
          console.log(langsArray);
          if (langsArray.length > 1) {
            for (let k of langsArray) {
              let x = k.toString(); let y = this.prog_types[0];
              if (this.colData[k] != []) { this.rowData.push({ "key": y.key + '_' + x, "value": x + " " + y.value }); this.rowCount++ }
              else this.colData[k] = [];
              this.getARow(this.prog_types[0].key, -1, k, offset); //-1 is sent for "combo" - Telugu Movies,English Music etc
              /*
              for(let j of this.colData[currentType]){
                if(j.program.language == k){
                  console.log(c++);
                  this.colData[k]?{}:this.colData[k]=[];
                  this.colData[k].push(j);
                }
              }*/

            }

          }
        } //if num==1 ends here

        if (data.length < 10 && this.colData[num == -1 ? rows + '_' + lang : rows]) {
          this.colData[num == -1 ? rows + '_' + lang : rows].current_offset = -1;
        }
        console.log(this.rowData);
        console.log(this.colData);

      },
      (err) => {
        console.log(err);
      });
  }

  switchToChannel(channelid, item?, override_num?) {
    let obj;
    if (channelid) {
      obj = this.help.switchToChannel(channelid, this.channelMetaData, this.providerMap, this.navParams.get('sb_id'), this.navParams.get('device_id'), this.STBRemote);
    }
    else {
      obj = this.help.switchToChannel(null, this.channelMetaData, this.providerMap, this.navParams.get('sb_id'), this.navParams.get('device_id'), this.STBRemote, override_num);
    }
    if (obj == 'NO CHANNEL') {
      this.currentActive = 'NONE';
      // this.toast.bottomToast_Short('Channel Number not available');
      this.alert.showPrompt('Sorry',
        'channel number not available, you can enter the channel number manually (if known)',
        [
          {
            name: 'channelno',
            placeholder: 'Channel Number'
          }
        ], 'Cancel', () => {

        }, 'Switch to channel', (data) => {
          if (data.channelno && parseInt(data.channelno)) {
            console.log('switching to ' + data.channelno);
            this.switchToChannel(null, null, data.channelno);
            this.toast.bottomToast_Short('Switching to ' + data.channelno);
          }
          else {
            this.toast.bottomToast_Short('Invalid Channel number');
          }
        });
    }
    else {
      if (item) {
        item.show_loader = true;
      }
      this.ds.postSwitchToChannel(
        obj,
        data => {
          // if (this.platform.is('android')) Vibration.vibrate(30);
          this.currentActive = 'NONE';

          if (item) {
            this.toast.topToast_Short('Playing ' + (item.program.name || '') + ' ...');
            item.show_loader = false;
          }
          console.log(data);
        },
        error => {
          if (item) {
            item.show_loader = false;

          }
          console.log(error);
        });
    }
  }

  goToViewAllNow(item, type) {
    this.modalCtrl.create('ViewAllNowPage', {
      programData: item,
      type: type,
      langs: this.langs,
      meta: this.channelMetaData,
      provider_map: this.providerMap,
      STBRemote: this.STBRemote,
      device_id: this.navParams.get('device_id'),
      sb_id: this.navParams.get('sb_id')
    }).present();

  }
  LoadMore(i) {
    console.log(i.split('_'));
    this.getARow(i.split('_')[0], i.split('_')[1] ? -1 : 3, i.split('_')[1] ? i.split('_')[1] : this.langs, this.colData[i].current_offset);
  }
  ionViewWillLeave() {
    this.currentActive = 'NONE';
  }
  public doCheck(onLoad: boolean) {

    let alert = this.alertCtrl.create();
    let alert2 = this.alertCtrl.create();
    // alert = this.alertCtrl.create();
    let langList = new Array();
    langList = this.channelMetaData[0].languages.slice(); //slice creates new MEMORY allocation for langlist
    alert.setTitle('Select Language(s)');
    let selectedLangs = this.local.getEpgLangs() ? this.local.getEpgLangs().split(',') : [];
    if (!onLoad) {
      for (let i of selectedLangs) {
        if (i) {
          alert.addInput({
            type: 'checkbox',
            label: i,
            value: i,
            checked: true
          });
          var index = langList.indexOf(i);
          if (index !== -1) {
            langList.splice(index, 1); //deletes from langlist ONLY, because of slice()
          }
        }
      }
      for (let i of langList) {
        alert.addInput({
          type: 'checkbox',
          label: i,
          value: i,
          checked: false
        });

      }

    }

    else {
      console.log(this.channelMetaData);
      for (let i of langList) {
        alert.addInput({
          type: 'checkbox',
          label: i,
          value: i
        });
      }
    }
    alert.addButton({
      text: 'Cancel',
      handler: data => {
        if (this.just_loaded) {
          this.local.setEpgLangs(langList.toString());
          this.ionViewDidLoad();
        }
      }
    });
    alert.addButton({
      text: 'Ok',
      handler: data => {
        if (data != "" || selectedLangs) {
          alert.dismiss();
          this.local.setEpgLangs(data);
          this.ionViewDidLoad();
          switch (this.current) {
            case 'now': {
              this.tabOneFunction();
              break;
            }
            case 'later': {
              this.tabTwoFunction();
              break;
            }
            case 'guide': {
              this.tabThreeFunction(0);
            }
          }
        }
      },

    });
    alert.present();


  }
  selectType() {

    let alert = this.alertCtrl.create();
    // alert = this.alertCtrl.create();
    alert.setTitle('Select a Category');
    let UniqueNames = [];
    this.types.forEach(function (value) {
      if (UniqueNames.indexOf(value.value) === -1) {
        UniqueNames.push(value.value);
      }
    });

    {
      let temp = this.local.getEpgTypes();
      alert.addInput({
        type: 'radio',
        label: 'All Categories',
        value: this.types,
        checked: this.local.getEpgTypes().length > 1 ? true : false
      })

      for (let i of UniqueNames) {
        alert.addInput({
          type: 'radio',
          label: i,
          value: this.findValue(i),
          checked: temp[0].value == i && !temp[1] ? true : false
        });
      }
    }
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Ok',
      handler: data => {
        if (data) { alert.dismiss(); }
        this.local.setEpgTypes(JSON.stringify(data));
        this.noMoreRows = false;
        this.ionViewDidLoad();
        switch (this.current) {
          case 'now': {
            this.tabOneFunction();
            break;
          }
          case 'later': {
            this.tabTwoFunction();
            break;
          }
          case 'guide': {
            this.tabThreeFunction(0);
          }
        }
      }
    });
    alert.present();


  }
  findValue(i): any {
    let x = [];
    for (let y of this.types) {
      if (y.value == i) x.push(y);
    }
    return x;
  }
  doInfinite(eve) {
    switch (this.current) {
      case 'now': {
        console.log('yoyoyoyoyo');
        if (this.current == "now" && this.rowCount <= this.prog_types.length && this.rowCount > 1 && !this.noMoreRows) {
          this.getARow(this.prog_types[this.rowCount - 1].key, 3, this.langs, 0, eve);
          this.rowCount++;
          /* this.ds.getPrograms(10,0,this.langs,this.prog_types[this.rowCount-1].key,false,
              (data)=>{
               event.complete();
               // console.log(data);
                for(let i of data){
                  i.channel_name = this.channelMetaData[3].channels[i.channelid].channelname;
                  console.log(i.channel_name);
                  if(!this.colData[i.program.type]) this.colData[i.program.type]=[];
                   this.colData[i.program.type].push(i);
                }
                
                 this.rowCount++;
                  console.log(this.prog_types);
                  //this.rowData.push(this.prog_types[this.rowCount-1]);
    
               // console.log(this.colData);
    
              },
              (err)=>
              {
                console.log(err);
              });*/
        } else {
          eve.complete();
        }
        break;
      }
      case 'later': {
        this.doInfinite2(eve);
        break;
      }
      case 'guide': {
        if (!this.showSearchResults) {
          this.doInfiniteForTab3(eve);
        }
        else if (eve) {
          eve.complete();
        }
        break;
      }

    }

    if (this.rowCount > this.prog_types.length) {
      this.noMoreRows = true;
    }
    //console.log(event);
  }

  settings(myEvent) {
    let popover = this.popCtrl.create(PopoverPage, {
      for: _data => {
        if (_data == 'lang') {
          this.doCheck(false);
          console.log('yoyo');
        }
        if (_data == 'cat') {
          this.selectType();
        }
      }
    });
    popover.present({
      ev: myEvent
    });
  }

  /* doInfinite1(event) {
     console.log(event);
     console.log("bello");
   }*/
  goToDetails(i, duration?) {
    /* console.log(i);
     this.navCtrl.push('ProgramDetailsPage',{
       upid:duration=='NONE'?'':i._id,
       duration:duration=='NONE'?'':duration,
       channel_meta:this.channelMetaData[3].channels,
       provider_map:this.providerMap,
       sb_id:this.navParams.get('sb_id'),
       programid:i.programid,
       mainprogramid:i.program.mainprogramid
     })*/
    this.currentActive = 'NONE';
    let detailModal = this.modalCtrl.create('ProgramDetailsPage', {
      upid: duration == 'NONE' ? '' : i._id,
      duration: duration == 'NONE' ? '' : duration,
      channel_meta: this.channelMetaData[3].channels,
      provider_map: this.providerMap,
      sb_id: this.navParams.get('sb_id'),
      programid: i.programid,
      mainprogramid: i.program.mainprogramid,
      reminders: this.reminders,
      STBRemote: this.STBRemote,
      device_id: this.navParams.get('device_id')
    });
    detailModal.present();
    detailModal.onWillDismiss(() => {
      this.currentActive = 'NONE';
    })
  }
  goToSearch() {
    let searchModal = this.modalCtrl.create('SearchModalPage', {
      channel_meta: this.channelMetaData[3].channels,
      provider_map: this.providerMap,
      sb_id: this.navParams.get('sb_id')
    }, { showBackdrop: true, enableBackdropDismiss: true });
    searchModal.present();

  }
  setActive(id) {
    console.log('yooo');
    this.currentActive = id;

  }

  //For Tab TWO -  LATER -----------------------------------------------------------------------------------------
  rowCount2; rowData2 = []; colData2 = [];
  tabTwoFunction() {
    this.currentActive = 'NONE';
    this.noMoreRowsForLater = false;
    this.current = "later";
    this.show_hide(2);
    this.rowData2 = this.prog_types; this.noMoreRows = false;
    this.colData2 = [];
    this.getReminders();
    if (this.prog_types.length == 1) {

      this.rowCount2 = 2;
      this.getARowForLater(this.prog_types[0].key, 1, this.langs);
    }
    else {

      this.rowCount2 = 5;
      this.getARowForLater(this.prog_types[0].key, 3, this.langs);
      this.getARowForLater(this.prog_types[1].key, 3, this.langs);
      this.getARowForLater(this.prog_types[2].key, 3, this.langs);
      this.getARowForLater(this.prog_types[3].key, 3, this.langs);
    }


  }
  getReminders() {
    this.ds.getReminders(
      data => {
        this.reminders = data;
        for (let i of this.reminders) {
          i.upid.channel_name = this.channelMetaData[3].channels[i.upid.channelid].channelname;
        }
        console.log(data);
      }, error => {
        console.log(error);
      }, false
    )
  }
  setReminder(item) {
    console.log(item);
    let obj;
    let obj1;
    obj1 = this.help.switchToChannel(item.channelid, this.channelMetaData, this.providerMap, this.navParams.get('sb_id'), this.navParams.get('device_id'), this.STBRemote);
    if (obj == 'NO CHANNEL') {
      this.currentActive = 'NONE';
      this.toast.bottomToast_Short('Channel Number not available');
    }
    obj = {
      "upid": item._id,
      "channelname": item.channel_name,
      "channelprogramid": item.id,
      "programname": item.program.mainprogramname || item.program.name,
      "starttime": item.starttime,
      "programid": item.programid,
      "ir_appliance_id": this.navParams.get('sb_id'),
      "ir_appliance_name": this.navParams.get('ir_appliance_name'),
      "device_id": this.navParams.get('device_id')
    };
    obj.codes = obj1.codes; obj.len = obj1.len; obj.proto_name = obj1.proto_name; obj.protocol = obj1.protocol;
    obj.wave_gap = obj1.wave_gap;

    item.program.mainprogramid ? obj.mainprogramid = item.program.mainprogramid : null;
    let num = this.help.channel_num_finder(item.channelid, this.providerMap, this.channelMetaData[3].channels);
    !num || num == 'unknown' ? null : obj.channelno = num;
    item.show_loader = true;
    this.ds.postSetReminder(obj, (data) => {
      item.show_loader = false;
      this.currentActive = 'NONE';
      if (data.status == 0) {
        this.toast.bottomToast_Short(data.message);
      }
      else if (data.status == 1) {
        if (!this.reminders) {
          this.reminders = [];
        }
        this.reminders.push({ _id: item._id, upid: item });
      }
    }, error => {
      item.show_loader = false;
      console.log(error);
    })
  }
  goToChannelList(fab: FabContainer) {
    fab.close();
    this.modalCtrl.create('ChannelListPage', {
      rev_providerMap2: this.rev_providerMap2,
      providerMap: this.providerMap,
      STBRemote: this.STBRemote,
      device_id: this.navParams.get('device_id'),
      sb_id: this.navParams.get('sb_id')
    }).present();
  }

  unsetReminder(ind, programid) {
    this.reminders[ind].show_loader = true;
    this.ds.postUnsetReminder({
      programid: programid
    }, data => {
      this.reminders[ind].show_loader = false;
      if (this.reminders) this.reminders.splice(ind, 1);
    }, error => {
      this.reminders[ind].show_loader = false;
      console.log(error);
    })

  }
  getARowForLater(type, num, lang) {
    this.ds.getProgramsForLater(20, 0, lang, type, false,
      (data) => {
        for (let i of data) {
          let pType = num == -1 ? lang + i.program.type : i.program.type;
          i.channel_name = this.channelMetaData[3].channels[i.channelid].channelname;

          if (!this.colData2[pType]) {
            this.colData2[pType] = [];
            // this.colData2[pType].day=[];
            //this.colData2[pType].week=[];
          }
          /*   let end = new Date(Date.now());
             if(i.starttime<=end.setHours(23,59,59,999)){
              this.colData2[pType].day.push(i);
             }
             else{
               this.colData2[pType].week.push(i);
             }*/
          if (i.program.name != 'Movie') {
            this.colData2[pType].push(i);
          }


        }



        console.log(this.rowData2);
        console.log(this.colData2);
      },
      (error) => {
        console.log(error);
      })

  }
  doInfinite2(eve) {
    //console.log('yoyoyoyoyo');
    if (this.current == "later" && this.rowCount2 <= this.prog_types.length && this.rowCount2 > 1 && !this.noMoreRowsForLater) {
      this.ds.getProgramsForLater(20, 0, this.langs, this.prog_types[this.rowCount2 - 1].key, false,
        (data) => {
          eve.complete();
          // console.log(data);
          for (let i of data) {
            i.channel_name = this.channelMetaData[3].channels[i.channelid].channelname;
            // console.log(i.channel_name);
            if (!this.colData2[i.program.type]) this.colData2[i.program.type] = [];
            this.colData2[i.program.type].push(i);
          }

          this.rowCount2++;
          console.log(this.prog_types);
          //this.rowData.push(this.prog_types[this.rowCount-1]);

          // console.log(this.colData);

        },
        (err) => {
          console.log(err);
        });
    }
    else {
      eve.complete();
    }

    if (this.rowCount2 > this.prog_types.length) {
      this.noMoreRowsForLater = true;
    }
    //console.log(event);
  }

  goToViewAll(item, type) {
    this.modalCtrl.create('LaterThisWeekPage', {
      programData: item,
      type: type,
      langs: this.langs,
      meta: this.channelMetaData,
      provider_map: this.providerMap,
      reminders: this.reminders
    }).present();
    /* this.navCtrl.push('LaterThisWeekPage', {
       programData: item,
       type: type,
       langs: this.langs,
       meta: this.channelMetaData,
       provider_map: this.providerMap
     })*/
  }


  //TAB2 ENDS HERE-------------------------------------------------------------------------------------------------



  //for TAB THREE-------------------------------------------------------------------------------------------------

  currentLangs = []; show_loading3; currentType; currentTypes = []; current_offset3; guideData = []; noMoreChannels = false; guideData2 = []; showSearchResults; fav_channels;
  tabThreeFunction(offset, infinite?, eve?) {
    if (!infinite) {
      this.show_hide(3);
    }
    this.current = "guide";
    this.noMoreChannels = false;
    if (this.guideData.length == 0 || infinite == true) {
      this.showSearchResults = false;
      this.current_offset3 = offset;
      if (offset == 0) {
        this.show_loading3 = true;
      }
      this.ds.getPrograms(15, offset, this.langs, this.local.getEpgTypes().length != 1 ? '' : this.local.getEpgTypes()[0].key, false,
        (data) => {
          this.show_loading3 = false;
          console.log(data);
          console.log(offset);
          if (eve) eve.complete();
          if (data.length == 0) this.noMoreChannels = true;
          if (offset == 0) {
            this.guideData = [];
            this.guideData = data;
          }
          else {
            this.guideData = this.guideData.concat(data);
          }
          console.log(this.guideData);
          if (!infinite) {
            this.ds.getFavChannels(
              data_fav => {
                console.log(data_fav);
                this.fav_channels = data_fav.fav_channels;
                for (let i of this.guideData) {
                  i.channelname = this.channelMetaData[3].channels[i.channelid].channelname;
                  i.thumbnail = "assets/img/channels/" + i.channelname.toString() + ".png";
                  if (this.fav_channels[i.channelid]) i.isFav = "heart";
                  else i.isFav = "heart-outline";
                }
              },
              error => {
                if (eve) eve.complete();
              }, false);
          }
          for (let i of this.guideData) {
            if (i.channelname) continue;
            i.channelname = this.channelMetaData[3].channels[i.channelid].channelname;
            i.thumbnail = "assets/img/channels/" + i.channelname.toString() + ".png";
            if (i.channelid && this.fav_channels && this.fav_channels[i.channelid]) i.isFav = "heart";
            else i.isFav = "heart-outline";
          }

        }, (error) => {
          this.show_loading3 = false;
          console.log(error);
        });

      this.guideData2 = [];
      console.log(this.channelMetaData[3].channels);
      for (let key in this.channelMetaData[3].channels) {
        this.channelMetaData[3].channels[key].thumbnail = '';
        this.guideData2.push(this.channelMetaData[3].channels[key]);
      }
      console.log(this.guideData2);
      /* this.ds.getFavChannels(
             data=>{
               console.log(data);
                 if(data.fav_channels){
                   for(let i of this.guideData){
                           i.thumbnail = "assets/img/channels/"+i.channelname.toString()+".png";
                           if(data.fav_channels[i.channelid]) i.isFav="heart";
                           else i.isFav="heart-outline";
                   }
                 }
             },
             error=>{
     
             },false);*/
    }
  }
  trackByFunction(ind, i) {
    return ind;
  }
  setFav(i, ind) {
    if (i.isFav == "heart") {
      this.ds.postEditFavChannels({
        "channel_id": i.channelid || i.id,
        "action": "remove"
      }, (data) => {
        i.isFav = "heart-outline";
        if (this.current == 'Favourites') {
          let data = [];
          this.favData.splice(ind, 1);
          console.log(this.favData)
        }
      }, error => {
        console.log(error);
      })
    }
    else if (i.isFav == "heart-outline") {
      this.ds.postEditFavChannels({
        "channel_id": i.channelid || i.id,
        "action": "add"
      }, (data) => {
        i.isFav = "heart";
      }, error => {
        console.log(error);
      })
    }
  }
  unsetFavProg(i, ind) {
    this.ds.postEditFavPrograms({
      "programid": i.doc.fav_program_id,
      "action": "remove"
    }, (data) => {
      this.fav_prog_data.splice(ind, 1);
      console.log(this.fav_prog_data);
    }, error => {
      console.log(error);
    })
  }

  doInfiniteForTab3(eve) {
    if (this.current == "guide" && !this.noMoreChannels) {
      this.tabThreeFunction(this.current_offset3 + 15, true, eve);
    }
    else {
      eve.complete();
    }
  }
  closeFab() {
    this.fab1.close();
    this.fab_opn = false;
  }
  filterLanguage() {
    let alert = this.alertCtrl.create();
    let langs = [];
    for (let i of this.channelMetaData[0].languages.slice()) {
      langs.push({ "name": i })
    }
    if (this.currentLangs.length == 0) {
      this.currentLangs = langs.slice();
    }
    console.log(this.currentLangs);
    for (let i of this.currentLangs) {
      alert.addInput({
        type: 'checkbox',
        label: i.name,
        value: i.name,
        checked: i.selected ? i.selected : false
      })
    }
    alert.addButton({
      text: 'Cancel'
    })
    alert.addButton({
      text: 'Okay',
      handler: (data) => {
        let j = 0;
        console.log(data);
        for (let i of this.currentLangs) {
          if (i.name == data[j]) {
            console.log(data[j]);
            i.selected = true;
            j++;
          }
          else {
            i.selected = false;
          }
        }
        this.tabThreeFunction(0);
      }
    })
    alert.present();


  }
  filterType() {
    let alert = this.alertCtrl.create();
    if (this.currentTypes.length == 0) this.currentTypes = this.types.slice();
    alert.addInput({
      type: 'radio',
      label: 'All Categories',
      value: this.types.slice(),
      checked: this.currentTypes.length > 1 ? true : false
    })
    for (let i of this.types) {
      alert.addInput({
        type: 'radio',
        label: i.value,
        value: i,
        checked: this.currentTypes[0] == i && this.currentTypes.length == 1 ? true : false
      })
    }
    alert.addButton({
      text: 'Cancel'
    })
    alert.addButton({
      text: 'Okay',
      handler: (data) => {
        console.log(data);
        this.currentTypes = [];
        this.currentTypes.push(data);
        this.tabThreeFunction(0);
      }

    })
    alert.present();
  }

  goToChannelPrograms(i) {
    /*  this.navCtrl.push('ChannelProgramsPage', {
        current: i,
        channel_meta: this.channelMetaData[3].channels,
        provider_map: this.providerMap,
        sb_id: this.navParams.get('sb_id')
      });*/
    this.modalCtrl.create('ChannelProgramsPage', {
      current: i,
      channel_meta: this.channelMetaData[3].channels,
      channel_meta2: this.channelMetaData,
      provider_map: this.providerMap,
      sb_id: this.navParams.get('sb_id'),
      ir_appliance_id: this.navParams.get('ir_appliance_id'),
      ir_appliance_name: this.navParams.get('ir_appliance_name'),
      device_id: this.navParams.get('device_id'),
      STBRemote: this.STBRemote
    }).present();

  }
  guideData3 = [];
  filterItems3(ev) {
    let val = ev.target.value;
    this.guideData3 = [];
    // if the value is an empty string don't filter the items
    if (val) {
      if (val.trim() != '') {
        /* this.guideData3 = this.guideData2.filter((item: any) => {
           return (item.channelname.toLowerCase().indexOf(val.toLowerCase()) > -1);
         })*/
        this.ds.getSearchChannels(val,
          data => {
            this.guideData3 = data;
            this.showSearchResults = true;
            for (let i of this.guideData3) {
              i.thumbnail = 'assets/img/channels/' + i.channelname + '.png' || 'assets/img/loading.jpg';
              console.log(this.guideData3);
              if (this.fav_channels) {
                if (this.fav_channels[i.id]) i.isFav = "heart";
                else i.isFav = "heart-outline";
              }
            }
          }, error => {
            console.log(error);
          }, false)

      }
    }
    else if (!val) {
      this.showSearchResults = false;
      this.guideData3 = [];
    }
  }

  //-----------------------------------------------------TAB THREE ENDS HERE--------------------------------------

  favData = []; favs = []; fav_prog_data = []; dont_show_loader4;
  tabFourFunction() {
    this.current = "Favourites";
    this.show_hide(4);

    let temp;// this.favData = []; this.favs = []
    (this.favData.length && this.favs.length && (this.fav_toggle == true || this.fav_toggle == false)) ? this.dont_show_loader4 = true : this.fav_toggle = false;
    this.favData = []; this.favs = []; this.fav_prog_data = [];

    this.ds.getFavChannels(
      (data) => {
        this.favs = Object.keys(data.fav_channels);
        console.log(this.favs);
        for (let i of this.favs) {
          temp = this.channelMetaData[3].channels[i];
          if (temp) {
            this.favData.push({
              channelid: temp.id,
              channel_name: temp.channelname,
              isFav: "heart",
              thumbnail: 'assets/img/channels/' + temp.channelname + '.png'
            })
          }
        }
        console.log("Everything went fine till here.");
        console.log("got fav channels");
      },
      (error) => {
        console.log(error);
      }, !this.dont_show_loader4);
    this.ds.getFavPrograms('info',
      data => {
        this.fav_prog_data = data;
        console.log("Everything went fine till here.");
      },
      error => {
        console.log(error);
      }, false);

  }
  filterItems4(ev) {
    let val = ev.target.value;
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.favData = this.favData.filter((item: any) => {
        return (item.channel_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }
  toggleFav(val) {
    this.fav_toggle = val;

  }
  fab_opn = false;
  fabOpen(ev) {
    this.fab_opn = !this.fab_opn;
    //console.log(ev);
  }








}

























@Component({
  template: `
    <ion-list>
      <button ion-item (click)="close('lang')" > Language(s)
        <ion-icon item-right md="ios-arrow-forward"></ion-icon>
      </button>
      <button ion-item (click)="close('cat')"> Category 
        <ion-icon item-right md="ios-arrow-forward"></ion-icon>
      </button>      
    </ion-list>
  `,
  styles: ['.item-block{ background-color: #313131 !important;}',
    '.list-md{margin:0px !important;width:200px !important;float:right;}',
    '.list-ios{margin:0px !important;width:200px !important;float:right;}',
    '.item-ios{color:white !important;}',
    '.item-md{color:white !important;}',
    '.item{border-top: none !important}',
    '.popover-md{box-shadow: none;background:transparent !important;}',
    '.popover-ios .popover-arrow::after{background-color:#313131 !important;}']
})
export class PopoverPage {
  callback;
  constructor(public viewCtrl: ViewController, private params: NavParams) {
    this.callback = this.params.get('for');
  }

  close(out) {
    this.callback(out);
    this.viewCtrl.dismiss();
  }


}



