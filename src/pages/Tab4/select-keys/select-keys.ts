import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../providers/DataService';
import { AlertService } from '../../../providers/AlertService';

@IonicPage()
@Component({
  selector: 'page-select-keys',
  templateUrl: 'select-keys.html',
})
export class SelectKeysPage {
  remote; data; is_stb; remote_keys = []; remote_keys1; remote_keys2; all_buttons; ind_in_all_buttons = -1;
  constructor(public navCtrl: NavController, public navParams: NavParams, private alert: AlertService, private ds: DataService, private viewCtrl: ViewController) {
    this.remote = this.navParams.get('remote');
    this.all_buttons = this.navParams.get('all_buttons');
    let ind = 0;
    if (this.all_buttons) {
      for (let i of this.all_buttons) {
        if (i.irapp == this.remote._id.split('_')[3]) {
          this.ind_in_all_buttons = ind;
        }
        ind++;
      }
    }
    else {
      this.all_buttons = [];
    }
    if (this.ind_in_all_buttons == -1) {
      this.all_buttons.push({
        irapp: this.remote._id.split('_')[3],
        keys: []
      });
      this.ind_in_all_buttons = this.all_buttons.length - 1;
    }
    console.log(this.remote);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SelectKeysPage');
    this.ds.getRemoteFromDb(this.remote.remote_codeset,
      data => {
        this.data = data;
        this.is_stb = this.data[0].is_stb;
        console.log(this.data);
        let temp = Object.keys(this.data[0].keys[0]);
        let ind = 0;
        for (let i of temp) {
          this.remote_keys.push({
            name: i,
            checked: false
          });
          if (this.all_buttons[this.ind_in_all_buttons].keys) {
            for (let j of this.all_buttons[this.ind_in_all_buttons].keys) {
              if (j.name == i) {
                this.remote_keys[ind].checked = true;
              }
            }
          }
          ind++;

        }
        this.remote_keys1 = this.remote_keys.slice();
        console.log(this.remote_keys1);

      }, error => {
        console.log(error);
        if (error.status == 404) {
          this.navCtrl.pop();
          this.alert.showAlert('Sorry', error.message);
        }
      }, true);
  }
  dismiss(data) {
    this.viewCtrl.dismiss(data);
  }

  filterItems(ev: any) {

    this.remote_keys1 = this.remote_keys.slice();
    let val = ev.target.value;
    //let result_data = this.remote_keys1.slice();
    if (val && val.trim() != '') {
      this.remote_keys1 = this.remote_keys1.filter((item: any) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
    else {
      this.remote_keys1 = this.remote_keys.slice();
    }

  }

  ionViewWillLeave() {
    this.all_buttons[this.ind_in_all_buttons].keys = [];
    for (let i of this.remote_keys) {
      if (i.checked) {
        this.all_buttons[this.ind_in_all_buttons].keys.push({
          name: i.name
        });
      }
    }
  }

  detectChanges(eve, key_name) {
    console.log(eve.checked);
    for (let i of this.remote_keys) {
      if (i.name == key_name) {
        i.checked = eve.checked;
        if (eve.checked == false) {
          let ind1 = 0;
          for (let j of this.all_buttons[this.ind_in_all_buttons].keys) {
            if (j.name == i.name) {
              this.all_buttons[this.ind_in_all_buttons].keys.splice(ind1, 1);
            }
            ind1++;
          }
        }
        break;
      }
    }
    for (let i of this.remote_keys1) {
      if (i.name == key_name) {
        i.checked = eve.checked;
        break;
      }
    }

  }
}
