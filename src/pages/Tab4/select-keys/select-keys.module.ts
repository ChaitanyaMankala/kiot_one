import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectKeysPage } from './select-keys';

@NgModule({
    declarations: [
        SelectKeysPage,
    ],
    imports: [
        IonicPageModule.forChild(SelectKeysPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class SelectKeysPageModule { }