import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../providers/DataService';
import { ToastService } from '../../../providers/ToastService';

@IonicPage()
@Component({
  selector: 'page-create-custom-remote',
  templateUrl: 'create-custom-remote.html',
})
export class CreateCustomRemotePage {
  custom_name; all_buttons; isEditId;
  constructor(public navCtrl: NavController, public navParams: NavParams, private toast: ToastService, private ds: DataService) {
    this.all_buttons = this.navParams.get('all_buttons');
    this.isEditId = this.navParams.get('isEditId')
    if (this.isEditId) {
      this.custom_name = this.navParams.get('custom_name_obj').name;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateCustomRemotePage');
  }
  createCustomRemote() {
    this.ds.postCreateCustomRemote({
      "home_id": this.navParams.get('home_id'),
      "name": this.custom_name,
      "buttons": this.all_buttons
    }, data => {
      console.log(data);
      this.navCtrl.pop();
      this.navCtrl.pop();
      this.toast.bottomToast_Short('Custom Remote Created');
    }, error => {
      console.log(error);
    });
  }

  editCustomRemote() {
    let ind = 0;
    for (let i of this.all_buttons) {
      if (!i.keys.length) {
        this.all_buttons.splice(ind, 1);
      }
      ind++;
    }
    this.ds.postEditCustomRemote({
      "_id": this.isEditId,
      "home_id": this.navParams.get('home_id'),
      "name": this.custom_name,
      "buttons": this.all_buttons
    }, data => {
      console.log(data);
      this.navCtrl.pop();
      this.navCtrl.pop();
      this.toast.bottomToast_Short('Edits saved');
    }, error => {
      console.log(error);
    })
  }
}
