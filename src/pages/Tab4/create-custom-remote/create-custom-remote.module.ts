import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateCustomRemotePage } from './create-custom-remote';

@NgModule({
    declarations: [
        CreateCustomRemotePage,
    ],
    imports: [
        IonicPageModule.forChild(CreateCustomRemotePage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class CreateCustomRemotePageModule { }