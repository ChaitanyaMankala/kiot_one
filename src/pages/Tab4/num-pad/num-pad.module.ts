import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NumPadPage } from './num-pad';

@NgModule({
    declarations: [
        NumPadPage,
    ],
    imports: [
        IonicPageModule.forChild(NumPadPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class NumPadPageModule { }