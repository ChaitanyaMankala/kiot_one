import { Component } from '@angular/core';
import { ViewController, NavParams, Platform, ModalController,IonicPage } from 'ionic-angular';
//import { Vibration } from 'ionic-native';

//providers
import { DataService } from '../../../providers/DataService';
import { Helper } from '../../../providers/Helper';
import { ToastService } from '../../../providers/ToastService';

//pages
import { ChannelListPage } from '../channel-list/channel-list';

@IonicPage()
@Component({
  selector: 'page-num-pad',
  templateUrl: 'num-pad.html'
})
export class NumPadPage {
  channel_number: string; text = ''; invalid; channel_meta; thumbnail; STBRemote; remote_keys; providerMap; rev_providerMap; rev_providerMap2;
  constructor(public viewCtrl: ViewController, private toast: ToastService, public modalCtrl: ModalController, public navParams: NavParams, private help: Helper, private ds: DataService, private platform: Platform) {
    this.channel_number = '';
    this.invalid = '';
    this.channel_meta = this.navParams.get('channel_meta');
    console.log(this.channel_meta);
    this.providerMap = this.navParams.get('providerMap');
    this.rev_providerMap = this.navParams.get('rev_providerMap');
    this.rev_providerMap2 = this.navParams.get('rev_providerMap2');
    this.STBRemote = this.navParams.get('STBRemote');
    this.remote_keys = this.STBRemote[0].keys[0];
    /*this.viewCtrl.onWillDismiss(() => {
      let blurred = this.navParams.get('blurred');
      blurred[0] = false;
      console.log('here' + blurred);
    })*/

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad NumPadPage');
  }
  dismiss(data) {
    this.viewCtrl.dismiss(data);
  }
  addToNum(num) {
    this.invalid = '';
    if (this.channel_number.length < 4) {
      if (this.channel_number == '' && num == '0') {

      }
      else {
        this.channel_number += num;
        this.checkThumbnail();
      }
    }
    else {
      this.invalid = 'invalid';
    }

  }
  backspaceFunc() {
    if (this.channel_number.length > 0) {
      this.channel_number = this.channel_number.substr(0, this.channel_number.length - 1);
      this.checkThumbnail();
    }
  }
  clearAllFunc() {
    this.channel_number = '';
    this.thumbnail = null;
  }
  checkThumbnail() {
    console.log(this.rev_providerMap);
    let num = this.rev_providerMap[this.channel_number];
    console.log(num);
    console.log(this.rev_providerMap2);
    if (this.rev_providerMap2[num]) {
      this.thumbnail = 'assets/img/channels/' + this.rev_providerMap2[num] + '.png'
    }
    else {
      this.thumbnail = null;
    }
  }
  switchToChannel() {
    this.ds.postSwitchToChannel(
      {
        "channelid": this.channel_number,
        "ir_device": this.navParams.get('sb_id'),
        "device": this.navParams.get('device_id'),
        "protocol": this.STBRemote[0].protocol,
        "len": this.STBRemote[0].len,
        "proto_name": this.STBRemote[0].proto_name,
        "wave_gap": this.STBRemote[0].wave_gap,
        "codes": this.help.getCodesForChannel(this.channel_number, this.remote_keys)
      },
      data => {
        // if (this.platform.is('android')) Vibration.vibrate(30);
        console.log(data);
        this.thumbnail = null;
        this.text = "Playing " + this.channel_number + ' ...';
        this.channel_number = '';
        setTimeout(() => {
          this.text = '';
        }, 1000)
        //this.toast.topToast_Short('Playing ' + this.channel_number)
        //this.clearAllFunc();
      },
      error => {
        console.log(error);
      }
    )
  }
  goToChannelList() {
    this.modalCtrl.create('ChannelListPage', {
      rev_providerMap2: this.navParams.get('rev_providerMap2'),
      providerMap: this.navParams.get('providerMap'),
      STBRemote: this.navParams.get('STBRemote'),
      device_id: this.navParams.get('device_id'),
      sb_id: this.navParams.get('sb_id')
    }).present();
  }
}

