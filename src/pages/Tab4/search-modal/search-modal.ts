import { ViewController,NavParams,AlertController,ModalController,IonicPage} from 'ionic-angular';
import { Component } from '@angular/core';
//import { Vibration } from 'ionic-native';

//providers
import { DataService } from '../../../providers/DataService';
import { ProgramDetailsPage } from '../program-details/program-details';


@IonicPage()
@Component({
  selector: 'page-search-modal',
  templateUrl: 'search-modal.html'
})
export class SearchModalPage {
searchResult;text;searchKey;
  constructor(private viewCtrl: ViewController,private navParams:NavParams,private modalCtrl:ModalController,private ds:DataService,private alertCtrl:AlertController) {
   /* this.viewCtrl.onWillDismiss(()=>{
              let blurred = this.navParams.get('blurred');
              blurred[0] = false;
              console.log('here'+blurred);
      })*/
      this.searchResult = [];
      this.text = false;

  }
  dismissSwipe(event){
    console.log(event);
  }
  dismiss(data) {
    this.viewCtrl.dismiss(data);
  }
  searchPrograms(event){
    console.log(event.target.value);
    this.searchKey = event.target.value;
    if(this.searchKey!=''){
          this.ds.getSearchPrograms(this.searchKey,
          data=>{
              console.log(data);
              this.text=false;
              this.searchResult = data;
              if(this.searchResult.length == 0){
                this.text = 'No Results found!'
              }
          },error=>{
            console.log(error);

          },false);
    }
    else{
        this.searchKey='';
        this.text=false;
        this.searchResult=[];
    }
  }
  goToDetails(i){

        let detailModal = this.modalCtrl.create('ProgramDetailsPage',{
          upid:'',
          duration:'',
          channel_meta:this.navParams.get('channel_meta'),
          provider_map:this.navParams.get('provider_map'),
          sb_id:this.navParams.get('sb_id'),
          programid:i.programid,
          mainprogramid:i.program.mainprogramid
        },{showBackdrop:true,enableBackdropDismiss:true});
        detailModal.present();
  }




}
