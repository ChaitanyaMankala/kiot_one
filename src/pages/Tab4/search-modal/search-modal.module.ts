import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchModalPage } from './search-modal';
import { PipesModule } from '../../../pipes/pipes.module';

@NgModule({
    declarations: [
        SearchModalPage,
    ],
    imports: [
        IonicPageModule.forChild(SearchModalPage),
        PipesModule
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class SearchModalPageModule { }