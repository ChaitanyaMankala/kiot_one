import { Component } from '@angular/core';
import { NavController, NavParams,ModalController,ViewController,IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../providers/DataService';
import { Helper } from '../../../providers/Helper';
import { ToastService } from '../../../providers/ToastService';

//pages
import { ProgramDetailsPage } from '../program-details/program-details';

@IonicPage()
@Component({
  selector: 'page-later-this-week',
  templateUrl: 'later-this-week.html'
})
export class LaterThisWeekPage {
programData;type;noMoreRows;langs;channelMetaData;data;offset;current_active;reminders;
  constructor(public navCtrl: NavController, public navParams: NavParams,private toast:ToastService,private ds:DataService,private help:Helper,private viewCtrl:ViewController,private modalCtrl:ModalController) {
    this.programData = this.navParams.get('programData').slice();
    this.langs = this.navParams.get('langs');
    this.type = this.navParams.get('type');
    console.log(this.programData);
    this.noMoreRows = false;
    this.channelMetaData = this.navParams.get('meta');
    this.offset =20;
    this.reminders = this.navParams.get('reminders');
  }

  ionViewWillEnter() {
        this.current_active = 'NONE';
    console.log('ionViewDidLoad LaterThisWeekPage');
  }
  dismiss(data){
    this.viewCtrl.dismiss(data);
  }
  doInfinite(event){
      console.log(this.noMoreRows);
      console.log(this.offset);
      console.log(event);
      this.ds.getProgramsForLater(10,this.offset,this.langs,this.type.key,false,
      (data)=>{
        this.data=data;
        event.complete();
            if(this.data.length==0){
              this.noMoreRows = true;
            }
            console.log(this.data);
            let ind=0;
            for(let i of this.data){
              i.channel_name = this.channelMetaData[3].channels[i.channelid].channelname;
              if(i.program.name=='Movie'){
                  this.data.splice(ind);
              }
              ind++;
            }
            this.programData = this.programData.concat(this.data);
            this.offset+=10;
                  console.log(this.noMoreRows);
      console.log(this.offset);
      },
      (error)=>{
          console.log(error);
      })
  }
  onTap(i){
    this.current_active = i;
    console.log(i);
  }
  goToProgramDetails(i){
     /* this.navCtrl.push('ProgramDetailsPage',{
        upid:i._id,
        channel_meta:this.channelMetaData[3].channels,
        provider_map:this.navParams.get('provider_map'),
        programid:i.programid,
        mainprogramid:i.program.mainprogramid
      })*/
      let detailsModal = this.modalCtrl.create('ProgramDetailsPage',{
        upid:i._id,
        channel_meta:this.channelMetaData[3].channels,
        provider_map:this.navParams.get('provider_map'),
        programid:i.programid,
        reminders:this.reminders,
        mainprogramid:i.program.mainprogramid
      });
      detailsModal.present();
  }

  setReminder(item) {
    console.log(item);
    let obj;
    obj = {
      "upid":item._id,
      "channelname": item.channel_name,
      "channelprogramid": item.id,
      "programname": item.program.mainprogramname || item.program.name,
      "starttime": item.starttime,
      "programid": item.programid,
      "ir_appliance_id": this.navParams.get('ir_appliance_id'),
      "ir_appliance_name": this.navParams.get('ir_appliance_name')
    };
    item.program.mainprogramid ? obj.mainprogramid = item.program.mainprogramid : null;
    let num = this.help.channel_num_finder(item.channelid, this.navParams.get('provider_map'), this.channelMetaData[3].channels);
    !num || num == 'unknown' ? null : obj.channelno = num;
    this.ds.postSetReminder(obj, (data) => {
      this.current_active = 'NONE';
      if (data.status == 0) {
          this.toast.bottomToast_Short(data.message);
      }
      else if (data.status == 1) {
        if (!this.reminders) {
          this.reminders = [];
        }
        this.reminders.push({ _id:item._id,upid: item });
      }
    }, error => {
      console.log(error);
    })
  }
  unsetReminder(ind, programid) {
    this.ds.postUnsetReminder({
      programid: programid
    }, data => {
      if (this.reminders) this.reminders.splice(ind, 1);
    }, error => {
      console.log(error);
    })

  }

}
