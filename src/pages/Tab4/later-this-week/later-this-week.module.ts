import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LaterThisWeekPage } from './later-this-week';
import { PipesModule } from '../../../pipes/pipes.module';

@NgModule({
    declarations: [
        LaterThisWeekPage,
    ],
    imports: [
        IonicPageModule.forChild(LaterThisWeekPage),
        PipesModule
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class LaterThisWeekPageModule { }