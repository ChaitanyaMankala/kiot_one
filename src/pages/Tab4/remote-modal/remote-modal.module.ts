import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RemoteModalPage } from './remote-modal';

@NgModule({
    declarations: [
        RemoteModalPage,
    ],
    imports: [
        IonicPageModule.forChild(RemoteModalPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class RemoteModalPageModule { }