import { ViewController, NavParams, AlertController, ModalController, NavController, IonicPage } from 'ionic-angular';
import { Component } from '@angular/core';
//import { Vibration } from 'ionic-native';

//providers
import { DataService } from '../../../providers/DataService';

//pages
import { NumPadPage } from '../num-pad/num-pad';
import { ChannelListPage } from '../channel-list/channel-list';
import { RealRemotePage } from '../real-remote/real-remote';

@IonicPage()
@Component({
  selector: 'page-remote-modal',
  templateUrl: 'remote-modal.html'
})
export class RemoteModalPage {
  codeset; remote_keys = {}; data; ir_apppliance; remote_keys1; paired_tv; device_id; irSignal: any; is_stb; sb_id; TVData;
  constructor(private viewCtrl: ViewController, private navCtrl: NavController, private navParams: NavParams, private modalCtrl: ModalController, private ds: DataService, private alertCtrl: AlertController) {
    this.codeset = this.navParams.get('codeset');
    this.device_id = this.navParams.get('device_id');
    this.sb_id = this.navParams.get('sb_id');
    this.irSignal = {
      "device": this.device_id,
      "switchboard": this.sb_id

    }
    this.ir_apppliance = this.navParams.get('ir_appliance');
    this.paired_tv = this.navParams.get('paired_tv');
    this.data = this.navParams.get('STBRemote');
    this.remote_keys = this.data[0].keys[0];
    this.remote_keys1 = Object.keys(this.remote_keys);
    /* this.viewCtrl.onWillDismiss(()=>{
               let blurred = this.navParams.get('blurred');
               blurred[0] = false;
               console.log('here'+blurred);
       })*/
    this.TVData = this.navParams.get('tv_data');
    console.log(this.TVData);
  }
  dismissSwipe(event) {
    console.log(event);
  }
  ionViewWillEnter() {
    /*  this.ds.getRemoteFromDb(this.navParams.get('codeset'),
         data=>{
           this.data=data;
               this.is_stb = this.data[0].is_stb;
           console.log(this.data);
           this.remote_keys = this.data[0].keys[0];
          this.remote_keys1 = Object.keys(this.remote_keys);
          console.log(this.remote_keys1);
         },error=>{
             console.log(error);
         });*/

  }
  dismiss(data) {
    this.viewCtrl.dismiss(data);
  }
  onWillDismiss() {

  }
  temp_timer;
  volumeLongPress(str, key?) {
    if (str == 'start') {
      this.triggerIr(key);
      setTimeout(() => { }, 700);
      this.temp_timer = setInterval(() => {
        this.triggerIr(key);
      }, 700)
    }
    else if (str == 'end') {
      clearInterval(this.temp_timer);
    }
  }
  triggerIr(i, data?) {
    console.log(this.remote_keys[i]);
    if (i) {
      switch (this.data[0].type) {
        case 1: {
          this.irSignal.protocol = this.data[0].protocol;
          this.irSignal.code = this.remote_keys[i];
          this.irSignal.len = this.data[0].len;
          this.irSignal.proto_name = this.data[0].proto_name;
          this.irSignal.wave_gap = this.data[0].wave_gap;
          break;
        }
        case 2: {
          this.irSignal.code = this.remote_keys[i].code;
          this.irSignal.first_seq_len = this.remote_keys[i].first_seq_len;
          this.irSignal.sec_seq_len = this.remote_keys[i].sec_seq_len;
          this.irSignal.len = parseInt(this.remote_keys[i].first_seq_len) + parseInt(this.remote_keys[i].sec_seq_len);
          this.irSignal.freq = this.data[0].freq;
          this.irSignal.rpt_cnt = this.data[0].rpt_cnt;
          break;
        }
      }
    }
    this.ds.postTriggerIr(data || this.irSignal,
      (data) => {
        console.log(data);
        // Vibration.vibrate(30);
      },
      (error) => {
        console.log(error);
      }, false)

  }
  triggerTV(_command) {
    if (this.TVData.length == 1) {
      this.TVIrSignal(this.TVData[0], _command);
    }
    else if(this.paired_tv[0] && this.paired_tv[0].remote_codeset){
      this.TVIrSignal(this.paired_tv[0], _command);
    }
    else {
      this.askForTV(_command);
    }
  }
  askForTV(_command, override?) {
    let alert = this.alertCtrl.create();
    for (let i of this.TVData) {
      alert.addInput({
        type: 'radio',
        label: i.name + " in " + i.room_name,
        value: i
      })
    }
    alert.addButton({
      text: 'Cancel',
      handler: () => { }
    })
    alert.addButton({
      text: 'Ok',
      handler: (tv) => {
        if (override) {
          this.openTV(tv);
        }
        else {
          this.TVIrSignal(tv, _command);
        }
      }
    });
    alert.present();
  }
  TVIrSignal(tv, _command) {
    this.ds.getRemoteFromDb(tv.remote_codeset,
      (data) => {
        let signalData = data[0];

        if (signalData.keys[0]['POWER ON'] && _command == 'POWER') {
          let alert2 = this.alertCtrl.create();
          alert2.addInput({
            type: 'radio',
            label: 'Power On',
            value: 'POWER ON'
          });
          alert2.addInput({
            type: 'radio',
            label: 'Power Off',
            value: 'POWER OFF'
          });
          alert2.addButton({
            text: 'Cancel',
            handler: () => { }
          });
          alert2.addButton({
            text: 'Go',
            handler: (power) => {
              let irSignal = {
                device: tv.device_id,
                switchboard: tv.sb_id,
                code: signalData.keys[0][power].code,
                first_seq_len: signalData.keys[0][power].first_seq_len,
                sec_seq_len: signalData.keys[0][power].sec_seq_len,
                len: parseInt(signalData.keys[0][power].first_seq_len) + parseInt(signalData.keys[0][power].sec_seq_len),
                freq: signalData.freq,
                rpt_cnt: signalData.rpt_cnt
              }
              this.triggerIr(null, irSignal);
            }
          });
          alert2.present();
        }
        else {
          let irSignal = {
            device: tv.device_id,
            switchboard: tv.sb_id,
            code: signalData.keys[0][_command].code,
            first_seq_len: signalData.keys[0][_command].first_seq_len,
            sec_seq_len: signalData.keys[0][_command].sec_seq_len,
            len: parseInt(signalData.keys[0][_command].first_seq_len) + parseInt(signalData.keys[0][_command].sec_seq_len),
            freq: signalData.freq,
            rpt_cnt: signalData.rpt_cnt
          }
          this.triggerIr(null, irSignal);
        }
      }, (error) => {
        console.log('error');
      });
  }

  goToNumPad() {
    let numPadModal = this.modalCtrl.create('NumPadPage', {
      channel_meta: this.navParams.get('channel_meta'),
      providerMap: this.navParams.get('providerMap'),
      rev_providerMap: this.navParams.get('rev_providerMap'),
      rev_providerMap2: this.navParams.get('rev_providerMap2'),
      device_id: this.navParams.get('device_id'),
      sb_id: this.navParams.get('sb_id'),
      STBRemote: this.navParams.get('STBRemote')
    });
    numPadModal.present();
  }

  goToChannelList() {
    this.modalCtrl.create('ChannelListPage', {
      rev_providerMap2: this.navParams.get('rev_providerMap2'),
      providerMap: this.navParams.get('providerMap'),
      STBRemote: this.navParams.get('STBRemote'),
      device_id: this.navParams.get('device_id'),
      sb_id: this.navParams.get('sb_id')
    }).present();
  }

  openTV(tv_override?) {
    let tv;
    tv = tv_override ? tv_override : this.paired_tv[0] ? this.paired_tv[0] : null;
    if (tv) {
      if (this.paired_tv[0] && !this.edit_tv) {
        this.gotoTvPage(tv);
      }
      else {
        this.ds.postEditIrApp({
          id: this.ir_apppliance._id.split('_')[3] || this.ir_apppliance._id._id,
          "paired_appliance": {
            "app_type": "tv",
            "appliance": tv._id.split('_')[3] || tv._id
          }
        }, data => {
          this.gotoTvPage(tv);
          this.paired_tv[0] = tv;
          this.edit_tv = false;
        }, error => {
          console.log(error);
        })
      }
    }
    else {
      this.askForTV(null, true);
    }

  }
  gotoTvPage(tv) {
    this.navCtrl.push('RealRemotePage', {
      codeset: tv.remote_codeset,
      device_id: tv.paired_sb.device_id,
      sb_id: tv.paired_sb._id,
      type: tv.type,
      ir_appliance_id: tv._id.split('_')[3] || tv._id,
      remote_name: tv.name,
      comm_mode: 'online'
    });
  }
  edit_tv = false;
  changeTV() {
    this.edit_tv = true;
    this.askForTV(null, true);
  }

}
