import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RemotesListPage } from './remotes-list';

@NgModule({
    declarations: [
        RemotesListPage,
    ],
    imports: [
        IonicPageModule.forChild(RemotesListPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class RemotesListPageModule { }