import { Component } from '@angular/core';
import { NavController, NavParams, ModalController,IonicPage } from 'ionic-angular';

//pages
import { SelectKeysPage } from '../select-keys/select-keys';
import { CreateCustomRemotePage } from '../create-custom-remote/create-custom-remote';

//providers
import { AlertService } from '../../../providers/AlertService';

@IonicPage()
@Component({
  selector: 'page-remotes-list',
  templateUrl: 'remotes-list.html',
})
export class RemotesListPage {
  data; all_buttons = []; isEdit; isEditId;
  constructor(public navCtrl: NavController, public navParams: NavParams, private alert: AlertService, private modalCtrl: ModalController) {
    this.data = this.navParams.get('list');
    this.isEdit = this.navParams.get('isEdit');
    if (this.isEdit) {
      this.all_buttons = this.isEdit.buttons;
      this.isEditId = this.isEdit._id;
      for (let i of this.all_buttons) {
        i.irapp = i.irapp._id;
      }
    }
    console.log(this.data);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RemotesListPage');
  }
  goToKeysList(item) {
    console.log(item);
    if (item.type == 'Air Conditioner') {
      this.alert.showAlert('Sorry', 'Air Conditioner remotes are not supported!');
    }
    else {
      this.modalCtrl.create('SelectKeysPage', {
        remote: item,
        all_buttons: this.all_buttons
      }).present();
    }
  }
  goToNextStep() {
    this.navCtrl.push('CreateCustomRemotePage', {
      all_buttons: this.all_buttons,
      isEditId: this.isEditId,
      home_id: this.navParams.get('home_id'),
      custom_name_obj:this.isEdit
    })
  }

}
