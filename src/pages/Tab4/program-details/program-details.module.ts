import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProgramDetailsPage } from './program-details';
import { PipesModule } from '../../../pipes/pipes.module';
import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
    declarations: [
        ProgramDetailsPage,
    ],
    imports: [
        IonicPageModule.forChild(ProgramDetailsPage),
        PipesModule,
        Ionic2RatingModule
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class ProgramDetailsPageModule { }