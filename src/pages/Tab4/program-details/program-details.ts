import { Component } from '@angular/core';
import { NavController, NavParams, Platform, ViewController, IonicPage } from 'ionic-angular';
//import { Vibration } from 'ionic-native';

//providers
import { DataService } from '../../../providers/DataService';
import { Helper } from '../../../providers/Helper';

@IonicPage()
@Component({
  selector: 'page-program-details',
  templateUrl: 'program-details.html'
})
export class ProgramDetailsPage {
  show_loader;
  programid; upid; fav_details; mainprogramid; fav_progs; STBRemote; data_loaded = false; reminders; programInfo = { "cast": [] }; cast = []; director = { "realname": false }; hrs; channel_number; mins; fav; starttime; channel_num; watch_remind; provider_map; progress; flag; channel_meta; channel_name; channel_thumbnail;
  constructor(public navCtrl: NavController, public viewCtrl: ViewController, public navParams: NavParams, private platform: Platform, private ds: DataService, private help: Helper) {
    this.upid = this.navParams.get('upid');
    this.progress = this.navParams.get('duration');
    this.channel_meta = this.navParams.get('channel_meta');
    this.provider_map = this.navParams.get('provider_map');
    this.programid = this.navParams.get('programid');
    this.mainprogramid = this.navParams.get('mainprogramid');
    this.STBRemote = this.navParams.get('STBRemote');
    console.log(this.mainprogramid);
    this.fav_details = this.upid ? false : true;
    this.ds.getFavPrograms('basic',
      data => {
        console.log(data);
        data.indexOf(this.mainprogramid || this.programid) > -1 ? this.fav = 'favorite_active' : this.fav = 'favorite';
      },
      error => {
        console.log(error);
      }, false);
    this.reminders = this.navParams.get('reminders');
    //console.log(this.reminders);



  }
  programdetails;
  ionViewDidLoad() {
    console.log('ionViewDidLoad ProgramDetailsPage');
    this.ds.getProgramDetails(this.upid, this.programid, this.mainprogramid,
      (data) => {
        this.programdetails = data[0];
        this.data_loaded = true;
        this.programInfo = data[0].program;
        if (!this.navParams.get('channel_name')) {
          this.channel_name = this.channel_meta[data[0].channelid].channelname;
        }
        else {
          this.channel_name = this.navParams.get('channel_name');
        }
        this.channel_thumbnail = 'assets/img/channels/' + this.channel_name + '.png';
        if (!this.navParams.get('channel_number')) {
          this.channel_num = data[0].channelid;
          this.channel_number = this.help.channel_num_finder(this.channel_num, this.provider_map, this.channel_meta);
        }
        else {
          this.channel_number = this.navParams.get('channel_number')
        }
        console.log(this.channel_number);
        if (data[0].starttime > Date.now()) {
          this.watch_remind = 'remind';
        }
        else {
          this.watch_remind = 'watch_now';
        }
        if (this.reminders) {
          for (let i of this.reminders) {
            if (i.upid._id == data[0]._id) {
              this.watch_remind = 'unremind';
            }
          }
        }
        console.log(data);
        for (let i of this.programInfo.cast) {
          if (i.role == "cast") this.cast.push(i);
          if (i.role == "Director") this.director = i;

        }
        this.hrs = Math.floor(this.help.findHrsInterval(data[0].starttime, data[0].endtime));
        this.mins = this.help.findMinsInterval(data[0].starttime, data[0].endtime) % 60;
        this.starttime = new Date(data[0].starttime);
        let temp = new Date(Date.now());
        temp.setHours(23, 23, 59, 999);
        if (this.starttime < temp) {
          this.flag = 'Today';
        }
        else {
          this.flag = false;
        }
        console.log(this.starttime);
      },
      (error) => {
        console.log(error);
      })
  }
  dismiss(data) {
    this.viewCtrl.dismiss(data);
  }
  switchToChannel() {

    if (this.watch_remind == 'watch_now') {
      let remote_keys = this.STBRemote[0].keys[0];
      this.show_loader = true;
      this.ds.postSwitchToChannel(
        {
          "channelid": this.channel_number,
          "ir_device": this.navParams.get('sb_id'),
          "device": this.navParams.get('device_id'),
          "protocol": this.STBRemote[0].protocol,
          "len": this.STBRemote[0].len,
          "proto_name": this.STBRemote[0].proto_name,
          "wave_gap": this.STBRemote[0].wave_gap,
          "codes": this.help.getCodesForChannel(this.channel_number, remote_keys)
        },
        data => {
          //  if (this.platform.is('android')) Vibration.vibrate(30);
          this.show_loader = false;
          console.log(data);
        },
        error => {
          this.show_loader = true;
          console.log(error);
        }
      )
    }

    else if (this.watch_remind == 'remind') {
      console.log('later');
      this.setReminder(this.programdetails);
    }
    else if (this.watch_remind == 'unremind') {
      this.unsetReminder(this.programid);
    }
  }

  setFav() {
    this.show_loader = true;
    if (this.fav == 'favorite_active') {
      this.ds.postEditFavPrograms(
        {
          programid: this.mainprogramid || this.programid,
          action: 'remove'
        },
        data => {
          this.show_loader = false;
          this.fav = 'favorite';
        },
        error => {
          this.show_loader = false;
          console.log(error);
        }
      )
    }
    else if (this.fav == 'favorite') {
      this.ds.postEditFavPrograms(
        {
          programid: this.mainprogramid || this.programid,
          action: 'add'
        },
        data => {
          this.show_loader = false;
          this.fav = 'favorite_active';
        },
        error => {
          this.show_loader = false;
          console.log(error);
        }
      )
    }

  }
  setReminder(item) {
    console.log(item);
    let obj;
    obj = {
      "upid": item._id,
      "channelname": this.channel_name,
      "channelprogramid": item.id,
      "programname": item.program.mainprogramname || item.program.name,
      "starttime": item.starttime,
      "programid": item.programid,
      "ir_appliance_id": this.navParams.get('ir_appliance_id'),
      "ir_appliance_name": this.navParams.get('ir_appliance_name')
    };
    obj.mainprogramid = item.program.mainprogramid ? item.program.mainprogramid : null;
    !this.channel_number || this.channel_number == 'unknown' ? null : obj.channelno = this.channel_number;
    this.show_loader = true;
    this.ds.postSetReminder(obj, (data) => {
      this.show_loader = false;
      // this.currentActive = 'NONE';
      console.log(data);
      this.watch_remind = 'unremind';
      this.programdetails.channel_name = this.channel_name;
      this.reminders.push({ upid: this.programdetails });


    }, error => {
      this.show_loader = false;
      console.log(error);
    })
  }
  unsetReminder(programid) {
    this.show_loader = true;
    this.ds.postUnsetReminder({
      programid: programid
    }, data => {
      this.show_loader = false;
      if (this.starttime > Date.now()) {
        this.watch_remind = 'remind';
      }
      else {
        this.watch_remind = 'watch_now';
      }
      let j = 0;
      for (let i of this.reminders) {
        if (i.upid._id == this.programdetails._id) {
          this.reminders.splice(j, 1);
        }
        j++;
      }
    }, error => {
      this.show_loader = false;
      console.log(error);
    })
  }
}
