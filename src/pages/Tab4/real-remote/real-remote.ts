import { Component, EventEmitter } from '@angular/core';
import { NavController, NavParams, ViewController, PopoverController, ModalController, IonicPage } from 'ionic-angular';
import { trigger, transition, style, animate } from '@angular/animations';

//providers
import { PouchService } from '../../../providers/PouchService';
import { DataService } from '../../../providers/DataService';
import { AlertService } from '../../../providers/AlertService';
import { LocalStorageService } from '../../../providers/LocalStorageService';
import { SqliteService } from '../../../providers/SqliteService';
import { LoadingService } from '../../../providers/LoadingService';
import { Helper } from '../../../providers/Helper';
import { LocalCommService } from '../../../providers/LocalCommunicationServ';

//pages
import { EpgMainPage } from '../epg-main/epg-main';
import { ExtraButtonsPage } from '../extra-buttons/extra-buttons';
import { CreateTimerPage } from '../create-timer/create-timer';
import { NumPad2Page } from '../num-pad2/num-pad2';

@IonicPage()
@Component({
  selector: 'page-real-remote',
  templateUrl: 'real-remote.html',
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({ opacity: 0 }),
          animate('170ms', style({ opacity: 1 }))
        ]),
        transition(':leave', [
          style({ opacity: 1 }),
          animate('170ms', style({ opacity: 0 }))
        ])
      ]
    )
  ]
})
export class RealRemotePage {

  codeset; remote_keys = []; total_tuples = 200; data; kiot_internet; remote_keys1; progress = []; remote_name; devicesInfo; device_id; current_power; comm_mode; ir_appliance_id; irSignal: any; extra_buttons = []; ac_modes; basic_buttons; is_stb; sb_id; remote; remote_codeset;
  constructor(public navCtrl: NavController, private LocalComm: LocalCommService, public navParams: NavParams, private help: Helper, private modalCtrl: ModalController, private popCtrl: PopoverController, private load: LoadingService, private sql: SqliteService, private local: LocalStorageService, private alert: AlertService, private ps: PouchService, private ds: DataService) {
    this.kiot_internet = this.local.getInternetStatus();
    this.progress.push(0);
    this.devicesInfo = this.navParams.get('devicesInfo');
    this.comm_mode = this.navParams.get('comm_mode');
    this.current_ac_status = this.navParams.get('current_ac_status');
    this.codeset = this.navParams.get('codeset');
    this.device_id = this.navParams.get('device_id');
    this.sb_id = this.navParams.get('sb_id');
    this.ir_appliance_id = this.navParams.get('ir_appliance_id');
    this.irSignal = {
      "device": this.device_id,
      "switchboard": this.sb_id
    };
    this.current_power = false;
    this.remote = this.navParams.get('type');
    this.remote_name = this.navParams.get('remote_name');
    this.basic_buttons = {
      "TV": {
        'POWER': 1, 'INPUT AV': 1, 'MENU': 1,
        'BACK': 1, 'CHANNEL UP': 1, 'CHANNEL DOWN': 1,
        'VOLUME UP': 1, 'VOLUME DOWN': 1, 'ENTER': 1,
        'UP': 1, 'DOWN': 1, 'LEFT': 1, 'RIGHT': 1, 'MUTE': 1
      },
      "Projector": {
        'POWER': 1, 'POWER ON': 1, 'POWER OFF': 1, 'VOLUME UP': 1, 'VOLUME DOWN': 1,
        'UP': 1, 'DOWN': 1, 'LEFT': 1, 'RIGHT': 1, 'MUTE': 1, 'ENTER': 1
      },
      "AV Receiver": {
        'POWER': 1, 'POWER ON': 1, 'POWER OFF': 1, 'VOLUME UP': 1, 'VOLUME DOWN': 1,
        'UP': 1, 'DOWN': 1, 'LEFT': 1, 'RIGHT': 1, 'MUTE': 1, 'ENTER': 1
      },
      "Disc Player": {
        'POWER': 1, 'POWER ON': 1, 'POWER OFF': 1, 'STOP': 1, 'NEXT': 1, 'PREV': 1,
        'UP': 1, 'DOWN': 1, 'LEFT': 1, 'RIGHT': 1, 'MUTE': 1, 'ENTER': 1,
        'PLAY': 1, 'PAUSE': 1, 'REV': 1, 'FWD': 1, 'OPN/CLOSE': 1
      }
    };
  }


  getExtraButtons() {
    for (let i of this.remote_keys1) {
      if (this.basic_buttons[this.remote][i] || parseInt(i) || i == '0') continue;
      this.extra_buttons.push(i);
    }

  }
  temp_timer;
  volumeLongPress(str, key?) {
    if (str == 'start') {
      this.triggerIr(key);
      setTimeout(() => { }, 700);
      this.temp_timer = setInterval(() => {
        this.triggerIr(key);
      }, 700)
    }
    else if (str == 'end') {
      clearInterval(this.temp_timer);
    }
  }
  goToExtraButtons() {
    this.navCtrl.push('ExtraButtonsPage', {
      extra_buttons: this.remote_keys1,
      data: this.data,
      device_id: this.device_id,
      sb_id: this.sb_id,
      comm_mode: this.comm_mode,
      devicesInfo: this.devicesInfo
    });
  }
  showNumPad = false;
  openNumPad() {/*
    this.popCtrl.create('NumPopPage', {
      irSignal: {
        "device": this.device_id,
        "switchboard": this.sb_id
      },
      data: this.data
    }).present();*/
    this.modalCtrl.create('NumPad2Page', {
      data: this.data,
      remote_keys: this.remote_keys,
      comm_mode: this.comm_mode,
      irSignal: this.irSignal,
      device_id: this.device_id,
      devicesInfo: this.devicesInfo
    }).present();
    // this.showNumPad = !this.showNumPad;

  }
  ionViewDidLoad() {


    /* this.ps.initDB('KIOT');
     this.ps.getADoc('codeset',this.codeset+"22").then((data)=>
      {
        console.log(data);
        this.data = data;
        this.remote_keys = this.data[0].keys[0];
        this.remote_keys1 = Object.keys(this.remote_keys);
        console.log(this.remote_keys1);
       });*/
    this.ds.getRemoteFromDb(this.navParams.get('codeset'),
      data => {
        if (this.remote == 'Air Conditioner') {
          this.data = data;
          this.ac_remote_function();
        }
        else {
          this.data = data;
          this.is_stb = this.data[0].is_stb;
          console.log(this.data);
          this.remote_keys = this.data[0].keys[0];
          this.remote_keys1 = Object.keys(this.remote_keys);
          //this.getExtraButtons();
          console.log(this.remote_keys1);
        }
      }, error => {
        console.log(error);
        if (error.status == 404) {
          this.navCtrl.pop();
          this.alert.showAlert('Sorry', error.message);
        }
      });
  }
  triggerIr(i, ac_code?, ac?) {
    // console.log(this.remote_keys[i]);
    console.log(this.irSignal);
    switch (ac || this.data[0].type) {
      case 1: {
        this.irSignal.protocol = this.data[0].protocol;
        this.irSignal.code = this.remote_keys[i];
        this.irSignal.len = this.data[0].len;
        this.irSignal.proto_name = this.data[0].proto_name;
        this.irSignal.wave_gap = this.data[0].wave_gap;
        break;
      }
      case 2: {
        this.irSignal.code = this.remote_keys[i].code;
        this.irSignal.first_seq_len = this.remote_keys[i].first_seq_len;
        this.irSignal.sec_seq_len = this.remote_keys[i].sec_seq_len;
        this.irSignal.len = parseInt(this.remote_keys[i].first_seq_len) + parseInt(this.remote_keys[i].sec_seq_len);
        this.irSignal.freq = this.data[0].freq;
        this.irSignal.rpt_cnt = this.data[0].rpt_cnt;
        break;
      }
      case 'ac': {
        this.irSignal.code = ac_code.code;
        this.irSignal.first_seq_len = ac_code.first_seq_len;
        this.irSignal.sec_seq_len = ac_code.sec_seq_len;
        this.irSignal.len = parseInt(ac_code.first_seq_len) + parseInt(ac_code.sec_seq_len);
        this.irSignal.freq = this.data[0].freq;
        this.irSignal.rpt_cnt = this.data[0].rpt_cnt;
        this.irSignal.device = this.device_id;
        this.irSignal.switchboard = this.sb_id;
        break;
      }
    }

    console.log(this.irSignal);
    if (this.comm_mode == 'online') {
      return new Promise((resolve, reject) => {
        this.ds.postTriggerIr(this.irSignal,
          (data) => {
            console.log(data);
            if (this.remote == 'Air Conditioner') {
              this.setAcStatus(ac_code);
            }
            if (this.remote_keys['POWER ON'] && (i == 'POWER ON' || i == 'POWER OFF')) {
              this.current_power = !this.current_power;
            }
            resolve(1);
          },
          (error) => {
            console.log(error);
            reject(error);
          });

      });
    }
    else if (this.comm_mode == 'offline') {
      return new Promise((resolve, reject) => {
        this.LocalComm.sendRemoteSignal(this.devicesInfo[this.device_id].wifi_local_name, this.devicesInfo[this.device_id].apikey, this.help.getIrOfflineObject(this.irSignal, ac || this.data[0].type),this.device_id)
          .subscribe(data => {
            if (this.remote == 'Air Conditioner') {
              this.setAcStatus(ac_code);
            }
            if (this.remote_keys['POWER ON'] && (i == 'POWER ON' || i == 'POWER OFF')) {
              this.current_power = !this.current_power;
            }
            resolve(data);
          }, error => {
            reject(error);
          });
      });
    }

  }
  test(event) {
    console.log(event);
    return this.codeset;
  }
  goToEpgPage() {
    this.ds.getChannelGroupData(
      data => {
        // console.log("I think i am all done safely");
        // this.alert.showAlert("Reached view Enter",JSON.stringify(data));
        this.navCtrl.push('EpgMainPage', {
          channelGroupData: data
        });
      },
      error => {
        // this.alert.showAlert("Reached view Enter",JSON.stringify(error));
        //console.log("Opps mann.. check it");
        console.log(error);
      });

  }








  //THIS IS FOR AC AIR CONDITIONER____________________________________________________________________________
  current_ac_status; wait_flag; ac_ir_code; timer_data = []; time_left = [];
  ac_remote_function() {
    let status = this.local.getCurrentAcStatus(this.ir_appliance_id);
    let default_status = {
      temp_set: 18,
      fan_set: 'FAN_AUTO',
      power_set: 'ON',
      swing_set: 'ON',
      mode_set: 'COOL'
    };
    this.current_ac_status = status ? status : default_status;
    this.local.setCurrentAcStatus(this.ir_appliance_id, this.current_ac_status);
    this.wait_flag = true;
    // this.load.showLoading('Downloading Remote..');
    if (this.local.getInternetStatus() == 'online') {
      this.ds.getAcTimer(this.ir_appliance_id,
        data => {
          console.log(data);
          this.timer_data = data;
          this.setAcTimer();
        },
        error => {
          console.log(error);
        });
    }
    this.sql.openDb('kiot_ac.db').then((data) => {

      console.log(this.sql);
      this.sql.moveRemoteToDb()
        .then(data => {
          //  this.load.showLoading('Downloading Remote..');
          console.log(data);
          this.total_tuples = this.data[0].keys.length;
          this.progress = [];
          this.progress.push(0);
          this.sql.insertDataInTable(this.ir_appliance_id, this.data, this.codeset, this.progress)
            .then(data => {
              console.log('success');
              //   this.load.dismiss();
              this.getAcModes()
                .then(() => {
                  this.nextCodes();
                  this.getPowerCodes();
                }).catch(error => console.log(error));
              this.wait_flag = false;
            }).catch(error => {
              console.log(error);
              //     this.load.dismiss();
            });
        })
        .catch(
        error => {
          this.load.dismiss();
          console.log(error);
        }
        )
    }).catch(
      error => {
        //  this.load.dismiss();
        console.log(error);
      }
      )


  }


  fan_speeds = ["AUTO", "LOW", "MID", "HIGH"];
  temp_up_next;
  temp_down_next;
  mode_next;
  power_next;
  fan_spd_next;

  triggerAcIr(button) {
    switch (button) {
      case 'TEMP UP': {
        this.triggerIr(null, this.temp_up_next, 'ac').then(
          () => {
            this.nextCodes();
          }
        ).catch(error => console.log(error));
        break;
      }
      case 'TEMP DOWN': {
        this.triggerIr(null, this.temp_down_next, 'ac')
          .then(() => {
            this.nextCodes();
          }).catch(error => console.log(error));

        break;
      }
      case 'MODE': {
        this.triggerIr(null, this.mode_next, 'ac')
          .then(() => {
            this.nextCodes();
          }).catch(error => console.log(error));
        break;
      }
      case 'POWER': {
        this.triggerIr(null, this.power_next, 'ac')
          .then(() => {
            this.nextCodes();
          }).catch(error => console.log(error));
        break;
      }
      case 'FAN_SPD': {
        this.triggerIr(null, this.fan_spd_next, 'ac')
          .then(() => {
            this.nextCodes();
          }).catch(error => console.log(error));
        break;
      }

    }

  }

  nextCodes() {
    //  if(!this.wait_flag){
    console.log(this.current_ac_status);

    let query = ' WHERE func=? AND temp_set=? AND codeset=? AND power_set=? AND swing_set=? AND mode_set=?';
    let params = ['TEMP UP', (parseInt(this.current_ac_status.temp_set) + 1).toString(), this.codeset, this.current_ac_status.power_set, this.current_ac_status.swing_set, this.current_ac_status.mode_set];
    this.getATuple(query, params).then(data => this.temp_up_next = data);


    query = ' WHERE func=? AND temp_set=? AND codeset=? AND power_set=? AND swing_set=? AND mode_set=?';
    params = ['TEMP DOWN', (parseInt(this.current_ac_status.temp_set) - 1).toString(), this.codeset, this.current_ac_status.power_set, this.current_ac_status.swing_set, this.current_ac_status.mode_set];
    this.getATuple(query, params).then(data => this.temp_down_next = data);

    let nextMode = this.getNextMode();
    query = ' WHERE func=? AND mode_set=? AND codeset=? AND power_set=?';
    params = ['MODE ' + nextMode, nextMode, this.codeset, this.current_ac_status.power_set];
    this.getATuple(query, params).then(data => this.mode_next = data);

    query = this.current_ac_status.power_set == 'ON' ? ' WHERE func=? AND codeset=?' : ' WHERE func=? AND codeset=? AND mode_set=?';
    params = this.current_ac_status.power_set == 'ON' ? ['POWER OFF', this.codeset] : ['POWER ON', this.codeset, 'COOL'];
    this.getATuple(query, params).then(data => this.power_next = data);


    let speed = this.fan_speeds[this.fan_speeds.indexOf(this.current_ac_status.fan_set.slice(4, this.current_ac_status.fan_set.length)) + 1];
    speed = speed ? speed : this.fan_speeds[0];
    let speed2 = speed;
    speed2 = speed2 == 'HIGH' ? 'HI' : speed2;
    query = '  WHERE func=? AND temp_set=? AND codeset=? AND fan_set=? AND power_set=? AND swing_set=? AND mode_set=?';
    params = ['FAN SPD ' + speed, (parseInt(this.current_ac_status.temp_set)).toString(), this.codeset, 'FAN_' + speed2, this.current_ac_status.power_set, this.current_ac_status.swing_set, this.current_ac_status.mode_set];
    this.getATuple(query, params).then(data => this.fan_spd_next = data);

    console.log(this.temp_up_next, this.temp_down_next, this.mode_next, this.fan_spd_next, this.power_next);
  }
  setAcStatus(sent_signal) {
    this.current_ac_status = {
      temp_set: parseInt(sent_signal.temp_set),
      fan_set: sent_signal.fan_set,
      power_set: sent_signal.power_set,
      swing_set: sent_signal.swing_set,
      mode_set: sent_signal.mode_set
    }
    console.log(sent_signal);
    console.log(this.current_ac_status);
    this.local.setCurrentAcStatus(this.ir_appliance_id, this.current_ac_status);
  }
  getATuple(query, params, all_tuples?) {
    return new Promise((resolve, reject) => {
      this.sql.getATuple('AC_REMOTE', query, params)
        .then(data => {
          this.ac_ir_code = data;
          if (this.ac_ir_code.rows.length > 0) {
            console.log(data);
            return all_tuples ? resolve(this.ac_ir_code) : resolve(this.ac_ir_code.rows.item(0));
            // this.triggerIr(null, this.ac_ir_code.rows[0], 'ac');
          }
          else {
            console.log(this.ac_ir_code)
            resolve(false);
          }
        }).catch(err => console.log(err));
    });
  }
  temp;
  getAcModes() {
    let query = " WHERE func LIKE 'MODE%' AND codeset=?";
    let params = [this.codeset];
    this.ac_modes = [];
    return new Promise((resolve, reject) => {
      this.sql.getModes('AC_REMOTE', query, params)
        .then(data => {
          console.log(data);
          this.temp = data;

          for (let j = 0; j < this.temp.rows.length; j++) {
            let i = this.temp.rows.item(j)['func'];
            this.ac_modes.push(i.toString().slice(5, i.toString().length));
          }
          //  this.ac_modes.push("COOL");
          // this.ac_modes.push("AUTO");
          console.log(this.ac_modes);

          resolve(1);

        }).catch(err => {
          reject(err);
        })

    })

  }
  getNextMode() {
    let next = this.ac_modes[this.ac_modes.indexOf(this.current_ac_status.mode_set) + 1];
    next = next ? next : this.ac_modes[0];
    // this.current_ac_status.mode_set = next;
    return next;
  }
  power_codes = [];
  getPowerCodes() {
    let query = ' WHERE func=? AND codeset=?';
    let params = ['POWER OFF', this.codeset]; let params2 = ['POWER ON', this.codeset];
    this.getATuple(query, params).then(data => this.power_codes.push(data));
    this.getATuple(query, params2, true).then((data: any) => {
      this.power_codes.push(data.rows.item(0));
      this.power_codes.push(data.rows.item(1));
      this.power_codes.push(data.rows.item(2));
    });
    console.log(this.power_codes);
  }
  goToCreateTimer() {

    let x = this.modalCtrl.create('CreateTimerPage',
      {
        ac_remote_data: this.data,
        sb_id: this.sb_id,
        device_id: this.device_id,
        power_codes: this.power_codes,
        modes: this.ac_modes,
        ir_appliance_id: this.ir_appliance_id,
        timer_data: this.timer_data,
        time_left: this.time_left
      });
    x.onDidDismiss(() => {
      this.setAcTimer();
    })
    x.present();


  }
  deleteTimer() {
    this.alert.showConfirm('Delete Timer', 'This cancels the timer and its action', 'No', 'Delete',
      () => { },
      () => {
        this.ds.postDeleteTimer({
          scheduleId: this.timer_data[0]._id
        }, data => {
          this.timer_data.splice(0);
          console.log(data);
        }, error => {
          console.log(error);
        });
      });
  }
  timer_interval = []; clear_timer = true;
  setAcTimer() {
    this.clear_timer = true;
    if (this.timer_data[0]) {
      let starttime = new Date();
      starttime = this.timer_data[0].createdTime;
      let endtime = new Date(starttime);
      endtime.setHours(endtime.getHours() + parseInt(this.timer_data[0].hour));
      endtime.setMinutes(endtime.getMinutes() + parseInt(this.timer_data[0].mins));

      this.time_left[0] = parseInt(this.help.findHrsInterval(Date.now(), endtime));
      this.time_left[1] = Math.ceil(this.help.findMinsInterval(Date.now(), endtime)) % 60;
      if (this.time_left[0] != 0 || this.time_left[1] != 0) {
        this.clear_timer = false;
      }
      if (this.time_left[0] > 0) {
        this.timer_interval[0] = setInterval(() => {
          this.time_left[0]--;
          if (this.time_left[0] == -1) {
            this.clear_timer = true;
          }
        }, 3600000);
      }
      this.timer_interval[1] = setInterval(() => {
        this.time_left[1]--;
        if (this.time_left[1] == 0 && this.time_left[0] == 0) {
          this.clear_timer = true;
          this.clearTimerIntervals();
        } else if (this.time_left[1] == 0) {
          this.time_left[1] = 59;
        }
      }, 60000);
    }
  }

  ionViewWillLeave() {
    this.clearTimerIntervals();
  }

  clearTimerIntervals() {
    clearInterval(this.timer_interval[0]);
    clearInterval(this.timer_interval[1]);
  }

}






@Component({
  template: `
<ion-grid no-padding>
    <ion-row>
      <ion-col col-11 text-center no-padding>
        <ion-row>
          <ion-col width-33 text-center no-padding>
            <button class="btn_calc" ion-button (click)="triggerIr('1')">1</button>
          </ion-col>
          <ion-col width-33 text-center no-padding>
            <button class="btn_calc" ion-button (click)="triggerIr('2')">2</button>
          </ion-col>
          <ion-col width-33 text-center no-padding>
            <button class="btn_calc" ion-button (click)="triggerIr('3')">3</button>
          </ion-col>
        </ion-row> 
        <ion-row>
          <ion-col width-33 text-center no-padding>
            <button class="btn_calc" ion-button (click)="triggerIr('4')">4</button>
          </ion-col>
          <ion-col width-33 text-center no-padding>
            <button class="btn_calc" ion-button (click)="triggerIr('5')">5</button>
          </ion-col>
          <ion-col width-33 text-center no-padding>
            <button class="btn_calc" ion-button (click)="triggerIr('6')">6</button>
          </ion-col>
        </ion-row>
        <ion-row>
          <ion-col width-33 text-center no-padding>
            <button class="btn_calc" ion-button (click)="triggerIr('7')">7</button>
          </ion-col>
          <ion-col width-33 text-center no-padding>
            <button class="btn_calc" ion-button (click)="triggerIr('8')">8</button>
          </ion-col>
          <ion-col width-33 text-center no-padding>
            <button class="btn_calc" ion-button (click)="triggerIr('9')">9</button>
          </ion-col>
        </ion-row> 
        <ion-row>
          <ion-col width-33 text-center no-padding>
            <button text-center class="btn_calc" ion-button (click)="switchToChannel()">
             <!--ion-icon name="close"></ion-icon-->
             Go
            </button>
          </ion-col>
          <ion-col width-33 text-center no-padding>
            <button class="btn_calc" ion-button (click)="triggerIr('0')" >0</button>
          </ion-col>
          <ion-col width-33 text-center no-padding>
            <button class="btn_calc" ion-button (click)="backspaceFunc()">
             <ion-icon name="backspace"></ion-icon>
            </button>
          </ion-col>
        </ion-row>
      </ion-col>
      <ion-col col-1 text-center no-padding class="bg-gold"> &nbsp;
      </ion-col>
    </ion-row>
  </ion-grid>
  `,
  selector: 'page-num-pad1',
  styles: []
})
export class NumPopPage {
  irSignal; data; remote_keys;
  constructor(public viewCtrl: ViewController, private params: NavParams, private ds: DataService) {
    console.log('YO');
    this.irSignal = this.params.get('irSignal');
    this.data = this.params.get('data');
    this.remote_keys = this.data[0].keys[0];
  }

  close(out) {
    this.viewCtrl.dismiss();
  }
  triggerIr(i) {
    // console.log(this.remote_keys[i]);
    console.log(this.irSignal);
    switch (this.data[0].type) {
      case 1: {
        this.irSignal.protocol = this.data[0].protocol;
        this.irSignal.code = this.remote_keys[i];
        this.irSignal.len = this.data[0].len;
        this.irSignal.proto_name = this.data[0].proto_name;
        this.irSignal.wave_gap = this.data[0].wave_gap;
        break;
      }
      case 2: {
        this.irSignal.code = this.remote_keys[i].code;
        this.irSignal.first_seq_len = this.remote_keys[i].first_seq_len;
        this.irSignal.sec_seq_len = this.remote_keys[i].sec_seq_len;
        this.irSignal.len = parseInt(this.remote_keys[i].first_seq_len) + parseInt(this.remote_keys[i].sec_seq_len);
        this.irSignal.freq = this.data[0].freq;
        this.irSignal.rpt_cnt = this.data[0].rpt_cnt;
        break;
      }
    }

    this.ds.postTriggerIr(this.irSignal,
      (data) => {
        console.log(data);
      },
      (error) => {
        console.log(error);
      });

  }
}



