import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RealRemotePage } from './real-remote';
import { PipesModule } from '../../../pipes/pipes.module';

@NgModule({
    declarations: [
        RealRemotePage,
    ],
    imports: [
        IonicPageModule.forChild(RealRemotePage),
        PipesModule
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class RealRemotePageModule { }