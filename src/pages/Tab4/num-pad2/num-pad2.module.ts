import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NumPad2Page } from './num-pad2';

@NgModule({
    declarations: [
        NumPad2Page,
    ],
    imports: [
        IonicPageModule.forChild(NumPad2Page),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class NumPad2PageModule { }