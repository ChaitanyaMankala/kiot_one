import { Component } from '@angular/core';
import { NavController, NavParams, ViewController,IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../providers/DataService';
import { LocalStorageService } from '../../../providers/LocalStorageService';
import { Helper } from '../../../providers/Helper';
import { LocalCommService } from '../../../providers/LocalCommunicationServ';

@IonicPage()
@Component({
  selector: 'page-num-pad2',
  templateUrl: 'num-pad2.html',
})
export class NumPad2Page {
  data;
  remote_keys;
  comm_mode;
  irSignal;
  devicesInfo;
  device_id;
  constructor(public navCtrl: NavController,private viewCtrl:ViewController, private LocalComm: LocalCommService, public navParams: NavParams, private help: Helper, private local: LocalStorageService, private ds: DataService) {
    this.data = this.navParams.get('data');
    this.remote_keys = this.navParams.get('remote_keys');
    this.comm_mode = this.navParams.get('comm_mode');
    this.irSignal = this.navParams.get('irSignal');
    this.device_id = this.navParams.get('device_id');
    this.devicesInfo = this.navParams.get('devicesInfo');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NumPad2Page');
  }
  dismiss(data) {
    this.viewCtrl.dismiss(data);
  }


  triggerIr(i, ac_code?, ac?) {
    // console.log(this.remote_keys[i]);
    console.log(this.irSignal);
    switch (this.data[0].type) {
      case 1: {
        this.irSignal.protocol = this.data[0].protocol;
        this.irSignal.code = this.remote_keys[i];
        this.irSignal.len = this.data[0].len;
        this.irSignal.proto_name = this.data[0].proto_name;
        this.irSignal.wave_gap = this.data[0].wave_gap;
        break;
      }
      case 2: {
        this.irSignal.code = this.remote_keys[i].code;
        this.irSignal.first_seq_len = this.remote_keys[i].first_seq_len;
        this.irSignal.sec_seq_len = this.remote_keys[i].sec_seq_len;
        this.irSignal.len = parseInt(this.remote_keys[i].first_seq_len) + parseInt(this.remote_keys[i].sec_seq_len);
        this.irSignal.freq = this.data[0].freq;
        this.irSignal.rpt_cnt = this.data[0].rpt_cnt;
        break;
      }

    }

    console.log(this.irSignal);
    if (this.comm_mode == 'online') {
      return new Promise((resolve, reject) => {
        this.ds.postTriggerIr(this.irSignal,
          (data) => {
            console.log(data);

            resolve(1);
          },
          (error) => {
            console.log(error);
            reject(error);
          });

      });
    }
    else if (this.comm_mode == 'offline') {
      return new Promise((resolve, reject) => {
        this.LocalComm.sendRemoteSignal(this.devicesInfo[this.device_id].wifi_local_name, this.devicesInfo[this.device_id].apikey, this.help.getIrOfflineObject(this.irSignal, ac || this.data[0].type),this.device_id)
          .subscribe(data => {
            resolve(data);
          }, error => {
            reject(error);
          });
      });
    }

  }




}
