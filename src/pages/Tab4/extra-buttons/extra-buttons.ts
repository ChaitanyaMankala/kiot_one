import { Component } from '@angular/core';
import { NavController, NavParams,IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../providers/DataService';
import { Helper } from '../../../providers/Helper';
import { LocalCommService } from '../../../providers/LocalCommunicationServ';

@IonicPage()
@Component({
  selector: 'page-extra-buttons',
  templateUrl: 'extra-buttons.html',
})
export class ExtraButtonsPage {
  extra_buttons; remote_keys; irSignal; device_id; sb_id; data;comm_mode;devicesInfo;
  constructor(public navCtrl: NavController,private help:Helper,private LocalComm:LocalCommService, public navParams: NavParams, private ds: DataService) {
    this.devicesInfo = this.navParams.get('devicesInfo');
    this.comm_mode = this.navParams.get('comm_mode');
    this.data = this.navParams.get('data');
    this.extra_buttons = this.navParams.get('extra_buttons');
    this.extra_buttons2 = this.extra_buttons;
    this.remote_keys = this.data[0].keys[0];
    this.device_id = this.navParams.get('device_id');
    this.sb_id = this.navParams.get('sb_id');
    console.log(this.extra_buttons);
    console.log(this.remote_keys);
    this.irSignal = {
      "device": this.device_id,
      "switchboard": this.sb_id

    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ExtraButtons');
  }
  triggerIr(i, ac_code?, ac?) {
    // console.log(this.remote_keys[i]);
    console.log(this.irSignal);
    switch (ac || this.data[0].type) {
      case 1: {
        this.irSignal.protocol = this.data[0].protocol;
        this.irSignal.code = this.remote_keys[i];
        this.irSignal.len = this.data[0].len;
        this.irSignal.proto_name = this.data[0].proto_name;
        this.irSignal.wave_gap = this.data[0].wave_gap;
        break;
      }
      case 2: {
        this.irSignal.code = this.remote_keys[i].code;
        this.irSignal.first_seq_len = this.remote_keys[i].first_seq_len;
        this.irSignal.sec_seq_len = this.remote_keys[i].sec_seq_len;
        this.irSignal.len = parseInt(this.remote_keys[i].first_seq_len) + parseInt(this.remote_keys[i].sec_seq_len);
        this.irSignal.freq = this.data[0].freq;
        this.irSignal.rpt_cnt = this.data[0].rpt_cnt;
        break;
      }
      case 'ac': {
        this.irSignal = ac_code;
        this.irSignal.device = this.device_id;
        this.irSignal.switchboard = this.sb_id;
        break;
      }
    }
    if (this.comm_mode == 'online') {
        this.ds.postTriggerIr(this.irSignal,
          (data) => {
            console.log(data);
          },
          (error) => {
            console.log(error);
          });
    }
    else if (this.comm_mode == 'offline') {
        this.LocalComm.sendRemoteSignal(this.devicesInfo[this.device_id].wifi_local_name, this.devicesInfo[this.device_id].apikey, this.help.getIrOfflineObject(this.irSignal, ac || this.data[0].type),this.device_id)
          .subscribe(data => {
            console.log(data);
          }, error => {
              console.log(error);
          });
    }

  }
  extra_buttons2;
  filterItems(ev: any) {

    this.extra_buttons2 = this.extra_buttons;
    let val = ev.target.value;
    let result_data = this.extra_buttons2.slice();
    if (val && val.trim() != '') {
      this.extra_buttons2 = result_data.filter((item: any) => {
        return (item.toLowerCase().indexOf(val.toLowerCase())>-1);
      })
    }
    else {
      this.extra_buttons2 = result_data;
    }

  }
}
