import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RemotePage } from './remote.page';

@NgModule({
    declarations: [
        RemotePage,
    ],
    imports: [
        IonicPageModule.forChild(RemotePage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class RemotePageModule { }