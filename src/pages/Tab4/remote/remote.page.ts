import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ModalController,IonicPage } from 'ionic-angular';
//import { Vibration } from '@ionic-native/vibration';
import {
  MqttMessage,
  MqttModule,
  MqttService
} from 'ngx-mqtt';
//providers
import { DataService } from '../../../providers/DataService';
import { LocalStorageService } from '../../../providers/LocalStorageService';
import { ToastService } from '../../../providers/ToastService';
import { LocalCommService } from "../../../providers/LocalCommunicationServ";
import { AlertService } from '../../../providers/AlertService';
import { LoadingService } from '../../../providers/LoadingService';

//pages
import { RealRemotePage } from '../real-remote/real-remote';
import { EpgMainPage } from '../epg-main/epg-main';
import { NotificationDispPage } from '../../Tab1/notification-disp/notification-disp';
import { RemotesListPage } from '../remotes-list/remotes-list';
import { CustomRemotePage } from '../custom-remote/custom-remote';
import { CreateIrAppPage } from '../../Tab5/6irAppliances/create-ir-app//create-ir-app';

@IonicPage()
@Component({
  selector: 'page-remote',
  templateUrl: 'remote.page.html'
})
export class RemotePage {

  constructor(public navCtrl: NavController, private _mqttService: MqttService, private load: LoadingService, private modalCtrl: ModalController, private alert: AlertService, private LocalComm: LocalCommService, private toast: ToastService, private alertCtrl: AlertController, public navParams: NavParams, private ds: DataService, private local: LocalStorageService) {
    this.dont_show_loader = false
  }
  data = []; TVData = []; custom_remote_name; kiot_internet; custom_remote_id; observableArray = []; dev_avail = {}; devicesInfo; on_off_string; dont_show_loader;
  ionViewWillEnter(event?, goto_remote?) {
    this.custom_remote_name = null;
    this.kiot_internet = this.local.getInternetStatus();
    if (this.kiot_internet == 'online') {
      this.ds.getUnseenNotifications(
        data => {
          this.num_of_notifications = data;
          console.log(data);
        }, error => {
          console.log(error);
        });
    }
    if (this.local.getCurrentHome()) {
      this.TVData = [];
      this.dont_show_loader = this.data[0] ? false : true;
      this.ds.getIrAppliances(this.local.getCurrentHome(),
        (data) => {


          event ? event.complete() : null;
          console.log(data);
          this.data = data.appliances;
          let devicesList = [];
          this.devicesInfo = data.devices;
          for (let i of this.data) {
            if (i.paired_sb) {
              devicesList.push(i.paired_sb.device_id);
              if (!this.devicesInfo[i.paired_sb.device_id]) {
                this.devicesInfo[i.paired_sb.device_id] = {};
              }
              if (this.devicesInfo[i.paired_sb.device_id]) {
                this.devicesInfo[i.paired_sb.device_id].done = false;
              }
            }
          }
          let obj = {
            home: this.local.getCurrentHome(),
            devices: devicesList
          };
          this.TVData = [];
          for (let i of this.data) {
            let buffer = {};
            i.icon_name = i.type.toString().replace(/\s/g, "-");
            if (i.paired_sb) {
              if (i.type == 'TV') {
                i.device_id = i.paired_sb.device_id,
                  i.sb_id = i.paired_sb._id,
                  i.room_name = i.room_id.name,
                  this.TVData.push(i);
              }
              if (!this.dev_avail[i.paired_sb._id]) this.dev_avail[i.paired_sb._id] = {};

              if (this.devicesInfo[i.paired_sb.device_id] && !this.devicesInfo[i.paired_sb.device_id].done) {
                this.devicesInfo[i.paired_sb.device_id].done = true;
                let pubTpoic = '/KU/' + i.paired_sb.device_id + '/' + this.local.getCurrentHome() + '/ping/set';
                console.log(pubTpoic);
                this._mqttService.unsafePublish(pubTpoic, 'a');
                this.observableArray.push(this._mqttService.observe('/KU/' + i.paired_sb.device_id + '/' + this.local.getCurrentHome() + '/ping/get')
                  .subscribe((data: MqttMessage) => {
                    console.log(i.paired_sb.device_id);
                    this.dev_avail[i.paired_sb._id].on_available = true;
                    console.log(JSON.parse(data.payload.toString()));
                  }));
                if (this.local.getLocalIp(this.devicesInfo[i.paired_sb.device_id].wifi_local_name)) {
                  this.local.setSecret(i.paired_sb.device_id, this.devicesInfo[i.paired_sb.device_id].apiSecret);
                  this.LocalComm.pingDevice('http://', this.local.getLocalIp(this.devicesInfo[i.paired_sb.device_id].wifi_local_name) + "/api/switch?apikey=" + this.devicesInfo[i.paired_sb.device_id].apikey, i.paired_sb.device_id)
                    .subscribe((data11: any) => {
                      this.dev_avail[i.paired_sb._id].off_available = true;
                    }, error => {
                      console.log(error);
                      this.dev_avail[i.paired_sb._id].off_available = false;
                    });
                }
                else {
                  this.dev_avail[i.paired_sb._id].off_available = false;
                }
              }

            }
          }
          console.log(this.dev_avail);
          if (goto_remote) {
            goto_remote.id = goto_remote._id.split('_')[0] == 'remote' ? goto_remote._id.split('_')[3] : goto_remote._id;
            for (let i of this.data) {
              if (goto_remote._id == i._id) {
                this.goToRemote(i);
              }
            }
          }
        },
        (error) => {
          console.log(error);
        }, this.dont_show_loader, true);

      this.ds.getCustomRemote(this.local.getCurrentHome(),
        data1 => {
          console.log(data1);
          if (data1[0]) {
            this.custom_remote_name = data1[0].name;
            this.custom_remote_id = data1[0]._id;
          }
          else {
            this.custom_remote_name = -1;
          }
        }, error => {
          console.log(error);
        }, false);
    }
  }
  ionViewDidLeave() {
    for (let i of this.observableArray) {
      i.unsubscribe();
    }
  }
  retry_loading = false;
  retry() {
    this.retry_loading = true;
    this.ds.ping()
      .then(data => {
        console.log(data);
        this.kiot_internet = this.local.getInternetStatus();
        this.retry_loading = false;
        if (this.kiot_internet == 'online') {
          this.ionViewWillEnter();
          this.toast.bottomToast_Short('kiot is back Online!');
        }
      });
  }

  goToRemote(i, ind?) {
    console.log(i);
    if (this.show_loading_epg == -1) {
      this.on_off_string = "";
      this.on_off_string = this.local.getInternetStatus() == 'online' ? '1' : this.local.getInternetStatus() == 'offline' ? '0' : '-1';
      if (i.paired_sb) {
        this.on_off_string += this.dev_avail[i.paired_sb._id].on_available ? '1' : '0';
        this.on_off_string += this.dev_avail[i.paired_sb._id].off_available ? '1' : '0';
      }
      else {
        this.on_off_string += '10';
      }
      switch (this.on_off_string) {
        case '111': {
          this.goToRealRemote(i, 'online', ind);
          break;
        }
        case '110': {
          this.goToRealRemote(i, 'online', ind);
          break;
        }
        case '101': {
          this.goToRealRemote(i, 'offline');
          break;
        }
        case '011': {
          this.goToRealRemote(i, 'offline');
          break;
        }
        case '001': {
          this.goToRealRemote(i, 'offline');
          break;
        }
        case '000': {
          this.alert.showAlert('Unable to Communicate', 'We cannot communicate with the KIOT Switchboard');
          //this.goToRealRemote(i, 'offline');
          break;
        }
        case '100': {
          this.alert.showAlert('Unable to Communicate', 'We cannot communicate with the KIOT Switchboard');
          //this.goToRealRemote(i, 'offline');
          break;
        }
        default: {
          console.log(this.on_off_string);
          break;
        }
      }

    }
  }
  num_of_notifications;
  goToNotifications() {
    let noti_modal = this.modalCtrl.create('NotificationDispPage');
    noti_modal.present();
    noti_modal.onWillDismiss((data) => {
      if (data) {
        this.num_of_notifications = 0;
      }
    });

  }
  addCustomRemote() {
    this.navCtrl.push('RemotesListPage', {
      list: this.data,
      home_id: this.local.getCurrentHome()
    });
  }
  goToCustomRemote() {
    if (this.custom_remote_name && this.custom_remote_name != -1) {
      this.navCtrl.push('CustomRemotePage', {
        remote_id: this.custom_remote_id,
        list: this.data
      });
    }
    else {
      this.addCustomRemote();
    }
  }

  goToRealRemote(i, comm_mode, ind?) {

    if (i.paired_sb == 'null' || i.paired_sb == 'undefined' || i.paired_sb == null) {

      this.doRadio(i);
    }
    else {
      if (i.type == 'Set Top Box' && this.kiot_internet == 'online') {
        this.goToEpgPage(i, this.TVData, comm_mode, ind);
      }
      else {
        let obj;
        obj = {
          codeset: i.remote_codeset,
          device_id: i.paired_sb.device_id,
          sb_id: i.paired_sb._id,
          type: i.type,
          ir_appliance_id: i._id.split('_')[3] || i._id,
          remote_name: i.name,
          comm_mode: comm_mode,
          devicesInfo: this.devicesInfo
        };
        i.type == 'Air Conditioner' ? obj.current_ac_status = this.local.getCurrentAcStatus(i._id) : null;
        this.navCtrl.push('RealRemotePage', obj);
      }
    }
  }


  data2;
  public doRadio(i) {
    let alert = this.alertCtrl.create();
    // alert = this.alertCtrl.create();
    alert.setTitle('Select Paired Device to continue');
    this.ds.getSboards(this.local.getCurrentHome(),
      (data) => {
        this.data2 = data;
        for (let i of this.data2) {
          for (let j of i.devices) {
            alert.addInput({
              type: 'radio', label: j.sboard_name + ' in ' + i.room_name,
              value: JSON.stringify(
                {
                  "sb": j._id,
                  "room": i.room_id
                }
              )
            });
          }
        }

        alert.addButton('Cancel');
        alert.addButton({
          text: 'Select',
          handler: data => {
            alert.dismiss();
            i.paired_sb = [];
            i.paired_sb.device_id = data._id;
            i.paired_sb._id = JSON.parse(data).sb;
            let obj = {
              "id": i._id.split('_')[0] == 'remote' ? i._id.split('_')[3] : i._id,
              "room_id": JSON.parse(data).room,
              "paired_sb": JSON.parse(data).sb
            };
            this.ds.postEditIrApp(obj,
              (data) => {
                /*  this.checkAvailability(i)
                  .then((data)=>{
                    this.goToRemote(i);
                  });*/
                this.data = [];
                this.ionViewWillEnter(null, i);
                //this.goToRemote(i);

              },
              (error) => {
                console.log(error);
              })

          }
        });

        alert.present();
      },
      (error) => {
        console.log(error);
      }, false, 'room,swappliance,kiotdevice')

  }
  /*checkAvailability(i) {
    return new Promise((resolve, reject) => {
      this.ds.getDeviceAvailableOnline(data => {
        this.dev_avail[i.paired_sb._id].on_available = data.status;
        this.LocalComm.pingDevice('http://', this.local.getLocalIp(this.devicesInfo[i.paired_sb.device_id].wifi_local_name) + "/api/switch?apikey=" + this.devicesInfo[i.paired_sb.device_id].apikey)
          .subscribe((data11: any) => {
            this.dev_avail[i.paired_sb._id].off_available = true;
            resolve(1);
          }, error => {
            console.log(error);
            this.dev_avail[i.paired_sb._id].off_available = false;
            resolve(1);
          });
      }, error => {
        console.log(error);
        this.dev_avail[i.paired_sb._id].on_available = false;
        this.LocalComm.pingDevice('http://', this.local.getLocalIp(this.devicesInfo[i.paired_sb.device_id].wifi_local_name) + "/api/switch?apikey=" + this.devicesInfo[i.paired_sb.device_id].apikey)
          .subscribe((data11: any) => {
            this.dev_avail[i.paired_sb._id].off_available = true;
            resolve(1);
          }, error => {
            console.log(error);
            this.dev_avail[i.paired_sb._id].off_available = false;
            resolve(1);
          });
      }, false);
    });
  }*/
  show_loading_epg = -1;
  goToEpgPage(i, tv_data, comm_mode, ind) {
    console.log(i);
    //this.load.showLoading('Loading...');
    this.show_loading_epg = ind;
    let paired_tv;
    paired_tv = [];
    if (i.paired_appliance && i.paired_appliance.appliance) {
      for (let j of this.data) {
        let x = j._id.split('_')[3] || j._id;
        if (x == i.paired_appliance.appliance) {
          paired_tv[0] = j;
          break;
        }
      }
    }
    this.ds.getChannelGroupData(
      data => {
        // this.load.dismiss();
        this.show_loading_epg = -1;
        // console.log("I think i am all done safely");
        // this.alert.showAlert("Reached view Enter",JSON.stringify(data));
        this.navCtrl.push('EpgMainPage', {
          channelGroupData: data,
          codeset: i.remote_codeset,
          device_id: i.paired_sb.device_id,
          sb_id: i.paired_sb._id,
          tv_data: tv_data,
          stb_provider_id: i.stb_provider_id,
          ir_appliance_id: i._id.split('_')[3] || i._id,
          ir_appliance_name: i.name,
          ir_appliance: i,
          paired_tv: paired_tv
        });
      },
      error => {
        // this.load.dismiss();
        this.show_loading_epg = -1;
        // this.alert.showAlert("Reached view Enter",JSON.stringify(error));
        //console.log("Opps mann.. check it");
        console.log(error);
      });

  }
  doRefresh(event) {
    this.ionViewWillEnter(event);
  }

  goToCreateRemote() {
    if (this.local.getCurrentHome()) {
      this.navCtrl.push('CreateIrAppPage', {
        home_id: this.local.getCurrentHome()
      });
    }
  }
}
