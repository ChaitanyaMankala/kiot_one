import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../providers/DataService';
import { LocalStorageService } from '../../../providers/LocalStorageService';

//pages
import { RemotesListPage } from '../remotes-list/remotes-list';

@IonicPage()
@Component({
    selector: 'page-custom-remote',
    templateUrl: 'custom-remote.html',
})
export class CustomRemotePage {
    buttons_list; remote_id; irSignal; comm_mode = 'online'; raw_data;
    constructor(public navCtrl: NavController, private local: LocalStorageService, public navParams: NavParams, private ds: DataService) {
        this.remote_id = this.navParams.get('remote_id');

    }

    ionViewWillEnter() {
        console.log('ionViewDidLoad CustomRemotePage');
        this.ds.getCustomRemoteinDetail(this.local.getCurrentHome(), this.remote_id,
            data => {
                this.raw_data = data;
                this.buttons_list = data.buttons;
                console.log(data);
            }, error => {
                console.log(error);
            }, true)
    }
    triggerIr(i, j) {
        // console.log(this.remote_keys[i]);
        // console.log(this.irSignal);
        this.irSignal = {
            "device": i.irapp.paired_sb.device_id,
            "switchboard": i.irapp.paired_sb._id
        };
        switch (i.irapp.remote_codeset.type) {
            case 1: {
                this.irSignal.protocol = i.irapp.remote_codeset.protocol;
                this.irSignal.code = i.irapp.remote_codeset.keys[j];
                this.irSignal.len = i.irapp.remote_codeset.len;
                this.irSignal.proto_name = i.irapp.remote_codeset.proto_name;
                this.irSignal.wave_gap = i.irapp.remote_codeset.wave_gap;
                break;
            }
            case 2: {
                this.irSignal.code = i.irapp.remote_codeset.keys[j].code;
                this.irSignal.first_seq_len = i.irapp.remote_codeset.keys[j].first_seq_len;
                this.irSignal.sec_seq_len = i.irapp.remote_codeset.keys[j].sec_seq_len;
                this.irSignal.len = parseInt(i.irapp.remote_codeset.keys[j].first_seq_len) + parseInt(i.irapp.remote_codeset.keys[j].sec_seq_len);
                this.irSignal.freq = i.irapp.remote_codeset.freq;
                this.irSignal.rpt_cnt = i.irapp.remote_codeset.rpt_cnt;
                break;
            }
        }

        console.log(this.irSignal);
        this.ds.postTriggerIr(this.irSignal,
            (data) => {
                console.log(data);
            },
            (error) => {
                console.log(error);
            });

    }
    goToEditRemote() {
        this.navCtrl.push('RemotesListPage', {
            list: this.navParams.get('list'),
            isEdit: this.raw_data,
            home_id: this.local.getCurrentHome()
        })
    }

}
