import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CustomRemotePage } from './custom-remote';

@NgModule({
    declarations: [
        CustomRemotePage,
    ],
    imports: [
        IonicPageModule.forChild(CustomRemotePage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class CustomRemotePageModule { }