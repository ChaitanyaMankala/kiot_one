import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, ViewController, IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../providers/DataService';
import { Helper } from '../../../providers/Helper';
import { ToastService } from '../../../providers/ToastService';


//pages
import { ProgramDetailsPage } from '../program-details/program-details';

@IonicPage()
@Component({
  selector: 'page-channel-programs',
  templateUrl: 'channel-programs.html'
})
export class ChannelProgramsPage {
  programsData = []; currentOffset; channelid; days = []; noMoreRows; channelname; temp; channel_meta; provider_map;
  constructor(public navCtrl: NavController, private toast: ToastService, private help: Helper, public navParams: NavParams, private viewCtrl: ViewController, private ds: DataService, private modalCtrl: ModalController) {
    this.temp = this.navParams.get('current');
    //if(this.temp.program) this.programsData.push(this.temp);
    this.channelname = this.temp.channelname || this.temp.channel_name;
    this.currentOffset = 0;
    this.noMoreRows = false;
    console.log(this.programsData);
    this.getPrograms(0, false);
    this.channel_meta = this.navParams.get('channel_meta');
    this.provider_map = this.navParams.get('provider_map');
    this.channelid = this.temp.channelid || this.temp.id;
    this.later_programs = {};
    let x = new Date(Date.now());
    x.setHours(0, 0, 0, 0);
    this.later_programs[x.toString()] = [];
    for (let i = 0; i < 3; i++) {
      x.setDate(x.getDate() + 1);
      this.later_programs[x.toString()] = [];
    }
    this.days = Object.keys(this.later_programs);

  }

  ionViewWillEnter() {
    console.log('ionViewDidLoad ChannelProgramsPage');


  }
  dismiss(data) {
    this.viewCtrl.dismiss(data);
  }
  later_programs;
  getPrograms(offset, event) {
    this.ds.getChannelPrograms(20, offset, this.temp.channelid || this.temp.id, false,
      (data) => {
        if (event) event.complete();
        if (data.length == 0) this.noMoreRows = true;
        //this.programsData = this.programsData.concat(data);
        if (offset == 0) {
          data[0].duration = (this.help.findHrsInterval(data[0].starttime, Date.now())) / (this.help.findHrsInterval(data[0].starttime, data[0].endtime));
          data[0].duration = (data[0].duration * 100).toString() + '%';
        }
        //console.log(data);

        for (let i of data) {
          let current = new Date(i.starttime);
          for (let j of this.days) {
            let x = new Date(j)
            if (current.getDate() == x.getDate()) {
              this.later_programs[j].push(i);
              break;
            }
          }


        }
        console.log(this.later_programs);
        this.currentOffset += 20;
      },
      (error) => {
        console.log(error);
      })
  }
  doInfinite(event) {
    this.getPrograms(this.currentOffset, event);
  }
  switchToChannel(item?, override_num?) {
    let obj;
    if (this.later_programs[this.days[0]][0].channelid) {
      obj = this.help.switchToChannel(this.later_programs[this.days[0]][0].channelid, this.navParams.get('channel_meta2'), this.navParams.get('provider_map'), this.navParams.get('sb_id'), this.navParams.get('device_id'), this.navParams.get('STBRemote'));
      if (obj == 'NO CHANNEL') {
        this.toast.bottomToast_Short('Sorry, Channel number is not available');
      }
      else {
        if (item) {
          item.show_loader = true;
        }
        this.ds.postSwitchToChannel(
          obj,
          data => {
            // if (this.platform.is('android')) Vibration.vibrate(30);
            if (item) {
              item.show_loader = false;
            }
            console.log(data);
            this.toast.bottomToast_Short('Now playing: ' + this.channelname);
          },
          error => {
            if (item) {
              item.show_loader = false;

            }
            console.log(error);
          });
      }
    }
  }
  goToProgramDetails(i) {
    /*  this.navCtrl.push('ProgramDetailsPage',{
          upid:i._id,
          channel_meta:this.channel_meta,
          provider_map:this.provider_map,
          sb_id:this.navParams.get('sb_id'),
          programid:i.programid,
          mainprogramid:i.program.mainprogramid
        }
      )*/
    let detailsModal = this.modalCtrl.create('ProgramDetailsPage', {
      upid: i._id,
      channel_meta: this.channel_meta,
      provider_map: this.provider_map,
      sb_id: this.navParams.get('sb_id'),
      programid: i.programid,
      mainprogramid: i.program.mainprogramid,
      ir_appliance_id: this.navParams.get('ir_appliance_id'),
      ir_appliance_name: this.navParams.get('ir_appliance_name'),
      device_id: this.navParams.get('device_id'),
      STBRemote: this.navParams.get('STBRemote')
    });
    detailsModal.present();
  }
}
