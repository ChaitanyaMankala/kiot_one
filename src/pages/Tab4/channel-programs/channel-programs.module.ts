import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChannelProgramsPage } from './channel-programs';
import { PipesModule } from '../../../pipes/pipes.module';

@NgModule({
    declarations: [
        ChannelProgramsPage,
    ],
    imports: [
        IonicPageModule.forChild(ChannelProgramsPage),
        PipesModule
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class ChannelProgramsPageModule { }