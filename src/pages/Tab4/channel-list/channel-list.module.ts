import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChannelListPage } from './channel-list';

@NgModule({
    declarations: [
        ChannelListPage,
    ],
    imports: [
        IonicPageModule.forChild(ChannelListPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class ChannelListPageModule { }