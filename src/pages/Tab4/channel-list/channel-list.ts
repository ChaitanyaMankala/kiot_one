import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../providers/DataService';
import { Helper } from '../../../providers/Helper';
import { ToastService } from '../../../providers/ToastService';

@IonicPage()
@Component({
  selector: 'page-channel-list',
  templateUrl: 'channel-list.html',
})
export class ChannelListPage {
  rev_providerMap2; channelList = []; channelMetaData; current_platform; providerMap; STBRemote; remote_keys; rev_providerMap;
  constructor(public navCtrl: NavController, private toast: ToastService, public navParams: NavParams, private help: Helper, private ds: DataService, private viewCtrl: ViewController) {
    this.rev_providerMap2 = this.navParams.get('rev_providerMap2');
    this.providerMap = this.navParams.get('providerMap');
    this.STBRemote = this.navParams.get('STBRemote');
    this.remote_keys = this.STBRemote[0].keys[0];
    console.log(this.rev_providerMap2);
    console.log(this.providerMap);
    this.current_platform = this.help.getPlatform();

  }

  ionViewDidLoad() {
    let obj; let list = []; let max = 0;
    obj = {};
    for (let key in this.rev_providerMap2) {

      if (this.rev_providerMap2.hasOwnProperty(key) && this.providerMap[key]) {
        // console.log(key + " -> " + this.rev_providerMap2[key]);

        obj[this.providerMap[key]] = {
          channel_num: this.providerMap[key],
          channel_name: this.rev_providerMap2[key]
        };
        max = parseInt(this.providerMap[key]) > max ? parseInt(this.providerMap[key]) : max;
      }
    }

    for (let i = 1; i <= max; i++) {
      if (obj[i]) {
        this.channelList.push({
          channel_num: obj[i].channel_num,
          channel_name: obj[i].channel_name,
        });
      }
    }
    this.channelList2 = this.channelList;
  }
  dismiss(data) {
    this.viewCtrl.dismiss(data);
  }
  channelList2;
  filterItems(ev: any) {

    this.channelList2 = this.channelList;
    let val = ev.target.value;

    let result_data = this.channelList2.slice();
    if (val && val.trim() != '') {
      if (parseInt(val)) {
        this.channelList2 = result_data.filter((item: any) => {
          return (item.channel_num.indexOf(val.toLowerCase()) > -1);
        });
      }
      else {
        this.channelList2 = result_data.filter((item: any) => {
          return (item.channel_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        });
      }
    }
    else {
      this.channelList2 = result_data;
    }

  }
  switchToChannel(channel) {
    this.ds.postSwitchToChannel(
      {
        "channelid": channel.channel_num,
        "ir_device": this.navParams.get('sb_id'),
        "device": this.navParams.get('device_id'),
        "protocol": this.STBRemote[0].protocol,
        "len": this.STBRemote[0].len,
        "proto_name": this.STBRemote[0].proto_name,
        "wave_gap": this.STBRemote[0].wave_gap,
        "codes": this.help.getCodesForChannel(channel.channel_num, this.remote_keys)
      },
      data => {
        // if (this.platform.is('android')) Vibration.vibrate(30);
        console.log(data);
        this.toast.topToast_Short('Now Playing: ' + channel.channel_name);
      },
      error => {
        console.log(error);
      }
    )
  }

}


