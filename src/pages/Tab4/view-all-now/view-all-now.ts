import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ModalController, Platform,IonicPage } from 'ionic-angular';
//import { Vibration } from 'ionic-native';

//providers
import { DataService } from '../../../providers/DataService';
import { Helper } from '../../../providers/Helper';
import { AlertService } from '../../../providers/AlertService';
import { ToastService } from '../../../providers/ToastService';

//pages
import { ProgramDetailsPage } from '../program-details/program-details';

@IonicPage()
@Component({
  selector: 'page-view-all-now',
  templateUrl: 'view-all-now.html'
})
export class ViewAllNowPage {
  programData; type; noMoreRows; langs; channelMetaData; data; offset; current_active; providerMap;
  constructor(public navCtrl: NavController, private alert: AlertService, public navParams: NavParams, private toast: ToastService, private platform: Platform, private viewCtrl: ViewController, private help: Helper, private ds: DataService, private modalCtrl: ModalController) {
    this.programData = this.navParams.get('programData');
    this.langs = this.navParams.get('langs');
    this.type = this.navParams.get('type');
    console.log(this.programData);
    this.noMoreRows = false;
    this.channelMetaData = this.navParams.get('meta');
    this.offset = 10;
    this.providerMap = this.navParams.get('provider_map');

    if (this.type.key.split('_')[1]) {
      this.langs = this.type.key.split('_')[1];
      this.programData = [];
      this.offset = 0;
      this.doInfinite();
    }
  }

  ionViewDidLoad() {
    this.current_active = 'NONE';
    console.log('ionViewDidLoad ViewAllNowPage');
  }
  dismiss(data) {
    this.viewCtrl.dismiss();
  }
  doInfinite(event?, type_override?) {
    console.log(this.noMoreRows);
    console.log(this.offset);
    console.log(event);

    let type1;
    if (this.type.key.split('_')[1]) {
      type1 = this.type.key.split('_')[0];
    }
    else {
      type1 = this.type.key;
    }

    this.ds.getPrograms(10, this.offset, this.langs, type1, false,
      (data) => {
        this.data = data;
        if (event) event.complete();
        if (this.data.length == 0) {
          this.noMoreRows = true;
        }
        console.log(this.data);
        for (let i of this.data) {
          i.channel_name = this.channelMetaData[3].channels[i.channelid].channelname;
          i.duration = (this.help.findHrsInterval(i.starttime, Date.now())) / (this.help.findHrsInterval(i.starttime, i.endtime));
          i.duration = (i.duration * 100).toString() + '%';
        }
        this.programData = this.programData.concat(this.data);
        this.offset += 10;
        console.log(this.noMoreRows);
        console.log(this.offset);
      },
      (error) => {
        if (event) {
          event.complete();
        }
        console.log(error);
      })
  }
  onTap(i) {
    this.current_active = i;
    console.log(i);
  }

  goToProgramDetails(i, duration?) {
    this.current_active = 'NONE';
    let detailModal = this.modalCtrl.create('ProgramDetailsPage', {
      upid: duration == 'NONE' ? '' : i._id,
      duration: duration == 'NONE' ? '' : duration,
      channel_meta: this.channelMetaData[3].channels,
      provider_map: this.providerMap,
      sb_id: this.navParams.get('sb_id'),
      programid: i.programid,
      mainprogramid: i.program.mainprogramid,
      device_id: this.navParams.get('device_id'),
      STBRemote: this.navParams.get('STBRemote')
    });
    detailModal.present();
  }

  switchToChannel(channelid, override_num?, i?) {
    let obj;
    if (channelid) {
      obj = this.help.switchToChannel(channelid, this.channelMetaData, this.providerMap, this.navParams.get('sb_id'), this.navParams.get('device_id'), this.navParams.get('STBRemote'));
    }
    else {
      obj = this.help.switchToChannel(null, this.channelMetaData, this.providerMap, this.navParams.get('sb_id'), this.navParams.get('device_id'), this.navParams.get('STBRemote'), override_num);
    }
    if (obj == 'NO CHANNEL') {
      this.current_active = 'NONE';
      this.alert.showPrompt('Sorry',
        'channel number not available, you can enter the channel number manually (if known)',
        [
          {
            name: 'channelno',
            placeholder: 'Channel Number'
          }
        ], 'Cancel', () => {

        }, 'Switch to channel', (data) => {
          if (data.channelno && parseInt(data.channelno)) {
            console.log('switching to ' + data.channelno);
            this.switchToChannel(null, data.channelno);
            this.toast.bottomToast_Short('Switching to ' + data.channelno);
          }
          else {
            this.toast.bottomToast_Short('Invalid Channel number');
          }
        });
    }
    else {
      this.ds.postSwitchToChannel(
        obj,
        data => {
          //   if (this.platform.is('android')) Vibration.vibrate(30);
          this.current_active = 'NONE';
          if (i) {
            this.toast.topToast_Short('Playing ' + (i.program.name || '') + ' ...');
          }
          console.log(data);
        },
        error => {
          console.log(error);
        }
      )
    }
  }

}
