import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewAllNowPage } from './view-all-now';
import { PipesModule } from '../../../pipes/pipes.module';

@NgModule({
    declarations: [
        ViewAllNowPage,
    ],
    imports: [
        IonicPageModule.forChild(ViewAllNowPage),
        PipesModule
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class ViewAllNowPageModule { }