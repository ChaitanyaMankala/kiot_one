import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateTimerPage } from './create-timer';
import { PipesModule } from '../../../pipes/pipes.module';

@NgModule({
    declarations: [
        CreateTimerPage,
    ],
    imports: [
        IonicPageModule.forChild(CreateTimerPage),
        PipesModule
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class CreateTimerPageModule { }