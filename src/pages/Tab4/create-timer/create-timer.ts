import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../providers/DataService';
import { ToastService } from '../../../providers/ToastService';
import { LocalStorageService } from '../../../providers/LocalStorageService';
import { Helper } from '../../../providers/Helper';

@IonicPage()
@Component({
  selector: 'page-create-timer',
  templateUrl: 'create-timer.html',
})
export class CreateTimerPage {
  ac_remote_data; sb_id; device_id; power_codes; timer_toggle; timer_data; time_left; timer_time; mode_set; modes; ir_appliance_id;
  constructor(public navCtrl: NavController, private help: Helper, public navParams: NavParams, private ds: DataService, private local: LocalStorageService, private toast: ToastService, private viewCtrl: ViewController) {
    this.ac_remote_data = this.navParams.get('ac_remote_data');
    this.sb_id = this.navParams.get('sb_id');
    this.device_id = this.navParams.get('device_id');
    this.power_codes = this.navParams.get('power_codes');
    this.ir_appliance_id = this.navParams.get('ir_appliance_id');
    this.modes = this.navParams.get('modes');
    console.log(this.power_codes);
    console.log(this.ac_remote_data);
    this.timer_time = "00:30";
    this.timer_data = this.navParams.get('timer_data');
    this.time_left = this.navParams.get('time_left');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateTimer');
  }
  dismiss(data) {
    this.viewCtrl.dismiss(data);
  }
  createTimer() {
    let obj;
    obj = {
      isScheduleSceneItem: false,
      homeId: this.local.getCurrentHome(),
      SBid: this.sb_id,
      device: this.device_id,
      isTimed: true,
      isActive: false,
      hour: this.timer_time.slice(0, 2),
      mins: this.timer_time.slice(3, 5),
      freq: this.ac_remote_data[0].freq,
      rpt_cnt: this.ac_remote_data[0].rpt_cnt,
      ir_appliance_id: this.ir_appliance_id,
      createdTime: Date.now(),
      ir_state: this.timer_toggle ? true : false
    };
    if (this.timer_toggle) {
      for (let i of this.power_codes) {
        if (i.power_set == 'ON' && i.mode_set == this.mode_set) {
          obj.code = i.code;
          obj.first_seq_len = i.first_seq_len;
          obj.sec_seq_len = i.sec_seq_len;
          obj.code = i.code;
          obj.len = parseInt(i.first_seq_len) + parseInt(i.sec_seq_len);
        }
      }
    }
    else {
      for (let i of this.power_codes) {
        if (i.power_set == 'OFF') {
          obj.code = i.code;
          obj.first_seq_len = i.first_seq_len;
          obj.sec_seq_len = i.sec_seq_len;
          obj.code = i.code;
          obj.len = parseInt(i.first_seq_len) + parseInt(i.sec_seq_len);
        }
      }

    }
    this.ds.postCreateTimer(obj,
      data => {
        console.log(data);
        this.timer_data.push({
          id: data.scheduleId,
          createdTime: Date.now(),
          ir_state: this.timer_toggle ? true : false,
          hour: this.timer_time.slice(0, 2),
          mins: this.timer_time.slice(3, 5)
        });
        this.setAcTimer();
      }, error => {
        console.log(error);
      })
  }
  setAcTimer() {
    let starttime = new Date(Date.now());
    //starttime = this.timer_data[0].createdTime;
    let endtime = new Date(starttime);
    endtime.setHours(endtime.getHours() + parseInt(this.timer_data[0].hour));
    endtime.setMinutes(endtime.getMinutes() + parseInt(this.timer_data[0].mins));
    this.time_left[0] = parseInt(this.help.findHrsInterval(Date.now(), endtime));
    this.time_left[1] = parseInt(this.help.findMinsInterval(Date.now(), endtime)) % 60;
    this.toast.topToast_Short('Timer Started');
    this.dismiss(0);
  }
}
