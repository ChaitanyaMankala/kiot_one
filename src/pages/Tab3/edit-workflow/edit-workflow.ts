import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../providers/DataService';
import { Helper } from '../../../providers/Helper';
import { ToastService } from '../../../providers/ToastService';

@IonicPage()
@Component({
  selector: 'page-edit-workflow',
  templateUrl: 'edit-workflow.html',
})
export class EditWorkflowPage {
  all_workflows = []; w_ind; wflow;
  constructor(public navCtrl: NavController, public navParams: NavParams, private toast: ToastService, private help: Helper, private ds: DataService) {
    this.all_workflows = this.navParams.get('all_workflows');
    this.w_ind = this.navParams.get('w_ind');
    this.wflow = this.all_workflows[this.w_ind];
    this.wflow.start_time = this.help.arihantsTimeToString(this.wflow.starts_at, true);
    this.wflow.end_time = this.help.arihantsTimeToString(this.wflow.ends_at, true);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditWorkflowPage');
  }
  validateTime() {

  }
  editWorkflow() {
    let obj;
    obj = {
      group: this.wflow._id,
      name: this.wflow.name,
      starts_at: parseInt(this.wflow.start_time.slice(0, 2)) * 100 + parseInt(this.wflow.start_time.slice(3, 5)),
      ends_at: parseInt(this.wflow.end_time.slice(0, 2)) * 100 + parseInt(this.wflow.end_time.slice(3, 5))
    };
    this.ds.postEditWorkflowGroup(obj, data => {
      obj.start_str = this.help.arihantsTimeToString(obj.starts_at);
      obj.end_str = this.help.arihantsTimeToString(obj.ends_at);
      obj._id = obj.group;
      this.all_workflows[this.w_ind] = obj;
      this.navCtrl.pop();
      this.toast.bottomToast_Short('Saved');
    }, error => {
      console.log(error);
    })
  }

}
