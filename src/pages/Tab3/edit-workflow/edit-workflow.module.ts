import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditWorkflowPage } from './edit-workflow';

@NgModule({
    declarations: [
        EditWorkflowPage,
    ],
    imports: [
        IonicPageModule.forChild(EditWorkflowPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class EditWorkflowPageModule { }