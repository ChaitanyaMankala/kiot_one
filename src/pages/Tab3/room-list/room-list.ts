import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../providers/DataService';
import { LocalStorageService } from '../../../providers/LocalStorageService';
import { AlertService } from '../../../providers/AlertService';
import { ToastService } from '../../../providers/ToastService';

//pages
import { AppsForScenePage } from '../apps-for-scene/apps-for-scene';

@IonicPage()
@Component({
  selector: 'page-room-list',
  templateUrl: 'room-list.html'
})
export class RoomListPage {
  room_data; actions; for; scene_name; days; all_scenes; isEdit; actions_dictionary;
  constructor(public navCtrl: NavController, public navParams: NavParams, private alert: AlertService, private toast: ToastService, private ds: DataService, private modalCtrl: ModalController, private local: LocalStorageService) {
    this.all_scenes = this.navParams.get('all_scenes');
    this.for = this.navParams.get('for');
    this.scene_name = this.navParams.get('scene_name');
    this.days = this.navParams.get('days');
    this.isEdit = this.navParams.get('isEdit');
    this.ds.getSboards(this.local.getCurrentHome(),
      data => {
        console.log(data);
        this.room_data = data;
        //  this.makeADictionary();
        if (this.isEdit) {
          if (this.isEdit != 0) {
            this.takeActionsForEdit();
            this.for == 'unscheduled' ? this.addActionsUnscheduled() : this.addActionsScheduled();
            console.log(this.actions);
          }
        }
        else {
          this.actions = [];
        }

      }, error => {
        console.log(error);
        console.log(JSON.stringify(error.json()));
      }, false, 'room,swappliance');

  }


  ionViewDidLoad() {


  }
  goNext(ind) {
    let obj = {
      room_data: this.room_data,
      ind: ind,
      actions: this.actions,
      for: this.for,
      isEdit: this.isEdit,
      actions_dictionary: this.actions_dictionary
    }
    /*  this.navCtrl.push('AppsForScenePage',obj);*/
    this.modalCtrl.create('AppsForScenePage', obj).present();

  }
  checkApps() {
    this.actions = [];
    let no_of_apps = 0;
    if (this.for == 'unscheduled') {
      for (let i of this.room_data) {
        if (i.count == 0) continue;
        for (let j of i.devices) {
          for (let k of j.switch_appliances) {
            if (!k.select_toggle) continue;
            no_of_apps++;
            this.constructAction(k, j.device_id, j._id, i.room_id);
          }
        }
      }
      this.createSceneApiCall(false, no_of_apps, this.isEdit);
    }
    else {
      for (let i of this.room_data) {
        if (i.count == 0) continue;
        for (let j of i.devices) {
          for (let k of j.switch_appliances) {
            if (!k.select_toggle) continue;
            if (k.scheduleList) {
              no_of_apps++;
              for (let l of k.scheduleList) {
                this.constructAction(k, j.device_id, j._id, i.room_id, l);
              }
            }
          }
        }
      }
      this.createSceneApiCall(true, no_of_apps, this.isEdit);
    }

  }
  constructAction(item, device_id, sb_id, room_id, item2?) {
    let obj;
    obj = {
      "SBid": sb_id,
      "actionType": "switchAction",
      "switch_appliance_id": item._id,
      "act_type": "switch",
      "device": device_id,
      "switch_no": item.switch_no,
      "room_id": room_id
    };
    if (item2) {
      obj.switch_state = item2.state ? 1 : 0;
      obj.hour = item2.time.slice(0, 2);
      obj.mins = item2.time.slice(3, 5);
      obj.days = this.days;
      obj.isRepeat = true;
    }
    else {
      obj.switch_state = item.state_toggle ? 1 : 0
    }
    if (item.switch_type == 'regulator') {
      obj.dimm_value = item.state_dimm_value;
    }
    this.actions.push(obj);
    console.log(this.actions);
  }

  createSceneApiCall(schedule, no_of_apps, isEdit?) {
    if (this.actions.length == 0) {
      console.log('select atleast one, bro!!');
      this.toast.bottomToast_Long('Please Select atleast One appliance from any room');
    }
    else {
      let obj;
      obj = {
        actions: this.actions,
        homeId: this.local.getCurrentHome(),
        isActive: true,
        no_of_apps: no_of_apps
      };
      if (schedule) {
        obj.scheduleSceneName = this.scene_name;
        obj.hour = parseInt(this.for.slice(0, 2));
        obj.mins = parseInt(this.for.slice(3, 5));
        obj.days = this.days;
        if (this.isEdit) obj.scheduleSceneId = this.all_scenes[this.isEdit - 1]._id;
      } else {
        obj.sceneName = this.scene_name;
        if (this.isEdit) obj.sceneId = this.all_scenes[this.isEdit - 1]._id;
      }
      if (this.isEdit) {
        this.ds.postEditScene(schedule,
          obj, data => {
            console.log(data);
            let user1 = this.local.getUser();
            obj.user = {
              name: user1.name,
              _id: user1._id
            }
            obj._id = obj.scheculeSceneId || obj.sceneId;
            this.all_scenes[this.isEdit - 1] = obj;
            this.navCtrl.pop();
            this.navCtrl.pop();
            this.toast.bottomToast_Short('Scene Edited');

          }, error => {
            console.log(error);
          });
      }
      else {
        this.ds.postCreateScene(schedule,
          obj, data => {
            console.log(data);
            let user1 = this.local.getUser();
            obj.user = {
              name: user1.name,
              _id: user1._id
            }
            obj._id = data.scheduleSceneId || data.sceneId;
            this.all_scenes.push(obj);
            this.navCtrl.pop();
            this.navCtrl.pop();
            this.toast.bottomToast_Short('Scene Created');

          }, error => {
            console.log(error);
          });
      }
    }
  }
  takeActionsForEdit() {
    this.actions = this.all_scenes[parseInt(this.isEdit) - 1].actions;
    this.actions_dictionary = {};
    let sch = this.all_scenes[parseInt(this.isEdit) - 1].scheduleSceneName ? true : false;
    for (let i of this.actions) {
      this.actions_dictionary[i.SBid] = 1;
      if (!this.actions_dictionary[i.switch_appliance_id]) {
        if (sch) {
          this.actions_dictionary[i.switch_appliance_id] = [];
          this.actions_dictionary[i.switch_appliance_id].push(i);
        }
        else {
          this.actions_dictionary[i.switch_appliance_id] = i;
        }
      }
      else {
        this.actions_dictionary[i.switch_appliance_id].push(i);
      }
    }

  }
  addActionsUnscheduled() {
    for (let x of this.room_data) {
      for (let i of x.devices) {
        if (this.actions_dictionary[i._id] == 1) {
          for (let j of i.switch_appliances) {
            if (this.actions_dictionary[j._id]) {
              j.select_toggle = true;
              j.state_toggle = this.actions_dictionary[j._id].switch_state;
            }
          }
        }
      }
    }
  }
  addActionsScheduled() {
    for (let x of this.room_data) {
      for (let i of x.devices) {
        if (this.actions_dictionary[i._id] == 1) {
          for (let j of i.switch_appliances) {
            if (this.actions_dictionary[j._id]) {
              j.select_toggle = true;
              for (let k of this.actions_dictionary[j._id]) {
                if (!j.scheduleList) j.scheduleList = [];
                j.scheduleList.push({
                  state: k.switch_state,
                  time: (parseInt(k.hour) < 10 ? k.hour[0] == '0' ? k.hour : '0' + k.hour : k.hour) + ':' + (parseInt(k.mins) < 10 ? '0' + k.mins : k.mins)
                });
              }
            }
          }
        }
      }
    }
  }

  //not using now------
  makeADictionary() {
    for (let i of this.room_data) {
      this.room_data[i._id] = i;
      for (let j of i.devices) {
        this.room_data[i._id][j._id] = j;
        for (let k of j.switchappliances) {
          this.room_data[i._id][j._id][k._id] = k;
        }
      }
    }
    console.log(this.room_data);
  }

}
