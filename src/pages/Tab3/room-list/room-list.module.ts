import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RoomListPage } from './room-list';

@NgModule({
    declarations: [
        RoomListPage,
    ],
    imports: [
        IonicPageModule.forChild(RoomListPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class RoomListPageModule { }