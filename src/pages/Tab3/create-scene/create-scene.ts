import { Component } from '@angular/core';
import { NavController, NavParams,IonicPage } from 'ionic-angular';

//pages
import { RoomListPage } from '../room-list/room-list';

//providers
import { Helper } from '../../../providers/Helper';

@IonicPage()
@Component({
  selector: 'page-create-scene',
  templateUrl: 'create-scene.html',
})
export class CreateScenePage {
  scene_name; schedule_toggle; scene_time; schedule_repeats; repeat_selectText
  constructor(public navCtrl: NavController, public navParams: NavParams,private help:Helper) {
    this.schedule_toggle = false;
    this.schedule_repeats = [true, true, true, true, true, false, false];
    this.repeat_selectText = 'Everyday';
    this.scene_time = 'Select';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateScene');
  }
  repeatChange(event) {
    console.log(event);
    if (this.schedule_repeats.length == 7) {
      this.repeat_selectText = 'Everyday';
    }
    else {
      this.repeat_selectText = null;
    }
  }
  goToRoomList() {

    this.navCtrl.push('RoomListPage', {
      for: this.schedule_toggle ? this.scene_time : 'unscheduled',
      scene_name: this.scene_name,
      days: this.help.getDaysArray(this.schedule_repeats),
      all_scenes: this.navParams.get('all_scenes')
    });
  }
}
