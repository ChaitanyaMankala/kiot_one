import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateScenePage } from './create-scene';

@NgModule({
    declarations: [
        CreateScenePage,
    ],
    imports: [
        IonicPageModule.forChild(CreateScenePage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class CreateScenePageModule { }