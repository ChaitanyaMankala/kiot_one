import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../providers/DataService';
import { LocalStorageService } from '../../../providers/LocalStorageService';
import { Helper } from '../../../providers/Helper';

//pages
import { ConfigWorkflowPage } from '../config-workflow/config-workflow';

@IonicPage()
@Component({
  selector: 'page-create-workflow',
  templateUrl: 'create-workflow.html',
})
export class CreateWorkflowPage {
  all_rooms = []; all_devices; my_sboards = [];
  wflow; invalid_time; all_workflows = []; self_destroy;
  constructor(public navCtrl: NavController, public navParams: NavParams, private help: Helper, private local: LocalStorageService, private ds: DataService) {
    this.my_sboards = this.navParams.get('my_sboards');
    this.all_workflows = this.navParams.get('all_workflows');
    this.invalid_time = true;
    this.all_devices = {};
    this.self_destroy = false;
    //this.getRoomsAndDevices();
    this.wflow = {
      name: "",
      start_time: "",
      end_time: ""
    };
  }

  ionViewWillEnter() {
    if (this.self_destroy) {
      this.navCtrl.pop();
    }
    console.log('ionViewDidLoad CreateWorkflowPage');
  }
  getRoomsAndDevices() {
    this.ds.getSboards(this.local.getCurrentHome(),
      data => {
        this.my_sboards = data;
        for (let i of data) {
          this.all_rooms.push({
            room_id: i.room_id,
            room_name: i.room_name
          });
          this.all_devices[i.room_id] = i.devices;
        }
      }, error => {
        console.log(error);
      }, false, 'room,swappliance')
  }
  goToConfigWorkflow() {
    this.wflow.start_time_obj = parseInt(this.wflow.start_time.slice(0, 2)) * 100 + parseInt(this.wflow.start_time.slice(3, 5));
    this.wflow.end_time_obj = parseInt(this.wflow.end_time.slice(0, 2)) * 100 + parseInt(this.wflow.end_time.slice(3, 5));
    let x;
    x = {
      home_id: this.local.getCurrentHome(),
      name: this.wflow.name,
      starts_at: this.wflow.start_time_obj,
      ends_at: this.wflow.end_time_obj
    };
    this.ds.postCreateWorkflowGroup(x, data => {
      x._id = data._id;
      x.start_str = this.help.arihantsTimeToString(x.starts_at);
      x.end_str = this.help.arihantsTimeToString(x.ends_at);
      this.all_workflows.push(x);
      this.self_destroy = true;
      this.navCtrl.push('ConfigWorkflowPage', {
        w_group: data._id,
        all_workflows: [],
        my_sboards: this.my_sboards
      });
    }, error => {
      console.log(error);
    });


  }

  validateTime() {
    console.log(this.wflow.end_time);
    if (this.wflow.end_time != this.wflow.start_time)
      this.invalid_time = false;
  }
}
