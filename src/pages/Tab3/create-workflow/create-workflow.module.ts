import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateWorkflowPage } from './create-workflow';

@NgModule({
    declarations: [
        CreateWorkflowPage,
    ],
    imports: [
        IonicPageModule.forChild(CreateWorkflowPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class CreateWorkflowPageModule { }