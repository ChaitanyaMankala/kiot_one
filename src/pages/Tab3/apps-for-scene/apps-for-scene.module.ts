import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppsForScenePage } from './apps-for-scene';
import { PipesModule } from '../../../pipes/pipes.module';

@NgModule({
    declarations: [
        AppsForScenePage,
    ],
    imports: [
        IonicPageModule.forChild(AppsForScenePage),
        PipesModule
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class AppsForScenePageModule { }