import { Component } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';
import { NavController, NavParams, ViewController, ModalController, IonicPage } from 'ionic-angular';

//pages
import { ScheduleSwitchPage } from '../schedule-switch/schedule-switch';

//providers
import { AlertService } from '../../../providers/AlertService';

@IonicPage()
@Component({
  selector: 'page-apps-for-scene',
  templateUrl: 'apps-for-scene.html',
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({ transform: 'translateY(-100%)', opacity: 0 }),
          animate('170ms', style({ transform: 'translateY(0)', opacity: 1 }))
        ]),
        transition(':leave', [
          style({ transform: 'translateY(0)', opacity: 1 }),
          animate('170ms', style({ transform: 'translateY(-100%)', opacity: 0 }))
        ])
      ]
    )
  ]
})
export class AppsForScenePage {
  room_data; ind; current_room; action_list = []; actions; for; isEdit; actions_dictionary;
  constructor(public navCtrl: NavController, public navParams: NavParams, private alert: AlertService, private viewCtrl: ViewController, private modalCtrl: ModalController) {
    this.room_data = this.navParams.get('room_data');
    this.ind = this.navParams.get('ind');
    this.current_room = this.room_data[this.ind];
    this.actions = this.navParams.get('actions');
    this.for = this.navParams.get('for');
    this.isEdit = this.navParams.get('isEdit');
    this.actions_dictionary = this.navParams.get('actions_dictionary')
    // if (this.for != 'unscheduled') this.setScheduleLists();
    console.log(this.current_room);
    if (this.isEdit) {
      // this.for == 'unscheduled' ? this.addActionsUnscheduled() : this.addActionsScheduled();
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppsForScene');
  }
  addActionsUnscheduled() {
    for (let i of this.current_room.devices) {
      if (this.actions_dictionary[i._id] == 1) {
        for (let j of i.switch_appliances) {
          if (this.actions_dictionary[j._id]) {
            j.select_toggle = true;
            j.state_toggle = this.actions_dictionary[j._id].switch_state;
          }
        }
      }
    }
  }
  addActionsScheduled() {
    for (let i of this.current_room.devices) {
      if (this.actions_dictionary[i._id] == 1) {
        for (let j of i.switch_appliances) {
          if (this.actions_dictionary[j._id]) {
            j.select_toggle = true;
            for (let k of this.actions_dictionary[j._id]) {
              if (!j.scheduleList) j.scheduleList = [];
              j.scheduleList.push({
                state: k.switch_state,
                time: (parseInt(k.hour) < 10 ? k.hour[0] == '0' ? k.hour : '0' + k.hour : k.hour) + ':' + (parseInt(k.mins) < 10 ? '0' + k.mins : k.mins)
              });
            }
          }
        }
      }
    }
  }

  setAction(item, device_id, sb_id, eventu1, eventu2) {
    console.log(eventu1);
    if (eventu1._checked) {
      let obj;
      obj = {
        "SBid": sb_id,
        "actionType": "switchAction",
        "switch_appliance_id": item._id,
        "act_type": "switch",
        "device": device_id,
        "switch_no": item.switch_no,
        "switch_state": eventu2 ? eventu2._checked : item.state_toggle ? 0 : 1
      }
      this.actions[item._id] = obj;
    }
    else {
      if (this.actions[item._id]) {
        this.actions[item._id] = null;
      }
    }
  }
  dismiss(data) {
    /*  console.log(this.action_list);
        for (let i of this.action_list) {
              if(!i)continue;
              console.log(i);
              this.constructAction(i.charAt(0),i.charAt(1));
        }*/
    this.viewCtrl.dismiss(data);
    console.log(this.room_data);


  }
  goToScheduleSwitch(apps) {
    //console.log('YOLO');

    if (!apps.scheduleList) {
      apps.scheduleList = [];
      apps.scheduleList.push({
        state: 1,
        time: this.for
      });
    }
    apps.showAcc = apps.showAcc ? !apps.showAcc : true;


    // this.modalCtrl.create(ScheduleSwitchPage,{}).present();
  }
  edit_scheduleList(apps, ind) {
    if (ind == apps.scheduleList.length - 1) {
      apps.scheduleList.push({
        state: 1,
        time: this.for
      });
    }
    else {
      apps.scheduleList.splice(ind, 1);
    }
  }
  checkBoxChange(ev, apps) {
    console.log(ev.checked);
    if (ev.checked && !apps.scheduleList && this.for != 'unscheduled') {
      apps.scheduleList = [];
      apps.scheduleList.push({
        state: 1,
        time: this.for
      });
    }
    apps.showAcc = ev.checked;
  }






  //NOT USING NOW---
  setScheduleLists() {
    for (let sboard of this.current_room.devices) {
      for (let apps of sboard.switch_appliances) {
        apps.scheduleList = [];
        apps.scheduleList.push({
          state: 1,
          time: this.for
        });
      }
    }
  }
  checkTime(eve, ind, ind1, ind2) {
    console.log(eve);
    if (eve.ampm == 'pm') {
      eve.hour += 12;
    }
    if (parseInt(this.for.slice(0, 2)) > eve.hour) {
      this.throwTimeErr();
      this.current_room.devices[ind1].switch_appliances[ind2].scheduleList[ind].time = this.for;
    }
    else if (parseInt(this.for.slice(0, 2)) == eve.hour) {
      if (parseInt(this.for.slice(3, 5)) > eve.minute) {
        this.throwTimeErr();
        this.current_room.devices[ind1].switch_appliances[ind2].scheduleList[ind].time = this.for;
      }
    }
  }
  throwTimeErr() {
    let timeFormatter = '';
    let ampm = ' AM';
    if (parseInt(this.for.slice(0, 2)) > 12) {
      timeFormatter += (parseInt(this.for.slice(0, 2)) - 12).toString();
      ampm = ' PM';
    } else { timeFormatter += this.for.slice(0, 2); }
    timeFormatter += ':';
    timeFormatter += this.for.slice(3, 5);
    timeFormatter += ampm;
    this.alert.showAlert('Wrong Time', 'Time must be after the trigger time, ' + timeFormatter);
  }
}
