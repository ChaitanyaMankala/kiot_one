import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScenesPage } from './scenes.page';

@NgModule({
    declarations: [
        ScenesPage,
    ],
    imports: [
        IonicPageModule.forChild(ScenesPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class ScenesPageModule { }