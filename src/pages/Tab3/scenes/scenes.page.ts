import { Component } from '@angular/core';
import { NgStyle } from '@angular/common';
import { NavController, ModalController, IonicPage } from 'ionic-angular';

//pages
import { CreateScenePage } from '../create-scene/create-scene';
import { SceneDetailsPage } from '../scene-details/scene-details';
import { NotificationDispPage } from '../../Tab1/notification-disp/notification-disp';
import { CreateWorkflowPage } from '../create-workflow/create-workflow';
import { ConfigWorkflowPage } from '../config-workflow/config-workflow';

//providers
import { DataService } from '../../../providers/DataService';
import { LocalStorageService } from '../../../providers/LocalStorageService';
import { ToastService } from '../../../providers/ToastService';
import { Helper } from '../../../providers/Helper';

@IonicPage()
@Component({
  selector: 'ib-page-scenes',
  templateUrl: 'scenes.page.html'
})

export class ScenesPage {
  current; all_scenes = []; scenes = []; routines = []; selected_scene; kiot_internet; curr_home; dont_show_loader; all_workflows = []; my_sboards;

  constructor(public navCtrl: NavController, private help: Helper, private modalCtrl: ModalController, private ds: DataService, private local: LocalStorageService, private toast: ToastService) {
    this.current = 'scenes';
  }

  ionViewWillEnter() {
    this.kiot_internet = this.local.getInternetStatus();
    this.curr_home = this.local.getCurrentHome();
    console.log(this.all_scenes);
    if (this.kiot_internet == 'online') {
      if (this.all_scenes.length) {
        this.separateSceneRoutine();
      }
      this.ds.getUnseenNotifications(
        data => {
          this.num_of_notifications = data;
          console.log(data);
        }, error => {
          console.log(error);
        });
    }
    if (this.current == 'scenes') {
      this.scenesInit();
    }
  }
  selectScene(i) {
    this.selected_scene = i._id;
  }
  separateSceneRoutine() {
    let scenes = []; let routines = []; let c = 0;
    for (let i of this.all_scenes) {
      i.ind = c;
      if (i.scheduleSceneName) {
        if (!i.progress && i.progress != 0) {
          let x = new Date(Date.now());
          let hr = x.getHours();
          let min = x.getMinutes();
          let total = i.actions.length || 1; let completed = 0;
          for (let j of i.actions) {
            if (j.hour < hr || (j.hour == hr && j.mins < min)) {
              completed++;
            }
          }
          i.progress = (completed / total) * 100;
        }
        routines.push(i);
      }
      else if (i.sceneName) {
        scenes.push(i);
      }
      c++;
    }
    this.scenes = scenes;
    this.routines = routines;
  }

  scenesInit() {

    if (this.curr_home && !this.all_scenes.length) {
      this.dont_show_loader = this.all_scenes[0] ? true : false;
      if (this.kiot_internet == 'offline') {
        this.dont_show_loader = true;
      }
      this.ds.getAllScenes(this.local.getCurrentHome(),
        data => {
          let x = new Date(Date.now());
          let hr = x.getHours();
          let min = x.getMinutes();
          for (let i of data.scheduleScens) {
            let total = i.actions.length || 1; let completed = 0;
            for (let j of i.actions) {
              if (j.hour < hr || (j.hour == hr && j.mins < min)) {
                completed++;
              }
            }
            i.progress = (completed / total) * 100;
          }
          this.all_scenes = data.scenes.concat(data.scheduleScens);
          this.separateSceneRoutine();
          console.log(this.all_scenes);
        }, error => {
          console.log(error);
        }, this.dont_show_loader);
    }
  }
  workflowsInit() {
    this.current = 'workflows';
    if (this.curr_home && !this.all_workflows.length) {
      this.ds.getAllWorkflowGroups(this.curr_home,
        data => {
          this.all_workflows = data;
          for (let i of this.all_workflows) {
            i.start_str = this.help.arihantsTimeToString(i.starts_at);
            i.end_str = this.help.arihantsTimeToString(i.ends_at);
          }
        }, error => {
          console.log(error);
        });
      this.ds.getSboards(this.local.getCurrentHome(),
        data => {
          this.my_sboards = data;
        }, error => {
          console.log(error);
        }, false, 'room,swappliance')
    }
  }
  retry_loading = false;
  retry() {
    this.retry_loading = true;
    this.ds.ping()
      .then(data => {
        console.log(data);
        this.kiot_internet = this.local.getInternetStatus();
        this.retry_loading = false;
      });
  }

  num_of_notifications;
  goToNotifications() {
    let noti_modal = this.modalCtrl.create('NotificationDispPage');
    noti_modal.present();
    noti_modal.onWillDismiss((data) => {
      if (data) {
        this.num_of_notifications = 0;
      }
    });

  }

  goToCreate() {
    this.nullifySelected()
    if (this.current == 'workflows') {
      this.goToCreateWorkflow();
    }
    else {
      this.goToCreateScene();
    }
  }
  nullifySelected() {
    this.selected_scene = 'NONE';
  }
  goToCreateScene() {
    this.nullifySelected();
    //console.log('Yo Im going');
    this.navCtrl.push('CreateScenePage', {
      all_scenes: this.all_scenes
    });
  }
  ionViewWillLeave() {
    this.nullifySelected();
  }
  goToCreateWorkflow() {
    this.nullifySelected()
    this.navCtrl.push('CreateWorkflowPage', {
      all_workflows: this.all_workflows,
      my_sboards: this.my_sboards
    });
  }
  goToWorkflow(ind) {
    this.nullifySelected()
    this.navCtrl.push('ConfigWorkflowPage', {
      w_group: this.all_workflows[ind]._id,
      all_workflows: this.all_workflows,
      w_ind: ind,
      my_sboards: this.my_sboards,
      is_edit: true
    })
  }
  goToDetails(ind) {
    this.nullifySelected()
    console.log(ind);
    this.navCtrl.push('SceneDetailsPage', {
      all_scenes: this.all_scenes,
      ind: ind
    });

  }
  triggerScene(scene, ind) {
    this.nullifySelected()
    let obj;
    if (scene.scheduleSceneName) {
      obj = {
        isActive: !scene.isActive,
        scheduleSceneId: scene._id || scene.scheduleSceneId
      };
    }
    else {
      obj = {
        sceneId: scene._id || scene.sceneId
      }
    }
    this.all_scenes[ind].show_loader = true;
    this.ds.postTriggerScene(
      scene.scheduleSceneName ? true : false,
      obj, data => {
        console.log(data);
        this.all_scenes[ind].show_loader = false;
        this.changeSceneStates(scene.scheduleSceneName ? true : false, scene, ind);

      }, error => {
        this.all_scenes[ind].show_loader = false;
        console.log(error);
      }
    )
  }
  changeSceneStates(scheduled, scene, ind) {
    this.nullifySelected()
    if (scheduled) {
      this.all_scenes[ind].isActive = !scene.isActive;
      this.all_scenes[ind].isActive ? this.toast.bottomToast_Short('Scene Activated') : this.toast.bottomToast_Short('Scene Deactivated');
    }
    else {
      this.toast.bottomToast_Short('Scene Activated');
    }

  }


}
