import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditScenePage } from './edit-scene';

@NgModule({
    declarations: [
        EditScenePage,
    ],
    imports: [
        IonicPageModule.forChild(EditScenePage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class EditScenePageModule { }