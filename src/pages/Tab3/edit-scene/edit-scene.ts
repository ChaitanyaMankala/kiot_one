import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../providers/DataService';
import { ToastService } from '../../../providers/ToastService';
import { LocalStorageService } from '../../../providers/LocalStorageService';
import { Helper } from '../../../providers/Helper';

//pages
import { RoomListPage } from '../room-list/room-list';

@IonicPage()
@Component({
  selector: 'page-edit-scene',
  templateUrl: 'edit-scene.html',
})
export class EditScenePage {
  all_scenes; ind; current_scene; scene_name; scene_time; repeat_selectText; schedule_repeats;
  constructor(public navCtrl: NavController, public navParams: NavParams, private help: Helper, private ds: DataService, private local: LocalStorageService, private toast: ToastService) {
    this.all_scenes = this.navParams.get('all_scenes');
    this.ind = this.navParams.get('ind');
    this.current_scene = this.all_scenes[this.ind];
    this.scene_name = this.current_scene.scheduleSceneName || this.current_scene.sceneName;
    this.scene_time = (parseInt(this.current_scene.hour) < 10 ? '0' + this.current_scene.hour.toString() : this.current_scene.hour) + ':' + (parseInt(this.current_scene.mins) < 10 ? '0' + this.current_scene.mins.toString() : this.current_scene.mins);
    //this.repeatChange();
    this.schedule_repeats = [false, false, false, false, false, false, false];
    if (this.current_scene.days) {
      for (let i of this.current_scene.days) {
        this.schedule_repeats[i] = true;
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditScene');
  }
  goToRoomListForEdit() {
    this.navCtrl.push('RoomListPage', {
      isEdit: this.ind + 1,
      scene_name: this.scene_name,
      for: this.current_scene.scheduleSceneName ? this.scene_time : 'unscheduled',
      all_scenes: this.all_scenes,
      days: this.current_scene.days
    });
  }
  editScene() {
    let obj;
    obj = {
      actions: this.current_scene.actions,
      homeId: this.local.getCurrentHome(),
      no_of_apps: this.current_scene.no_of_apps,
      isActive: this.current_scene.isActive,
      user: {
        name: this.current_scene.user.name
      }
    };
    if (this.current_scene.scheduleSceneName) {
      obj.scheduleSceneName = this.scene_name;
      obj.hour = parseInt(this.scene_time.slice(0, 2));
      obj.mins = parseInt(this.scene_time.slice(3, 5));
      obj.days = this.help.getDaysArray(this.schedule_repeats);
      obj.scheduleSceneId = this.current_scene._id || this.current_scene.scheduleSceneId;
      //  obj._id = obj.scheduleSceneId;
    }
    else {
      obj.sceneName = this.scene_name;
      obj.sceneId = this.current_scene._id || this.current_scene.sceneId;
      //   obj._id = obj.sceneId;
    }

    this.ds.postEditScene(
      this.current_scene.scheduleSceneName ? true : false,
      obj,
      data => {
        console.log(obj);
        this.all_scenes[this.ind] = obj;
        this.navCtrl.pop();
        this.toast.bottomToast_Short('Scene Edited');
      }, error => {
        console.log(error);
      }
    )

  }
  repeatChange() {
    // console.log(event);
    if (this.current_scene.scheduleSceneName) {
      if (this.current_scene.days.length == 7) {
        this.repeat_selectText = 'Everyday';
      }
      else {
        this.repeat_selectText = null;
      }
    }
  }
}
