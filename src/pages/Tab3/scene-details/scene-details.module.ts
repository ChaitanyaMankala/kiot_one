import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SceneDetailsPage } from './scene-details';

@NgModule({
    declarations: [
        SceneDetailsPage,
    ],
    imports: [
        IonicPageModule.forChild(SceneDetailsPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class SceneDetailsPageModule { }