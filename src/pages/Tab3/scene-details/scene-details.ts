import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../providers/DataService';
import { ToastService } from '../../../providers/ToastService';
import { AlertService } from '../../../providers/AlertService';
import { Helper } from '../../../providers/Helper';

//pages
import { EditScenePage } from '../edit-scene/edit-scene';

@IonicPage()
@Component({
  selector: 'page-scene-details',
  templateUrl: 'scene-details.html',
})
export class SceneDetailsPage {
  all_scenes; ind; current_scene; sw_appliance_dict; rooms_dict; schedule_repeats = [false, false, false, false, false, false, false];
  constructor(public navCtrl: NavController, private help: Helper, public navParams: NavParams, private ds: DataService, private toast: ToastService, private alert: AlertService) {
    this.sw_appliance_dict = {};
    this.rooms_dict = {};
    this.all_scenes = this.navParams.get('all_scenes');
    this.ind = this.navParams.get('ind');
    this.current_scene = this.all_scenes[this.ind];
    console.log(this.all_scenes[this.ind]);
    let days_array = ['M ', 'T ', 'W ', 'Th ', 'F ', 'Sa ', 'Su '];
    this.current_scene.days_string = "";
    if (this.current_scene.days) {
      for (let i of this.current_scene.days) {
        this.current_scene.days_string += days_array[i];
      }
    }
    if (this.current_scene.days) {
      for (let i of this.current_scene.days) {
        this.schedule_repeats[i] = true;
      }
    }
    let rooms; let switches;
    rooms = {}; switches = {};
    for (let i of this.current_scene.actions) {
      if (!switches[i.switch_appliance_id]) {
        switches[i.switch_appliance_id] = 1;
      }
      if (!rooms[i.room_id]) {
        rooms[i.room_id] = 1;
      }
    }
    this.ds.getSwitchesDetails(Object.keys(switches), Object.keys(rooms),
      data => {
        console.log(data);
        for (let i of data.switch_appliances) {
          if (!this.sw_appliance_dict[i._id]) {
            this.sw_appliance_dict[i._id] = i.name;
          }
        }
        for (let i of data.rooms) {
          if (!this.rooms_dict[i._id]) {
            this.rooms_dict[i._id] = i.name;
          }
        }
      }, err => {
        console.log(err);
      });
  }

  ionViewWillEnter() {
    this.current_scene = this.all_scenes[this.ind];
    this.schedule_repeats = [false, false, false, false, false, false, false];
    if (this.current_scene.days) {
      for (let i of this.current_scene.days) {
        this.schedule_repeats[i] = true;
      }
    }
    this.help.sorter(this.current_scene.actions, 'hour', 'min');
    for (let i of this.current_scene.actions) {
      i = this.help.timeFormatter(i, 'hour', 'mins', 'time_string', true);
    }
  }
  deleteScene() {
    let obj;
    this.current_scene.scheduleSceneName ? obj = { scheduleSceneId: this.current_scene._id } : obj = { sceneId: this.current_scene._id };
    this.ds.postDeleteScene(
      this.current_scene.scheduleSceneName ? true : false,
      obj,
      data => {
        console.log(data);
        this.navCtrl.pop();
        this.all_scenes.splice(this.ind, 1);
        //  this.toast.bottomToast_Short('Scene Deleted');
      }, error => {
        console.log(error);
      }
    )
  }
  confirmDeleteScene() {
    this.alert.showConfirm('Confirm Delete', 'This deletes the scene and all the scene data', 'Cancel',
      'Delete', () => {

      }, () => {
        this.deleteScene();
      });
  }
  goToEditScene() {
    this.navCtrl.push('EditScenePage', {
      all_scenes: this.all_scenes,
      ind: this.ind
    });
  }

}
