import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScheduleSwitchPage } from './schedule-switch';

@NgModule({
    declarations: [
        ScheduleSwitchPage,
    ],
    imports: [
        IonicPageModule.forChild(ScheduleSwitchPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class ScheduleSwitchPageModule { }