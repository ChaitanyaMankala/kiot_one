import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-schedule-switch',
  templateUrl: 'schedule-switch.html',
})
export class ScheduleSwitchPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ScheduleSwitch');
  }
  dismiss(data) {
    this.viewCtrl.dismiss(data);
  }
}
