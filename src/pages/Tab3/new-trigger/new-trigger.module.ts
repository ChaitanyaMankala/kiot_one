import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewTriggerPage } from './new-trigger';
import { PipesModule } from '../../../pipes/pipes.module';

@NgModule({
    declarations: [
        NewTriggerPage,
    ],
    imports: [
        IonicPageModule.forChild(NewTriggerPage),
        PipesModule
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class NewTriggerPageModule { }