import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController, ActionSheetController, Select, IonicPage } from 'ionic-angular';

//providers
import { AlertService } from '../../../providers/AlertService';
import { DataService } from '../../../providers/DataService';
import { LocalStorageService } from '../../../providers/LocalStorageService';
import { Helper } from '../../../providers/Helper';

const default_temperature = 22;

var CONFIG = require('./trigger_config.json');

@IonicPage()
@Component({
	selector: 'page-new-trigger',
	templateUrl: 'new-trigger.html',
})
export class NewTriggerPage {
	@ViewChild('triggerDevice') triggerDeviceSelect: Select;
	sensor_type; all_triggers = []; trigger_device; workflow_id; devices_in_rooms = []; devices_in_rooms_safe = []; w_group; all_remotes; trigger_index; trigger_type; trigger_value; actions; all_switches; all_rooms = [];
	units = CONFIG.units; workflows_on_device;

	constructor(public navCtrl: NavController, private help: Helper, private ds: DataService, private local: LocalStorageService, public navParams: NavParams, private alert: AlertService, private actionSheetCtrl: ActionSheetController, private viewCtrl: ViewController) {
		this.workflows_on_device = {};
		this.w_group = this.navParams.get('w_group');
		this.workflow_id = this.navParams.get('workflow')
		this.all_remotes = this.navParams.get('all_remotes');
		this.sensor_type = this.navParams.get('sensor_type');
		this.trigger_type = this.navParams.get('trigger_type');
		this.trigger_value = this.navParams.get('trigger_value');
		this.trigger_device = this.navParams.get('trigger_device');
		this.all_triggers = this.navParams.get('all_triggers');
		this.actions = this.navParams.get('actions').slice();
		this.trigger_index = this.navParams.get('trigger_index');
		this.all_switches = {};
		this.all_rooms = this.navParams.get('all_rooms');
		for (let room of this.all_rooms) {
			for (let device of room.devices) {
				if (!this.all_switches[room.room_id]) this.all_switches[room.room_id] = [];
				for (let app of device.switch_appliances) {
					app.device = device._id;
				}
				this.all_switches[room.room_id] = this.all_switches[room.room_id].concat(device.switch_appliances);
				this.devices_in_rooms.push({
					str: device.sboard_name + " in " + room.room_name,
					id: device._id
				});

			}
			this.devices_in_rooms_safe = this.devices_in_rooms.slice();
		}
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad NewTriggerPage');
	}

	dismiss(data, check?) {
		if (check && this.sensor_type != 'Select' && this.trigger_type != 'Select' && this.trigger_value != 'Select') {
			this.alert.showConfirm('Discard Changes?', 'Do you want to save this trigger', 'save', 'discard',
				() => {
					this.saveTrigger().then(() => {
						this.dismiss(0);
					})
				}, () => {
					this.dismiss(0);
				})
		}
		else {
			this.viewCtrl.dismiss(data);
		}
	}
	changeTemp(ind, action) {
		if (!this.actions[ind].ir_action_value) this.actions[ind].ir_action_value = default_temperature;
		if ((this.actions[ind].ir_action_value < 17 && !action) || (this.actions[ind].ir_action_value > 29 && action)) {

		}
		else {
			if (action) {
				this.actions[ind].ir_action_value++;
			}
			else {
				this.actions[ind].ir_action_value--;
			}
		}
	}
	saveAndDismiss() {
		this.saveTrigger()
			.then(() => {
				this.dismiss(0);
			})
	}
	saveAction(ind) {
		let action = this.actions[ind];
		let clear = true;
		if (!action.app || !action.room) {
			this.alert.showAlert('Invalid', 'Enter appliance and room details');
		}
		else {
			/*let x = '';
			if (action.type == 'switch') {
				let act = action.sw_action ? 'ON' : 'OFF';
				if (action.app.switch_type == 'regulator') {
					if (!action.sw_dimm && action.sw_action != 0) {
						action.sw_dimm = 0;
					}
					x = 'Turn <span class="txt-orange">' + act + '</span> and set speed to <span class="txt-orange">' + action.sw_dimm + '</span> on ' + action.app.name + ' in ' + action.room.room_name;
				}
				else {
					x = 'Turn <span class="txt-orange">' + act + '</span> ' + action.app.name + ' in ' + action.room.room_name;
				}
			}
			else if (action.type == 'ir') {
				if (action.ir_action == 'POWER ON' || action.ir_action == 'POWER OFF') {
					x = '<span class="txt-orange">' + action.ir_action + '</span> ' + action.app.name + ' in ' + action.room.room_name;
				}
				else if (action.ir_action == 'temp') {
					if (!action.ir_action_value) {
						action.ir_action_value = 22;
					}
					x = 'Set Temperature to <span class="txt-orange">' + action.ir_action_value + '</span> on ' + action.app.name + ' in ' + action.room.room_name;
				}
				else {
					this.alert.showAlert('Invalid', 'Action cannot be empty');
					clear = false;
				}
			}*/
			let x = this.help.constructActionString(action);
			if (x.clear) {
				this.actions[ind].form = false;
				this.actions[ind].action = action;
				this.actions[ind].action_string = x.action_str;
			}
		}

	}
	addAction() {
		this.actions.push({
			"type": "",
			"value": "",
			form: true
		});
	}
	deleteAction(index) {
		if (this.actions.length > 1) {
			this.actions.splice(index, 1);
		}
		else {
			this.alert.showAlert('Cannot Delete', 'Atleast One Action is needed');
		}
	}
	editAction(index) {
		this.actions[index].form = true;
	}

	saveTrigger() {
		return new Promise((resolve, reject) => {
			this.validateAll().then(
				validated => {
					if (validated) {
						let trigger_device_obj = CONFIG.trigger_dictionary[this.sensor_type] + '_' + this.trigger_type;

						let w_actions = [];
						for (let i of this.actions) {
							switch (i.type) {
								case 'switch': {
									w_actions.push({
										"type": i.type,
										"switch_no": i.app.switch_no,
										"switch_value": i.app.switch_type == 'regulator' ? i.sw_dimm : i.sw_action ? 1 : 0,
										"switch_appliance": i.app._id,
										"device": i.app.device,
										"room_id": i.room.room_id
									});
									break;
								}
								case 'ir': {
									w_actions.push({
										"type": i.type,
										"ir_action": i.ir_action,
										"ir_action_value": i.ir_action == 'temp' ? i.ir_action_value : null,
										"ir_appliance": i.app._id.split('_')[3] ? i.app._id.split('_')[3] : i.app._id,
										"device": i.app.paired_sb._id,
										"room_id": i.room.room_id
									});
									break;
								}
							}
						}
						if (this.trigger_index != this.all_triggers.length) {
							this.ds.postEditWorkflow({
								workflow_id: this.all_triggers[this.trigger_index]._id,
								device_id: this.trigger_device.id,
								workflow: {
									w_type: trigger_device_obj,
									trigger_value: this.trigger_value,
									w_group: this.w_group,
									w_actions: w_actions
								}
							}, data => {
								console.log(data);
								this.all_triggers[this.trigger_index] = {
									sensor_type: this.sensor_type,
									trigger_type: this.trigger_type,
									trigger_value: this.trigger_value,
									trigger_type_obj: trigger_device_obj,
									trigger_device: this.trigger_device,
									actions: this.actions,
									enabled: true,
									_id: this.all_triggers[this.trigger_index]._id
								};
								resolve(true);
							}, error => {
								reject(-1);
							})
						}
						else {
							this.ds.postCreateWorkflow({
								device_id: this.trigger_device.id,
								workflow: {
									w_type: trigger_device_obj,
									trigger_value: this.trigger_value,
									w_group: this.w_group,
									w_actions: w_actions
								}
							}, data => {
								console.log(data);
								this.all_triggers[this.trigger_index] = {
									sensor_type: this.sensor_type,
									trigger_type: this.trigger_type,
									trigger_value: this.trigger_value,
									trigger_type_obj: trigger_device_obj,
									trigger_device: this.trigger_device,
									actions: this.actions,
									_id: data._id,
									enabled: true
								};
								resolve(true);
							}, error => {
								reject(-1);
							})
						}
					}
					else {
						reject(-1)
					}
				});
		})

	}
	validateAll() {
		return new Promise((resolve, reject) => {
			if (!this.actions.length) {
				this.throwerror('fill all the details', 'Invalid');
				resolve(false);
			}
			let c = 1;
			for (let i of this.actions) {
				if (i.form) {
					this.throwerror('Save Action ' + c + ' and proceed (click on save action)', 'Save all actions');
					resolve(false);
				}
				else if (i.type == 'ir' && !i.paired_sb) {
					this.throwerror('paired device id deleted for ' + i.app.name + ' change paired device in settings or choose another device', 'No Paired Device');
				}
				c++;
			}
			this.validateWorkflowForDevice(this.trigger_device)
				.then((data) => {
					resolve(data);
				})
			//return true;
		})

	}
	sensorSheet() {
		this.actionSheetCtrl.create({
			title: 'Select Sensor Type',
			buttons: [
				{
					text: 'Temperature',
					role: 'Temperature',
					handler: () => {
						this.sensor_type = "temperature";
						this.next(1);
					}
				},
				{
					text: 'Humidity',
					role: 'Humidity',
					handler: () => {
						this.sensor_type = "humidity";
						this.next(1);
					}
				}

			]
		}).present();
	}
	valueSheet() {
		this.actionSheetCtrl.create({
			title: 'Trigger Type',
			buttons: [
				{
					text: 'above',
					role: 'above',
					handler: () => {
						this.trigger_type = "above";
						this.next(2);
					}
				},
				{
					text: 'below',
					role: 'below',
					handler: () => {
						this.trigger_type = "below";
						this.next(2);
					}
				}
			]
		}).present();
	}
	triggerValueSheet() {
		this.alert.showPrompt('enter ' + this.sensor_type, '', [
			{
				"name": "trigger_val",
				"placeholder": this.sensor_type + ' in ' + this.units[this.sensor_type]
			}
		], 'cancel', () => { },
			'Okay', (val) => {
				let valid = false;
				let trigger_val = val.trigger_val;
				switch (this.sensor_type) {
					case 'temperature': {
						if (parseInt(trigger_val) && parseInt(trigger_val) > -1) {
							this.trigger_value = parseInt(trigger_val).toString();
							valid = true;
						}
						else {
							this.throwerror('Invalid Temperature');
						}
						break;
					}
					case 'humidity': {
						if (parseInt(trigger_val) && parseInt(trigger_val) > 0 && parseInt(trigger_val) < 100) {
							this.trigger_value = parseInt(trigger_val).toString();
							valid = true;
						}
						else {
							this.throwerror('Invalid Humidity');
						}
						break;
					}
				}
				if (valid) {
					this.next(3);
				}
			})
	}
	triggerDeviceSheet() {
		this.triggerDeviceSelect.open();
	}

	validateWorkflowForDevice(eve) {
		return new Promise((resolve, reject) => {
			let x = CONFIG.trigger_dictionary[this.sensor_type] + '_' + this.trigger_type;
			this.getWorkflowsOnDevice(eve.id)
				.then(
				(data) => {
					let clear = true;
					for (let i of data) {
						if (i._id == this.workflow_id) {
							continue;
						}
						if (i.w_type == x) {
							this.clearDevice('Already Exists', this.sensor_type + ' ' + this.trigger_type + ' already exists for ' + this.trigger_device.str);
							clear = false;
							break;
						}
						else if (i.w_type.split('_')[0] == CONFIG.trigger_dictionary[this.sensor_type]) {
							if (this.trigger_type == 'below') {
								if (parseInt(i.trigger_value) < parseInt(this.trigger_value)) {
									this.clearDevice('Invalid Value', this.sensor_type + ' below ' + i.trigger_value + ' is accepted');
									clear = false;
									break;
								}
							}
							else if (this.trigger_type == 'above') {
								if (parseInt(i.trigger_value) > parseInt(this.trigger_value)) {
									this.clearDevice('Invalid Value', this.sensor_type + ' above ' + i.trigger_value + ' is accepted');
									clear = false;
									break;
								}
							}
						}
					}
					if (clear) {
						this.trigger_device = eve;
						resolve(true);
					}
					else {
						resolve(false);
					}
				}).catch(error => {
					console.log(error);
				});
		})

	}

	getWorkflowsOnDevice(dev_id) {
		return new Promise<any>((resolve, reject) => {
			let x = [];
			if (this.workflows_on_device[dev_id]) {
				resolve(this.workflows_on_device[dev_id]);
			}
			else {
				this.ds.getAllWorkflowsOnADevice(dev_id,
					data => {
						this.workflows_on_device[dev_id] = data;
						resolve(this.workflows_on_device[dev_id]);
					}, error => {
						console.log(error);
						reject(-1);
					});
			}
		})
	}
	clearDevice(title, msg) {
		//this.trigger_device.id = -1;
		//this.trigger_device.str = 'Select';
		this.devices_in_rooms = this.devices_in_rooms_safe.slice();
		this.throwerror(msg, title);
	}
	next(current) {
		switch (current) {
			case 1: {
				if (this.trigger_type == 'Select') {
					this.valueSheet();
				}
				break;
			}
			case 2: {
				if (this.trigger_value == 'Select') {
					this.triggerValueSheet();
				}
				break;
			}
			case 3: {
				if (this.trigger_device.str == 'Select') {
					this.triggerDeviceSheet();
				}
				break;
			}
		}
	}
	throwerror(str, head?) {
		this.alert.showAlert(head || 'Error', str);
	}


}
