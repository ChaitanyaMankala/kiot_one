import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, IonicPage } from 'ionic-angular';

//providers
import { DataService } from '../../../providers/DataService';
import { LocalStorageService } from '../../../providers/LocalStorageService';
import { AlertService } from '../../../providers/AlertService';
import { Helper } from '../../../providers/Helper';
import { ToastService } from '../../../providers/ToastService';

//pages
import { NewTriggerPage } from '../new-trigger/new-trigger';
import { EditWorkflowPage } from '../edit-workflow/edit-workflow';

var CONFIG = require('../new-trigger/trigger_config.json');

@IonicPage()
@Component({
  selector: 'page-config-workflow',
  templateUrl: 'config-workflow.html',
})
export class ConfigWorkflowPage {
  all_workflows; new_wflow = []; all_switches; w_ind; is_edit; all_rooms; all_remotes_dict; devices_in_rooms; all_triggers = []; all_remotes = {}; w_group;
  constructor(public navCtrl: NavController, private help: Helper, private toast: ToastService, private alert: AlertService, private local: LocalStorageService, public navParams: NavParams, public modalCtrl: ModalController, private ds: DataService) {
    this.is_edit = this.navParams.get('is_edit') || false;
    this.w_group = this.navParams.get('w_group');
    this.all_workflows = this.navParams.get('all_workflows');
    this.w_ind = this.navParams.get('w_ind');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfigWorkflowPage');
    this.getAllRemotes();

  }
  ionViewWillEnter() {
    console.log(this.all_triggers);

  }

  populateEdit() {
    if (this.is_edit) {
      this.all_switches = {};
      let rooms = this.navParams.get('my_sboards');
      this.all_rooms = {};
      this.devices_in_rooms = {};
      for (let room of rooms) {
        for (let device of room.devices) {
          this.all_rooms[room.room_id] = room;

          for (let app of device.switch_appliances) {
            app.device = device._id;
            this.all_switches[app._id] = app;
          }
          this.devices_in_rooms[device._id] = {
            str: device.sboard_name + " in " + room.room_name,
            id: device._id
          };
        }
      }
      this.ds.getAllWorkflows(this.w_group,
        data => {
          for (let i of data) {
            for (let j of i.w_actions) {
              j.room = this.all_rooms[j.room_id];
              j.app = this.all_switches[j.switch_appliance] || this.all_remotes_dict[j.ir_appliance];
              j.action_string = this.help.constructActionString(j).action_str;
            }
            this.all_triggers.push({
              actions: i.w_actions,
              sensor_type: CONFIG.rev_trigger_dictionary[i.w_type.split('_')[0]],
              trigger_type: i.w_type.split('_')[1],
              trigger_value: i.trigger_value,
              trigger_device: this.devices_in_rooms[i.device_id],
              _id: i._id,
              enabled: i.enabled
            });
          }
        },
        error => {
          console.log(error);
        })
    }
  }

  createWorkflow() {
    let triggers = [];
    for (let i of this.all_triggers) {
      triggers.push({
        actions: i.actions,
        trigger: i.trigger_type_obj,
        device_id: i.trigger_device.id,
        trigger_value: i.trigger_value
      })
    }
    this.ds.postCreateWorkflow({
      workflow: triggers
    }, data => {
      this.all_workflows.push(data);
      this.navCtrl.pop();
      this.navCtrl.pop();
    }, error => {
      console.log(error);
    })
  }
  getAllRemotes() {
    this.all_remotes_dict = {};
    this.ds.getIrAppliances(this.local.getCurrentHome(),
      data => {
        for (let i of data.appliances) {
          if (i.room_id) {
            if (!this.all_remotes[i.room_id._id]) {
              this.all_remotes[i.room_id._id] = [];
            }
            if (i.type == "Air Conditioner") {
              this.all_remotes_dict[i._id.split('_')[3] || i._id] = i;
              this.all_remotes[i.room_id._id].push(i);
            }
          }
        }
        if (!this.all_triggers.length && !this.is_edit) {
          this.openModalToAdd();
        }
        if (this.is_edit) {
          this.populateEdit();
        }
      }, error => {
        console.log(error);
      }, true)
  }

  openModalToAdd() {
    let x = "Select";
    this.modalCtrl.create('NewTriggerPage', {
      w_group: this.w_group,
      all_triggers: this.all_triggers,
      all_rooms: this.navParams.get('my_sboards'),
      trigger_index: this.all_triggers.length,
      all_remotes: this.all_remotes,
      sensor_type: x,
      trigger_type: x,
      trigger_value: x,
      trigger_device: {
        str: x,
        id: -1
      },
      actions: [{
        "type": "",
        "value": "",
        form: true
      }]
    }).present();
  }
  editTrigger(ind) {
    let x = this.all_triggers[ind];
    this.modalCtrl.create('NewTriggerPage', {
      w_group: this.w_group,
      all_triggers: this.all_triggers,
      all_remotes: this.all_remotes,
      all_rooms: this.navParams.get('my_sboards'),
      trigger_index: ind,
      sensor_type: x.sensor_type,
      trigger_type: x.trigger_type,
      trigger_value: x.trigger_value,
      trigger_device: x.trigger_device,
      actions: x.actions
    }).present();
  }
  deleteTrigger(ind) {
    this.alert.showConfirm('Delete Trigger?', 'The trigger and its actions will be deleted from the work flow',
      'Cancel', 'Delete', () => {

      }, () => {
        this.ds.postDeleteWorkflow({
          workflow_id: this.all_triggers[ind]._id,
          device_id: this.all_triggers[ind].trigger_device.id
        }, data => {
          this.all_triggers.splice(ind, 1);
        }, error => {
          console.log(error);
        })

      })
  }

  deleteWorkflow() {
    this.alert.showConfirm('Delete Workflow?', 'This deletes all the triggers in the workflow',
      'Cancel', 'Delete', () => {

      }, () => {
        this.ds.postDeleteWorkflowGroup({
          group: this.w_group
        }, data => {
          this.all_workflows.splice(this.w_ind, 1);
          this.navCtrl.pop();
          this.toast.bottomToast_Short('Workflow Deleted');
        }, error => {
          console.log(error);
        })
      });
  }
  editWorkflow() {
    this.navCtrl.push('EditWorkflowPage', {
      all_workflows: this.all_workflows,
      w_ind: this.w_ind
    })
  }
  doneAdding() {
    this.toast.bottomToast_Short('Workflow saved');
    this.navCtrl.pop();
  }

  disableTrigger(ind) {
    this.ds.postEditWorkflow({
      workflow_id: this.all_triggers[ind]._id,
      device_id: this.all_triggers[ind].trigger_device.id,
      workflow: {
        enabled: this.all_triggers[ind].enabled ? false : true
      }
    },
      data => {
        this.toast.bottomToast_Short('Done');
        this.all_triggers[ind].enabled = !this.all_triggers[ind].enabled;
      }, error => {
        console.log(error);
      })
  }
}
