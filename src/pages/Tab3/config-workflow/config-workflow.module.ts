import { IonicPage } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfigWorkflowPage } from './config-workflow';

@NgModule({
    declarations: [
        ConfigWorkflowPage,
    ],
    imports: [
        IonicPageModule.forChild(ConfigWorkflowPage),
    ],
schemas: [CUSTOM_ELEMENTS_SCHEMA]})
export class ConfigWorkflowPageModule { }